# HISTORY

## v3.3.11.0

- [#121](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-excel-addin/-/issues/121) An alternative option to retrieve the last X periods
- [#122](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-excel-addin/-/issues/122) View data "as of" in DLM Excel Add-in
- [#108](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-excel-addin/-/issues/108) View historic versions in DLM Excel Add-in
- [#137](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-excel-addin/-/issues/137) bugfix for auth refresh token when Excel opened for a long time
- [#138](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-excel-addin/-/issues/138) redirectUri for oAuth in addin configurable (default https://dotstat-suite-addin.oecd.org)
- [#144](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-excel-addin/-/issues/144) ILO SDMX Web Service Update
- [#146](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-excel-addin/-/issues/146) QUERY SYNTAX dialog update

## v3.3.5.2

- [#134](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-excel-addin/-/issues/134) Add Archive space
- [#136](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-excel-addin/-/issues/136) Auth openid scope configurable

## v3.3.3.1

- [#127](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-excel-addin/-/issues/127) Warn message for bulk delete actions
- [#126](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-excel-addin/-/issues/126) ACTION column conditional formating for DELETE method
- [#125](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-excel-addin/-/issues/125) Fix meta attribute column index calculation
- [#124](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-excel-addin/-/issues/124) Change link about ref. metadata definition to DLM feature doc 
- [#120](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-excel-addin/-/issues/120) Sdmx startperiod/endperiod input filter fixes
- [#54](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-excel-addin/-/issues/54) Crud operation support with ACTION column

## v3.3.0.0

- [#48](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-excel-addin/-/issues/48) Default Localisation in .Stat Excel add-in
- [#119](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-excel-addin/-/issues/119) Need for an uninstall script of the .Stat DLM Excel add-in
- [#67](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-excel-addin/-/issues/67) Display of the data(flow) label
- [#110](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-excel-addin/-/issues/110) Label columns missing in ref.metadata tables
- [#83](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-excel-addin/-/issues/83) Set icons for ribbon groups when window is narrowed

## v3.2.5.0

- [#102](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-excel-addin/-/issues/102) Upgrade webview component  to webview2 to support Windows Server
- [#111](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-excel-addin/-/issues/111) Fix Eurostat connection

## v3.2.3.0

- [#86](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-excel-addin/-/issues/86) Fix for regression issue in NSI v8.12.1 for structurespecific XML request
- [#84](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-excel-addin/-/issues/84) New browserless authentication inside main excel window
- [#73](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-excel-addin/-/issues/73) Metadata editor UI changes
- [#88](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-excel-addin/-/issues/88) Prevent crush on oauth token refresh
- [#89](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-excel-addin/-/issues/89) Metadata hierarchical attributes path bug fix
- [#79](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-excel-addin/-/issues/79) Notification message text change after saving data/metadata
- [#66](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-excel-addin/-/issues/66) Allow saving data without attributes
- [#87](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-excel-addin/-/issues/87) Time filter constraints shown in UI
- [#93](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-excel-addin/-/issues/93) OECD PP `Process` dataspace renames to `Design`
- [#96](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-excel-addin/-/issues/96) Metadata help link reference change

## v3.1.7.0

- [#80](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-excel-addin/-/issues/80) Fix of IMF sdmx markup cannot be parsed due to SDMX rules vialoation
- [#29](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-quality-assurance/-/issues/29) Added CI pipeline with nuget dependency scan

## v3.1.5.0

- [#70](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-excel-addin/-/issues/70) IMF connection bugfix.

## v3.1.4.0

- [#63](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-excel-addin/-/issues/63) Issue with daily date format when saving data.
- [#64](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-excel-addin/-/issues/64) Problem saving double values with the excell-addin (handling regional decimal separator).

## v3.1.2.0
- [#24](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-excel-addin/-/issues/24) Html editor replaced, fixes for metadata retrieval and saving back.
- [#17](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-excel-addin/-/issues/17) Added Daily,Weekly,Semestrial,Business frequencies in a filter form
- [#43](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-excel-addin/-/issues/43) Bug fix for the usecase when in datasource there is a dataflow with the same ID, but different agency

## v3.0.0.24
- [#24](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-excel-addin/-/issues/24) Added metadata retrieval edit & save back features.
- [#35](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-excel-addin/-/issues/35) Apply actual content constraint to filter only display members with data
- [#49](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-excel-addin/-/issues/49) Fix of error raised with no detailed reason
- [#51](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-excel-addin/-/issues/51) Output empty data/metadata table for sdmx connections when no data (404) response is recieved

## v3.0.0.22

- [#46](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-excel-addin/-/issues/46) Fix 'Save to Database' button unavailable in newly added rows at the end of the extracted table.

## v3.0.0.20

- Connection urls adjustments for ECO & OECD .stat connections

## v3.0.0.17

- [#49](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-excel-addin/-/issues/49) .net framework upgraded to 4.6.1, sdmxsource updated to the latest version

## v3.0.0.14
- [#42](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-excel-addin/-/issues/42) Fix of error raised with no detailed reason

## v3.0.0.13
- [#31](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-excel-addin/-/issues/31) Enforce TLS 1.2
- [#32](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-excel-addin/-/issues/32) Apply default sort on SID column for SDMX outputs


## v3.0.0.6
- [#18](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-excel-addin/-/issues/18) support authentication with third-party identity providers and add "Log In/Out" button


## v3.0.0.5
- [#15](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-excel-addin/-/issues/15)
- [#13](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-excel-addin/-/issues/13)
- [#12](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-excel-addin/-/issues/12)
- [#10](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-excel-addin/-/issues/10)
 

## v3.0.0.4 
- [#4](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-excel-addin/-/issues/4)
- [#7](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-excel-addin/-/issues/7)
- [#6](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-excel-addin/-/issues/6)
- [#3](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-excel-addin/-/issues/3)
- [#8](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-excel-addin/-/issues/8)
 
