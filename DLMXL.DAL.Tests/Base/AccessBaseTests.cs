﻿using System;
using System.Linq;
using DLMXL.Models.Interfaces;
using NUnit.Framework;

namespace DLMXL.DAL.Tests.Base
{
    [TestFixture]
    public abstract class AccessBaseTests
    {
        protected readonly IRepository Repository;

        protected AccessBaseTests(IRepository repository)
        {
            if(repository == null)
                throw new ArgumentNullException(nameof(repository));

            Repository = repository;
        }

        [Test]
        public void TestDabases()
        {
            var databases = Repository.Databases;

            Assert.IsNotNull(databases);
            Assert.IsTrue(databases.Count >= 1);
        }

        [Test]
        public void TestDatasets()
        {
            var db = Repository.Databases?.First();

            Assert.IsNotNull(db);

            var datasets = db.Datasets;

            Assert.IsNotNull(datasets);

            Console.WriteLine($"Total: {datasets.Count}");

            foreach (var item in datasets.Take(10))
                Console.WriteLine($"{item.Code} - {item.DefaultName}");
        }


        [Test]
        public void TestDimensions()
        {
            var dataset = Repository.Databases?.First()?.Datasets?.First();

            Assert.IsNotNull(dataset);

            foreach (var item in dataset.Dimensions)
                Console.WriteLine($"{item.Code} - {item.DefaultName}");

            var dim = dataset.Dimensions.First();

            Assert.IsNotNull(dim);

            Console.WriteLine("-------------------------------------");

            foreach (var mem in dim.Members)
                Console.WriteLine($"{mem.Code} - {mem.DefaultName}");
        }

        [Test]
        public void TestQueries()
        {
            foreach (var db in Repository.Databases)
                foreach (var ds in db.Datasets)
                {
                    if(ds.Queries == null)
                        continue;

                    foreach (var q in ds.Queries)
                        Console.WriteLine($"{q.DefaultName} - {q.SdmxQuery}");

                    return;
                }
        }
    }
}
