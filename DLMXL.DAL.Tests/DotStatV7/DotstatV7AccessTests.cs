﻿using DLMXL.DAL.Access.Direct;
using DLMXL.DAL.Tests.Base;
using DLMXL.DAL.Tests.Moq;
using NUnit.Framework;

namespace DLMXL.DAL.Tests.DotStatV7
{
    [TestFixture]
    public class DotstatV7AccessTests : AccessBaseTests
    {
        public DotstatV7AccessTests() : base(TestRepository.Build("DotStat", ".STAT", new DotStatV7DirectAccess("DOTSTATDB", "DotStat", "http://dotstat.oecd.org/OECDStatWCF_QueryManager/my/queries/")))
        {}
    }
}
