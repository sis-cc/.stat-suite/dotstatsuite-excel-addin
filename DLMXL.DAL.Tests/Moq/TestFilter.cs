﻿using System;
using DLMXL.Models;
using DLMXL.Models.Interfaces;

namespace DLMXL.DAL.Tests.Moq
{
    public class TestFilter : ISdmxFilter
    {
        private string _sdmx;

        public Frequency Frequency { get; set; }
        public string StartTime { get; set; }
        public string EndTime { get; set; }
        public DateTime? UpdatedAfter { get; set; }
        public int? LastPeriods { get; set; }
        public HistoricVersionType? HistoricVersionType { get; set; }
        public SWFilter SWFilter { get; set; }

        public string SdmxQuery
        {
            get { return SWFilter?.GetSdmxQueryString() ?? _sdmx; }
            set { _sdmx = value; }
        }

        public TestFilter()
        {}

        public TestFilter(string query)
        {
            SdmxQuery = query;
        }
    }
}
