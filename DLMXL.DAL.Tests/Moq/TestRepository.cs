﻿using DLMXL.Models;

namespace DLMXL.DAL.Tests.Moq
{
    public class TestRepository : Repository<TestRepository>
    {
        protected TestRepository(string code, string name, IDataAccess dataAccess) : base(code, name, dataAccess)
        {}

        public static TestRepository Build(string code, string name, IDataAccess dataAccess)
        {
            return Build(new TestRepository(code, name, dataAccess));
        }
    }
}
