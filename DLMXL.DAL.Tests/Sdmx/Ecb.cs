﻿using DLMXL.DAL.Access.Sdmx;
using DLMXL.DAL.Tests.Base;
using DLMXL.DAL.Tests.Moq;
using DLMXL.Models;
using NUnit.Framework;

namespace DLMXL.DAL.Tests.Sdmx
{
    public class Ecb : AccessBaseTests
    {
        public Ecb() : base(TestRepository.Build("Ecb", "Ecb", new GenericRestApi(
                "Ecb",
                "https://sdw-wsrest.ecb.europa.eu/service/",
                "dataflow/all/all/latest",
                "datastructure/all/{0}/{2}/?references=children",
                "data/{0}"
            )))
        {
        }


        [TestCase("JDF_ICPF_PENSION_FUNDS/all")]
        public void GetData(string query)
        {
            var filter = Repository.BuildFilterFromSdmxQueryString<TestFilter>(query);

            Assert.IsNotNull(filter.SWFilter);

            var data = Repository.DataAccess.LoadData(OutputType.Flat, filter);

            Assert.IsNotNull(data);
        }
    }
}
