﻿using DLMXL.DAL.Access.Sdmx;
using DLMXL.DAL.Tests.Base;
using DLMXL.DAL.Tests.Moq;
using DLMXL.Models;
using NUnit.Framework;

namespace DLMXL.DAL.Tests.Sdmx
{
    [TestFixture]
    public class EurostatAccessTests : AccessBaseTests
    {
        public EurostatAccessTests() : base(TestRepository.Build("Eurostat", "Eurostat", new EurostatApi("Eurostat", "https://ec.europa.eu/eurostat/api/dissemination/sdmx/2.1/")))
        {}

        [TestCase("earn_ses_agt01/A..TOTAL.TOTAL.M.EE")]
        [TestCase("earn_ses_agt01/all")]
        [TestCase("earn_ses_agt01/all/?startPeriod=2003")]
        [TestCase("earn_ses_agt01/all/?startPeriod=2004&endPeriod=2005")]
        public void GetData(string query)
        {
            var filter  = Repository.BuildFilterFromSdmxQueryString<TestFilter>(query);

            Assert.IsNotNull(filter.SWFilter);

            var data   = Repository.DataAccess.LoadData(OutputType.Flat, filter);

            Assert.IsNotNull(data);
        }
    }
}
