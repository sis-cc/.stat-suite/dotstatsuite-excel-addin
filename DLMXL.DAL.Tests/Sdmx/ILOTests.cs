﻿using DLMXL.DAL.Access.Sdmx;
using DLMXL.DAL.Tests.Base;
using DLMXL.DAL.Tests.Moq;
using DLMXL.Models;
using NUnit.Framework;

namespace DLMXL.DAL.Tests.Sdmx
{
    public class ILOTests : AccessBaseTests
    {
        public ILOTests() : base(TestRepository.Build("ILO", "ILO", new GenericRestApi(
                "ILO",
                "https://www.ilo.org/sdmx/rest/",
                "dataflow/all/all/latest",
                "datastructure/all/{0}/{2}/?references=children",
                "data/{0}"
            )))
        {
        }


        [TestCase("DF_EAR_4HRL_SEX_OCU_CUR_NB/EST.....")]
        public void GetData(string query)
        {
            var filter = Repository.BuildFilterFromSdmxQueryString<TestFilter>(query);

            Assert.IsNotNull(filter.SWFilter);

            var data = Repository.DataAccess.LoadData(OutputType.Flat, filter);

            Assert.IsNotNull(data);
        }
    }
}
