﻿using DLMXL.DAL.Access.Sdmx;
using DLMXL.DAL.Tests.Base;
using DLMXL.DAL.Tests.Moq;
using DLMXL.Models;
using NUnit.Framework;

namespace DLMXL.DAL.Tests.Sdmx
{
    [TestFixture]
    public class IMFTests : AccessBaseTests
    {
        public IMFTests() : base(TestRepository.Build("IMF", "IMF", new IMFApi("IMF", "http://dataservices.imf.org/REST/")))
        {}

        [TestCase("DOT/M.AT.TXG_FOB_USD+TMG_CIF_USD./?startperiod=1960&endPeriod=2015")]
        [TestCase("APDREO2017M04/A.AU+BD.BCA_BP6_USD+BCA_GDP_BP6_PT/?startperiod=1960&endPeriod=2015")]
        [TestCase("APDREO2017M04/?startperiod=2015&endPeriod=2015")]
        public void GetData(string query)
        {
            var filter  = Repository.BuildFilterFromSdmxQueryString<TestFilter>(query);

            Assert.IsNotNull(filter.SWFilter);

            var data   = Repository.DataAccess.LoadData(OutputType.Flat, filter);

            Assert.IsNotNull(data);
        }
    }
}
