﻿using System;
using System.Linq;
using DLMXL.DAL.Access.Sdmx;
using DLMXL.DAL.Tests.Base;
using DLMXL.DAL.Tests.Moq;
using NUnit.Framework;

namespace DLMXL.DAL.Tests.Sdmx
{
    public class MsdTest : AccessBaseTests
    {
        public MsdTest() : base(TestRepository.Build("QA-stable", "QA-stable", new GenericRestApi(
                "QA-stable",
                "https://nsi-qa-stable.siscc.org/rest/",
                "dataflow/all/all/latest",
                "dataflow/{1}/{0}/{2}?references=all&detail=referencepartial",
                "data/{0}",
                isMetadata:true
            )))
        {
        }


        [Test]
        public void GetMsd()
        {
            var db = Repository.Databases?.First();

            Assert.IsNotNull(db);

            var ds = db.Datasets.FirstOrDefault(x=>x.Code == "DF_JENS_DAILY");

            Assert.IsNotNull(ds);

            var msd = ds.Msd;

            Assert.IsNotNull(msd);
        }

        [TestCase("DF_JENS_DAILY/............../", "v2/data/dataflow/UNSD/DF_JENS_DAILY/1.0/*.*.*.*.*.*.*.*.*.*.*.*.*.*.*/?attributes=msd&measures=none")]
        [TestCase("DF_JENS_DAILY/D.N.SI_POV_NAHC.ET._T._T..._T._T._T..._T._T/?startperiod=2019", "v2/data/dataflow/UNSD/DF_JENS_DAILY/1.0/D.N.SI_POV_NAHC.ET._T._T.*.*._T._T._T.*.*._T._T/?attributes=msd&measures=none")]
        public void MetdataQuery(string query, string expectedMetadataQuery)
        {
            var filter = Repository.BuildFilterFromSdmxQueryString<TestFilter>(query);
            
            Assert.AreEqual(expectedMetadataQuery, Repository.DataAccess.GetMetaDataQueryString(filter));
        }


        [TestCase("MILLED_RICE/all")]
        //[TestCase("DF_JENS_DAILY/D.N.SI_POV_NAHC.ET._T._T..._T._T._T..._T._T/?startperiod=2019")]
        [TestCase("DF_JENS_DAILY/all")]
        public void GetMetadata(string query)
        {
            var filter = Repository.BuildFilterFromSdmxQueryString<TestFilter>(query);
            var metadata = Repository.DataAccess.LoadMetadata(filter);

            Assert.IsNotNull(metadata);

            Console.WriteLine(metadata.GetUpperBound(0));
            Console.WriteLine(metadata.GetUpperBound(1));
        }

        [TestCase("DF_JENS_DAILY/all")]
        [TestCase("UNSD,DF_JENS_DAILY,1.0/all")]
        public void BuilFilterTest(string query)
        {
            var filter = Repository.BuildFilterFromSdmxQueryString<TestFilter>(query);

            Assert.IsNotNull(filter);
        }
    }
}
