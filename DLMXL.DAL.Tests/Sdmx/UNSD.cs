﻿using DLMXL.DAL.Access.Sdmx;
using DLMXL.DAL.Tests.Base;
using DLMXL.DAL.Tests.Moq;
using DLMXL.Models;
using NUnit.Framework;

namespace DLMXL.DAL.Tests.Sdmx
{
    public class Unsd : AccessBaseTests
    {
        public Unsd() : base(TestRepository.Build("Unsd", "Unsd", new GenericRestApi(
                "Unsd",
                "https://data.un.org/ws/rest/",
                "dataflow/all/all/latest",
                "datastructure/all/{0}/latest/?references=children",
                "data/{0}"
            )))
        {
        }


        [Test]
        public void GetData()
        {
            var query = "DF_UNData_UNFCC/..AUS.";
            var filter = Repository.BuildFilterFromSdmxQueryString<TestFilter>(query);

            Assert.IsNotNull(filter.SWFilter);

            var data = Repository.DataAccess.LoadData(OutputType.Flat, filter);

            Assert.IsNotNull(data);
        }
    }
}
