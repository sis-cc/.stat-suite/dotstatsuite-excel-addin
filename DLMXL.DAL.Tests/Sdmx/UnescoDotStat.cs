﻿using DLMXL.DAL.Access.Sdmx;
using DLMXL.DAL.Tests.Base;
using DLMXL.DAL.Tests.Moq;
using DLMXL.Models;
using NUnit.Framework;

namespace DLMXL.DAL.Tests.Sdmx
{
    [TestFixture]
    public class UnescoDotStat : AccessBaseTests
    {
        public UnescoDotStat() : base(TestRepository.Build("Unesco", "Unesco", new DotStatV7Sdmx(
                "Unesco2",
                "http://data.uis.unesco.org/RestSDMX/sdmx.ashx/"
            )))
        {

        }

        [TestCase("CAI_DS/.ALB")]
        public void GetData(string query)
        {
            var filter = Repository.BuildFilterFromSdmxQueryString<TestFilter>(query);

            Assert.IsNotNull(filter.SWFilter);

            var data = Repository.DataAccess.LoadData(OutputType.Flat, filter);

            Assert.IsNotNull(data);
        }
    }
}
