﻿using DLMXL.DAL.Access.Sdmx;
using DLMXL.DAL.Tests.Base;
using DLMXL.DAL.Tests.Moq;
using DLMXL.Models;
using NUnit.Framework;

namespace DLMXL.DAL.Tests.Sdmx
{
    [TestFixture]
    public class WorldbankTests : AccessBaseTests
    {
        public WorldbankTests() : base(TestRepository.Build("worldbank", "Worldbank (WDI)", new GenericRestApi(
            "worldbank",
            "http://api.worldbank.org/v2/sdmx/rest/",
            "dataflow",
            "datastructure/{1}/{0}?references=children",
            "data/{0}"
        )))
        {}

        [TestCase("WDI/A.SP_POP_TOTL.AFG/?startperiod=2011")]
        public void GetData(string query)
        {
            var filter = Repository.BuildFilterFromSdmxQueryString<TestFilter>(query);

            Assert.IsNotNull(filter.SWFilter);

            var data = Repository.DataAccess.LoadData(OutputType.Flat, filter);

            Assert.IsNotNull(data);
        }
    }
}
