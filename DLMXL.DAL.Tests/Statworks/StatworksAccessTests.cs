﻿using DLMXL.DAL.Access.Direct;
using DLMXL.DAL.Tests.Base;
using DLMXL.DAL.Tests.Moq;
using NUnit.Framework;

namespace DLMXL.DAL.Tests.Statworks
{
    [TestFixture]
    public class StatworksAccessTests : AccessBaseTests
    {
        public StatworksAccessTests() : base(TestRepository.Build("Statworks", "Statworks", new StatworksDirectAccess("vs-statworks-1", "StatworksMain")))
        {}
    }
}
