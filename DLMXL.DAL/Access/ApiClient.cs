﻿using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;

namespace DLMXL.DAL.Access
{
    internal static class ApiClient
    {
        private static HttpClient _client = new HttpClient(new HttpClientHandler()
        {
            AutomaticDecompression = DecompressionMethods.GZip | DecompressionMethods.Deflate
        });

        public static T Get<T>(string url, string token) where T : class
        {
            using (var requestMessage = GetMessage(HttpMethod.Get, url, token))
            {
                return ReadSync<T>(requestMessage);
            }
        }

        public static T Post<T>(string url, string token, IEnumerable<KeyValuePair<string, string>> @params = null) where T : class
        {
            using (var requestMessage = GetMessage(HttpMethod.Post, url, token))
            {
                if (@params != null)
                {
                    requestMessage.Content = new FormUrlEncodedContent(@params);
                }

                return ReadSync<T>(requestMessage);
            }
        }

        private static T ReadSync<T>(HttpRequestMessage message)
        {
            var response = _client
                .SendAsync(message)
                .GetAwaiter()
                .GetResult();

            return response.Content
                .ReadAsAsync<T>()
                .Result;
        }

        private static HttpRequestMessage GetMessage(HttpMethod method, string url, string token)
        {
            var message = new HttpRequestMessage(method, url);

            if (!string.IsNullOrEmpty(token))
            {
                message.Headers.Authorization = new AuthenticationHeaderValue("Bearer", token);
            }

            message.Headers.Add("Accept", "application/json");

            return message;
        }
	}
}
