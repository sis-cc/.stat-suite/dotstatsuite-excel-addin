﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using CsvHelper.Configuration;
using DLMXL.Models;
using DLMXL.Models.Interfaces;

namespace DLMXL.DAL.Access
{
    public abstract class BaseDataAccess
    {
        protected readonly string Code;

        protected virtual string StartTimeParam => "startTime";
        protected virtual string EndTimeParam => "endTime";
        
        public virtual bool IsFlagsOutput => true;
        public virtual bool IsDateOutput => true;
        public virtual string AuthId => null;
        public virtual bool IsAuthRequired => false;
        public virtual bool IsAuthenticated => false;
        public virtual string Userinfo => null;
        public virtual bool IsMetadata => false;
        public virtual bool IsHistoricVersion => false;
        public virtual bool IsSaveWithAttributesOnly => false;
        public virtual bool IsQueryWithFullId => true;
        public virtual bool IsQueryWithAllAsEmpty => true;
        public virtual bool IsActionColumn => IsMetadata;
        public virtual bool IsExternal => true;
        public virtual string DsnName => null;

        protected BaseDataAccess(string code = null)
        {
            Code = string.IsNullOrEmpty(code)
                ? null
                : Path.GetInvalidFileNameChars().Aggregate(code, (current, c) => current.Replace(c, '_'));
        }

        #region Output

        protected object[,] Output(
            OutputType type,
            List<Mapper> mapper,
            List<object[]> tempData,
            int dimIndex,
            int nonTimeDimCount,
            bool outputCodes,
            bool outputLabels,
            Dictionary<string, int> sids,
            HashSet<string> timePoints
        )
        {
            switch (type)
            {
                case OutputType.TimeSeriesAcross:
                    return TimeSeriesAccross(mapper, tempData, dimIndex, nonTimeDimCount, outputCodes, outputLabels,
                        sids, timePoints);

                case OutputType.TimeSeriesDown:
                    return TimeSeriesDown(mapper, tempData, dimIndex, nonTimeDimCount, outputCodes, outputLabels, sids,
                        timePoints);
            }

            return Flat(mapper, tempData, dimIndex, nonTimeDimCount, outputCodes, outputLabels);
        }

        private object[,] Flat(
            List<Mapper> mapList,
            List<object[]> tempData,
            int dimIndex,
            int nonTimeDimCount,
            bool outputCodes,
            bool outputLabels
        )
        {
            var rows = tempData.Count + 1;
            // when output labels & codes every dimension has 2 columns in output
            var columns = mapList.Count + (outputCodes && outputLabels ? nonTimeDimCount : 0);
            var result = new object[rows, columns];
            var clmn = 0;

            // headers
            for (var w = 0; w < mapList.Count; w++)
            {
                var mapper = mapList[w];

                if (mapper.Type == ColumnType.Dimension && (w - dimIndex) < nonTimeDimCount)
                {
                    if (outputCodes)
                        result[0, clmn++] = mapper.Header;

                    if (outputLabels)
                        result[0, clmn++] = mapper.Header;
                }
                else
                {
                    result[0, clmn++] = mapper.Header;
                }
            }


            // data
            for (var h = 0; h < tempData.Count; h++)
            {
                clmn = 0;

                for (var w = 0; w < mapList.Count; w++)
                {
                    var row = h + 1;
                    var mapper = mapList[w];

                    if (mapper.Type == ColumnType.Dimension && (w - dimIndex) < nonTimeDimCount)
                    {
                        if (outputCodes)
                            result[row, clmn++] = FormatDimMemberAsString(tempData[h][w]);

                        if (outputLabels)
                            result[row, clmn++] = mapper.Transform(tempData[h][w]);
                    }
                    else
                    {
                        result[row, clmn++] = tempData[h][w];
                    }
                }
            }

            return result;
        }

        private object[,] TimeSeriesAccross(
            List<Mapper> mapList,
            List<object[]> tempData,
            int dimIndex,
            int nonTimeDimCount,
            bool outputCodes,
            bool outputLabels,
            Dictionary<string, int> sids,
            HashSet<string> timePoints
        )
        {
            var timePointsSorted = timePoints.OrderBy(x => x).ToArray();
            var timeIndexes = timePointsSorted.Select((o, i) => new { o, i }).ToDictionary(x => x.o, x => x.i);

            var timeIndex = dimIndex + nonTimeDimCount;
            var outputDataColumnsCount = mapList.Count(x => x.Type.IsValue());
            var labelsColumns = dimIndex + nonTimeDimCount * (outputCodes && outputLabels ? 2 : 1);
            var labelMappers = mapList.Where((x, i) => !x.Type.IsValue() && i != timeIndex).ToArray();
            // outputDimColumns

            var height = sids.Count + 2;
            var width = labelsColumns + (outputDataColumnsCount * timePointsSorted.Length);

            var result = new object[height, width];
            var clmn = 0;

            // headers 1, 2 row (time)
            for (var i = 0; i < timePointsSorted.Length; i++)
            for (var c = 0; c < outputDataColumnsCount; c++)
            {
                result[0, labelsColumns + c + (outputDataColumnsCount * i)] = timePointsSorted[i];
                result[1, labelsColumns + c + (outputDataColumnsCount * i)] = mapList[timeIndex + 1 + c].Header;
            }

            // headers 2 row (SID + Dimensions columns)
            foreach (var mapper in labelMappers)
            {
                if (mapper.Type == ColumnType.Dimension)
                {
                    if (outputCodes)
                        result[1, clmn++] = mapper.Header;

                    if (outputLabels)
                        result[1, clmn++] = mapper.Header;
                }
                else
                {
                    result[1, clmn++] = mapper.Header;
                }
            }


            // data
            for (var i = 0; i < tempData.Count; i++)
            {
                var data = tempData[i];
                var sid = outputCodes ? data[0].ToString() : GetSID(data, dimIndex, nonTimeDimCount);
                var xIndex = timeIndexes[GetTimeUid(data, timeIndex)];
                var yIndex = 2 + sids[sid];
                clmn = 0;

                for (var w = 0; w < labelMappers.Length; w++)
                {
                    var mapper = labelMappers[w];

                    if (mapper.Type == ColumnType.Dimension)
                    {
                        if (outputCodes)
                            result[yIndex, clmn++] = FormatDimMemberAsString(data[w]);

                        if (outputLabels)
                            result[yIndex, clmn++] = mapper.Transform(data[w]);
                    }
                    else
                    {
                        result[yIndex, clmn++] = data[w];
                    }
                }

                for (var c = 0; c < outputDataColumnsCount; c++)
                    result[yIndex, labelsColumns + c + xIndex * outputDataColumnsCount] = data[timeIndex + 1 + c];
            }

            return result;
        }


        // -------------------------------------------------------------

        private object[,] TimeSeriesDown(
            List<Mapper> mapList,
            List<object[]> tempData,
            int dimIndex,
            int nonTimeDimCount,
            bool outputCodes,
            bool outputLabels,
            Dictionary<string, int> sids,
            HashSet<string> timePoints
        )
        {
            var sidSorted = sids.OrderBy(x => x.Value).Select(x => x.Key).ToArray();
            var timePointsSorted = timePoints.OrderBy(x => x).ToArray();
            var timeIndexes = timePointsSorted.Select((o, i) => new { o, i }).ToDictionary(x => x.o, x => x.i);

            var timeIndex = dimIndex + nonTimeDimCount;
            var outputDataColumnsCount = mapList.Count(x => x.Type.IsValue());
            var headerHeight = 1 /*SID*/ + nonTimeDimCount * (outputLabels ? (outputCodes ? 2 : 1) : 0);
            var height = headerHeight + 1 + timePointsSorted.Length;
            var width = 1 + (outputDataColumnsCount * sidSorted.Length);

            var result = new object[height, width];

            // SID 
            for (var i = 0; i < sidSorted.Length; i++)
            for (var c = 1; c <= outputDataColumnsCount; c++)
                result[0, c + (outputDataColumnsCount * i)] = sidSorted[i];

            // headers 2 row (Value columns)
            for (var i = 0; i < sidSorted.Length; i++)
            for (var c = 1; c <= outputDataColumnsCount; c++)
                result[headerHeight, c + i * outputDataColumnsCount] = mapList[timeIndex + c].Header;

            // Time column
            for (var i = 0; i < timePointsSorted.Length; i++)
                result[headerHeight + 1 + i, 0] = timePointsSorted[i];

            // data
            for (var i = 0; i < tempData.Count; i++)
            {
                var data = tempData[i];
                var sid = outputCodes ? data[0].ToString() : GetSID(data, dimIndex, nonTimeDimCount);
                var xIndex = sids[sid];
                var yIndex = headerHeight + 1 + timeIndexes[GetTimeUid(data, timeIndex)];

                for (var c = 1; c <= outputDataColumnsCount; c++)
                    result[yIndex, c + xIndex * outputDataColumnsCount] = data[timeIndex + c];

                if (!outputLabels)
                    continue;

                for (var c = 1; c <= outputDataColumnsCount; c++)
                {
                    yIndex = 1; // SID

                    for (var d = dimIndex; d < dimIndex + nonTimeDimCount; d++)
                    {
                        if (outputCodes)
                            result[yIndex++, c + xIndex * outputDataColumnsCount] = FormatDimMemberAsString(data[d]);

                        result[yIndex++, c + xIndex * outputDataColumnsCount] = mapList[d].Transform(data[d]);
                    }
                }
            }

            return result;
        }

        private string FormatDimMemberAsString(object obj)
        {
            if (obj == null)
                return null;

            var str = obj.ToString();

            return str.All(char.IsDigit)
                ? "=\"" + str + "\""
                : str;
        }

        protected string GetSID(object[] list, int start, int count)
        {
            return string.Join(".", list.Skip(start).Take(count));
        }

        protected string GetTimeUid(object[] list, int timeIndex)
        {
            return list[timeIndex].ToString();
        }

        protected Func<object, string> GetLabel(IDimension dim, LabelOutput output, string lang)
        {
            if (dim.IsTimeSeries || output == LabelOutput.Code)
                return (o) => o.ToString();

            var dic = dim.Members.ToDictionary(
                m => m.Code,
                m => m.Names.TryGetValue(lang, out var label) ? label : m.Code
            );

            return (o) =>
            {
                if (o == null)
                    return null;

                var key = o.ToString();

                return dic.TryGetValue(key, out var label)
                    ? label
                    : key;
            };
        }

        public virtual string GetSdmxQueryString(ISdmxFilter filter, List<KeyValuePair<string, string>> @params = null)
        {
            return filter.SdmxQuery.Trim('/') + "/all" + BuildQueryParams(filter, @params);
        }

        public virtual string GetMetaDataQueryString(ISdmxFilter filter)
        {
            throw new NotImplementedException();
        }

        protected virtual string BuildQueryParams(ISdmxFilter filter, List<KeyValuePair<string, string>> @params = null)
        {
            if (@params == null)
                @params = new List<KeyValuePair<string, string>>();

            if (!string.IsNullOrEmpty(StartTimeParam) && !string.IsNullOrEmpty(filter.StartTime))
                @params.AddItem(StartTimeParam, filter.StartTime);

            if (!string.IsNullOrEmpty(EndTimeParam) && !string.IsNullOrEmpty(filter.EndTime))
                @params.AddItem(EndTimeParam, filter.EndTime);

            return @params.Any()
                ? "?" + string.Join("&", @params.Select(pair => pair.Key + "=" + pair.Value))
                : null;
        }

        public virtual T InitFilter<T>(T filter, IList<KeyValuePair<string, string>> @params = null)
            where T : ISdmxFilter
        {
            foreach (var param in @params)
            {
                var key = param.Key;

                if (key.Equals(StartTimeParam, StringComparison.InvariantCultureIgnoreCase))
                {
                    filter.StartTime = param.Value;
                }
                else if (key.Equals(EndTimeParam, StringComparison.InvariantCultureIgnoreCase))
                {
                    filter.EndTime = param.Value;
                }
            }

            var freq = filter.SWFilter.FilterDimensions.FirstOrDefault(x => x.Dimension.IsFrequency);

            if (freq != null)
                foreach (var code in freq.SelectedMembers)
                {
                    if (code.IsYear)
                    {
                        filter.Frequency = Frequency.Anually;
                        break;
                    }

                    if (code.IsQuarter)
                    {
                        filter.Frequency = Frequency.Quarterly;
                        break;
                    }

                    if (code.IsMonth)
                    {
                        filter.Frequency = Frequency.Monthly;
                        break;
                    }
                }

            return filter;
        }



        #endregion

        #region Cache management

        protected string GetTempFile(bool isData, string filename = null)
        {
            var dir = GetTempDir();

            filename = (isData ? "Data" : filename)
                       + "_" + Code + "_"
                       + (isData ? DateTime.Now.ToString("_yyyy_MM_dd_HH_mm_ss_ffffff") : null)
                       + ".tmp";

            return Path.Combine(dir, filename);
        }

        protected bool TempFileExists(string filename)
        {
            if (string.IsNullOrEmpty(filename))
                return false;

            var fi = new FileInfo(filename);

            return fi.Exists && fi.LastWriteTime > DateTime.Now.AddDays(-7);
        }

        private static string GetTempDir()
        {
            var dir = Path.Combine(Path.GetTempPath(), "Datawizard");

            if (!Directory.Exists(dir))
                Directory.CreateDirectory(dir);

            return dir;
        }

        public static void ClearCache(string sourceId = null)
        {
            var dir = new DirectoryInfo(GetTempDir());

            foreach (var f in dir.GetFiles("*.*", SearchOption.TopDirectoryOnly))
                f.Delete();
        }

        public virtual void ClearObjectCache()
        {
        }

        #endregion

        #region Saving

        public virtual Tuple<string, int> Save(
            IDataset dataset,
            ICellAccess source,
            OutputType outputType,
            bool withCodes,
            bool withLabels,
            bool withFlags,
            bool isMetadata,
            bool withActionColumn,
            SaveInterruptHander dialogInterrupt
        )
        {
            throw new NotImplementedException();
        }

        /// <returns>[DIMENSIONS (TIME LAST), VALUE, ATTRIBUTES]</returns>
        internal static IEnumerable<string[]> ObservationReader(
            IDataset dataset,
            ICellAccess source,
            OutputType outputType,
            bool withCodes,
            bool withLabels,
            bool withFlags,
            bool isMetadata,
            bool withActionColumn,
            SaveInterruptHander dialogInterrupt
        )
        {
            var reader = ObservationReader(dataset, source, outputType, withCodes, withLabels, withFlags, isMetadata, withActionColumn);
            var index = 0;
            InterruptCode interrupt = 0;

            foreach (var row in reader)
            {
                index++;

                if (withActionColumn && dialogInterrupt != null)
                {
                    interrupt |= dialogInterrupt(row, dataset, index, interrupt);
                }

                yield return row;
            }
        }

        /// <returns>[DIMENSIONS (TIME LAST), VALUE, ATTRIBUTES]</returns>
        internal static IEnumerable<string[]> ObservationReader(
            IDataset dataset, 
            ICellAccess source, 
            OutputType outputType, 
            bool withCodes, 
            bool withLabels, 
            bool withFlags,
            bool isMetadata,
            bool withActionColumn
        )
        {
            var rows            = source.Rows;
            var columns         = source.Columns;

            var selectedRegions = source.Selection().ToArray();
            var selectedCells	= selectedRegions.Sum(x => x.Count);

            // ---------------------------------------------------
            var hasTime = dataset.HasATimeDimension;
            var attrs = dataset.Attributes.Count;
            var dims = dataset.Dimensions.Count;
            var nonTimeDims = dims - (hasTime ? 1 : 0);
            // ---------------------------------------------------

            var notTimeDimColumnCount = Convert.ToInt32(withCodes) * nonTimeDims + Convert.ToInt32(withLabels) * nonTimeDims;
            var attrColumnCount = Convert.ToInt32(withFlags) * attrs;
            var resultColumnCount = dims + 1 + attrColumnCount + (withActionColumn ? 1 : 0);

            if (isMetadata)
            {
                var metaAttrs = dataset.Msd.MetadataAttributes.Count;
                var isSelection = selectedCells > 0 && selectedRegions.Any(x => x.Rows.End > 1);
                resultColumnCount = (withActionColumn ? 1 : 0) + dims + metaAttrs;

                var expectedColumns = 1 /*SID*/
                      + (withActionColumn ? 1 : 0)  
                      + (hasTime ? 1 : 0)
                      + notTimeDimColumnCount
                      + metaAttrs;

                if (columns != expectedColumns)
                    throw new InvalidOperationException($"Invalid column number, expected/actual: {expectedColumns}/{columns}");

                for (var row = 2; row <= rows; row++)
                {
                    if (isSelection && !selectedRegions.Any(x => row >= x.Rows.Start && row <= x.Rows.End))
                        continue;

                    var result = new string[resultColumnCount];
                    var sid = source[row, 1];
                    var dimOutputIndex = 0;

                    // ACTION COLUMN
                    if (withActionColumn)
                    {
                        result[dimOutputIndex++] = ActionReplace(source[row, 2]);
                    }

                    sid.Split('.').CopyTo(result, dimOutputIndex);

                    var offset = 2 /* SID + 1 VBA array start index */ + notTimeDimColumnCount - nonTimeDims;

                    for (var i = nonTimeDims + dimOutputIndex; i < resultColumnCount; i++)
                        result[i] = source[row, offset + i];

                    yield return result;
                }
            }
            else if (outputType == OutputType.Flat)
            {
				var isSelection = selectedCells > 0 && selectedRegions.Any(x=>x.Rows.End > 1);

				var expectedColumns = 2 /*SID + Value*/
                    + (withActionColumn ? 1 : 0)
                    + (hasTime ? 1 : 0)
					+ notTimeDimColumnCount
					+ attrColumnCount;

				if (columns != expectedColumns)
					throw new InvalidOperationException($"Invalid column number, expected/actual: {expectedColumns}/{columns}");

				for (var row = 2; row <= rows; row++)
				{
					if (isSelection && !selectedRegions.Any(x => row >= x.Rows.Start && row <= x.Rows.End))
						continue;

					var result	= new string[resultColumnCount];
					var sid		= source[row, 1];
                    var dimOutputIndex = 0;

                    // ACTION COLUMN
                    if (withActionColumn)
                    {
                        result[dimOutputIndex++] = ActionReplace(source[row, 2]);
                    }

                    sid.Split('.').CopyTo(result, dimOutputIndex);

                    var offset = 2 /* SID + 1 VBA array start index */ + notTimeDimColumnCount - nonTimeDims;

                    for (var i = nonTimeDims + dimOutputIndex; i < resultColumnCount; i++)
						result[i] = source[row,  offset + i];

					yield return result;
				}
            }
			// ---------------------------------------------------
			else if (outputType == OutputType.TimeSeriesDown)
            {
	            var yOffset = /*sid + value*/ 3 + Convert.ToInt32(withLabels) * notTimeDimColumnCount;

	            var isSelection = selectedCells > 0 && selectedRegions.Any(x=>x.Rows.End >= yOffset);

				for (var row = yOffset; row <= rows; row++)
				{
					Range[] selectedColums = null;

					if (isSelection)
					{
						selectedColums = selectedRegions
							.Where(x => row >= x.Rows.Start && row <= x.Rows.End)
							.Select(x => x.Columns)
							.ToArray();

						if(!selectedColums.Any())
							continue;
					}

					for (var xOffset = 2;xOffset <= columns;xOffset += (1 + attrColumnCount))
					{
						if (selectedColums!=null)
						{
							var observation = new Range(xOffset, xOffset + attrColumnCount);

							if(!selectedColums.Any(c=>c.IsOverlapped(observation)))
								continue;
						}

						var result	= new string[resultColumnCount];
						var sid		= source[1, xOffset].Split('.');
						sid.CopyTo(result, 0);

						// time
						result[nonTimeDims] = source[row, 1];
						
						// value
						result[nonTimeDims + 1] = source[row, xOffset];

						// attributes
						if (withFlags)
							for (var i = 1; i <= attrColumnCount; i++)
								result[nonTimeDims + 1 + i] = source[row, xOffset + i];

						yield return result;
					}
				}
			}
			// ---------------------------------------------------
			else if (outputType == OutputType.TimeSeriesAcross)
			{
				var valueColumnStart = 2 /* SID + non 0 index of Excel */ + notTimeDimColumnCount;

				var isSelection = selectedCells > 0 && selectedRegions.Any(x=>x.Rows.End > 2);

				for (var row = 3; row <= rows; row++)
				{
					Range[] selectedColums = null;

					if (isSelection)
					{
						selectedColums = selectedRegions
							.Where(x => row >= x.Rows.Start && row <= x.Rows.End)
							.Select(x => x.Columns)
							.ToArray();

						if(!selectedColums.Any())
							continue;

						// remove selections that don't overlap with values, these selections take complete row without column restriction.
						selectedColums = selectedColums
							.Where(x=>x.End >= valueColumnStart)
							.ToArray();
					}

					var sid		= source[row, 1].Split('.');

					for (var offset	= valueColumnStart;offset <= columns;offset += (1 + attrColumnCount))
					{
						if (selectedColums!=null && selectedColums.Any())
						{
							var observation = new Range(offset, offset + attrColumnCount);

							if(!selectedColums.Any(c=>c.IsOverlapped(observation)))
								continue;
						}

						var result	= new string[resultColumnCount];
						sid.CopyTo(result, 0);

						// time
						result[nonTimeDims] = source[1, offset];
						
						// value
						result[nonTimeDims + 1] = source[row, offset];

						// attributes
						if (withFlags)
							for (var i = 1; i <= attrColumnCount; i++)
								result[nonTimeDims + 1 + i] = source[row, offset + i];

						yield return result;
					}
				}
			}
		}

        private static string ActionReplace(string action)
        {
            return string.IsNullOrEmpty(action) ? "M" : action.Substring(0,1).ToUpper();
        }

		internal static string SaveCsv(IDataset dataset, IEnumerable<string[]> data, bool withFlags, bool withActionColumn, out int count)
		{
			count = 0;

			var filename = Path.Combine(Path.GetTempPath(), $"{DateTime.Now:yyyyMMddHHmmssfff}.csv");

			var csvConfig = new CsvConfiguration(CultureInfo.InvariantCulture)
			{
				Encoding = Encoding.UTF8,
				Delimiter = ","
			};

			using (var csvWriter = new CsvHelper.CsvWriter(new StreamWriter(filename), csvConfig))
			{
                // headers -----------------------------
                csvWriter.WriteField("STRUCTURE");
                csvWriter.WriteField("STRUCTURE_ID");

                if (withActionColumn)
                {
                    csvWriter.WriteField("ACTION");

                    // order by ACTION, adjust content
                    data = data
                        .OrderBy(x => x[0], new ActionComparer())
                        .ToArray();
                }

                foreach (var dim in dataset.Dimensions.OrderBy(dim => dim.TimeLastOrder()))
					csvWriter.WriteField(dim.Code);

				csvWriter.WriteField("OBS_VALUE");

				if(withFlags)
					foreach (var attr in dataset.Attributes)
						csvWriter.WriteField(attr.Code);

				csvWriter.NextRecord();

				// data ------------------------------

				var dataflowId = dataset.DfId.ToString();

				foreach (var row in data)
				{
                    csvWriter.WriteField("DATAFLOW");
                    csvWriter.WriteField(dataflowId);

					foreach (var column in row)
						csvWriter.WriteField(column);

					csvWriter.NextRecord();

					count++;
				}
			}

			return filename;
		}

        internal static string SaveMetaCsv(IDataset dataset, IEnumerable<string[]> data, bool withActionColumn, out int count)
        {
            count = 0;
            var filename = Path.Combine(Path.GetTempPath(), $"{DateTime.Now:yyyyMMddHHmmssfff}.csv");

            var csvConfig = new CsvConfiguration(CultureInfo.InvariantCulture)
            {
                Encoding = Encoding.UTF8,
                Delimiter = ","
            };

            using (var csvWriter = new CsvHelper.CsvWriter(new StreamWriter(filename), csvConfig))
            {
                // headers -----------------------------
                csvWriter.WriteField("STRUCTURE");
                csvWriter.WriteField("STRUCTURE_ID");

                if (withActionColumn)
                {
                    csvWriter.WriteField("ACTION");
                    
                    // order by ACTION, adjust content
                    data = data
                        .OrderBy(x => x[0], new ActionComparer())
                        .Select(x => AdjustMetadataBasedOnAction(x, dataset.Dimensions.Count))
                        .ToArray();
                }

                foreach (var dim in dataset.Dimensions.OrderBy(dim => dim.TimeLastOrder()))
                    csvWriter.WriteField(dim.Code);

                foreach (var attr in dataset.Msd.MetadataAttributes)
                    csvWriter.WriteField(attr.Path);

                csvWriter.NextRecord();

                // data ------------------------------

                var dataflowId = dataset.DfId.ToString();

                foreach (var row in data)
                {
                    csvWriter.WriteField("DATAFLOW");
                    csvWriter.WriteField(dataflowId);

                    foreach (var column in row)
                        csvWriter.WriteField(column);

                    csvWriter.NextRecord();

                    count++;
                }
            }

            return filename;
        }

        private static string[] AdjustMetadataBasedOnAction(string[] source, int dimCount)
        {
            // DELETE adjustment: all non-empty measures/attributes with dash -
            if (source[0] == "D")
            {
                for (var i = dimCount + 1; i < source.Length; i++)
                {
                    source[i] = "-";
                }
            }
            // Merge/Replace adjustment: replace all empty dimension values of ref. metadata with tilde ~
            else if (source[0] == "M" || source[0] == "R")
            {
                for (var i = 1; i < dimCount + 1; i++)
                {
                    if (string.IsNullOrEmpty(source[i]))
                    {
                        source[i] = "~";
                    }
                }
            }

            return source;
        }

        internal class ActionComparer : IComparer<string>
        {
            public int Compare(string x, string y)
            {
                return Transform(x).CompareTo(Transform(y));
            }

            private int Transform(string str)
            {
                switch (str)
                {
                    case "D": return 0;
                    case "R": return 1;
                    default: return 2;
                }
            }
        }

        internal static IEnumerable<string[]> ReadCsv(string filename)
        {
            using (var csvReader = new CsvHelper.CsvReader(new StreamReader(filename), CultureInfo.InvariantCulture))
            {
                while (csvReader.Read())
                {
                    yield return csvReader.Context.Parser.Record;
                }
            }
        }

        #endregion

        #region Auth

		public virtual void LogOut()
		{
			throw new NotImplementedException();
		}

        public virtual IAuth Auth => throw new NotImplementedException();

        #endregion

        #region Query Dialog

        protected virtual string GetSdmxQueryDialogString(ISdmxFilter filter, bool production, bool updateDate) => this.GetSdmxQueryString(filter);
        protected virtual string GetDataParams(ISdmxFilter filter, bool production, bool updateDate) => null;

        public virtual IList<KeyValuePair<string, string>> GetConnectionStrings(ISdmxFilter filter, bool production, bool updateDate)
        {
            var sdmxQuery = this.GetSdmxQueryDialogString(filter, production, updateDate);
            var spParams = this.GetDataParams(filter, production, updateDate);

            var list = new List<KeyValuePair<string, string>>()
            {
                new KeyValuePair<string, string>("Sdmx Query", sdmxQuery),
                new KeyValuePair<string, string>("STATA", GetStataString(spParams)),
                new KeyValuePair<string, string>("Eviews", GetEviewsString(spParams)),
                new KeyValuePair<string, string>("SAS", GetSasString(spParams)),
                new KeyValuePair<string, string>("SQL", GetSqlString(spParams)),
                new KeyValuePair<string, string>("R", GetRString(spParams)),
                new KeyValuePair<string, string>("Python", GetPythonString(spParams)),
            };

            return list
                .Where(x=>!string.IsNullOrEmpty(x.Value))
                .ToArray();
        }

        
        protected virtual string GetStataString(string spParams) => null;
        protected virtual string GetEviewsString(string spParams) => null;
        protected virtual string GetSasString(string spParams) => null;
        protected virtual string GetSqlString(string spParams) => null;
        protected virtual string GetRString(string spParams) => null;
        protected virtual string GetPythonString(string spParams) => null;


        #endregion
    }
}
