﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using System.Xml;
using System.Xml.Serialization;
using DLMXL.Models;
using DLMXL.Models.Exceptions;
using DLMXL.Models.Interfaces;

namespace DLMXL.DAL.Access.Direct {

    /// <summary>
    /// This class should be used as a Singleton
    /// </summary>
    public sealed class DotStatV7DirectAccess : SqlDirectAccess, IDataAccess
    {
        private readonly Lazy<Dictionary<int, IList<IDataset>>> _datasets;
        private readonly Lazy<Dictionary<string, List<IQuery>>> _queries;

        public bool IsSaveAllowed => false;
        public bool IsDataSpace => false;


        public DotStatV7DirectAccess(string server, string database, string queryUri) : base(server, database)
        {
            _datasets   = new Lazy<Dictionary<int, IList<IDataset>>>(LoadAllDatasets);
            _queries    = new Lazy<Dictionary<string, List<IQuery>>>(()=> DotStatQueries.GetQueries(queryUri.TrimEnd('/')));
        }

        #region IDataAccess
        public void BulkPreLoad()
        {}

        public IList<IDatabase> LoadDatabases()
        {
            var lookup  = new Dictionary<int, KeyValuePair<int?, SWDatabase>>();

            using (var conn = GetConnection())
            using (var cmd = new SqlCommand("get.themeList", conn))
            {
                cmd.CommandType     = CommandType.StoredProcedure;
                cmd.CommandTimeout  = 60;
                cmd.Parameters.AddWithValue("@Language", "en");
                    
                using (var reader = cmd.ExecuteReader())
                    while (reader.Read())
                    {
                        var id          = reader.ColumnValue<int>("IDTheme");
                        var name		= reader.ColumnValue<string>("Name");
                        var names		= new Dictionary<string, string>(){{"en", name}};

                        var parentId    = reader.ColumnValue<int>("IDParentTheme");
                        var database    = SWDatabase.Build(this, id, null, name, names);
                        lookup[id]      = new KeyValuePair<int?, SWDatabase>(id == parentId ? (int?) null : parentId, database);
                    }
            }

            foreach (var pair in lookup.Values)
            {
                if (!pair.Key.HasValue)
                    continue;

                var database                = pair.Value;
                var parent                  = lookup[pair.Key.Value].Value;

                parent.AddChildMember(database);
                database.SetParent(parent);
            }

            return new List<IDatabase>(lookup.Values.Select(x=>x.Value));
        }

        public IList<IDataset> LoadDatasets(IDatabase db)
        {
            if (!_datasets.Value.ContainsKey(db.Id))
                return new IDataset[0];

            var list = _datasets.Value[db.Id];

            foreach (var dataset in list)
                dataset.Database = db;

            return list;
        }

        public IList<IQuery> LoadQueries(IDataset ds)
        {
            if (!_queries.Value.ContainsKey(ds.Code))
                return new IQuery[0];

            return _queries.Value[ds.Code];
        }

        public IList<IDimension> FetchDimensions(IDataset ds)
        {
            var res         = new List<IDimension>();

            using (var conn = GetConnection())
            {
                var taskEn = Task<DataTable>.Factory.StartNew(() => FetchDimensions(conn, ds.Code, "en"));
                var taskFr = Task<DataTable>.Factory.StartNew(() => FetchDimensions(conn, ds.Code, "fr"));

                Task.WaitAll(taskEn, taskFr);

                var result = from tableEn in taskEn.Result.AsEnumerable()
                             join tableFr in taskFr.Result.AsEnumerable() on tableEn.Field<string>("Code") equals tableFr.Field<string>("Code")
                             select new
                             {
                                 dimCode    = tableEn.ColumnValue<string>("Code"),
                                 dimNameE   = tableEn.ColumnValue<string>("DimensionName"),
                                 dimNameF   = tableFr.ColumnValue<string>("DimensionName"),
                                 dimIsTime  = tableEn.ColumnValue<bool>("IsTimeSeriesDim")
                             };

                foreach (var item in result)
                {
                    var dim = SWDimension.Build(
							ds,
                            item.dimCode,
							item.dimNameE,
							new Dictionary<string, string>()
							{
								{"en", item.dimNameE},
								{"fr", item.dimNameF}
							},
                            item.dimIsTime,
                            false,
                            item.dimCode.ToUpper().StartsWith("FREQ")
                    );

                    var members = FetchDimensionMemebers(conn, dim);
                    dim.ResetMembers(members);
                    res.Add(dim);
                }
            }

            return res;
        }

        private DataTable FetchDimensions(SqlConnection conn, string dsCode, string language)
        {
            var table = new DataTable();

            using (var cmd = new SqlCommand("get.dimensionList", conn))
            {
                cmd.CommandType     = CommandType.StoredProcedure;
                cmd.CommandTimeout  = 60;
                cmd.Parameters.AddWithValue("@datasetCode", dsCode);
                cmd.Parameters.AddWithValue("@LanguageCode", language);

                using (var adapter = new SqlDataAdapter(cmd))
                    adapter.Fill(table);
            }

            return table;
        }

        private IList<ICodelistMember> FetchDimensionMemebers(SqlConnection conn, SWDimension dim)
        {
            var list    = new LinkedList<ICodelistMember>();
            var lookup  = new Dictionary<string, CodelistMember>(StringComparer.InvariantCultureIgnoreCase);

            var taskEn  = Task<DataTable>.Factory.StartNew(() => FetchDimensionMemebers(conn, dim, "en"));
            var taskFr  = Task<DataTable>.Factory.StartNew(() => FetchDimensionMemebers(conn, dim, "fr"));

            Task.WaitAll(taskEn, taskFr);

            var result = from tableEn in taskEn.Result.AsEnumerable()
                         join tableFr in taskFr.Result.AsEnumerable() on tableEn.Field<string>("Code") equals tableFr.Field<string>("Code") into leftJoinGroup
                         from fr in leftJoinGroup.DefaultIfEmpty()
                         select new
                         {
                             memCode        = tableEn.ColumnValue<string>("Code"),
                             memParentCode  = tableEn.ColumnValue<string>("ParentCode"),
                             memNameE       = tableEn.ColumnValue<string>("Name"),
                             memNameF       = fr?.ColumnValue<string>("Name") ?? tableEn.ColumnValue<string>("Name")
                         };

            foreach (var item in result)
            {
                // Note: This supposes that the order sent by .Stat always puts the parents before their childs
                CodelistMember parent;
                lookup.TryGetValue(item.memParentCode, out parent);

                var mem = CodelistMember.Build(
                    parent,
                    item.memCode,
                    item.memNameE,
					new Dictionary<string, string>()
					{
						{"en", item.memNameE},
						{"fr", item.memNameF}
					}
                );

                parent?.AddChildMember(mem);

                list.AddLast(mem);
                lookup[item.memCode] = mem;
            }

            return list.ToArray();
        }

        private DataTable FetchDimensionMemebers(SqlConnection conn, SWDimension dim, string language)
        {
            var table = new DataTable();

            using (var cmd = new SqlCommand("get.dimensionMemberList", conn))
            {
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandTimeout = 60;
                cmd.Parameters.AddWithValue("@datasetCode", dim.Dataset.Code);
                cmd.Parameters.AddWithValue("@dimCode", dim.Code);
                cmd.Parameters.AddWithValue("@LanguageCode", language);

                using (var adapter = new SqlDataAdapter(cmd))
                    adapter.Fill(table);
            }

            return table;
        }

        public IList< IControlCodeMember > FetchControlCodes(IDataset ds) {

            var list = new List<IControlCodeMember>();

            using (var conn = GetConnection())
            using (var cmd = new SqlCommand("get.flagList", conn))
            {
                cmd.CommandType         = CommandType.StoredProcedure;
                cmd.CommandTimeout      = 60;
                cmd.Parameters.AddWithValue("@datasetCode", ds.Code);

                var idx = 1;

                using (var reader = cmd.ExecuteReader())
                    while (reader.Read())
                    {
                        var code            = reader.ColumnValue<string>("Flag").ToUpper();

                        list.Add(SWControlCodeMember.Build(
                            idx++,
                            code,
							null
                        ));
                    }
            }

            return list.ToArray();
        }

        private Dictionary<int, IList<IDataset>> LoadAllDatasets()
        {
            var dic = new Dictionary<int, IList<IDataset>>();

            using (var conn = GetConnection())
            using (var cmd = new SqlCommand("get.datasetList", conn))
            {
                cmd.CommandType     = CommandType.StoredProcedure;
                cmd.CommandTimeout  = 60;

                using (var reader = cmd.ExecuteReader())
                    while (reader.Read())
                    {
                        var themeId = reader.ColumnValue("ThemeID", -1);

                        if(themeId <= -1)
                            continue;

                        var list = dic.GetOrAdd(themeId, (x) => new List<IDataset>());
                        var code = reader.ColumnValue("Code", "");
                        var name = reader.ColumnValue("Name", "");

                        list.Add(new SWDataset(
                            this,
                            null,
                            code,
							name,
                            new Dictionary<string, string>()
                            {
	                            {"en",  name}
                            }
                        ));
                    }
            }

            return dic;
        }

        public IList<IAttribute> FetchAttributes(IDataset ds)
        {
	        return null;
        }

        public IList<KeyValuePair<string, string>> GetLanguages(IDataset ds)
        {
	        return new[]
	        {
				new KeyValuePair<string, string>("en", "English"), 
				new KeyValuePair<string, string>("fr", "French")
	        };
        }

        public IMsd LoadMsd(IDataset ds)
        {
            return null;
        }

		#endregion

		#region Get data
		public object[,] LoadData(
            OutputType outputType,
            ISdmxFilter sdmxFilter,
            bool production = true,
            LabelOutput labelOutput = LabelOutput.Code,
			string language = null,
            bool flags = false,
            bool updateDate = false,
            bool withActionColumn = false
        )
        {
            var outputCodes             = (labelOutput & LabelOutput.Code) > 0;
            var outputLabels            = labelOutput > LabelOutput.Code;
            var dimIndex				= 1;

            var filter						= sdmxFilter.SWFilter;
            var dimensions        = filter.Dataset.Dimensions.OrderBy(dim => dim.TimeLastOrder()).ToArray();
            var isTime					= filter.Dataset.HasATimeDimension;
            var nonTimeDimCount         = dimensions.Count() - (isTime ? 1 : 0);

            var readerIndex             = 1;
            var mapper                  = new List<Mapper>();
            var tempData                = new List<object[]>(50000);
            
            var isTimeSeries                = false;
            Dictionary<string, int> sids    = null;
            var sidOrder                    = 0;
            HashSet<string> timePoints      = null;

            if (outputType == OutputType.TimeSeriesDown || outputType == OutputType.TimeSeriesAcross)
            {
                if (!isTime)
                    throw new InvalidOperationException("No time dimension");

                isTimeSeries    = true;
                sids            = new Dictionary<string, int>();
                timePoints      = new HashSet<string>();
            }

			// SID
            mapper.Add(new Mapper(ColumnType.Calculated, "SID", -1));

            dimensions
                .Select(dim => new Mapper(ColumnType.Dimension, dim.Code, readerIndex++, GetLabel(dim, labelOutput, language)))
                .ForEach(m => mapper.Add(m));

            mapper.Add(new Mapper(ColumnType.Value, "Value", readerIndex++));

            if (flags)
                mapper.Add(new Mapper(ColumnType.Attribute, "Flag", readerIndex++));

            if (updateDate)
                mapper.Add(new Mapper(ColumnType.Attribute, "Update date", readerIndex + 3, Mapper.ToSqlDt));

            // --------------------------------------------------------------------

            try
            {
                using (var conn = GetConnection())
                using (var cmd = new SqlCommand("get.Data", conn))
                {
                    cmd.CommandType     = CommandType.StoredProcedure;
                    cmd.CommandTimeout = 600;
                    cmd.Parameters.AddRange(GetSqlParams(sdmxFilter, production, updateDate));

                    using (var reader = cmd.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            var data = new object[mapper.Count];

                            for (var i = 0; i < mapper.Count; i++)
                            {
                                if (mapper[i].Type == ColumnType.Calculated) continue;

                                data[i] = mapper[i].GetData(reader);
                            }

                            var sid = GetSID(data, dimIndex, nonTimeDimCount);

                            data[0] = sid;

                            tempData.Add(data);

                            if (!isTimeSeries)
                                continue;

                            if (!sids.ContainsKey(sid))
                                sids.Add(sid, sidOrder++);

                            timePoints.Add(GetTimeUid(data, dimIndex + nonTimeDimCount));
                        }
                    }
                }
            }
            catch (SqlException ex)
            {
                if (ex.IsSqlTimeoutError())
                    throw new DlmXLManagedException("OECD.STAT database data load operation is taking too much time and thus cancelled by the server, please try narrowing filter down");

                throw;
            }

            return Output(outputType,
                mapper,
                tempData,
                dimIndex,
                nonTimeDimCount,
                outputCodes,
                outputLabels,
                sids,
                timePoints
            );
        }

        protected override SqlParameter[] GetSqlParams(ISdmxFilter filter, bool production, bool updateDate)
        {
            var sdmxParams = new List<KeyValuePair<string, string>>();

            if (updateDate)
                sdmxParams.AddItem("returntimestamps", "yes");

            var sdmxQuery = this.GetSdmxQueryString(filter, sdmxParams);

            return new[]
            {
                new SqlParameter("@SDMXRESTfulQuery", SqlDbType.NVarChar) {Value = sdmxQuery}
            };
        }

        public object[,] LoadMetadata(
            ISdmxFilter sdmxFilter,
            LabelOutput labelOutput = LabelOutput.Code,
            string language = null,
            bool withActionColumn = false
        )
        {
            throw new NotImplementedException();
        }
        #endregion

        #region Dotstat specific

        [Serializable]
        public class DotStatQuery : IQuery
        {
            internal Lazy<string> SdmxApi { get; set; }
            public string SdmxQuery => SdmxApi.Value;
          
            [XmlElement("Id")]
            public int Id { get; set; }
            [XmlElement("Name")]
            public string DefaultName { get; set; }
            public string DatasetCode { get; set; }

            public string GetLabel(string pattern, string language = null)
            {
                throw new NotImplementedException();
            }

            #region IQuery

            [XmlIgnore]
			public Dictionary<string, string> Names => new Dictionary<string, string>()
			{
				{"en", DefaultName}
			};

            public string Uid => this.Id.ToString();
            public int Index => 0;
            public string Code => null;

            public string ToString(LabelOutput code)
            {
                return DefaultName;
            }
            #endregion
        }

        [Serializable, XmlRoot("ArrayOfQueryCoreInfo")]
        public class DotStatQueries
        {
            private const string NS = "http://schemas.datacontract.org/2004/07/DotStat.QueryManager.Contract";
            private static readonly XmlSerializer Serializer = new XmlSerializer(typeof(DotStatQueries), NS);

            [XmlElement("QueryCoreInfo")]
            public DotStatQuery[] Queries;

            static string GetWebServiceXml(string url)
            {
                try
                {
                    using (var wc = new WebClient())
                    {
                        wc.Proxy = null;
                        wc.Credentials = CredentialCache.DefaultCredentials;
                        wc.Headers.Add("Content-Type", "text/xml");
                        return wc.DownloadString(url);
                    }
                }
                catch
                {
                    return null;
                }
            }

            static DotStatQueries Deserialize(string xml)
            {
                try
                {
                    using (var reader = new StringReader("<?xml version=\"1.0\"?>" + xml))
                        return Serializer.Deserialize(reader) as DotStatQueries;
                }
                catch
                {
                    return null;
                }
            }

            static string GetSdmx(string baseUri, int id)
            {
                var xml = GetWebServiceXml($"{baseUri}/{id}/rest");

                return string.IsNullOrEmpty(xml) 
                                    ? null 
                                    : ExtractQuery(xml);
            }

            static string ExtractQuery(string xml)
            {
                using (var reader = new XmlTextReader(new StringReader(xml)))
                    if (reader.Read() && reader.IsStartElement() && reader.Name == "string")
                        return reader.ReadElementString();

                return null;
            }

            internal static Dictionary<string, List<IQuery>> GetQueries(string baseUri)
            {
                var dic = new Dictionary<string, List<IQuery>>();
                var xml = GetWebServiceXml(baseUri);

                if (string.IsNullOrEmpty(xml))
                    return dic;

                var obj = Deserialize(xml);

                if (obj == null)
                    return dic;

                foreach (var query in obj.Queries)
                {
                    var key = query.DatasetCode;

                    if (!dic.ContainsKey(key))
                        dic.Add(key, new List<IQuery>());

                    query.SdmxApi = new Lazy<string>(() => GetSdmx(baseUri, query.Id));

                    dic[key].Add(query);
                }

                return dic;
            }
        }

        #endregion

    }
}
