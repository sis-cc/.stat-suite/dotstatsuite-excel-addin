﻿using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using DLMXL.Models.Interfaces;

namespace DLMXL.DAL.Access.Direct
{
    public abstract class SqlDirectAccess : BaseDataAccess
    {
        public override bool IsExternal => false;
        public override bool IsQueryWithFullId => true;
        private readonly string _server;
        private readonly string _database;

        protected SqlDirectAccess(string server, string database)
        {
            _server     = server;
            _database   = database;
        }

        protected virtual SqlConnection GetConnection(bool open = true)
        {
            var connection = new SqlConnection($"Integrated Security=SSPI;MultipleActiveResultSets=true;Persist Security Info=False;initial catalog={_database};data source={_server};Application name=Datawizard;Connection Timeout=5");
            if(open) connection.Open();
            return connection;
        }

        protected abstract SqlParameter[] GetSqlParams(ISdmxFilter filter, bool production, bool updateDate);



        #region Query Dialog

        protected override string GetDataParams(ISdmxFilter filter, bool production, bool updateDate)
        {
            var sqlParams = GetSqlParams(filter, production, updateDate)
                .Select(x => x.Value.ToString())
                .ToArray();

            return string.Join(",", sqlParams.Select(p => "'" + p + "'"));
        }

        protected override string GetStataString(string spParams)
        {
            return $"odbc load, exec (\"[get].[Data] {spParams}\") dsn(\"{DsnName}\") clear";
        }

        protected override string GetEviewsString(string spParams)
        {
            return $"pageload(type=odbc,page={DsnName}) {DsnName} \"{{call [get].[Data]({spParams})}}\"";
        }

        protected override string GetSasString(string spParams)
        {
            return $@"CONNECT TO ODBC(DSN={DsnName});
CREATE TABLE DOTSTAT_EXTRACT AS 
SELECT * FROM CONNECTION TO ODBC(EXEC [get].[Data] {spParams});
QUIT;";
        }

        protected override string GetSqlString(string spParams)
        {
            return $"exec [get].[Data] {spParams}";
        }

        protected override string GetRString(string spParams)
        {
            return $"result<-sqlQuery(sqlCon, \"exec get.Data {spParams}\");";
        }

        #endregion
    }
}
