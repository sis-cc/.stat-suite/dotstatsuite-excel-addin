﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Xml.Linq;
using System.Xml.XPath;
using DLMXL.Models;
using DLMXL.Models.Exceptions;
using DLMXL.Models.Interfaces;

namespace DLMXL.DAL.Access.Direct
{
    public sealed class StatworksDirectAccess : SqlDirectAccess, IDataAccess
    {
        private readonly Lazy<Dictionary<string, List<StatworksQuery>>> _queries;

        public bool IsSaveAllowed => false;
        public bool IsDataSpace => true;

        public StatworksDirectAccess(string server, string database) : base(server, database)
        {
            _queries = new Lazy<Dictionary<string, List<StatworksQuery>>>(LoadQueries);
        }

        #region IDataAccess
        public void BulkPreLoad()
        {}

        public IList<IDatabase> LoadDatabases()
        {
            var list = new LinkedList<IDatabase>();

            using (var conn = GetConnection())
            using (var cmd = new SqlCommand("dbo.spDatabaseList", conn))
            {
                cmd.CommandType     = CommandType.StoredProcedure;
                cmd.CommandTimeout  = 60;

                using (var reader = cmd.ExecuteReader())
	                while (reader.Read())
	                {
		                var nameEn = reader.ColumnValue<string>("NAME_E");
		                var nameFr = reader.ColumnValue<string>("NAME_F");

		                list.AddLast(SWDatabase.Build(
			                this,
			                reader.ColumnValue<int>("ID"),
			                reader.ColumnValue<string>("CODE"),
			                nameEn,
			                new Dictionary<string, string>()
			                {
				                {"en", nameEn},
				                {"fr", nameFr}
			                }
		                ));
	                }
            }

            return list.ToArray();
        }

        public IList<IDataset> LoadDatasets(IDatabase db)
        {
            var list = new LinkedList<IDataset>();

            using (var conn = GetConnection())
            using (var cmd = new SqlCommand("dbo.spDatasetList", conn))
            {
                cmd.CommandType     = CommandType.StoredProcedure;
                cmd.CommandTimeout  = 60;
                cmd.Parameters.AddWithValue("@databaseId", db.Id);

                using (var reader = cmd.ExecuteReader())
	                while (reader.Read())
	                {
		                var id = reader.ColumnValue<int>("ID");
		                var code = reader.ColumnValue<string>("Code");
		                var nameEn = reader.ColumnValue<string>("NAME_E");
		                var nameFr = reader.ColumnValue<string>("NAME_F");

		                list.AddLast(new SWDataset(
			                this,
			                db,
			                code,
			                nameEn,
			                new Dictionary<string, string>()
			                {
				                {"en", nameEn},
				                {"fr", nameFr}
			                },
			                null,
			                null,
			                id
		                ));
	                }
            }

            return list.ToArray();
        }

        public IList<IQuery> LoadQueries(IDataset ds)
        {
            var key         = ds.Database.Id + "." + ds.Id;
            
            if(!_queries.Value.ContainsKey(key))
                return null;

            var list = _queries.Value[key];

            foreach (var query in list)
                query.Dataset = ds;

            return new List<IQuery>(list);
        }

        public IList<IDimension> FetchDimensions(IDataset ds)
        {
            var list = new List<IDimension>();

            using (var conn = GetConnection())
            using (var cmd = new SqlCommand("dbo.GetDimensionAndRefsWithMembers", conn))
            {
                cmd.CommandType         = CommandType.StoredProcedure;
                cmd.CommandTimeout      = 60;
                cmd.Parameters.AddWithValue("@DatasetId", ds.Id);

                using (var reader = cmd.ExecuteReader())
                {
                    var stillHaveRows = reader.Read();

                    while (stillHaveRows)
                    {
                        var dimensionRefid  = reader.ColumnValue<int>("DIMENSION_REF_ID");

                        var id              = reader.ColumnValue<int>("DIMENSION_ID");
                        var code            = reader.ColumnValue<string>("DIMENSION_NAME_CODE");
                        var nameEn          = reader.ColumnValue<string>("DIMENSION_NAME_E");
                        var nameFr          = reader.ColumnValue<string>("DIMENSION_NAME_F");
                        var index           = reader.ColumnValue<int>("DIMENSION_DATASET_ORDER") - 1;
                        var isTimeSeries    = reader.ColumnValue<bool>("DIMENSION_REF_IS_TIME") || dimensionRefid == 4 || dimensionRefid == 6;
                        var isLocation      = dimensionRefid == 5;
                        var isFrequency     = dimensionRefid == 3;

                        var dim = SWDimension.Build(
                            ds,
                            code,
							nameEn,
                            new Dictionary<string, string>()
                            {
	                            {"en", nameEn},
	                            {"fr", nameFr}
                            },
                            isTimeSeries,
                            isLocation,
                            isFrequency,
                            id,
                            index
                        );

                        var members = LoadMembers(dim, reader, out stillHaveRows);

                        dim.ResetMembers(members);

                        list.Add(dim);
                    }
                }
            }

            return list.ToArray();
        }

        private static IEnumerable<CodelistMember> LoadMembers(SWDimension dim, IDataReader rd, out bool stillHaveRows)
        {
            stillHaveRows   = true;

            var searchList  = new Dictionary<int, CodelistMember>(100);
            var ml          = new List<CodelistMember>(100);

            var dimId       = rd.ColumnValue<int>("DIMENSION_ID");
            var refId       = rd.ColumnValue<int>("DIMENSION_REF_ID");
            var rootIndex   = 1;

            do
            {
                var memId           = rd.ColumnValue<int>("DIMENSION_MEMBER_ID", -1);

                if (memId == -1)
                    throw new InvalidOperationException($"The dimension {dim.Code} has no members and cannot be loaded");

                var code            = rd.ColumnValue<string>("DIMENSION_MEMBER_NAME_CODE");
                var englishName     = rd.ColumnValue<string>("DIMENSION_MEMBER_NAME_E");
                var frenchName      = rd.ColumnValue<string>("DIMENSION_MEMBER_NAME_F");
                var memType         = GetType(rd.ColumnValue<byte>("DIMENSION_MEMBER_TYPE"));
                var parentId        = rd.ColumnValue<int>("DIMENSION_MEMBER_PARENT_ID", -1);
                var index           = rootIndex;

                rootIndex++;

                CodelistMember parent = null;

                if (parentId > -1 && !searchList.TryGetValue(parentId, out parent))
                        throw new InvalidProgramException($"The data was given in incorrect order.\nThe parent members must always come before its children (Parent of '{memId}', {parentId})");

                var mem = CodelistMember.Build(
                                    parent, 
                                    code,
                                    englishName,
                                    new Dictionary<string, string>()
                                    {
	                                    {"en", englishName},
	                                    {"fr", frenchName}
                                    },
                                    memId,
                                    index,
                                    memType
                            );

                if (!searchList.ContainsKey(memId))
                {
                    if (parent == null)
                    {
                        ml.Add(mem);
                    }
                    else if (!parent.IsMultipleChoice)
                    {
                        parent.AddChildMember(mem);
                        ml.Add(mem);
                    }

                    searchList.Add(memId, mem);
                }

                stillHaveRows   = rd.Read();
            }
            while (stillHaveRows && rd.ColumnValue<int>("DIMENSION_REF_ID") == refId && rd.ColumnValue<int>("DIMENSION_ID") == dimId);

            ml.Capacity = ml.Count;

            return ml;
        }

        private static SWValueType GetType(byte type)
        {
            switch (type)
            {
                case 0: return SWValueType.Value;
                case 2: return SWValueType.SimpleMultipleChoice;
                case 1:
                case 3: return SWValueType.AlternativeMultipleChoice;
                case 4: return SWValueType.OrderedMultipleChoice;
                case 5: return SWValueType.Text;
                case 6: return SWValueType.Date;
            }

            throw new InvalidProgramException($"Database contains invalid membertype {type}");
        }

        private Dictionary<string, List<StatworksQuery>> LoadQueries()
        {
            var dic = new Dictionary <string, List<StatworksQuery>>();

            using (var conn = GetConnection())
            using (var cmd = new SqlCommand("dbo.GetObjectList", conn))
            {
                cmd.CommandType         = CommandType.StoredProcedure;
                cmd.CommandTimeout      = 60;
                cmd.Parameters.AddWithValue("@objectType", "Query");

                using (var reader = cmd.ExecuteReader())
                    while (reader.Read())
                    {
                        var id          = reader.ColumnValue<int>("id");
                        var name        = reader.ColumnValue<string>("name");
                        var scope       = reader.ColumnValue<string>("scope");
                        var xmlDef      = reader.ColumnValue<string>("definition");
                        var key         = string.Join(".", scope.Split('.'), 1, 2);
                        var list        = dic.GetOrAdd(key, (x)=>new List<StatworksQuery>());
                        var query       = new StatworksQuery(id,name,xmlDef);
                        list.Add(query);
                    }
            }

            return dic;
        }

        public IList<IAttribute> FetchAttributes(IDataset ds)
        {
	        return null;
        }

        public IList<KeyValuePair<string, string>> GetLanguages(IDataset ds)
        {
	        return new[]
	        {
		        new KeyValuePair<string, string>("en", "English"), 
		        new KeyValuePair<string, string>("fr", "French") 
	        };
        }

        public IMsd LoadMsd(IDataset ds)
        {
            return null;
        }

        #endregion

        #region Unused
        public IList<IControlCodeMember> FetchControlCodes(IDataset ds)
        {
            var list = new List<IControlCodeMember>();

            using (var conn = GetConnection())
            using (var cmd = new SqlCommand("dbo.spControlCodeList", conn))
            {
                cmd.CommandType         = CommandType.StoredProcedure;
                cmd.CommandTimeout      = 60;
                cmd.Parameters.AddWithValue("@dsId", ds.Id);

                using (var reader = cmd.ExecuteReader())
                    while (reader.Read())
                    {
                        var bitmask         = reader.ColumnValue<ulong>("CONTROL_CODE_BITMASK");
                        var code            = reader.ColumnValue<string>("CONTROL_CODE_NAME_CODE");
                        var nameEn          = reader.ColumnValue<string>("CONTROL_CODE_NAME_E");
                        var nameFr          = reader.ColumnValue<string>("CONTROL_CODE_NAME_F");
                        var dotStatCommon   = reader.ColumnValue<bool>("CONTROL_CODE_IS_OECDSTAT_COMMON");

                        var dim = SWControlCodeMember.Build(
                            FindHighestBitSet(bitmask),
                            dotStatCommon ? code.ToUpper() : code.ToLower(),
							nameEn,
                            new Dictionary<string, string>()
                            {
	                            {"en", nameEn},
	                            {"fr", nameFr}
                            }
                        );

                        list.Add(dim);
                    }
            }

            return list.ToArray();
        }

        static int FindHighestBitSet(ulong mask)
        {
            var res = 0;
            while ((mask >>= 1) != 0) res++;
            return res;
        }
        #endregion

        #region Get data
        public object[,] LoadData(
            OutputType outputType,
            ISdmxFilter sdmxFilter,
            bool production = true,
            LabelOutput labelOutput = LabelOutput.Code,
            string language = null,
            bool flags = false,
            bool updateDate = false,
            bool withActionColumn = false
        )
        {
            var outputCodes             = (labelOutput & LabelOutput.Code) > 0;
            var outputLabels            = labelOutput > LabelOutput.Code;

            var filter                  = sdmxFilter.SWFilter;
            var dimensions              = filter.Dataset.Dimensions.ToArray();            
            var isTime                  = filter.Dataset.HasATimeDimension;
            var nonTimeDimCount         = dimensions.Count() - (isTime ? 1 : 0);

            var readerIndex             = updateDate ? 1 : 0;
            var mapper                  = new List<Mapper>(nonTimeDimCount + 5);
            var tempData                = new List<object[]>(50000);
            
            var isTimeSeries                = false;
            var dimIndex                    = 1;
            Dictionary<string, int> sids    = null;
            var sidOrder                    = 0;
            HashSet<string> timePoints      = null;

            if (outputType == OutputType.TimeSeriesDown || outputType == OutputType.TimeSeriesAcross)
            {
                if(!isTime)
                    throw new InvalidOperationException("No time dimension");
                
                isTimeSeries    = true;
                sids            = new Dictionary<string, int>();
                timePoints      = new HashSet<string>();
            }

            // SID
            mapper.Add(new Mapper(ColumnType.Calculated, "SID", -1));

            dimensions
                .Select(dim => new Tuple<IDimension, int>(dim, readerIndex++))
                .OrderBy(t => t.Item1.TimeLastOrder())
                .ForEach(t => mapper.Add(new Mapper(ColumnType.Dimension, t.Item1.Code, t.Item2, GetLabel(t.Item1, labelOutput, language))));

            mapper.Add(new Mapper(ColumnType.Value, "Value", readerIndex++));

            if (flags)
                mapper.Add(new Mapper(ColumnType.Attribute, "Flag", readerIndex++));

            if (updateDate)
                mapper.Add(new Mapper(ColumnType.Attribute, "Update date", 0, Mapper.ToSqlDt));

            // ----------------------------------------

            try
            {
                using (var conn = GetConnection())
                using (var cmd = new SqlCommand("get.Data", conn))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.CommandTimeout = 600;
                    cmd.Parameters.AddRange(GetSqlParams(sdmxFilter, production, updateDate));

                    using (var reader = cmd.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            var data = new object[mapper.Count];

                            for (var i = 0; i < mapper.Count; i++)
                            {
                                if (mapper[i].Type == ColumnType.Calculated) continue;

                                data[i] = mapper[i].GetData(reader);
                            }

                            var sid = GetSID(data, dimIndex, nonTimeDimCount);

                            data[0] = sid;

                            tempData.Add(data);

                            if (!isTimeSeries)
                                continue;

                            if (!sids.ContainsKey(sid))
                                sids.Add(sid, sidOrder++);

                            timePoints.Add(GetTimeUid(data, dimIndex + nonTimeDimCount));
                        }
                    }
                }
            }
            catch (SqlException ex)
            {
                if (ex.IsSqlTimeoutError())
                    throw new DlmXLManagedException("Too much data were requested, please try narrowing the filter down");
                
                throw;
            }

            return Output(outputType, 
                mapper, 
                tempData, 
                dimIndex,
                nonTimeDimCount,
                outputCodes,
                outputLabels,
                sids, 
                timePoints
            );
        }

        protected override SqlParameter[] GetSqlParams(ISdmxFilter filter, bool production, bool updateDate)
        {
            var sdmxQuery   = this.GetSdmxQueryString(filter);
            var options     = new List<string>();

            if (filter.Frequency != 0)
                options.Add(filter.Frequency.ToString().Substring(0, 1));

            if (updateDate)
                options.Add("TIMESTAMP");

            return new[]
            {
                new SqlParameter("@sdmxRest", SqlDbType.NVarChar) {Value = sdmxQuery},
                new SqlParameter("@seriesDelimiter", SqlDbType.NVarChar) {Value = "$"},
                new SqlParameter("@workSpace", SqlDbType.NVarChar) {Value = production ? "PRODUCTION" : "IMPORT"},
                new SqlParameter("@options", SqlDbType.NVarChar) {Value = string.Join(";", options)}
            };
        }

        public object[,] LoadMetadata(
            ISdmxFilter sdmxFilter,
            LabelOutput labelOutput = LabelOutput.Code,
            string language = null,
            bool withActionColumn = false
        )
        {
            throw new NotImplementedException();
        }

        #endregion

        #region Statworks specific
        internal class StatworksQuery : IQuery
        {
	        public Dictionary<string, string> Names => new Dictionary<string, string>()
	        {
		        {"en", DefaultName}
	        };

            internal IDataset Dataset { get; set; }

            private string _xmlDef;
            private string _sdmx;

            internal StatworksQuery(int id, string name, string xmlDef)
            {
                Id          = id;
                DefaultName = name;
                _xmlDef     = xmlDef;
            }

            #region IQuery

            public string SdmxQuery => _sdmx ?? (_sdmx = ParseXml());
            public int Id { get; set; }
            public string DefaultName { get; set; }
            public string Uid => this.Id.ToString();
            public int Index => Id;
            public string Code => null;
            public string FrenchName => null;

            public string ToString(LabelOutput code)
            {
                return DefaultName;
            }
            public string GetLabel(string pattern, string language = null)
            {
                throw new NotImplementedException();
            }

            #endregion

            private string ParseXml()
            {
                if(Dataset == null)
                    throw new InvalidOperationException("Dataset empty");

                var filter  = new StatworksFilter(Dataset);
                var doc     = XDocument.Parse(_xmlDef);

                foreach (var xDim in doc.Root.XPathSelectElements("Filter/Dimension"))
                {
                    var dimId   = int.Parse(xDim.Attribute("Id").Value);
                    var dim     = filter.FilterDimensions.FirstOrDefault(x=>x.Dimension.Id == dimId);

                    if(dim == null)
                        continue;

                    foreach (var xMem in xDim.Elements())
                    {
                        var memId   = int.Parse(xMem.Attribute("Id").Value);
                        var mem     = dim.SelectableMembers.FirstOrDefault(x=>x.Id == memId);

                        if (mem!=null)
                            dim.AddSelectedMember(mem);
                    }
                }

                return filter.GetSdmxQueryString();
            }
        }

        internal class StatworksFilter : SWFilter
        {
            public StatworksFilter(IDataset dataset) : base(dataset)
            {}

            public override string GetSdmxQueryString()
            {
                var time    = FilterDimensions.FirstOrDefault(x => x.Dimension.IsTimeSeries);

                if (time == null || !time.SelectedMembers.Any())
                    return base.GetSdmxQueryString();

                var sdmx = new StringBuilder(base.GetSdmxQueryString() + "/all")
                    .Append("?startTime=").Append(time.SelectedMembers.First().Code)
                    .Append("&endTime=").Append(time.SelectedMembers.Last().Code);

                return sdmx.ToString();
            }
        }

        #endregion
    }
}
