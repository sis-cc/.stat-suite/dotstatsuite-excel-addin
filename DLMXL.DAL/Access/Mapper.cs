using System;
using System.Data;

namespace DLMXL.DAL.Access
{
    public enum ColumnType
    {
        Dimension,
        Value,
        Attribute,
        Calculated
    }

	public class Mapper
    {
        public string Header { get;}
        public ColumnType Type { get;}

        private readonly int _index;
        private readonly Func<object, object> _transform;


        public Mapper(ColumnType type, string header, int index, Func<object, string> transform = null)
        {
            Header          = header;
            _index          = index;
            Type            = type;
            _transform      = transform;
        }

        public object GetData(IDataReader reader)
        {
            var obj = reader[_index];

            return _transform == null || Type == ColumnType.Dimension
                ? obj
                : Transform(obj);
        }

        public object Transform(object obj)
        {
            return _transform(obj);
        }

        public static string ToSqlDt(object o)
        {
            if (IsNull(o))
                return null;

            var dt = Convert.ToDateTime(o);

            return dt.ToString("yyyy-MM-dd HH:mm:ss");
        }

        public static bool IsNull(object o)
        {
            return o == null || o is DBNull;
        }
    }
}