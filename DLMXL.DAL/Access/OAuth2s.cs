﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Security.Cryptography;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using DLMXL.Models.Interfaces;
using JWT;
using JWT.Algorithms;
using JWT.Builder;

namespace DLMXL.DAL.Access
{
    public sealed class OAuth : IAuth
    {
        public static string RedirectUri { get; private set; } = "https://dotstat-suite-addin.oecd.org";

        public static void Init(string redirectUri)
        {
            if (!string.IsNullOrEmpty(redirectUri))
            {
                RedirectUri = redirectUri;
            }
        }

        const string code_challenge_method = "S256";

        private static int cnt;
	    private static Lazy<HttpClient> Client = new Lazy<HttpClient>(() => new HttpClient(new HttpClientHandler() { AutomaticDecompression = DecompressionMethods.GZip | DecompressionMethods.Deflate }));
	    private static Dictionary<string, OAuth> _authDict = new Dictionary<string, OAuth>();

	    public readonly string Id;
	    private TokenResponse tokens;
	    private IUserinfo userinfo;

        private Lazy<ConfigResponse> config;
        private string clientID;
        private string clientSecret;
        private string baseUri;
        private string scope;

        private string state;
        private string code_verifier;
        private string code_challenge;

        public string AccessToken => tokens?.AccessToken;
        public string Userinfo => userinfo?.Summary;

        public static OAuth GetOAuth(
            string baseUri, 
            string clientID,
            string scope,
            string clientSecret=null
        )
        {
	        OAuth auth;

	        if (_authDict.TryGetValue(baseUri, out auth))
		        return auth;

	        return _authDict[baseUri] = new OAuth(Interlocked.Increment(ref cnt), baseUri, clientID, scope, clientSecret);
        }

        private OAuth(
            int id, 
            string baseUri, 
            string clientID,
            string scope,
            string clientSecret
        )
        {
            Id = "auth" + id;
            this.baseUri = baseUri.TrimEnd('/') + '/';
            this.clientID = clientID;
            this.scope = string.IsNullOrEmpty(scope) ? "openid profile email" : scope;
            this.clientSecret = clientSecret;
            
            this.state = RandomDataBase64Url(32);
            this.code_verifier = RandomDataBase64Url(32);
            this.code_challenge = Base64UrlencodeNoPadding(Sha256(code_verifier));

            this.config = new Lazy<ConfigResponse>(() => Get<ConfigResponse>(this.baseUri + ".well-known/openid-configuration", false).GetAwaiter().GetResult());
        }

        /*
        public void Authorize(Action onComplete, Action<Exception> onError, CancellationToken token) {

	        Authenticate().ContinueWith(t => {
	                if (t.Exception != null)
	                {
		                onError(t.Exception);
	                }
	                else
	                {
						this.SetTokens(t.Result);
						this.userinfo = UserInfo().Result;
						onComplete();
	                }
	            }
			);
        }
        */

        public async Task Authorize(NameValueCollection authQueryString)
        {
            SetTokens(await CodeExchange(authQueryString).ConfigureAwait(false));
            this.userinfo = await UserInfo();
        }

        public string GetAuthorizationUrl()
        {
            return string.Format("{0}?response_type=code&redirect_uri={1}&client_id={2}&state={3}&code_challenge={4}&code_challenge_method={5}&scope={6}",
                config.Value.AuthEndpoint,
                System.Uri.EscapeDataString(RedirectUri),
                clientID,
                state,
                code_challenge,
                code_challenge_method,
                scope
            );
        }

        async Task<TokenResponse> CodeExchange(NameValueCollection authQueryString)
        {
            var error = authQueryString["error"];
            var code = authQueryString["code"];
            var incomingState = authQueryString["state"];
            
            // Checks for errors.
            if (!string.IsNullOrEmpty(error))
            {
                throw new Exception($"OAuth authorization error: {error}.");
            }

            if (string.IsNullOrEmpty(code) || string.IsNullOrEmpty(incomingState))
            {
                throw new Exception("Malformed authorization response.");
            }

            // Compares the received state to the expected value, to ensure that
            // this app made the request which resulted in authorization.
            if (incomingState != state)
            {
                throw new Exception($"Received request with invalid state ({incomingState})");
            }

            return await Post<TokenResponse>(config.Value.TokenEndpoint, new []
	        {
				new KeyValuePair<string, string>("grant_type", "authorization_code"),
				new KeyValuePair<string, string>("code", code),
				new KeyValuePair<string, string>("code_verifier", code_verifier),
				new KeyValuePair<string, string>("client_id", this.clientID),
				new KeyValuePair<string, string>("redirect_uri", RedirectUri)
	        })
            .ConfigureAwait(false);
        }

        async Task<TokenResponse> RefreshTokens()
        {
            if (tokens == null || tokens.AccessExpire <= 0)
                return null;

	        await Task.Delay((tokens.AccessExpire - 20) * 1000);

            if (tokens == null)
                return null;

            try
            {
                return await Post<TokenResponse>(config.Value.TokenEndpoint, new[]
                    {
                        new KeyValuePair<string, string>("grant_type", "refresh_token"),
                        new KeyValuePair<string, string>("client_id", this.clientID),
                        new KeyValuePair<string, string>("refresh_token", tokens.RefreshToken)
                    },
                    false
                );
            }
            catch
            {
                return null;
            }
        }

        async Task<IUserinfo> UserInfo()
        {
            var certJson = await Client.Value.GetStringAsync(config.Value.JwksEndpoint);
            var key = JsonConvert.DeserializeObject<dynamic>(certJson).keys[0];
            return Decode((string) key.x5c[0], AccessToken);

            //return await Get<UserinfoResponse>(config.Value.UserinfoEndpoint, true);
        }

        private async void SetTokens(TokenResponse tokens)
        {
	        this.tokens = tokens;

	        if (tokens != null)
	        {
		        SetTokens(await RefreshTokens());
	        }
        }

        public void Logout()
        {
	        var result = Post<TokenResponse>(config.Value.LogoutEndpoint, new[]
	        {
		        new KeyValuePair<string, string>("client_id", this.clientID),
		        new KeyValuePair<string, string>("refresh_token", this.tokens.RefreshToken)
	        });

			this.SetTokens(null);
        }

        private async Task<T> Get<T>(string url, bool withAuth) where T:class
        {
	        using (var requestMessage = GetMessage(HttpMethod.Get, url))
	        {
		        var response    = await Client.Value.SendAsync(requestMessage).ConfigureAwait(false);
		        return await response.Content.ReadAsAsync<T>().ConfigureAwait(false);
	        }
        }

        private async Task<T> Post<T>(string url, IEnumerable<KeyValuePair<string, string>> @params, bool withAuth=true) where T:class
        {
	        using (var requestMessage = GetMessage(HttpMethod.Post, url))
	        {
		        requestMessage.Content = new FormUrlEncodedContent(@params);
		        var response = await Client.Value.SendAsync(requestMessage).ConfigureAwait(false);
                return await response.Content.ReadAsAsync<T>().ConfigureAwait(false);
	        }
        }

        private HttpRequestMessage GetMessage(HttpMethod method, string url, bool withAuth = true)
        {
	        var message = new HttpRequestMessage(method, url);

	        if (withAuth)
	        {
		        message.Headers.Authorization = new AuthenticationHeaderValue("Bearer", AccessToken);
	        }

	        if (RedirectUri != null)
	        {
				message.Headers.Add("Origin", RedirectUri);
	        }

			message.Headers.Add("Accept", "application/json");

	        return message;
        }

        /// <summary>
        /// Returns URI-safe data with a given input length.
        /// </summary>
        /// <param name="length">Input length (nb. output will be longer)</param>
        /// <returns></returns>
        private static string RandomDataBase64Url(uint length) {
            RNGCryptoServiceProvider rng = new RNGCryptoServiceProvider();
            byte[] bytes = new byte[length];
            rng.GetBytes(bytes);
            return Base64UrlencodeNoPadding(bytes);
        }

        /// <summary>
        /// Returns the SHA256 hash of the input string.
        /// </summary>
        /// <param name="inputStirng"></param>
        /// <returns></returns>
        private static byte[] Sha256(string inputStirng) {
            byte[] bytes = Encoding.ASCII.GetBytes(inputStirng);
            SHA256Managed sha256 = new SHA256Managed();
            return sha256.ComputeHash(bytes);
        }

        /// <summary>
        /// Base64url no-padding encodes the given input buffer.
        /// </summary>
        /// <param name="buffer"></param>
        /// <returns></returns>
        private static string Base64UrlencodeNoPadding(byte[] buffer) {
            string base64 = Convert.ToBase64String(buffer);

            // Converts base64 to base64url.
            base64 = base64.Replace("+", "-");
            base64 = base64.Replace("/", "_");
            // Strips padding.
            base64 = base64.Replace("=", "");

            return base64;
        }

        public sealed class ConfigResponse
        {
	        [JsonProperty("authorization_endpoint")]
			public string AuthEndpoint { get; set; }

			[JsonProperty("token_endpoint")]
	        public string TokenEndpoint { get; set; }

	        [JsonProperty("userinfo_endpoint")]
	        public string UserinfoEndpoint { get; set; }

	        [JsonProperty("end_session_endpoint")]
	        public string LogoutEndpoint { get; set; }

            [JsonProperty("jwks_uri")]
            public string JwksEndpoint { get; set; }
        }

        public sealed class TokenResponse
        {
	        [JsonProperty("access_token")]
			public string AccessToken { get; set; }

			[JsonProperty("expires_in")]
			public int AccessExpire { get; set; }

			[JsonProperty("refresh_token")]
			public string RefreshToken { get; set; }

			[JsonProperty("refresh_expires_in")]
			public int RefreshExpire { get; set; }

            [JsonProperty("refresh_token_expires_in")]
            public int RefreshExpire2 { set { RefreshExpire = value; } }
        }

        public interface IUserinfo
        {
            string Summary { get; }
        }

        public sealed class UserinfoResponse : IUserinfo
        {
	        [JsonProperty("name")]
	        public string Name { get; set; }

	        [JsonProperty("email")]
	        public string Email { get; set; }

	        public string Summary => $"{Name} ({Email})";
        }

        public sealed class TokenUserinfo : IUserinfo
        {
            [JsonProperty("given_name")]
            public string Firstname { get; set; }

            [JsonProperty("family_name")]
            public string Lastname { get; set; }

            [JsonProperty("email")]
            public string Email { get; set; }

            public string Summary => $"{Firstname} {Lastname} ({Email})";
        }

        #region JWT decoding
        public static IUserinfo Decode(string certificate, string token)
        {
            var cert = new X509Certificate2(Convert.FromBase64String(certificate));

            return JwtBuilder.Create()
                .WithAlgorithm(new RS256Algorithm(cert))
                .WithValidationParameters(ValidationParameters.None)
                .Decode<TokenUserinfo>(token);
        }
        #endregion
    }
}
