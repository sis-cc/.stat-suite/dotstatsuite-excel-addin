﻿using System;
using System.IO;
using System.Linq;
using System.Xml.Linq;
using Org.Sdmxsource.Sdmx.Api.Constants;

namespace DLMXL.DAL.Access.Sdmx
{
    public sealed class DotStatV7Sdmx : RestfulApi
    {
        public override bool IsQueryWithFullId => false;

        public DotStatV7Sdmx(string code, string baseUri) : base(code, baseUri, "GetDataStructure/all", "GetDataStructure/{0}", "GetData/{0}?format=compact_v2", SdmxSchemaEnumType.VersionTwo)
        {}

        #region Transformation

        protected override void TransformDataflow(Stream input, string outputFile)
        {
            var xDocument = XDocument.Load(input);

            var structure = xDocument.Element((XNamespace)SdmxConstants.MessageNs20 + "Structure");

            if (structure == null)
                throw new FormatException("Structure node is missing");

            // Some codes in codelist contain spaces
            var dataflows = structure.Element((XNamespace)SdmxConstants.MessageNs20 + "KeyFamilies");

            if(dataflows==null)
                throw new FormatException("KeyFamilies node is missing");

            dataflows.Name = (XNamespace)SdmxConstants.MessageNs20 + "Dataflows";

            var ns = (XNamespace) SdmxConstants.StructureNs20;

            foreach (var dataflow in dataflows.Elements(ns + "KeyFamily"))
            {
                dataflow.Name = ns + "Dataflow";

                var @ref = new XElement(ns + "KeyFamilyRef",
                        new XElement(ns + "KeyFamilyID", dataflow.Attribute("id")?.Value),
                        new XElement(ns + "KeyFamilyAgencyID", dataflow.Attribute("agencyID")?.Value)
                    );

                dataflow.Add(@ref);
            }

            xDocument.Save(outputFile);
        }

        protected override void TransformStructure(Stream input, string outputFile)
        {
            var xDocument = XDocument.Load(input);

            var structure = xDocument.Element((XNamespace)SdmxConstants.MessageNs20 + "Structure");

            if (structure == null)
                throw new FormatException("Structure node is missing");

            // Some codes in codelist are wrong (like '+' or '‡')
            var codeLists = structure.Elements((XNamespace)SdmxConstants.MessageNs20 + "CodeLists");

            foreach (var codeList in codeLists.Elements((XNamespace)SdmxConstants.StructureNs20 + "CodeList"))
            {
                var codes = codeList.Elements((XNamespace)SdmxConstants.StructureNs20 + "Code").ToArray();

                for (var i = 0; i < codes.Length; i++)
                {
                    var valueAttribute = codes[i].Attribute("value");

                    if (valueAttribute == null || CodePattern.IsMatch(valueAttribute.Value))
                        codes[i].Remove();
                }
            }

            xDocument.Save(outputFile);
        }

        protected override void TransformData(Stream input, string outputFile)
        {
            var xDocument = XDocument.Load(input);

            var structure = xDocument.Element((XNamespace)SdmxConstants.MessageNs20 + "CompactData");

            if (structure == null)
                throw new FormatException("Invalid datastructure xml");
            
            var dataset = structure.Element((XNamespace)"http://oecd.stat.org/Data" + "DataSet");

            dataset?.Attributes("keyFamilyURI").Remove();

            xDocument.Save(outputFile);
        }

        #endregion
    }
}
