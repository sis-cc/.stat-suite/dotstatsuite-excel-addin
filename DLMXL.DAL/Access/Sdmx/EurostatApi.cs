﻿using System;
using System.IO;
using System.Linq;
using System.Net;
using System.Xml;
using System.Xml.Linq;
using System.Xml.XPath;
using DLMXL.Models.Exceptions;
using Ionic.Zip;
using Org.Sdmxsource.Sdmx.Api.Util;

namespace DLMXL.DAL.Access.Sdmx
{
    public class EurostatApi : RestfulApi
    {
        private const string AsyncMessage           = "Eurostat prepares data for your query. Please press OK button and repeat your query again in 30 seconds.";
        private const string RetryAgainMessage      = "Datawizard wasn't able to complete your query. Please press OK button and change your filter parameters.\r\n\r\nEurostat original message:\r\n\r\n{0}";
        private const string NoResults              = "There is no data matched your criteria. Please press OK button and change your filter parameters";
        
        public override bool IsQueryWithFullId => false;
        public override bool IsQueryWithAllAsEmpty => false;

        public EurostatApi(string code, string baseUri) : base(code, baseUri, "dataflow/ESTAT/all/latest", "datastructure/ESTAT/{0}/?references=Children", "data/{0}")
        {}

        protected override IReadableDataLocation GetDataLocation(Func<Stream> getStream, string cacheKey, bool isData, Action<Stream, string> transformContent)
        {
            var tmpFile     = string.Empty;
            var asyncUrl    = string.Empty;
            
            if (!isData)
            {
                tmpFile     = this.GetTempFile(false, cacheKey);
            }
            else if(_contentCache.TryGetValue(cacheKey, out tmpFile))
            {
                asyncUrl    = tmpFile.StartsWith("http") ? tmpFile : null;
            }
            
            // =====================================================

            if (isData && !string.IsNullOrEmpty(asyncUrl) && !TryDownloadFile(2, asyncUrl, tmpFile = GetTempFile(true)))
                throw new DlmXlResumeActionException(AsyncMessage);

            if (!TempFileExists(tmpFile))
            {
                long size = 0;

                using (var stream = getStream())
                {
                    transformContent(stream, tmpFile = this.GetTempFile(isData, cacheKey));
                    size = (new FileInfo(tmpFile)).Length;
                }

                if (isData && size < 2000 && CheckIfAsyncData(tmpFile, out asyncUrl))
                {
                    File.Delete(tmpFile);
                    _contentCache[cacheKey] = asyncUrl;
                    throw new DlmXlResumeActionException(AsyncMessage);
                }
            }

            if(isData)
                _contentCache[cacheKey] = tmpFile;

            return _dataLocationFactory.GetReadableDataLocation(new FileStream(tmpFile, FileMode.Open));
        }

        private static bool CheckIfAsyncData(string tmpFile, out string asyncUrl)
        {
            asyncUrl                    = null;
            var doc                     = XDocument.Load(tmpFile);

            if(doc.Root == null)
                throw new DlmXLManagedException("Corrupted eurostat response");

            var namespaceManager        = new XmlNamespaceManager(new NameTable());
            namespaceManager.AddNamespace("f", "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/message/footer");
            namespaceManager.AddNamespace("c", "http://www.sdmx.org/resources/sdmxml/schemas/v2_1/common");
            var messages                = doc.Root.XPathSelectElements("f:Footer/f:Message/c:Text", namespaceManager).Select(x=>x.Value).ToArray();
            var firstLine               = messages.FirstOrDefault();

            if (firstLine == null)
                return false;

            if (firstLine.StartsWith("Due to the large query") && (asyncUrl = messages[1]).StartsWith("http"))
                return true;

            if(firstLine.StartsWith("No Results Found"))
                throw new DlmXlResumeActionException(NoResults);

            throw new DlmXlResumeActionException(RetryAgainMessage, string.Join(Environment.NewLine, messages));
        }

        private static bool TryDownloadFile(int trials, string uri, string tmpFile)
        {
            var num = 0;

            do
            {
                try
                {
                    using (var wc = new RestClient("*/*"))
                    using (var stream = wc.OpenRead(uri))
                    using (var mem = new MemoryStream())
                    {
                        stream.CopyTo(mem);
                        mem.Position = 0;

                        if(mem.Length < 2000)
                            throw new DlmXlResumeActionException(RetryAgainMessage, "Response is too large due to client request. Query size exceeds maximum limit (1000000 entries).");

                        using (var zip = ZipFile.Read(mem))
                        using (var entry = zip.Entries.First().OpenReader())
                        using (var fs = new FileStream(tmpFile, FileMode.Create))
                            entry.CopyTo(fs);
                    }

                    return true;
                }
                catch (WebException e)
                {
                    if (e.Status != WebExceptionStatus.ProtocolError || (e.Response as HttpWebResponse)?.StatusCode != HttpStatusCode.NotFound)
                        throw;

                    System.Threading.Thread.Sleep(TimeSpan.FromSeconds(10));
                }
            }
            while (++num < trials);

            return false;
        }
    }
}
