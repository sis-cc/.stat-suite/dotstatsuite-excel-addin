﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text.RegularExpressions;
using DLMXL.Models;
using DLMXL.Models.Interfaces;
using Org.Sdmxsource.Sdmx.Api.Constants;
using Org.Sdmxsource.Sdmx.Api.Model.Objects;

namespace DLMXL.DAL.Access.Sdmx
{
    public class GenericRestApi : RestfulApi
    {
        private static readonly Regex TransferResponseRegex = new Regex(@"The request with ID (\d+) was successfully registered", RegexOptions.Compiled | RegexOptions.IgnoreCase);
		
        public string TransferUri { get; }
        public string SaveDataspace { get; }
        public OAuth OAuth { get; }

        public override bool IsExternal { get; }
        public override bool IsSaveWithAttributesOnly { get; }
		public override bool IsMetadata{ get; }
        public override bool IsHistoricVersion { get; }
		public override bool IsSaveAllowed => !string.IsNullOrEmpty(TransferUri) && !string.IsNullOrEmpty(SaveDataspace);
        public override bool IsAuthRequired => OAuth != null;
        public override bool IsAuthenticated => OAuth?.AccessToken != null;
        public override string Userinfo => OAuth?.Userinfo;
        public override string AuthId => OAuth?.Id;

        public GenericRestApi(
            string code,
            string baseUri,
            string flowQuery,
            string structureQuery,
            string dataQuery,
            bool isExternal = true,
			bool isMetadata = false,
			bool isHistoricVersion = false,
			string transferUri = null,
            string saveDataspace = null,
            string authUri = null,
			string authClientId = null,
			string authScope = null,
			bool isSaveWithAttributesOnly = false
		)
        : base(code, baseUri, flowQuery, structureQuery, dataQuery, SdmxSchemaEnumType.VersionTwoPointOne)
        {
            this.IsExternal = isExternal;
            this.IsMetadata = isMetadata;
            this.IsHistoricVersion = isHistoricVersion;

			this.IsSaveWithAttributesOnly = isSaveWithAttributesOnly;

			this.TransferUri = transferUri?.TrimEnd('/');
            this.SaveDataspace = saveDataspace;

            if (Uri.IsWellFormedUriString(authUri, UriKind.Absolute))
            {
	            this.OAuth = OAuth.GetOAuth(authUri, authClientId, authScope);
            }
        }

		#region Auth

		protected override void SetAuth(HttpWebRequest request)
		{
			if (!IsAuthRequired)
				return;

			request.Headers.Add(HttpRequestHeader.Authorization, "Bearer " + OAuth.AccessToken);
		}

		public override void LogOut()
		{
            OAuth.Logout();
		}

        public override IAuth Auth => OAuth;

		#endregion

		#region Transfer service
		public override Tuple<string, int> Save(
			IDataset dataset,
			ICellAccess source,
			OutputType outputType,
			bool withCodes,
			bool withLabels,
			bool withFlags,
			bool isMetadata,
			bool withActionColumn,
			SaveInterruptHander dialogInterrupt
		)
		{

			string csvFilename = null;

			try
			{
				string transactionId = null;
				int savedObsCount;
				var data = ObservationReader(dataset, source, outputType, withCodes, withLabels, withFlags, isMetadata, withActionColumn, dialogInterrupt);

				csvFilename = isMetadata
								? SaveMetaCsv(dataset, data, withActionColumn, out savedObsCount)
								: SaveCsv(dataset, data, withFlags, withActionColumn, out savedObsCount);

				using (var response = FormUpload.Post(
					this.TransferUri + "/import/sdmxFile",
					SetAuth,
					new KeyValuePair<string, object>("dataspace", this.SaveDataspace),
					new KeyValuePair<string, object>("dataflow", dataset.DfId.ToString()),
					new KeyValuePair<string, object>("lang", "en"),
					new KeyValuePair<string, object>("file", new FileInfo(csvFilename))
				))
				{
					if (response.StatusCode == HttpStatusCode.OK)
					{
						using (var r = new StreamReader(response.GetResponseStream() ?? throw new InvalidOperationException()))
						{
							var m = TransferResponseRegex.Match(r.ReadToEnd());

							if (m.Success)
							{
								transactionId = m.Groups[1].Value;
							}
						}
					}
				}

				// Clear data cache
				_contentCache.Clear();

				return new Tuple<string, int>(transactionId, savedObsCount);
			}
			finally
			{
				if (!string.IsNullOrEmpty(csvFilename) && File.Exists(csvFilename))
					File.Delete(csvFilename);
			}
		}

        protected override List<ImportSummary> GetImportSummary(string dataflowId)
        {
            return TransferServiceClient.GetSummary(this, new DateTime(2020, 1, 1), dataflowId)
                .Where(x => x.Outcome != "Error")
                .ToList();
        }

        #endregion

        #region Query Dialog

        protected override string GetDataParams(ISdmxFilter filter, bool production, bool updateDate)
        {
			return this.BaseUri + GetDataQuery(filter, new List<KeyValuePair<string, string>>()
            {
				new KeyValuePair<string, string>("dimensionAtObservation","AllDimensions"),
				new KeyValuePair<string, string>("format","csvfile"),
            });
		}

        protected override string GetRString(string spParams)
        {
            return
$@"url=""{spParams}""
df<-read.csv(url)
";
        }

		protected override string GetStataString(string spParams)
		{
			return
$@"copy ""{spParams}"" ""<destination>"", replace
import delimited ""<destination>""
";
		}

		protected override string GetPythonString(string spParams)
        {
            return
$@"import pandas as pd
url=""{spParams}""
df = pd.read_csv(url)
";
        }

		#endregion

		#region Dynamic availability

        protected override ISdmxObjects GetDateDynamicAvailabilityStructure(ISdmxFilter filter, IDimension dimension)
        {
			var uri = "availableconstraint/" + filter.SdmxQuery + "/ALL/" + dimension.Code;

            try
            {
                using (var sdmxStream = Get(uri, "application/xml"))
                using (var sourceData = _dataLocationFactory.GetReadableDataLocation(sdmxStream))
                {
                    var workspace = _structureParsingManager.ParseStructures(sourceData);
                    return workspace.GetStructureObjects(false);
                }
            }
            catch
            {
                return null;
            }
		}

		protected override DateTime? GetDateDynamicAvailability(ISdmxFilter filter)
        {
            var timeDimension = filter.SWFilter.Dataset.Dimensions.First(x => x.IsTimeSeries);
			var sdmxObjects = GetDateDynamicAvailabilityStructure(filter, timeDimension);

            if (sdmxObjects != null)
            {
                var actualConstraint = sdmxObjects.ContentConstraintObjects.FirstOrDefault(c => c.IsDefiningActualDataPresent);

                if (actualConstraint != null)
                {
                    var constraintResolver = new SdmxConstraintResolver(actualConstraint);
                    var constraint = constraintResolver[timeDimension.Code];

                    if (constraint?.TimeRange != null)
                    {
                        return constraint.TimeRange.EndDate.Date;
                    }
                }
			}

            return null;
        }

		#endregion
	}
}