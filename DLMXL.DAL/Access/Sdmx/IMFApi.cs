﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Xml;
using System.Xml.Linq;
using System.Xml.XPath;
using DLMXL.Models.Interfaces;
using Org.Sdmxsource.Sdmx.Api.Constants;

namespace DLMXL.DAL.Access.Sdmx
{
    // ReSharper disable once InconsistentNaming
    public class IMFApi : RestfulApi
    {
        public override  bool IsQueryWithFullId => false;

        private readonly XmlNamespaceManager _nsResolver;

        public IMFApi(string code, string baseUri) : base(code, baseUri, "SDMX_XML.svc/Dataflow", "SDMX_XML.svc/DataStructure/{0}", "SDMX_XML.svc/CompactData/{0}", SdmxSchemaEnumType.VersionTwo)
        {
            _nsResolver = new XmlNamespaceManager(new NameTable());
            _nsResolver.AddNamespace("message", SdmxConstants.MessageNs20);
            _nsResolver.AddNamespace("structure", SdmxConstants.StructureNs20);
        }
        
        #region Transform

        protected override void TransformDataflow(Stream input, string outputFile)
        {
            var xDocument = XDocument.Load(input);

            var structure = xDocument.Element((XNamespace)SdmxConstants.MessageNs20 + "Structure");

            if (structure == null)
                throw new FormatException("Invalid datastructure xml");

            // Remove prefix "DS-" that is not expected when querying structure

            var dataflows = structure.XPathSelectElements("message:Dataflows/structure:Dataflow", _nsResolver);

            foreach (var dataflow in dataflows)
            {
                var idAttribute = dataflow.Attribute("id");

                if (idAttribute != null)
                {
                    idAttribute.Value = idAttribute.Value.Substring(3);
                }
            }

            xDocument.Save(outputFile);
        }

        protected override void TransformStructure(Stream input, string outputFile)
        {
            var xDocument = XDocument.Load(input);

            var structure = xDocument.Element((XNamespace)SdmxConstants.MessageNs20 + "Structure");

            if (structure == null)
                throw new FormatException("Invalid datastructure xml");

            // Clean SDMX xml markup to be valid for SDMX parser

            var codeLists = structure.XPathSelectElements("message:CodeLists/structure:CodeList", _nsResolver);

            foreach (var codeList in codeLists)
            {
                var idAttribute = codeList.Attribute("id");

                if (idAttribute != null)
                {
                    idAttribute.Value = CodePattern.Replace(idAttribute.Value, "_");
                }

                foreach (var code in codeList.Elements((XNamespace)SdmxConstants.StructureNs20 + "Code"))
                {
                    var valAttribute = code.Attribute("value");

                    if (valAttribute != null)
                    {
                        valAttribute.Value = CodePattern.Replace(valAttribute.Value, "_");
                    }
                }
            }

            var dimensions = structure.XPathSelectElements("message:KeyFamilies/structure:KeyFamily/structure:Components/structure:Dimension", _nsResolver);

            foreach (var dim in dimensions)
            {
                var clAttribute = dim.Attribute("codelist");

                if (clAttribute != null)
                {
                    clAttribute.Value = CodePattern.Replace(clAttribute.Value, "_");
                }
            }

            xDocument.Save(outputFile);
        }

        #endregion
    }
}
