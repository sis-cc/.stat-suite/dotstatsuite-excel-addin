﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Xml;
using DLMXL.Models;
using DLMXL.Models.Exceptions;
using DLMXL.Models.Interfaces;
using Org.Sdmxsource.Sdmx.Api.Constants;
using Org.Sdmxsource.Sdmx.Api.Exception;
using Org.Sdmxsource.Sdmx.Api.Util;
using Org.Sdmxsource.Util.Io;

namespace DLMXL.DAL.Access.Sdmx
{
    public abstract class RestfulApi : SdmxApi
    {
        protected override string StartTimeParam => "startPeriod";
        protected override string EndTimeParam => "endPeriod";

        protected RestfulApi(
            string code,
            string baseUri,
            string flowQuery,
            string structureQuery,
            string dataQuery,
            SdmxSchemaEnumType sdmxVersion = SdmxSchemaEnumType.VersionTwoPointOne
        )
            : base(code, baseUri, flowQuery, structureQuery, dataQuery, sdmxVersion)
        {
        }


        #region SdmxApi

        public override string GetSdmxQueryString(ISdmxFilter filter, List<KeyValuePair<string, string>> @params = null)
        {
            return filter.SdmxQuery.Trim('/') + "/" + BuildQueryParams(filter, @params);
        }

        protected override string BuildQueryParams(ISdmxFilter filter, List<KeyValuePair<string, string>> @params = null)
        {
            if (@params == null)
                @params = new List<KeyValuePair<string, string>>();

            if (filter.HistoricVersionType == HistoricVersionType.UpdatedAfter)
            {
                @params.AddItem("updatedAfter", $"{filter.UpdatedAfter:yyyy-MM-ddTHH:mm:ss}");
            }
            else if (filter.HistoricVersionType == HistoricVersionType.IncludeHistory)
            {
                @params.AddItem("includeHistory", "true");

                if (filter.UpdatedAfter.HasValue)
                {
                    @params.AddItem("updatedAfter", $"{filter.UpdatedAfter:yyyy-MM-ddTHH:mm:ss}");
                }
            }
            else if (filter.HistoricVersionType == HistoricVersionType.AsOf)
            {
                @params.AddItem("asOf", $"{filter.UpdatedAfter:yyyy-MM-ddTHH:mm:ss}");
            }

            return base.BuildQueryParams(filter, @params);
        }

        protected override Stream GetDataflows()
        {
            return Get(FlowQuery);
        }

        protected override Stream GetStructures(SdmxId sdmxId)
        {
            return Get(string.Format(StructureQuery, sdmxId.Id, sdmxId.Agency, sdmxId.Version));
        }

        protected override Stream GetData(ISdmxFilter filter)
        {
            var sdmxFormat = filter.HistoricVersionType != null
                ? "application/vnd.sdmx.data+csv;version=2.0"
                : "application/vnd.sdmx.structurespecificdata+xml;version=2.1";

            return Get(GetDataQuery(filter), sdmxFormat);
        }

        protected override string GetDataQuery(ISdmxFilter filter, List<KeyValuePair<string, string>> @params = null)
        {
            var query       = DataQuery.Split('?');
            @params = @params ?? new List<KeyValuePair<string, string>>();

            if (filter.Frequency != Frequency.All && filter.LastPeriods.HasValue)
            {
                var lastAvailableDate = GetDateDynamicAvailability(filter);

                if (lastAvailableDate.HasValue)
                {
                    var startPeriod = AdjustDate(filter.Frequency, lastAvailableDate.Value, filter.LastPeriods.Value);

                    @params.AddItem(StartTimeParam, startPeriod.Value);
                }
            }

            if (query.Length >= 2)
            {
                @params.AddRange(query[1].Split('&')
                    .Select(x => x.Split('='))
                    .Where(a => a.Length == 2)
                    .Select(a => new KeyValuePair<string, string>(a[0], a[1])));
            }

            return string.Format(query[0], this.GetSdmxQueryString(filter, @params));
        }

        protected override Stream GetMsd(SdmxId msd)
        {
            return Get($"metadatastructure/{msd.Agency}/{msd.Id}/{msd.Version}");
        }

        protected override Stream GetMetaData(ISdmxFilter filter)
        {
            return Get(GetMetaDataQueryString(filter), "application/vnd.sdmx.data+csv;version=2.0");
        }

        public override string GetMetaDataQueryString(ISdmxFilter filter)
        {
            var @params = new List<KeyValuePair<string, string>>()
            {
                new KeyValuePair<string, string>("attributes", "msd"),
                new KeyValuePair<string, string>("measures", "none")
            };

            //if (!string.IsNullOrEmpty(StartTimeParam) && !string.IsNullOrEmpty(filter.StartTime))
            //    @params.AddItem(StartTimeParam, filter.StartTime);

            //if (!string.IsNullOrEmpty(EndTimeParam) && !string.IsNullOrEmpty(filter.EndTime))
            //    @params.AddItem(EndTimeParam, filter.EndTime);

            var dfId = filter.SWFilter.Dataset.DfId;
            var sdmxQuery = filter.SWFilter.GetSdmxMetadataQueryString();
            var qs = string.Join("&", @params.Select(pair => pair.Key + "=" + pair.Value));

            return $"v2/data/dataflow/{dfId.Agency}/{dfId.Id}/{dfId.Version}/{sdmxQuery}/?{qs}";
        }
        #endregion

        #region Rest

        protected Stream Get(string url, string accept = "application/xml", int trials = 3)
        {
            url                             = BaseUri + url;
            var uri                         = url.IndexOf('.') > -1 ? UriWithDots(url) : new Uri(url);
            var cnt                         = 0;
            var timeout                     = 1;
            Exception ex                    = null;

            while (++cnt < trials)
            {
                try
                {
                    using (var client = new RestClient(accept, timeout, SetAuth))
                        return client.OpenRead(uri);
                }
                catch (WebException e)
                {
                    ex = e;
                    HttpStatusCode httpCode = 0;

                    // Try to parse error returned as Sdmx error message 

                    try
                    {
                        var httpResponse = e.Response as HttpWebResponse;
                        httpCode         = httpResponse.StatusCode;
                        
                        ParseSdmxErrorMessage(new ReadableDataLocationTmp(e.Response.GetResponseStream()));
                    }
                    catch (SdmxException sdmxException)
                    {
                        throw new DlmXlResumeActionException(sdmxException.Message);
                    }
                    catch
                    {
                        // not valid sdmx exception
                    }
                    
                    // If error was not valid Sdmx, try to guess by Http status codes

                    if (e.Status == WebExceptionStatus.Timeout)
                    {
                        timeout++;
                    }
                    else if (httpCode == HttpStatusCode.NotFound)
                    {
                        throw new DlmXLNoData("No data available for this selection");
                    }
                    else if (httpCode == HttpStatusCode.Unauthorized)
                    {
                        throw new DlmXlResumeActionException("Unauthorized request");
                    }
                    else
                    {
                        System.Threading.Thread.Sleep(TimeSpan.FromSeconds(5));
                    }

                    // --------------------------------------------------
                }
            }

            throw new DlmXlResumeActionException(ex?.Message);
        }

        private Uri UriWithDots(string url)
        {
            var uri = new Uri(url);
            System.Reflection.MethodInfo getSyntax = typeof(UriParser).GetMethod("GetSyntax", System.Reflection.BindingFlags.Static | System.Reflection.BindingFlags.NonPublic);
            System.Reflection.FieldInfo flagsField = typeof(UriParser).GetField("m_Flags", System.Reflection.BindingFlags.Instance | System.Reflection.BindingFlags.NonPublic);
            if (getSyntax != null && flagsField != null)
            {
                foreach (string scheme in new[] { "http", "https" })
                {
                    var parser = (UriParser) getSyntax.Invoke(null, new object[] { scheme });
                    if (parser != null)
                    {
                        int flagsValue = (int)flagsField.GetValue(parser);
                        // Clear the CanonicalizeAsFilePath attribute
                        if ((flagsValue & 0x1000000) != 0)
                            flagsField.SetValue(parser, flagsValue & ~0x1000000);
                    }
                }
            }
            uri = new Uri(url);
            return uri;
        }

        internal class RestClient : WebClient
        {
	        private readonly Action<HttpWebRequest> _modifyRequest;
            private readonly string  _accept;
            private int?    _timeoutInSeconds;

            public RestClient(string accept, int? timeoutInSeconds = null, Action<HttpWebRequest> modifyRequest = null)
            {
                _accept             = accept;
                _timeoutInSeconds   = timeoutInSeconds;
                _modifyRequest		= modifyRequest;
            }

            protected override WebRequest GetWebRequest(Uri address)
            {
                var request                     = (HttpWebRequest) base.GetWebRequest(address);
                request.AutomaticDecompression  = DecompressionMethods.GZip | DecompressionMethods.Deflate;
                request.UserAgent               = "Mozilla/5.0";
                request.Accept                  = _accept;

                _modifyRequest?.Invoke(request);

                if (_timeoutInSeconds.HasValue)
                {
                    request.Timeout             = _timeoutInSeconds.Value*60*1000;
                    request.ReadWriteTimeout    = _timeoutInSeconds.Value*60*1000;
                }

                return request;
            }
        }

        protected virtual void SetAuth(HttpWebRequest request)
        {}

        /// <summary>
        /// This is fixed method from SdmxMessageUtil class, in SdmxSource library.
        /// </summary>
        /// <param name="dataLocation"></param>
        private void ParseSdmxErrorMessage(IReadableDataLocation dataLocation)
        {
            if (dataLocation == null)
            {
                throw new ArgumentNullException("dataLocation");
            }

            using (var stream = dataLocation.InputStream)
            {
                using (XmlReader reader = XmlReader.Create(stream))
                {
                    SdmxErrorCode errorCode = null;
                    while (reader.Read())
                    {
                        switch (reader.NodeType)
                        {
                            case XmlNodeType.Element:
                                string nodeName = reader.LocalName;
                                if (nodeName.Equals("ErrorMessage"))
                                {
                                    var code = reader.GetAttribute("code");
                                    int clientCode;
                                    if (int.TryParse(code, out clientCode))
                                    {
                                        errorCode = SdmxErrorCode.ParseClientCode(clientCode);
                                    }
                                }
                                else if (nodeName.Equals("Text"))
                                {
                                    if (errorCode == null)
                                    {
                                        errorCode = SdmxErrorCode.GetFromEnum(SdmxErrorCodeEnumType.InternalServerError);
                                    }

                                    var message = reader.ReadString();

                                    switch (errorCode.EnumType)
                                    {
                                        case SdmxErrorCodeEnumType.NoResultsFound:
                                            throw new SdmxNoResultsException(message);
                                        case SdmxErrorCodeEnumType.NotImplemented:
                                            throw new SdmxNotImplementedException(message);
                                        case SdmxErrorCodeEnumType.SemanticError:
                                            throw new SdmxSemmanticException(message);
                                        case SdmxErrorCodeEnumType.Unauthorised:
                                            throw new SdmxUnauthorisedException(message);
                                        case SdmxErrorCodeEnumType.ServiceUnavailable:
                                            throw new SdmxServiceUnavailableException(message);
                                        case SdmxErrorCodeEnumType.SyntaxError:
                                            throw new SdmxSyntaxException(message);
                                        case SdmxErrorCodeEnumType.ResponseSizeExceedsServiceLimit:
                                            throw new SdmxResponseSizeExceedsLimitException(message);
                                        case SdmxErrorCodeEnumType.ResponseTooLarge:
                                            throw new SdmxResponseTooLargeException(message);
                                        case SdmxErrorCodeEnumType.InternalServerError:
                                            throw new SdmxInternalServerException(message);
                                        default:
                                            throw new SdmxException(message, errorCode);
                                    }
                                }

                                break;
                        }
                    }
                }
            }
        }
        #endregion

        #region Query Dialog
        protected override string GetSdmxQueryDialogString(ISdmxFilter filter, bool production, bool updateDate)
        {
            return this.BaseUri + GetDataQuery(filter);
        }

        protected override string GetDataParams(ISdmxFilter filter, bool production, bool updateDate)
        {
            return GetSdmxQueryDialogString(filter, production, updateDate);
        }

        #endregion

    }
}
