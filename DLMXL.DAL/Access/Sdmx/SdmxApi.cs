﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;
using DLMXL.Models;
using DLMXL.Models.Exceptions;
using DLMXL.Models.Interfaces;
using Org.Sdmxsource.Sdmx.Api.Constants;
using Org.Sdmxsource.Sdmx.Api.Model;
using Org.Sdmxsource.Sdmx.Api.Model.Data;
using Org.Sdmxsource.Sdmx.Api.Model.Objects;
using Org.Sdmxsource.Sdmx.Api.Model.Objects.Base;
using Org.Sdmxsource.Sdmx.Api.Model.Objects.ConceptScheme;
using Org.Sdmxsource.Sdmx.Api.Model.Objects.MetadataStructure;
using Org.Sdmxsource.Sdmx.Api.Model.Objects.Reference;
using Org.Sdmxsource.Sdmx.Api.Util;
using Org.Sdmxsource.Sdmx.DataParser.Engine.Reader;
using Org.Sdmxsource.Sdmx.DataParser.Manager;
using Org.Sdmxsource.Sdmx.Structureparser.Manager.Parsing;
using Org.Sdmxsource.Sdmx.Util.Date;
using Org.Sdmxsource.Util.Io;
using IDimension = DLMXL.Models.Interfaces.IDimension;

namespace DLMXL.DAL.Access.Sdmx
{
    public abstract class SdmxApi : BaseDataAccess, IDataAccess
    {
        protected readonly Regex CodePattern = new Regex("[^A-Za-z0-9*@_-]", RegexOptions.Compiled);

        protected string FlowQuery { get; }
        protected string StructureQuery { get; }
        protected string DataQuery { get; }
        protected string BaseUri { get; }

        public virtual bool IsExternal => true;
        public virtual bool IsSaveAllowed => false;
        public bool IsDataSpace => false;
        public override bool IsFlagsOutput => true;
        public override bool IsDateOutput => false;

        protected readonly SdmxSchemaEnumType               _sdmxVersion;
        protected readonly StructureParsingManager          _structureParsingManager;
        protected readonly ReadableDataLocationFactory      _dataLocationFactory;
        protected readonly Dictionary<SdmxId, ISdmxObjects> _sdmxObjectsCache;
        protected readonly Dictionary<string, string>       _contentCache;

        protected abstract Stream GetDataflows();
        protected abstract Stream GetStructures(SdmxId sdmxId);
        protected abstract Stream GetData(ISdmxFilter sdmxFilter);
        protected abstract string GetDataQuery(ISdmxFilter filter, List<KeyValuePair<string, string>> @params = null);
        protected abstract Stream GetMsd(SdmxId msd);
        protected abstract Stream GetMetaData(ISdmxFilter filter);

        protected SdmxApi(
            string code, 
            string baseUri, 
            string flowQuery, 
            string structureQuery, 
            string dataQuery, 
            SdmxSchemaEnumType sdmxVersion = SdmxSchemaEnumType.VersionTwoPointOne
        ) 
            : base(code)
        {
            BaseUri                     = baseUri;
            FlowQuery                   = flowQuery;
            StructureQuery              = structureQuery;
            DataQuery                   = dataQuery;
            _sdmxVersion                = sdmxVersion;

            _structureParsingManager    = new StructureParsingManager(sdmxVersion);
            _dataLocationFactory        = new ReadableDataLocationFactory();
            _sdmxObjectsCache           = new Dictionary<SdmxId, ISdmxObjects>();
            _contentCache               = new Dictionary<string, string>(StringComparer.OrdinalIgnoreCase);
        }

        #region IDataAccess
        public virtual void BulkPreLoad()
        {}

        public virtual IList<IDatabase> LoadDatabases()
        {
            return new IDatabase[] {SWDatabase.Build(this, -1, null, null)};
        }

        public virtual IList<IDataset> LoadDatasets(IDatabase db)
        {
            var list = new LinkedList<IDataset>();

            IStructureWorkspace workspace;

            using (var sourceData = GetDataLocation(GetDataflows, "DB_LIST", false, TransformDataflow))
                workspace = _structureParsingManager.ParseStructures(sourceData);

            var sdmxObjects = workspace.GetStructureObjects(false);


            if (sdmxObjects.Dataflows.Any())
            {
                foreach (var dataflow in sdmxObjects.Dataflows.Where(x=>x.IsExternalReference?.IsTrue == false))
                    list.AddLast(new SWDataset(this,
                            db,
                            dataflow.Id,
							dataflow.Name,
                            dataflow.Names.ToLocales(),
                            new SdmxId()
                            {
								Id = dataflow.DataStructureRef.MaintainableId,
								Agency = dataflow.DataStructureRef.AgencyId,
								Version = dataflow.DataStructureRef.Version
							}, 
                            new SdmxId()
                            {
								Id = dataflow.Id,
								Agency = dataflow.AgencyId,
								Version = dataflow.Version
                            }
                        ));
            }
            else if (sdmxObjects.DataStructures.Any())
            {
                foreach (var dataStructure in sdmxObjects.DataStructures)
                    list.AddLast(new SWDataset(this,
                            db,
                            dataStructure.Id,
							dataStructure.Name,
                            dataStructure.Names.ToLocales(),
                            new SdmxId()
                            {
                                Id = dataStructure.Id,
                                Agency = dataStructure.AgencyId,
                                Version = dataStructure.Version
                            }
                        ));
            }

            return list.OrderBy(x=>x.DefaultName).ToArray();
        }

        public IList<IQuery> LoadQueries(IDataset ds)
        {
            return null;
        }

        public virtual IList<IDimension> FetchDimensions(IDataset ds)
        {
            var sdmxObjects = this[ds];
            var structure   = sdmxObjects.DataStructures.First();
            var actualConstraint  = sdmxObjects.ContentConstraintObjects.FirstOrDefault(c => c.IsDefiningActualDataPresent);
            var list        = new List<IDimension>();
            var constraintResolver = new SdmxConstraintResolver(actualConstraint);

            foreach (var item in structure.DimensionList.Dimensions)
            {
                var names = GetConceptNames(sdmxObjects.ConceptSchemes, item.ConceptRef);

                var dim = SWDimension.Build(ds,
                    item.Id,
					names.Item1,
                    names.Item2,
                    item.TimeDimension,
                    false,
                    item.FrequencyDimension,
                    index:item.Position,
                    constraint:constraintResolver[item.Id]
                );

                list.Add(dim);

                if (item.TimeDimension)
                    continue;

                if (item.HasCodedRepresentation())
                {
                    var codelist_id = item.Representation.Representation.MaintainableId;
                    var codelist = sdmxObjects.Codelists.First(x => x.Id == codelist_id);

                    dim.ResetMembers(codelist.Items.Select(c => CodelistMember.Build(
                        null,
                        c.Id,
                        c.Name,
                        c.Names.ToLocales()
                    )));
                }
            }

            return list.OrderBy(x => x.Index).ToArray();
        }

        public virtual IList<IAttribute> FetchAttributes(IDataset ds)
        {
			var sdmxObjects = this[ds];
			var structure = sdmxObjects.DataStructures.First();
			var list = new List<IAttribute>();

			foreach (var sdmxAttr in structure.Attributes)
			{
				var names = GetConceptNames(sdmxObjects.ConceptSchemes, sdmxAttr.ConceptRef);

				var attr = new Models.Attribute(
					sdmxAttr.Id, 
					names.Item1, 
					names.Item2
				);

				if (sdmxAttr.HasCodedRepresentation())
				{
					var codelist_id = sdmxAttr.Representation.Representation.MaintainableId;
					var codelist = sdmxObjects.Codelists.First(x => x.Id == codelist_id);

					attr.ResetMembers(codelist.Items.Select(c => CodelistMember.Build(
						null, 
						c.Id,
						c.Name,
						c.Names.ToLocales()
					)));
				}

				list.Add(attr);
			}

			return list.ToArray();
		}

        public IList<KeyValuePair<string, string>> GetLanguages(IDataset ds)
        {
	        return ds
		        .Dimensions
		        .SelectMany(d => d.Names.Keys)
		        .Distinct()
		        .Select(lang => new KeyValuePair<string, string>(lang, System.Globalization.CultureInfo.GetCultureInfo(lang).EnglishName))
		        .ToArray();
        }

        public override void ClearObjectCache()
        {
	        _sdmxObjectsCache.Clear();
        }

        public virtual IMsd LoadMsd(IDataset ds)
        {
            if (!this.IsMetadata)
                return null;
            
            var sdmxObjects = this[ds];
            var dsd = sdmxObjects.DataStructures.First();
            var msdAnnotation = dsd.Annotations.FirstOrDefault(x => x.Type.Equals("Metadata", StringComparison.OrdinalIgnoreCase));

            if (msdAnnotation == null || string.IsNullOrEmpty(msdAnnotation.Title))
            {
                return null;
            }

            var arr = msdAnnotation.Title.Split('=');

            if (arr.Length != 2)
            {
                return null;
            }

            arr = arr[1].Split(new[] { ':', '(', ')' }, StringSplitOptions.RemoveEmptyEntries);

            if (arr.Length != 3)
            {
                return null;
            }

            // -------------------------------

            var msdSdmxId = new SdmxId()
            {
                Agency = arr[0],
                Id = arr[1],
                Version = arr[2]
            };

            var msd = new Msd(msdSdmxId);
            sdmxObjects = this[msd];

            if (sdmxObjects == null)
            {
                return null;
            }

            var msdBase = sdmxObjects.MetadataStructures.First();

            msd.MetadataAttributes = FlatMetadataAttributes(msdBase.ReportStructures.SelectMany(x => x.MetadataAttributes))
                .Select(Transform)
                .ToArray();

            return msd;
        }

        private IEnumerable<IMetadataAttributeObject> FlatMetadataAttributes(IEnumerable<IMetadataAttributeObject> attributes)
        {
            foreach (var attr in attributes)
            {
                if (!attr.Presentational.IsTrue)
                {
                    yield return attr;
                }

                if(attr.MetadataAttributes!=null)
                    foreach (var child in attr.MetadataAttributes)
                        yield return child;
            }
        }

        private IMetadataAttribute Transform(IMetadataAttributeObject a)
        {
            return new MetadataAttribute(a.Id)
            {
                Path = a.GetFullIdPath(false),
                MinOccurs = a.MinOccurs,
                MaxOccurs = a.MaxOccurs,
                LocalRepresentation = (a.Representation?.TextFormat?.TextType.EnumType ?? TextEnumType.String).ToString()
            };
        }

        #endregion

        #region Get data

        public object[,] LoadData(
			OutputType outputType, 
			ISdmxFilter sdmxFilter, 
			bool production = true, 
			LabelOutput labelOutput = LabelOutput.Code,
			string language = null, 
			bool flags = false, 
			bool updateDate = false,
            bool withActionColumn = false
        )
        {
            // -------------------------------------------
            var outputCodes             = (labelOutput & LabelOutput.Code) > 0;
            var outputLabels            = labelOutput > LabelOutput.Code;
            
            var filter						= sdmxFilter.SWFilter;
            var dimensions        = filter.Dataset.Dimensions.OrderBy(dim => dim.TimeLastOrder()).ToArray();
            var isTime					= filter.Dataset.HasATimeDimension;
            var nonTimeDimCount			= dimensions.Count() - (isTime ? 1 : 0);

            var mapper						= new List<Mapper>();
            var tempData					= new List<object[]>(50000);
            var sortColumns                 = new List<KeyValuePair<int, bool>>();
            var isTimeSeries                = false;
            HashSet<string> timePoints      = null;
            List<ImportSummary> importSummary = null;

            if (outputType == OutputType.TimeSeriesDown || outputType == OutputType.TimeSeriesAcross)
            {
                if (!isTime)
                    throw new InvalidOperationException("No time dimension");

                isTimeSeries    = true;
                timePoints      = new HashSet<string>();
            }

			// -------------------------------------------

			var mapHash	= new Dictionary<string, int>();
			var index		= 0;

            mapper.Add(new Mapper(ColumnType.Calculated, "SID", index++));

            // Action -------------

            var actionColumnIndex = -1;
            var updateDateColumnIndex = -1;
            var updateByColumnIndex = -1;

            if (withActionColumn)
            {
                actionColumnIndex = index;
                mapper.Add(new Mapper(ColumnType.Calculated, "ACTION", index++));
            }

            // Updated Date

            if (sdmxFilter.HistoricVersionType.HasValue)
            {
                sortColumns.Add(new KeyValuePair<int, bool>(actionColumnIndex, true)); // ACTION 

                if (sdmxFilter.HistoricVersionType == HistoricVersionType.IncludeHistory)
                {
                    updateDateColumnIndex = index;
                    mapper.Add(new Mapper(ColumnType.Calculated, "UPDATE_DATE", index++));
                    sortColumns.Add(new KeyValuePair<int, bool>(updateDateColumnIndex, true)); //UPDATE_DATE

                    updateByColumnIndex = index;
                    mapper.Add(new Mapper(ColumnType.Calculated, "UPDATE_BY", index++));
                }
            }

            var dimColumnStartIndex	= index;
			var timeColumnIndex	= dimColumnStartIndex + nonTimeDimCount;

			foreach (var dim in dimensions)
            {
				mapper.Add(new Mapper(ColumnType.Dimension, dim.Code, index, GetLabel(dim, labelOutput, language)));
				mapHash[dim.Code] = index++;
			}
			
			var valIndex		= index;

			mapper.Add(new Mapper(ColumnType.Value, "Value", index++));

			if (flags)
				foreach (var attr in filter.Dataset.Attributes)
				{
					mapper.Add(new Mapper(ColumnType.Attribute, attr.Code, index));
					mapHash[attr.Code] = index++;
				}
			
			// -------------------------------------------

			var groupAttributesExist	= false;
			var groupAttrRetriever		= flags ? new GroupAttributeRetriever() : null;
			var sdmxObjects				= this[sdmxFilter.SWFilter.Dataset];
			var structure				= sdmxObjects.DataStructures.First();
            var cacheKey                = GetDataQuery(sdmxFilter);

            try
            {
                if(updateByColumnIndex > 0)
                {
                    importSummary = this.GetImportSummary(sdmxFilter.SWFilter.Dataset.DfId.ToString());
                }
                
                using (var sourceData = GetDataLocation(() => GetData(sdmxFilter), cacheKey, true, TransformData))
                using (var reader = new DataReaderManager().GetDataReaderEngine(sourceData, structure, null))
                //using (var reader = new CompactDataReaderEngine(sourceData, null, structure))
                {
                    while(reader.MoveNextDataset())
                    {
                        var dsLvlAttrs = new object[mapper.Count];

                        if (sdmxFilter.HistoricVersionType.HasValue)
                        {
                            dsLvlAttrs[actionColumnIndex] = reader.CurrentAction.Action;

                            var validFrom = reader.CurrentDatasetHeader.ValidFrom;

                            if (updateDateColumnIndex > 0)
                            {
                                dsLvlAttrs[updateDateColumnIndex] = validFrom.ToLocalTime().ToString("yyyy-MM-dd HH:mm:ss");
                            }

                            if(updateByColumnIndex > 0 && importSummary!=null)
                            {
                                dsLvlAttrs[updateByColumnIndex] = importSummary
                                    .SingleOrDefault(x => x.ExecutionStart <= validFrom && x.ExecutionEnd >= validFrom)
                                    ?.UserEmail;
                            }
                        }

                        // dataset attributes
                        if (flags && reader.DatasetAttributes != null)
                            foreach (var attr in reader.DatasetAttributes)
                                dsLvlAttrs[mapHash[attr.Concept]] = attr.Code;

                        while (reader.MoveNextKeyable())
                        {
                            // group attributes
                            if (flags && !reader.CurrentKey.Series)
                            {
                                groupAttrRetriever.Add(reader.CurrentKey, mapHash);
                                groupAttributesExist = true;
                            }
                            // series 
                            else if (reader.CurrentKey.Series && reader.MoveNextObservation())
                            {
                                var data = new object[mapper.Count];

                                if (flags && groupAttributesExist)
                                {
                                    foreach (var attr in groupAttrRetriever.GetAttributes(reader.CurrentKey))
                                        data[attr.Key] = attr.Value;
                                }

                                if (dsLvlAttrs.Any(x => x != null))
                                {
                                    dsLvlAttrs.CopyTo(data, 0);
                                }

                                // dimensions
                                foreach (var dim in reader.CurrentKey.Key)
                                    data[mapHash[dim.Concept]] = dim.Code;

                                // attributes
                                if (flags && reader.CurrentKey.Attributes != null)
                                    foreach (var attr in reader.CurrentKey.Attributes)
                                        data[mapHash[attr.Concept]] = attr.Code;

                                var sid = GetSID(data, dimColumnStartIndex, nonTimeDimCount);

                                data[0] = sid;

                                do
                                {
                                    var row = new object[data.Length];
                                    data.CopyTo(row, 0);

                                    var obs = reader.CurrentObservation;

                                    row[timeColumnIndex] = obs.ObsTime;
                                    row[valIndex] = string.Equals(obs.ObservationValue, "NaN",
                                        StringComparison.OrdinalIgnoreCase)
                                        ? null
                                        : obs.ObservationValue;

                                    if (flags && obs.Attributes != null)
                                        foreach (var attr in obs.Attributes)
                                            row[mapHash[attr.Concept]] = attr.Code;

                                    tempData.Add(row);

                                    if (isTimeSeries)
                                        timePoints.Add(obs.ObsTime);
                                }
                                while (reader.MoveNextObservation());
                            }
                        }
                    }
                }
            }
            catch(DlmXLNoData)
            {
                if (!this.IsSaveAllowed || outputType != OutputType.Flat)
                {
                    throw;
                }
            }

            // ----------------------

            sortColumns.Add(new KeyValuePair<int, bool>(0, true));          // SID
            sortColumns.Add(new KeyValuePair<int, bool>(timeColumnIndex, true));  // TIME

            tempData = Sort(tempData, sortColumns);
            var sids = GetSids(tempData);

            // ----------------------

            return Output(outputType,
                mapper,
                tempData,
                dimColumnStartIndex,
                nonTimeDimCount,
                outputCodes,
                outputLabels,
                sids,
                timePoints
            );
        }

        private List<object[]> Sort(List<object[]> data, IList<KeyValuePair<int, bool>> sortColumns)
        {
            if (sortColumns== null || sortColumns.Count == 0)
                return data;

            var sortKey = sortColumns.First();

            var sorted = sortKey.Value
                ? data.OrderBy(x => x[sortKey.Key])
                : data.OrderByDescending(x=>x[sortKey.Key]);

            foreach(var key in sortColumns.Skip(1))
            {
                sorted = key.Value
                    ? sorted.ThenBy(x => x[key.Key])
                    : sorted.ThenByDescending(x => x[key.Key]);
            }

            return sorted.ToList();
        }

        private Dictionary<string, int> GetSids(List<object[]> data)
        {
            var sids = new Dictionary<string, int>();
            var sortOrder = 0;

            foreach (var sid in data.Select(row => row[0].ToString()))
            {
                if (!sids.ContainsKey(sid))
                {
                    sids.Add(sid, sortOrder++);
                }
            }

            return sids;
        }

        private class GroupAttributeRetriever
        {
	        private readonly Dictionary<string, AttributeBag> _dict = new Dictionary<string, AttributeBag>();

	        public void Add(IKeyable @group, Dictionary<string, int> mapHash)
	        {
		        var dims = @group.Key.Select(x => x.Concept).ToArray();
		        var signature = string.Join(":", dims);
		        AttributeBag bag = null;

				if (!_dict.TryGetValue(signature, out bag))
		        {
			        _dict[signature] = bag = new AttributeBag(dims);
		        }

		        bag.Add(@group, mapHash);
	        }

	        public IEnumerable<KeyValuePair<int, string>> GetAttributes(IKeyable series)
	        {
		        foreach (var bag in _dict.Values)
		        {
			        var result = bag.Fetch(series);

			        if (result != null)
						foreach(var pair in result)
							yield return pair;
		        }
	        }

	        private class AttributeBag
	        {
		        private readonly IList<string> _key;
		        private readonly Dictionary<string, IList<KeyValuePair<int, string>>> _dict;

		        public AttributeBag(IList<string> key)
		        {
			        _key = key;
			        _dict = new Dictionary<string, IList<KeyValuePair<int, string>>>();
		        }

		        public void Add(IKeyable group, Dictionary<string, int> mapHash) 
			        => _dict[String.Join(":", group.Key.Select(x => x.Code))] 
				        = group.Attributes.Select(a => new KeyValuePair<int, string>(mapHash[a.Concept], a.Code)).ToArray();

		        public IList<KeyValuePair<int, string>> Fetch(IKeyable series)
		        {
			        var key = string.Join(":", _key.Select(series.GetKeyValue));

			        IList<KeyValuePair<int, string>> result = null;

					return _dict.TryGetValue(key, out result) 
						? result 
						: null;
		        }
	        }
        }

        public virtual object[,] LoadMetadata(
            ISdmxFilter sdmxFilter,
            LabelOutput labelOutput = LabelOutput.Code,
            string language = null,
            bool withActionColumn = false
        )
        {
            var outputCodes = (labelOutput & LabelOutput.Code) > 0;
            var outputLabels = labelOutput > LabelOutput.Code;

            var dataset = sdmxFilter.SWFilter.Dataset;
            var dimensions = dataset.Dimensions.OrderBy(dim => dim.TimeLastOrder()).ToArray();
            var isTime = dataset.HasATimeDimension;
            var nonTimeDimCount = dimensions.Count() - (isTime ? 1 : 0);

            var mapper = new List<Mapper>();
            var tempData = new List<object[]>(50000);

            var mapHash = new Dictionary<string, int>();
            var index = 0;

            mapper.Add(new Mapper(ColumnType.Calculated, "SID", index++));

            if (withActionColumn)
            {
                mapper.Add(new Mapper(ColumnType.Calculated, "ACTION", index++));
            }

            var dimIndex  = index;
            var timeIndex = dimIndex + nonTimeDimCount;

            foreach (var dim in dimensions)
            {
                mapper.Add(new Mapper(ColumnType.Dimension, dim.Code, index, GetLabel(dim, labelOutput, language)));
                mapHash[dim.Code] = index++;
            }

            foreach (var attr in dataset.Msd.MetadataAttributes)
            {
                mapper.Add(new Mapper(ColumnType.Attribute, attr.Path, index));
                mapHash[attr.Path] = index++;
            }

            // -----------------------------------------

            var cacheKey = this.GetMetaDataQueryString(sdmxFilter);
            var dsd = this[dataset].DataStructures.First();
            var msd = this[dataset.Msd].MetadataStructures.First();

            try
            {
                using (var sourceData = GetDataLocation(() => GetMetaData(sdmxFilter), cacheKey, true, TransformData))
                using (var reader = new DataReaderManager().GetDataReaderEngine(sourceData, dsd, null, msd))
                {
                    if (!reader.MoveNextDataset())
                        return null;

                    // dataset attributes

                    while (reader.MoveNextKeyable())
                    {
                        if (reader.CurrentKey.Series && reader.MoveNextObservation())
                        {
                            var data = new object[mapper.Count];

                            // dimensions
                            foreach (var dim in reader.CurrentKey.Key)
                                if (!string.IsNullOrEmpty(dim.Code))
                                    data[mapHash[dim.Concept]] = dim.Code;

                            // attributes
                            if (reader.CurrentKey.Attributes != null)
                                foreach (var attr in reader.CurrentKey.Attributes)
                                    data[mapHash[attr.Concept]] = attr.Code;

                            data[0] = GetSID(data, dimIndex, nonTimeDimCount);

                            do
                            {
                                var row = new object[data.Length];
                                data.CopyTo(row, 0);

                                var obs = reader.CurrentObservation;

                                if (obs.Attributes != null)
                                    foreach (var attr in obs.Attributes)
                                        row[mapHash[attr.Concept]] = attr.Code;

                                if (isTime)
                                {
                                    row[timeIndex] = obs.ObsTime;
                                }

                                tempData.Add(row);
                            } 
                            while (reader.MoveNextObservation());
                        }
                    }
                }
            }
            catch (DlmXLNoData)
            {
                if (!this.IsSaveAllowed)
                {
                    throw;
                }
            }

            return Output(
                OutputType.Flat,
                mapper,
                tempData,
                dimIndex,
                nonTimeDimCount,
                outputCodes,
                outputLabels,
                null,
                null
            );
        }

        #endregion

        #region Sdmx

        protected ISdmxObjects this[IDataset ds] => _sdmxObjectsCache.GetOrAdd(StructureQuery.StartsWith("dataflow", StringComparison.OrdinalIgnoreCase) ? ds.DfId : ds.DsdId, LoadStructure);
        protected ISdmxObjects this[IMsd msd] => _sdmxObjectsCache.GetOrAdd(msd.SdmxId, LoadMsd);

        private Tuple<string, Dictionary<string,string>> GetConceptNames(ISet<IConceptSchemeObject> conceptSchemes, ICrossReference conceptRef)
        {
            if (conceptSchemes == null || conceptRef == null)
                throw new ArgumentNullException();

            switch (_sdmxVersion)
            {
                case SdmxSchemaEnumType.VersionTwoPointOne:
                {
                    var scheme  = conceptSchemes.First(x => x.Id == conceptRef.MaintainableId && x.AgencyId == conceptRef.AgencyId && x.Version == conceptRef.Version);
                    var concept = scheme.Items.First(x => x.Id == conceptRef.FullId);

                    return new Tuple<string, Dictionary<string, string>>(
	                    concept.Name, 
	                    concept.Names.ToLocales()
	                );
                }
                case SdmxSchemaEnumType.VersionTwo:
                {
                    var scheme  = conceptSchemes.First();
                    var concept = scheme.IdentifiableComposites.First(x => x.Id == conceptRef.FullId) as INameableObject;

                    return new Tuple<string, Dictionary<string, string>>(
	                    concept?.Name, 
	                    concept?.Names.ToLocales()
                    );
                }
            }

            throw new NotImplementedException();
        }

        protected virtual ISdmxObjects LoadStructure(SdmxId sdmxId)
        {
            using (var sourceData = GetDataLocation(() => GetStructures(sdmxId), sdmxId.FullId, false, TransformStructure))
            {
                var workspace = _structureParsingManager.ParseStructures(sourceData);

                return workspace.GetStructureObjects(false);
            }
        }

        protected virtual ISdmxObjects LoadMsd(SdmxId sdmxId)
        {
            try
            {
                using (var sourceData = GetDataLocation(() => GetMsd(sdmxId), sdmxId.FullId, false, TransformStructure))
                {
                    var workspace = _structureParsingManager.ParseStructures(sourceData);

                    return workspace.GetStructureObjects(false);
                }
            }
            catch(DlmXLNoData)
            {
                return null;
            }
        }

        protected virtual IReadableDataLocation GetDataLocation(Func<Stream> getStream, string cacheKey, bool isData, Action<Stream, string> transformContent)
        {
            var tmpFile = isData && _contentCache.ContainsKey(cacheKey)
                                ? _contentCache[cacheKey]
                                : this.GetTempFile(isData, cacheKey);

            if (!TempFileExists(tmpFile))
            {
                using (var stream = getStream())
                {
                    transformContent(stream, tmpFile);
                }
            }

            if (isData)
            {
                _contentCache[cacheKey] = tmpFile;
            }

            return _dataLocationFactory.GetReadableDataLocation(new FileStream(tmpFile, FileMode.Open));
        }

        #endregion

        #region Transform

        protected virtual void TransformDataflow(Stream input, string outputFile)
        {
            using (var fs = new FileStream(outputFile, FileMode.Create))
            {
                input.CopyTo(fs);
            }
        }

        protected virtual void TransformStructure(Stream input, string outputFile)
        {
            using (var fs = new FileStream(outputFile, FileMode.Create))
            {
                input.CopyTo(fs);
            }
        }

        protected virtual void TransformData(Stream input, string outputFile)
        {
            using (var fs = new FileStream(outputFile, FileMode.Create))
            {
                input.CopyTo(fs);
            }
        }

        #endregion

        #region Transfer service
        protected virtual List<ImportSummary> GetImportSummary(string dataflowId)
        {
            return null;
        }
        #endregion

        #region Sdmx Dates
        /// <summary>
        /// Returns the start date of a period adjusted backwards for lastXPeriod number
        /// </summary>
        /// <param name="frequency">Frequency</param>
        /// <param name="time">End date of a frequency period</param>
        /// <param name="lastXPeriod">period adjustment</param>
        /// <returns></returns>
        /// <exception cref="NotImplementedException"></exception>
        public static KeyValuePair<DateTime, string> AdjustDate(Frequency frequency, DateTime time, int lastXPeriod)
        {
            switch (frequency)
            {
                case Frequency.Anually:
                    return new DateTime(time.Year, time.Month, 1).AddMonths(-12 * lastXPeriod + 1).ToSdmxPeriod<Annual>();

                case Frequency.Quarterly:
                    return new DateTime(time.Year, time.Month, 1).AddMonths(-3 * lastXPeriod + 1).ToSdmxPeriod<Quarterly>();

                case Frequency.Monthly:
                    return new DateTime(time.Year, time.Month, 1).AddMonths(-1 * lastXPeriod + 1).ToSdmxPeriod<Monthly>();

                case Frequency.Daily:
                    return time.AddDays(-1 * lastXPeriod + 1).ToSdmxPeriod<Daily>();

                case Frequency.Semestrial:
                    return new DateTime(time.Year, time.Month, 1).AddMonths(-6 * lastXPeriod + 1).ToSdmxPeriod<Semester>();

                case Frequency.Weekly:
                    return time.AddDays(-7 * lastXPeriod + 1).ToSdmxPeriod<Weekly>();
            }

            throw new NotImplementedException();
        }



        #endregion

        #region Dynamic availability
        protected virtual ISdmxObjects GetDateDynamicAvailabilityStructure(ISdmxFilter filter, IDimension dimension)
        {
            return null;
        }

        protected virtual DateTime? GetDateDynamicAvailability(ISdmxFilter filter)
        {
            return null;
        }
        #endregion

    }
}
