﻿using System;
using System.Collections.Generic;
using System.Linq;
using DLMXL.Models;
using Org.Sdmxsource.Sdmx.Api.Model.Base;
using Org.Sdmxsource.Sdmx.Api.Model.Objects.Base;
using Org.Sdmxsource.Sdmx.Api.Model.Objects.Registry;
using Org.Sdmxsource.Util.Extensions;

namespace DLMXL.DAL.Access.Sdmx
{
    internal sealed class SdmxConstraintResolver
    {
        private readonly Dictionary<string, Constraint> _dict = new Dictionary<string, Constraint>();

        public SdmxConstraintResolver(IContentConstraintObject c)
        {
            if (c?.IncludedCubeRegion == null)
                return;

            _dict
                .AddAll(GetConstraints(c.IncludedCubeRegion.KeyValues, true));
        }

        private IEnumerable<KeyValuePair<string, Constraint>> GetConstraints(IList<IKeyValues> values, bool include)
        {
            return values.Select(kv => new KeyValuePair<string, Constraint>(kv.Id, new Constraint(include, kv.Values, Transform(kv.TimeRange))));
        }

        private TimeRange Transform(ITimeRange sdmxRange)
        {
            if (sdmxRange == null || (sdmxRange.StartDate == null && sdmxRange.EndDate==null))
                return null;

            return new TimeRange()
            {
                StartDate = Transform(sdmxRange.StartDate, sdmxRange.StartInclusive),
                EndDate = Transform(sdmxRange.EndDate, sdmxRange.EndInclusive),
            };
        }

        private SdmxDate Transform(ISdmxDate date, bool isInclusive)
        {
            return date != null 
                ? new SdmxDate() { Date = date.Date, IsInclusive = isInclusive } 
                : null;
        }

        public Constraint this[string code] => _dict.ContainsKey(code) ? _dict[code] : null;
    }
}
