﻿using System;
using System.Collections.Generic;
using System.IO;
using DLMXL.Models;
using DLMXL.Models.Interfaces;
using Org.Sdmxsource.Sdmx.Util.Objects.Container;
using Org.Sdmxsource.Sdmx.Api.Manager.Parse;
using Org.Sdmxsource.Sdmx.Api.Model.Objects;
using Org.Sdmxsource.Sdmx.Api.Util;
using System.Linq;
using Org.Sdmxsource.Sdmx.Structureparser.Manager.Parsing;
using Org.Sdmxsource.Util.Io;

namespace DLMXL.DAL.Access.Sdmx
{
	public sealed class TestSdmxApi : SdmxApi
    {
        public override bool IsMetadata { get;}

		public TestSdmxApi(bool isMetadata = false) : base("","", "", "", "")
        {
            IsMetadata = isMetadata;
        }

		protected override Stream GetData(ISdmxFilter filter)
		{
			throw new NotImplementedException();
		}

        protected override string GetDataQuery(ISdmxFilter filter, List<KeyValuePair<string, string>> @params = null)
        {
            throw new NotImplementedException();
        }

        protected override Stream GetMsd(SdmxId msd)
        {
            throw new NotImplementedException();
        }

        protected override Stream GetMetaData(ISdmxFilter filter)
        {
            throw new NotImplementedException();
        }

        protected override Stream GetDataflows()
		{
			throw new NotImplementedException();
		}

		protected override Stream GetStructures(SdmxId sdmxId)
		{
			throw new NotImplementedException();
		}

		public IDataset GetDatasetFromFile(string filename)
		{
			ISdmxObjects sdmxObjects = new SdmxObjectsImpl();
			IStructureParsingManager structureParsingManager = new StructureParsingManager();
			using (IReadableDataLocation dataLocation = new FileReadableDataLocation(filename))
			{
				sdmxObjects.Merge(structureParsingManager.ParseStructures(dataLocation).GetStructureObjects(false));
			}

			var df = sdmxObjects.Dataflows.First();

			var ds = new SWDataset(this,
				null,
				df.Id,
				df.Name,
				df.Names.ToLocales(),
				new SdmxId()
				{
					Id = df.DataStructureRef.MaintainableId,
					Agency = df.DataStructureRef.AgencyId,
					Version = df.DataStructureRef.Version
				},
				new SdmxId()
				{
					Id = df.Id,
					Agency = df.AgencyId,
					Version = df.Version
				}
			);

			_sdmxObjectsCache.Add(ds.DsdId, sdmxObjects);

            if (IsMetadata)
            {
                var msd = sdmxObjects.MetadataStructures.FirstOrDefault();

                if (msd != null)
                {
                    var msdId = new SdmxId()
                    {
                        Id = msd.Id,
                        Agency = msd.AgencyId,
                        Version = msd.Version
                    };

					_sdmxObjectsCache.Add(msdId, sdmxObjects);
				}
            }

			return ds;
		}
	}
}
