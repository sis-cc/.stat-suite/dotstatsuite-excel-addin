﻿using System.IO;
using System.Net;
using System.Text;
using System.Xml;
using DLMXL.Models.Exceptions;
using Org.Sdmxsource.Sdmx.Api.Constants;
using Org.Sdmxsource.Sdmx.Util.Sdmx;

namespace DLMXL.DAL.Access.Sdmx
{
    public abstract class WcfApi : SdmxApi
    {
        private const string RequestTemplate            = "<s:Envelope xmlns:s=\"http://schemas.xmlsoap.org/soap/envelope/\"><s:Header/><s:Body>{0}</s:Body></s:Envelope>";
        private const string MethodTemplate             = "<{0} xmlns=\"{1}\">{2}</{0}>";

        protected string ServiceNamespace { get; }

        protected WcfApi(
            string code,
            string baseUri,
            string serviceNamespace,
            string flowQuery,
            string structureQuery,
            string dataQuery,
            SdmxSchemaEnumType sdmxVersion = SdmxSchemaEnumType.VersionTwoPointOne
        )
            : base(code, baseUri, flowQuery, structureQuery, dataQuery, sdmxVersion)
        {
            ServiceNamespace = serviceNamespace;
        }

        protected Stream Soap(string method, string query, int trials = 3)
        {
            var tryNum         = 0;

            do
            {
                try
                {
                    var request                     = (HttpWebRequest) WebRequest.Create(this.BaseUri);
                    request.ContentType             = @"text/xml; charset=utf-8";
                    request.Method                  = "POST";
                    request.Timeout                 = (tryNum + 1) *90 * 1000;
                    request.AutomaticDecompression  = DecompressionMethods.GZip | DecompressionMethods.Deflate;
                    request.Headers.Add("SOAPAction", ServiceNamespace + "/" + method);

                    // ---------------
                    var soapMethod      = string.Format(MethodTemplate, method, ServiceNamespace, query);
                    var soapRequest     = string.Format(RequestTemplate, soapMethod);
                    // ---------------

                    request.ContentLength = soapRequest.Length;

                    using (var stream = new StreamWriter(request.GetRequestStream()))
                    {
                        stream.Write(soapRequest);
                    }

                    var tempStream = File.Create(Path.GetTempFileName(), 4096, FileOptions.DeleteOnClose);

                    using(var responseStream = request.GetResponse().GetResponseStream())
                    using (var reader = XmlReader.Create(responseStream, new XmlReaderSettings() { IgnoreWhitespace = true, IgnoreComments = true, IgnoreProcessingInstructions = true }))
                    using (var writer = XmlWriter.Create(tempStream, new XmlWriterSettings() { Encoding = Encoding.UTF8, Indent = true }))
                    {
                        SdmxMessageUtil.FindSdmx(reader);
                        writer.WriteNode(reader, true);
                        writer.Flush();
                    }

                    tempStream.Position = 0;

                    return tempStream;
                }
                catch (WebException e)
                {
                    if (e.Status == WebExceptionStatus.Timeout)
                        continue;

                    throw;
                }
            }
            while (++tryNum < trials);

            throw new DlmXlResumeActionException("Operation timeout");
        }

        #region SdmxApi

        protected override Stream GetDataflows()
        {
            return Soap(FlowQuery, null);
        }
        
        #endregion

    }
}
