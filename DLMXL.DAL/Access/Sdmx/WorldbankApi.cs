﻿using System.IO;
using System.Xml;
using DLMXL.Models.Exceptions;

namespace DLMXL.DAL.Access.Sdmx
{
    internal class WorldbankApi : GenericRestApi
    {
        public WorldbankApi(
            string code, 
            string baseUri, 
            string flowQuery, 
            string structureQuery, 
            string dataQuery, bool 
            isExternal = true, 
            bool isMetadata = false, 
            bool isHistoricVersion = false, 
            string transferUri = null, 
            string saveDataspace = null, 
            string authUri = null, 
            string authClientId = null, 
            string authScope = null, 
            bool isSaveWithAttributesOnly = false) 
            : base(
                code, 
                baseUri, 
                flowQuery, 
                structureQuery, 
                dataQuery, 
                isExternal, 
                isMetadata, 
                isHistoricVersion, 
                transferUri, 
                saveDataspace, 
                authUri, 
                authClientId, 
                authScope, 
                isSaveWithAttributesOnly)
        {

        }

        protected override void TransformData(Stream input, string outputFile)
        {
            bool deleteOutputFile = true;
            base.TransformData(input, outputFile);

            try
            {
                using (var reader = new XmlTextReader(new FileStream(outputFile, FileMode.Open)))
                {
                    if (reader.Read() && reader.NodeType == XmlNodeType.Element && reader.Name == "string")
                    {
                        throw new DlmXLManagedException(reader.ReadString());
                    }

                    deleteOutputFile = false;
                }
            }
            finally
            {
                if (deleteOutputFile)
                {
                    File.Delete(outputFile);
                }
            }
        }

    }
}
