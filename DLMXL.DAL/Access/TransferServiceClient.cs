﻿using System;
using System.Collections.Generic;
using DLMXL.DAL.Access.Sdmx;

namespace DLMXL.DAL.Access
{
    internal class TransferServiceClient
    {
        public static List<ImportSummary> GetSummary(GenericRestApi restApi, DateTime start, string artefact)
        {
            var uri = restApi.TransferUri + "/status/requests";
            
            var @params = new List<KeyValuePair<string, string>>
            {
                new KeyValuePair<string, string>("dataspace", restApi.SaveDataspace),
                new KeyValuePair<string, string>("artefact", artefact),
                new KeyValuePair<string, string>("submissionStart", start.ToString("dd-MM-yyyy HH:mm:ss")),
                new KeyValuePair<string, string>("executionStatus", "2"), // Completed
                // new KeyValuePair<string, string>("executionOutcome", "0") // Success
            };

            return ApiClient.Post<List<ImportSummary>>(uri, restApi.OAuth?.AccessToken, @params);
        }
    }

    public class ImportSummary
    {
        public string UserEmail { get; set; }
        public DateTime ExecutionStart { get; set; }
        public DateTime ExecutionEnd { get; set; }
        public string Outcome { get; set; }
    }
}
