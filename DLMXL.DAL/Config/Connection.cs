﻿using System.Configuration;
using DLMXL.Models;

namespace DLMXL.DAL.Config
{
    public abstract class Connection : ConfigurationElement
    {
        [ConfigurationProperty("id", IsRequired = true)]
        public string Id
        {
            get { return (string)this["id"]; }
            set { this["id"] = value; }
        }
        [ConfigurationProperty("name", IsRequired = true)]
        public string Name
        {
            get { return (string)this["name"]; }
            set { this["name"] = value; }
        }
        [ConfigurationProperty("dsn")]
        public string Dsn
        {
            get { return (string)this["dsn"]; }
            set { this["dsn"] = value; }
        }

        public abstract IDataAccess DataAccess { get; }
    }
}
