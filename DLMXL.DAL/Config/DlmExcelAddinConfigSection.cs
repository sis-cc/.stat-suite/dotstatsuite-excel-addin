﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;

namespace DLMXL.DAL.Config
{
    public class ConnectionCollection : ConfigurationElementCollection
    {
        public override ConfigurationElementCollectionType CollectionType => ConfigurationElementCollectionType.BasicMapAlternate;

        protected override string ElementName => string.Empty;
        protected override bool ThrowOnDuplicate => true;

        protected override ConfigurationElement CreateNewElement()
        {
            throw new NotImplementedException();
        }

        protected override ConfigurationElement CreateNewElement(string elementName)
        {
            var type                = Type.GetType("DLMXL.DAL.Config." + elementName, true, true);
            var c                   = Activator.CreateInstance(type) as Connection;
            return c;
        }

        protected override bool IsElementName(string elementName)
        {
            return true;
        }

        protected override object GetElementKey(ConfigurationElement element)
        {
            return ((Connection) element).Id;
        }

    }

    public class DlmExcelAddinConfigSection: ConfigurationSection
    {
        [ConfigurationProperty("title", IsRequired = true)]
        public string Title
        {
            get { return (string)this["title"]; }
            set { this["title"] = value; }
        }

        [ConfigurationProperty("helpLink", IsRequired = true)]
        public string HelpLink
        {
	        get { return (string)this["helpLink"]; }
	        set { this["helpLink"] = value; }
        }

        [ConfigurationProperty("languages", IsRequired = true)]
        public string LangString
        {
            get { return (string)this["languages"]; }
            set { this["languages"] = value; }
        }

        [ConfigurationProperty("authRedirectUri", IsRequired = false)]
        public string AuthRedirectUri
        {
            get { return (string)this["authRedirectUri"]; }
            set { this["authRedirectUri"] = value; }
        }

        private IList<KeyValuePair<string, string>> _languages;

        public IList<KeyValuePair<string, string>> Languages => _languages ?? (_languages = LangString
                .Split(',')
                .Select(l => l.Trim())
                .Select(l => new KeyValuePair<string, string>(l, System.Globalization.CultureInfo.GetCultureInfo(l).EnglishName))
                .ToList()
            );

        [ConfigurationProperty("connections", IsRequired = true)]
        [ConfigurationCollection(typeof(Connection))]
        public ConnectionCollection Connections => (ConnectionCollection) this["connections"];
    }
}
