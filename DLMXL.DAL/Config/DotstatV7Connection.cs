﻿using System.Configuration;
using DLMXL.DAL.Access.Direct;
using DLMXL.Models;

namespace DLMXL.DAL.Config
{
    public class DotstatV7Connection : Connection
    {
        [ConfigurationProperty("server", IsRequired = true)]
        public string Server
        {
            get { return (string)this["server"]; }
            set { this["server"] = value; }
        }

        [ConfigurationProperty("db", IsRequired = true)]
        public string Db
        {
            get { return (string)this["db"]; }
            set { this["db"] = value; }
        }

        [ConfigurationProperty("queryUri", IsRequired = true)]
        public string QueryUri
        {
            get { return (string)this["queryUri"]; }
            set { this["queryUri"] = value; }
        }

        public override IDataAccess DataAccess => new DotStatV7DirectAccess(Server, Db, QueryUri);
    }
}
