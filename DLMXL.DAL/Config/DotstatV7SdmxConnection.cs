﻿using System.Configuration;
using DLMXL.DAL.Access.Sdmx;
using DLMXL.Models;

namespace DLMXL.DAL.Config
{
    public class DotstatV7SdmxConnection : Connection
    {
        [ConfigurationProperty("baseUri", IsRequired = true)]
        public string BaseUri
        {
            get { return (string)this["baseUri"]; }
            set { this["baseUri"] = value; }
        }

        public override IDataAccess DataAccess => new DotStatV7Sdmx(Id, BaseUri);
    }
}
