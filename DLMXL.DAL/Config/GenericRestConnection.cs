﻿using System.Configuration;
using DLMXL.DAL.Access.Sdmx;
using DLMXL.Models;

namespace DLMXL.DAL.Config
{
    public class GenericRestConnection : Connection
    {
        [ConfigurationProperty("baseUri", IsRequired = true)]
        public string BaseUri
        {
            get { return (string)this["baseUri"]; }
            set { this["baseUri"] = value; }
        }

        [ConfigurationProperty("flowQuery", IsRequired = true)]
        public string FlowQuery
        {
            get { return (string)this["flowQuery"]; }
            set { this["flowQuery"] = value; }
        }

        [ConfigurationProperty("structureQuery", IsRequired = true)]
        public string StructureQuery
        {
            get { return (string)this["structureQuery"]; }
            set { this["structureQuery"] = value; }
        }

        [ConfigurationProperty("dataQuery", IsRequired = true)]
        public string DataQuery
        {
            get { return (string)this["dataQuery"]; }
            set { this["dataQuery"] = value; }
        }

        [ConfigurationProperty("isExternal", IsRequired = true)]
        public bool IsExternal
        {
            get { return (bool) this["isExternal"]; }
            set { this["isExternal"] = value; }
        }

        [ConfigurationProperty("isMetadata", IsRequired = false)]
        public bool IsMetadata
        {
            get { return (bool)this["isMetadata"]; }
            set { this["isMetadata"] = value; }
        }

        [ConfigurationProperty("isHistoricVersion", IsRequired = false)]
        public bool IsHistoricVersion
        {
            get { return (bool)this["isHistoricVersion"]; }
            set { this["isHistoricVersion"] = value; }
        }

        [ConfigurationProperty("isSaveWithAttributesOnly", IsRequired = false)]
        public bool IsSaveWithAttributesOnly
        {
            get { return (bool)this["isSaveWithAttributesOnly"]; }
            set { this["isSaveWithAttributesOnly"] = value; }
        }

        [ConfigurationProperty("transferUri", IsRequired = false)]
        public string TransferUri
        {
	        get { return (string) this["transferUri"]; }
	        set { this["transferUri"] = value; }
        }

        [ConfigurationProperty("saveDataspace", IsRequired = false)]
        public string SaveDataspace
        {
	        get { return (string) this["saveDataspace"]; }
	        set { this["saveDataspace"] = value; }
        }

        [ConfigurationProperty("authUri", IsRequired = false)]
        public string AuthUri
        {
	        get { return (string) this["authUri"]; }
	        set { this["authUri"] = value; }
        }

        [ConfigurationProperty("authClientId", IsRequired = false)]
        public string AuthClientId
        {
	        get { return (string) this["authClientId"]; }
	        set { this["authClientId"] = value; }
        }

        [ConfigurationProperty("authScope", IsRequired = false)]
        public string AuthScope
        {
            get { return (string)this["authScope"]; }
            set { this["authScope"] = value; }
        }

        public override IDataAccess DataAccess => new GenericRestApi(
	        Id, 
	        BaseUri,
	        FlowQuery,
	        StructureQuery,
	        DataQuery, 
	        IsExternal,
            IsMetadata,
            IsHistoricVersion,
            TransferUri,
	        SaveDataspace,
			AuthUri,
	        AuthClientId,
            AuthScope,
            IsSaveWithAttributesOnly
        );
    }
}
