﻿using System.Configuration;
using DLMXL.DAL.Access.Sdmx;
using DLMXL.Models;

namespace DLMXL.DAL.Config
{
    // ReSharper disable once InconsistentNaming
    public class IMFConnection : Connection
    {
        [ConfigurationProperty("baseUri", IsRequired = true)]
        public string BaseUri
        {
            get { return (string)this["baseUri"]; }
            set { this["baseUri"] = value; }
        }

        public override IDataAccess DataAccess => new IMFApi(Id, BaseUri);
    }
}
