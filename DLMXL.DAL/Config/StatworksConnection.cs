﻿using System.Configuration;
using DLMXL.DAL.Access.Direct;
using DLMXL.Models;

namespace DLMXL.DAL.Config
{
    public class StatworksConnection : Connection
    {
        [ConfigurationProperty("server", IsRequired = true)]
        public string Server
        {
            get { return (string)this["server"]; }
            set { this["server"] = value; }
        }

        [ConfigurationProperty("db", IsRequired = true)]
        public string Db
        {
            get { return (string)this["db"]; }
            set { this["db"] = value; }
        }

        public override IDataAccess DataAccess => new StatworksDirectAccess(Server, Db);
    }
}
