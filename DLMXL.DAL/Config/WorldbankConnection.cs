﻿using System.Configuration;
using DLMXL.DAL.Access.Sdmx;
using DLMXL.Models;

namespace DLMXL.DAL.Config
{
    public class WorldbankConnection : Connection
    {
        [ConfigurationProperty("baseUri", IsRequired = true)]
        public string BaseUri
        {
            get { return (string)this["baseUri"]; }
            set { this["baseUri"] = value; }
        }

        [ConfigurationProperty("flowQuery", IsRequired = true)]
        public string FlowQuery
        {
            get { return (string)this["flowQuery"]; }
            set { this["flowQuery"] = value; }
        }

        [ConfigurationProperty("structureQuery", IsRequired = true)]
        public string StructureQuery
        {
            get { return (string)this["structureQuery"]; }
            set { this["structureQuery"] = value; }
        }

        [ConfigurationProperty("dataQuery", IsRequired = true)]
        public string DataQuery
        {
            get { return (string)this["dataQuery"]; }
            set { this["dataQuery"] = value; }
        }

        [ConfigurationProperty("isExternal", IsRequired = true)]
        public bool IsExternal
        {
            get { return (bool) this["isExternal"]; }
            set { this["isExternal"] = value; }
        }

        public override IDataAccess DataAccess => new WorldbankApi(
	        Id, 
	        BaseUri,
	        FlowQuery,
	        StructureQuery,
	        DataQuery, 
	        IsExternal
        );
    }
}
