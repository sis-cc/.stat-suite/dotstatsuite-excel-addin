﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using DLMXL.DAL.Access;
using DLMXL.Models.Interfaces;
using Org.Sdmxsource.Sdmx.Api.Model.Objects.Base;
using Org.Sdmxsource.Sdmx.Util.Date;

namespace DLMXL.DAL
{
    public static class Extensions
    {
        #region String
        
        /// <summary>
        /// IMF setup handles .. as parent directory
        /// </summary>
        /// <param name="sdmxQuery"></param>
        /// <returns></returns>
        public static string TrimDots(this string sdmxQuery)
        {
            return sdmxQuery.EndsWith("/..") ? sdmxQuery.Substring(0, sdmxQuery.Length - 3) : sdmxQuery;
        }

        public static string AppendUriParams(this string uri, string @params)
        {
            if (@params == null || uri == null || uri.Length == 0)
                return uri;

            @params = @params.Trim('?');

            return uri.Contains('?')
                        ? uri + "&" + @params
                        : uri + "?" + @params;
         }
        #endregion

        #region Datareader
        public static T ColumnValue<T>(this IDataReader rd, string column, T def = default(T))
        {
            return rd[column].ColumnValue(def);
        }

        public static T ColumnValue<T>(this IDataReader rd, int columnIndex, T def = default(T))
        {
            return rd[columnIndex].ColumnValue(def);
        }

        public static T ColumnValue<T>(this DataRow row, string column, T def = default(T))
        {
            return row[column].ColumnValue(def);
        }

        public static T ColumnValue<T>(this DataRow row, int columnIndex, T def = default(T))
        {
            return row[columnIndex].ColumnValue(def);
        }

        private static T ColumnValue<T>(this object o, T def = default(T))
        {
            if (o == DBNull.Value || o == null)
                return def;

            if (o is T)
                return (T)o;

            return (T)Convert.ChangeType(o, typeof(T));
        }
        #endregion

        #region Dicitinary
        public static V GetOrAdd<K, V>(this IDictionary<K, V> dict, K key, Func<K,V> valueCreator)
        {
            V value;
            return dict.TryGetValue(key, out value) ? value : dict[key] = valueCreator(key);
        }
        #endregion

        #region Collections
        public static bool IsEmpty<T>(this IEnumerable<T> list)
        {
            return list == null || !list.Any();
        }

        public static List<T> AddItem<T>(this List<T> list, T item)
        {
            if (list != null)
                list.Add(item);

            return list;
        }

        public static List<KeyValuePair<K, V>> AddItem<K, V>(this List<KeyValuePair<K, V>> list, K key, V value)
        {
            return list.AddItem(new KeyValuePair<K, V>(key, value));
        }

        public static int MaxOrDefault<T>(this IEnumerable<T> coll, Func<T, int> func, int def = 0)
        {
            var elems = false;
            var max = int.MinValue;

            foreach (var elem in coll)
            {
                elems = true;
                var res = func(elem);
                if (res > max)
                {
                    max = res;
                }
            }
            return elems ? max : def;
        }
        public static void ForEach<T>(this IEnumerable<T> coll, Action<T> action)
        {
            foreach (T item in coll)
                action(item);
        }
        #endregion

        #region Exception
        public static bool IsSqlTimeoutError(this SqlException ex)
        {
            return ex.Number == -2 || ex.Number == 11;
        }
        #endregion

        public static int TimeLastOrder(this IDimension dim)
        {
            return dim.IsTimeSeries ? int.MaxValue : dim.Index;
        }

        internal static bool IsValue(this ColumnType type)
        {
	        return type == ColumnType.Value || type == ColumnType.Attribute;
        }

		#region Sdmx
		public static Dictionary<string, string> ToLocales(this IList<ITextTypeWrapper> names)
		{
			return names.ToDictionary(x => x.Locale, x => x.Value);
		}
        #endregion

        #region Sdmx Dates
        public static KeyValuePair<DateTime, string> ToSdmxPeriod<T>(this DateTime dt) where T : IPeriodicity, new()
        {
            return new KeyValuePair<DateTime, string>(dt, new T().ToString(dt));
        }
        #endregion
    }
}
