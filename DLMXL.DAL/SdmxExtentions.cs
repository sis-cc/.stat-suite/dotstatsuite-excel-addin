﻿using Org.Sdmx.Resources.SdmxMl.Schemas.V20.query;

namespace DLMXL.DAL
{
    internal static class SdmxExtentions
    {
        public static bool HasData(this TimeType type)
        {
            if (type == null)
                return false;

            return type.StartTime != null || type.EndTime != null;
        }
    }
}
