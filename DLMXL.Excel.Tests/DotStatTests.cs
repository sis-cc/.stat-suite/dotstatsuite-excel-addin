﻿using System;
using ExcelAddIn.DotStat;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using StatWorks.Library.Util;

namespace ExcelAddIn.Tests
{
    [TestClass]
    public class DotStatTests
    {
        [TestMethod]
        public void DeserializeQueries()
        {

var xml = @"<ArrayOfQueryCoreInfo xmlns:i=""http://www.w3.org/2001/XMLSchema-instance"" xmlns=""http://schemas.datacontract.org/2004/07/DotStat.QueryManager.Contract"">
    <QueryCoreInfo>
        <DatasetCode>TEST_MEI_JUL12</DatasetCode>
        <Id>1287</Id>
        <Name>Main Economic Indicators Europe Area Totals</Name>
        <QueryType>TeamQuery</QueryType>
    </QueryCoreInfo>
    <QueryCoreInfo>
        <DatasetCode>BERTRAND_COPY_NEW</DatasetCode>
        <Id>1215</Id>
        <Name>AA</Name>
        <QueryType>Public</QueryType>
    </QueryCoreInfo>
    <QueryCoreInfo>
        <DatasetCode>BERTRAND_CPY_QUERIES</DatasetCode>
        <Id>1875</Id>
        <Name>MyPublicQuery1</Name>
        <QueryType>Public</QueryType>
    </QueryCoreInfo>
</ArrayOfQueryCoreInfo>";

            var obj = DotStatQueries.Deserialize(xml);

            Assert.IsNotNull(obj.Queries);
            Assert.AreEqual(3, obj.Queries.Length);

            foreach(var q in obj.Queries)
                Console.WriteLine(q.Name);
        }

        [TestMethod]
        public void DesializeSingleQuery()
        {
            const string xml     = "<string xmlns=\"http://schemas.microsoft.com/2003/10/Serialization/\">BERTRAND_COPY_NEW/FRA/all?startTime=2010&amp;endTime=2011</string>";
            var str     = DotStatQuery.ExtractQuery(xml);

            Assert.AreEqual("BERTRAND_COPY_NEW/FRA/all?startTime=2010&endTime=2011", str);
        }

        [TestMethod]
        public void GetQueriesByUrl()
        {
            var url = Config.DotStatQueriesUrl;
            var xml = DotStatDirectAccess.GetWebServiceXml(url);
            Console.WriteLine(xml);
        }

        [TestMethod]
        public void GetSingleQueryByUrl()
        {
            var url = Config.DotStatSingleQueryUrl.F(1287);
            var xml = DotStatDirectAccess.GetWebServiceXml(url);
            Console.WriteLine(xml);
        }

        [TestMethod]
        public void GetQueriesAsDictionary()
        {
            var result = DotStatQueries.GetQueries();

            Assert.IsNotNull(result);

            Console.WriteLine(result.Keys.Count);
        }
    }
}
