﻿using System;
using System.Collections.Generic;
using DLMXL.Models;
using DLMXL.Models.Interfaces;

namespace ExcelAddIn.Tests.Moq
{
    public class TestDataAccess : IDataAccess
    {
        public bool IsExternal { get; }
        public bool IsMetadata { get; }
        public bool IsHistoricVersion { get; }
        public bool IsActionColumn { get; }
        public bool IsSaveWithAttributesOnly { get; }
        public bool IsSaveAllowed { get; }
        public bool IsDataSpace { get; }
        public bool IsFlagsOutput { get; }
        public bool IsDateOutput { get; }
        public bool IsQueryWithFullId { get; }
        public bool IsQueryWithAllAsEmpty { get; }

        public string Userinfo { get; }
        public bool IsAuthenticated { get; }
        public bool IsAuthRequired { get; }
		public string AuthId { get; }

        public virtual void BulkPreLoad()
        {}

        public IList<string> GetDataParams(ISdmxFilter filter, bool production, bool updateDate)
        {
            throw new NotImplementedException();
        }

        public virtual IList<IDatabase> LoadDatabases()
        {
            return new[] {SWDatabase.Build(this, 1, "DB", "DB Name")};
        }

        public virtual IList<IDataset> LoadDatasets(IDatabase db)
        {
            return new[] { new SWDataset(this, db, "DS", "DS Name") };
        }

        public virtual IList<IDimension> FetchDimensions(IDataset ds)
        {
            return new []
            {
                BuildDimension(ds, "DIM1", false),
                BuildDimension(ds, "DIM2", false)
            };
        }

        public virtual IList<IAttribute> FetchAttributes(IDataset ds)
        {
	        return null;
        }

		public IList<IQuery> LoadQueries(IDataset ds)
        {
            throw new NotImplementedException();
        }

        public virtual IList<IControlCodeMember> FetchControlCodes(IDataset ds)
        {
            throw new NotImplementedException();
        }

        public IMsd LoadMsd(IDataset ds)
        {
            return null;
        }

        public virtual object[,] LoadData(
	        OutputType outputType, 
	        ISdmxFilter sdmxFilter, 
	        bool production = true, 
	        LabelOutput labelOutput = LabelOutput.Code,
	        string language = null, 
	        bool flags = false, 
	        bool updateDate = false,
            bool withActionColumn = false
        )
        {
            throw new NotImplementedException();
        }

        public object[,] LoadMetadata(
            ISdmxFilter sdmxFilter,
            LabelOutput labelOutput = LabelOutput.Code,
            string language = null,
            bool withActionColumn = false
        )
        {
            throw new NotImplementedException();
        }

        public virtual string GetSdmxQueryString(ISdmxFilter filter, List<KeyValuePair<string, string>> @params = null)
        {
            throw new NotImplementedException();
        }

        public string GetMetaDataQueryString(ISdmxFilter filter)
        {
            throw new NotImplementedException();
        }

        public virtual T InitFilter<T>(T filter, IList<KeyValuePair<string, string>> @params = null) where T : ISdmxFilter
        {
            throw new NotImplementedException();
        }

        public IList<KeyValuePair<string, string>> GetLanguages(IDataset ds)
        {
	        throw new NotImplementedException();
        }

        public Tuple<string,int> Save(
            IDataset dataset, 
            ICellAccess source, 
            OutputType outputType, 
            bool withCodes, 
            bool withLabels, 
            bool withFlags,
            bool isMetadata,
            bool withActionColumn,
            SaveInterruptHander dialogInterrupt

        )
        {
	        throw new NotImplementedException();
        }

        public void ClearObjectCache()
        {
	        throw new NotImplementedException();
        }

        public IDimension BuildDimension(IDataset ds, string code, bool isTime)
        {
            return SWDimension.Build(
	            ds, 
	            code,
	            "Test DIM",
	            new Dictionary<string, string>()
	            {
		            {"en", code +  " Test DIM"}
	            },
	            isTime 
	        );
        }

        #region Auth

        public virtual void LogOut()
        {
	        throw new NotImplementedException();
        }

        public IAuth Auth => throw new NotImplementedException();

        #endregion

        #region Query Dialog

        public virtual IList<KeyValuePair<string, string>> GetConnectionStrings(ISdmxFilter filter, bool production, bool updateDate) => throw new NotImplementedException();

        #endregion
    }
}
