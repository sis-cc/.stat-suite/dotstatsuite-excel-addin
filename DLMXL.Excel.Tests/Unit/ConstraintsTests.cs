﻿using System;
using System.Globalization;
using System.IO;
using System.Linq;
using DLMXL.Models.Interfaces;
using NUnit.Framework;
using DLMXL.DAL.Access.Sdmx;

namespace ExcelAddIn.Tests
{
    [TestFixture]
    public class ConstraintsTests
	{
	    private IDataset _dataset;

		public ConstraintsTests()
		{
			_dataset = new TestSdmxApi().GetDatasetFromFile(Path.GetFullPath(@"Samples\MILLED_RICE,TN1,1.0_constraints.xml"));
		}

		[Test]
        public void ConstraintsExtraction()
        {
            var dimensions = _dataset.Dimensions;

			Assert.IsNotNull(dimensions);
			Assert.AreEqual(4, dimensions.Count);
            Assert.AreEqual(1, dimensions.First(x=>x.Code == "FREQ").Members.Count);
            Assert.AreEqual(1, dimensions.First(x=>x.Code == "UNIT_MEASURE").Members.Count);
            Assert.AreEqual(10, dimensions.First(x=>x.Code == "REF_AREA").Members.Count);

            var timeDim = dimensions.First(x => x.IsTimeSeries);

            Assert.IsNotNull(timeDim.Constraint);
            Assert.IsNotNull(timeDim.Constraint.TimeRange);
            Assert.AreEqual(new DateTime(2014,1,1), timeDim.Constraint.TimeRange.StartDate.Date);
            Assert.IsTrue(timeDim.Constraint.TimeRange.StartDate.IsInclusive);
            Assert.AreEqual(new DateTime(2017,12,31), timeDim.Constraint.TimeRange.EndDate.Date);
            Assert.IsTrue(timeDim.Constraint.TimeRange.EndDate.IsInclusive);
        }

        [TestCase(Frequency.Anually, "2019-12-31", 1, "2019-01-01", "2019")]
        [TestCase(Frequency.Anually,"2019-12-31", 5, "2015-01-01", "2015")]
        [TestCase(Frequency.Quarterly, "2020-03-31", 1, "2020-01-01", "2020-Q1")]
        [TestCase(Frequency.Quarterly, "2020-06-30", 1, "2020-04-01", "2020-Q2")]
        [TestCase(Frequency.Quarterly, "2020-03-31", 5, "2019-01-01", "2019-Q1")]
        [TestCase(Frequency.Monthly, "2020-03-31", 1, "2020-03-01", "2020-03")]
        [TestCase(Frequency.Monthly, "2020-03-31", 5, "2019-11-01", "2019-11")]
        [TestCase(Frequency.Daily, "2020-03-31", 1, "2020-03-31", "2020-03-31")]
        [TestCase(Frequency.Daily, "2020-03-31", 5, "2020-03-27", "2020-03-27")]
        [TestCase(Frequency.Semestrial, "2020-06-30", 1, "2020-01-01", "2020-S1")]
        [TestCase(Frequency.Semestrial, "2020-06-30", 5, "2018-01-01", "2018-S1")]
        [TestCase(Frequency.Semestrial, "2020-06-30", 4, "2018-07-01", "2018-S2")]
        [TestCase(Frequency.Weekly, "2024-08-04", 1, "2024-07-29", "2024-W31")]
        [TestCase(Frequency.Weekly, "2024-08-04", 3, "2024-07-15", "2024-W29")]
        public void LastPeriodTest(Frequency frequency, string sdmxTime, int lastXPeriod, string expectedDate, string expectedSdmx)
        {
            var startDate = DateTime.ParseExact(sdmxTime, "yyyy-MM-dd", CultureInfo.InvariantCulture.DateTimeFormat);
            var adjusted = SdmxApi.AdjustDate(frequency, startDate, lastXPeriod);

            Assert.AreEqual(expectedSdmx, adjusted.Value);
            Assert.AreEqual(DateTime.ParseExact(expectedDate, "yyyy-MM-dd", CultureInfo.InvariantCulture.DateTimeFormat), adjusted.Key);
        }
	}
}
