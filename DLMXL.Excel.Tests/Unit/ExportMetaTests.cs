﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using DLMXL.DAL.Access;
using DLMXL.Models;
using DLMXL.Models.Interfaces;
using Microsoft.Office.Interop.Excel;
using NUnit.Framework;
using DLMXL.DAL.Access.Sdmx;
using ExcelAddIn.UI.Meta;

namespace ExcelAddIn.Tests
{
    [TestFixture]
    public class ExportMetaTests
    {
	    private IDataset _dataset;
        private static Workbook TestBook;

		public ExportMetaTests()
		{
			TestBook = new Application().Workbooks.Open(Path.GetFullPath(@"Samples\Metadata.xlsx"));
			_dataset = new TestSdmxApi(true).GetDatasetFromFile(Path.GetFullPath(@"Samples\Metadata.xml"));
		}

		[TestFixtureTearDown]
		public void CleanUp()
		{
			TestBook.Close();
			TestBook = null;
		}

		[TestCase("B3", true, false, false, 4, 8)]
        [TestCase("B3", false, true, false, 4, 8)]
        [TestCase("B12", true, true, false, 4, 8)]
        [TestCase("B20", true, false, true, 4, 9)]
        [TestCase("B20", false, true, true, 4, 9)]
        [TestCase("B30", true, true, true, 4, 9)]
        public void Flat(string startCell, bool isCode, bool isLabel, bool isActionColumn, int expectedRows, int expectedColumns)
		{
            var sheet = (Worksheet)TestBook.Worksheets["metadata"];

            var data = BaseDataAccess.ObservationReader(
                _dataset,
                new ExcelCellAccess(sheet.Range[startCell].CurrentRegion),
                OutputType.Flat,
                isCode,
                isLabel,
                false,
                isMetadata:true,
                isActionColumn
            )
            .ToArray();

            Assert.AreEqual(expectedRows, data.Length);
            Assert.AreEqual(expectedColumns, data[0].Length);

            Console.WriteLine(string.Join(",", data[0]));
        }

        [TestCase("B3", false)]
        [TestCase("B20", true)]
        public void TestSave(string startCell, bool isActionColumn)
        {
            var sheet = (Worksheet)TestBook.Worksheets["metadata"];

            var data = BaseDataAccess.ObservationReader(
                    _dataset,
                    new ExcelCellAccess(sheet.Range[startCell].CurrentRegion),
                    OutputType.Flat,
                    true,
                    false,
                    false,
                    isMetadata: true,
                    isActionColumn
                )
                .ToArray();

            var file = BaseDataAccess.SaveMetaCsv(_dataset, data, isActionColumn, out var observationCount);
            var expectedRows = data.Length + 1;
            var expectedColumns = data[0].Length + 2;

            foreach (var line in BaseDataAccess.ReadCsv(file))
            {
                Assert.AreEqual(expectedColumns, line.Length);
                expectedRows--;
            }

            Assert.AreEqual(data.Length, observationCount);
            Assert.AreEqual(0, expectedRows);

            Console.WriteLine(file);

            //File.Delete(file);
        }

        [TestCase("not json content", "en,fr", 2, "not json content")]
        [TestCase("en:\"some English content\",fr: \"some French content\"", "en,fr", 2, "some English content")]
        public void ParseMultiLingualContent(string json, string locales, int expectedCount, string expectedEnValue)
        {
            var keys = MetaUtils
                        .Parse(json, locales.Split(','))
                        .ToArray();

            Assert.IsNotNull(keys);
            Assert.AreEqual(expectedCount, keys.Count());
            Assert.AreEqual(expectedEnValue, keys.First(x=>x.Key == "en").Value);
        }


        [Test]
        public void TestJsonSerialize()
        {
            var value = MetaUtils.ToJson(new[]
            {
                new KeyValuePair<string, string>("en", "some \"English\" string\n and multiline"),
                new KeyValuePair<string, string>("fr", "some French string\n and special chars ,;:\t here")
            });

            Console.WriteLine(value);
        }

        [TestCase("A1", 1)]
        [TestCase("C1", 3)]
        [TestCase("Z1", 26)]
        [TestCase("AZ1", 52)]
        public void TestCellAddressConvert(string cell, int expectedColumnIndex)
        {
            Assert.AreEqual(expectedColumnIndex, MetaUtils.RangeAddressToColumnIndex(cell));
        }
    }
}
