﻿using System;
using System.IO;
using System.Linq;
using DLMXL.DAL.Access;
using DLMXL.Models;
using DLMXL.Models.Interfaces;
using Microsoft.Office.Interop.Excel;
using NUnit.Framework;
using DLMXL.DAL.Access.Sdmx;

namespace ExcelAddIn.Tests
{
    [TestFixture]
    public class ExportTests
    {
	    private IDataset _dataset;
        private static Workbook TestBook;

		public ExportTests()
		{
			TestBook = new Application().Workbooks.Open(Path.GetFullPath(@"Samples\Attribute_test_1.xlsx"));
			_dataset = new TestSdmxApi().GetDatasetFromFile(Path.GetFullPath(@"Samples\Attribute_test_1.xml"));
		}

		[TestFixtureTearDown]
		public void CleanUp()
		{
			TestBook.Close();
			TestBook = null;
		}

		// without action column
		[TestCase("A1", true, false, false, false, 11)]
		[TestCase("N1", false, true, false, false, 11)]
		[TestCase("A30", true, false, true, false, 28)]
		[TestCase("A55", true, true, true, false, 28)]
		// with action column
        [TestCase("A1", true, false, false, true, 12)]
        [TestCase("O1", false, true, false, true, 12)]
        [TestCase("A30", true, false, true, true, 29)]
        [TestCase("A55", true, true, true, true, 29)]
		public void Flat(string startCell, bool isCode, bool isLabel, bool isFlags, bool isActionColumn, int expectedColumns)
		{
			var sheet = (Worksheet)TestBook.Worksheets[isActionColumn ? "flat_with_action" : "flat"];

			var data = BaseDataAccess.ObservationReader(
				_dataset,
				new ExcelCellAccess(sheet.Range[startCell].CurrentRegion),
				OutputType.Flat,
				isCode,
				isLabel,
				isFlags,
				false,
				isActionColumn
			)
			.ToArray();

			Assert.AreEqual(22, data.Length);
			Assert.AreEqual(expectedColumns, data[0].Length);

			Console.WriteLine(string.Join(",", data[21]));
		}

		[TestCase("A1", true, false, false, 11)]
		[TestCase("Q1", false, true, false, 11)]
		[TestCase("A30", true, false, true, 28)]
		[TestCase("A57", true, true, true, 28)]
		public void TimeSeriesAcross(string startCell, bool isCode, bool isLabel, bool isFlags, int expectedColumns)
		{
			var sheet = (Worksheet)TestBook.Worksheets["series horizontal"];

			var data = BaseDataAccess.ObservationReader(
					_dataset,
					new ExcelCellAccess(sheet.Range[startCell].CurrentRegion),
					OutputType.TimeSeriesAcross,
					isCode,
					isLabel,
					isFlags,
					false,
					false
				)
				.ToArray();

			Assert.AreEqual(88, data.Length);
			Assert.AreEqual(expectedColumns, data[0].Length);

			Console.WriteLine(string.Join(",", data[0]));
			Console.WriteLine(string.Join(",", data[87]));
		}

		[TestCase("A1", true, false, false, 11)]
		[TestCase("A10", false, true, false, 11)]
		[TestCase("A30", true, false, true, 28)]
		[TestCase("A40", true, true, true, 28)]
		public void TimeSeriesDown(string startCell, bool isCode, bool isLabel, bool isFlags, int expectedColumns)
		{
			var sheet = (Worksheet)TestBook.Worksheets["series vertical"];

			var data = BaseDataAccess.ObservationReader(
					_dataset,
					new ExcelCellAccess(sheet.Range[startCell].CurrentRegion),
					OutputType.TimeSeriesDown,
					isCode,
					isLabel,
					isFlags,
					false,
					false
				)
				.ToArray();

			Assert.AreEqual(88, data.Length);
			Assert.AreEqual(expectedColumns, data[0].Length);

			Console.WriteLine(string.Join(",", data[0]));
			Console.WriteLine(string.Join(",", data[87]));
		}

		[TestCase("A30", true, false)]
		[TestCase("A30", true, true)]
		public void TestSave(string startCell, bool withFlags, bool isActionColumn)
		{
			var sheet = (Worksheet) TestBook.Worksheets[isActionColumn ? "flat_with_action" : "flat"];

			var data = BaseDataAccess.ObservationReader(
					_dataset,
					new ExcelCellAccess(sheet.Range[startCell].CurrentRegion),
					OutputType.Flat,
					true,
					false,
                    withFlags,
					false,
					isActionColumn
				)
				.ToArray();

			var file = BaseDataAccess.SaveCsv(_dataset, data, withFlags, isActionColumn, out _);
			var expectedRows = data.Length + 1;
			var expectedColumns = data[0].Length + 2;

			using (var csv = new StreamReader(file))
			{
				string line;

				while ((line = csv.ReadLine()) != null)
				{
					expectedRows--;

					var columns = line.Split(',');

					Assert.AreEqual(expectedColumns, columns.Length);
				}
			}

			Assert.AreEqual(0, expectedRows);

            Console.WriteLine(file);
			//File.Delete(file);
		}

		[Test, Explicit]
		public void TestSaveBack()
		{
			var sheet = (Worksheet)TestBook.Worksheets["flat"];
			var output = OutputType.Flat;
			var cell = "A30";
			var isCode = true;
			var isLabel = false;
			var isFlags = true;

			var connection = new GenericRestApi(
				"",
				"", 
				"", 
				"", 
				"", 
				isExternal:true,
				isMetadata:false,
                isHistoricVersion: false,
				"http://alexpc:7081/1.2",
				"http://alexpc:8080/auth/realms/siscc/protocol/openid-connect/token",
				"stat-suite"
			);

			// connection.Authenticate("test-admin", "admin");

			var result = connection.Save(
				_dataset,
				new ExcelCellAccess(sheet.Range[cell].CurrentRegion),
				output,
				isCode,
				isLabel,
				isFlags,
				false,
				false,
				null
			);
		}

		[TestCase("A1", "E10", true, false, false, 1)] // only 1 cell is selected
		[TestCase("A1", "C6:F12", true, false, false, 7)]
		[TestCase("A1", "L1:L22", true, false, false, 21)]
		[TestCase("A1", "A1:L1", true, false, false, 22)] // selected only header
		[TestCase("A1", "A25:A26", true, false, false, 22)] // out of the scope of main range
		[TestCase("A1", "C3:C5,E10:E14", true, false, false, 8)] //multiple selection NB! comma as a separator is region specific setting in excel
		public void SelectionFlatLayout(string startCell, string selectedRange, bool isCode, bool isLabel, bool isFlags, int expectedObservations)
		{
			var sheet = (Worksheet)TestBook.Worksheets["flat"];
			var range = sheet.Range[startCell].CurrentRegion;
			var selection = sheet.Range[selectedRange];

			var data = BaseDataAccess.ObservationReader(
					_dataset,
					new ExcelCellAccess(range, selection),
					OutputType.Flat,
					isCode,
					isLabel,
					isFlags,
					false,
					false
				)
				.ToArray();

			Assert.AreEqual(expectedObservations, data.Length);
		}

// region with codes only (no labels & no flags)
		[TestCase("A1", "E10", true, false, false, 4)] // only 1 cell is selected
		[TestCase("A1", "K3", true, false, false, 1)]
		[TestCase("A1", "C6:F12", true, false, false, 28)] // selected only non value columns => all years of a row should be saved
		[TestCase("A1", "K5:K10", true, false, false, 6)] // selected 1 year
		[TestCase("A1", "H5:K10", true, false, false, 6)]
		[TestCase("A1", "L5:L10", true, false, false, 6)]
		[TestCase("A1", "K5:L10", true, false, false, 12)] // selected 2 years
		[TestCase("A1", "H5:L10", true, false, false, 12)]
		[TestCase("A1", "M5:N10", true, false, false, 12)]
		[TestCase("A1", "K22:M24", true, false, false, 9)] // selected 3 years
		[TestCase("A1", "K3:N3", true, false, false, 4)] // selected 4 years
		[TestCase("A1", "A1:L1", true, false, false, 88)] // selected only header
		[TestCase("A1", "A25:A26", true, false, false, 88)] // out of the scope of the main data range
		[TestCase("A1", "E3:G5,J7:K8,M11:N14", true, false, false, 22)] // multiple selections: 12 + 2 + 8
// region with labels + codes + flags
		[TestCase("A57", "F66", true, true, true, 4)] // only 1 cell is selected
		[TestCase("A57", "F63:I66", true, true, true, 16)]  // selected only non value columns => all years of a row should be saved
		[TestCase("A57", "Q62:T67", true, true, true, 6)] // selected 1 year
		[TestCase("A57", "T62:T67", true, true, true, 6)]
		[TestCase("A57", "T62:AK67", true, true, true, 6)]
		[TestCase("A57", "Z62:AA67", true, true, true, 6)]
		[TestCase("A57", "AK62:AL67", true, true, true, 12)] // selected 2 years
		[TestCase("A57", "Q62:BC67", true, true, true, 12)]
		[TestCase("A57", "AL57:AT58", true, true, true, 88)] // selected only header
		[TestCase("A57", "C83:E87", true, true, true, 88)] // out of the scope of the main data range
		[TestCase("A57", "H63:J65,AQ69:AR70,BU76:BV80", true, true, true, 24)] // multiple selections: 12 + 2 + 10
		public void SelectionTimeAcrossLayout(string startCell, string selectedRange, bool isCode, bool isLabel, bool isFlags, int expectedObservations)
		{
			var sheet = (Worksheet)TestBook.Worksheets["series horizontal"];
			var range = sheet.Range[startCell].CurrentRegion;
			var selection = sheet.Range[selectedRange];

			var data = BaseDataAccess.ObservationReader(
					_dataset,
					new ExcelCellAccess(range, selection),
					OutputType.TimeSeriesAcross,
					isCode,
					isLabel,
					isFlags,
					false,
					false
				)
				.ToArray();

			Assert.AreEqual(expectedObservations, data.Length);
		}


// region with codes only (no labels & no flags)
		[TestCase("A1", "D6", true, false, false, 1)] // only 1 cell is selected
		[TestCase("A1", "B3:E3", true, false, false, 4)] // selected 1 year
		[TestCase("A1", "M5:O5", true, false, false, 3)]
		[TestCase("A1", "F3:G3,V3:W3", true, false, false, 4)]
		[TestCase("A1", "C3:C4", true, false, false, 2)] // selected 2 years
		[TestCase("A1", "F3:K4", true, false, false, 12)]
		[TestCase("A1", "M5:N6", true, false, false, 4)]
		[TestCase("A1", "N4:S6", true, false, false, 18)] // selected 3 years
		[TestCase("A1", "W3:W6", true, false, false, 4)] // selected 4 years
		[TestCase("A1", "B1:W2", true, false, false, 88)] // selected only header
		[TestCase("A1", "Y4", true, false, false, 88)] // out of the scope of the main data range
		[TestCase("A1", "B3:B6,K3:M5,P5:W6", true, false, false, 29)] // multiple selections: 4 + 9 + 16

// region with labels + codes + flags
		[TestCase("A40", "E61", true, true, true, 1)] // only 1 cell is selected
		[TestCase("A40", "B60:E60", true, true, true, 1)] // selected 1 year
		[TestCase("A40", "K60:T60", true, true, true, 2)]
		[TestCase("A40", "AI62:BZ62", true, true, true, 4)]
		[TestCase("A40", "F61:G62", true, true, true, 2)] // selected 2 years
		[TestCase("A40", "S60:T61", true, true, true, 4)]
		[TestCase("A40", "B40:L59", true, true, true, 88)] // selected only header
		[TestCase("A40", "D70", true, true, true, 88)] // out of the scope of the main data range
		[TestCase("A40", "J60:K63,AK60:AL62,NP60:OG63", true, true, true, 14)] // multiple selections: 4 + 6 + 4
		public void SelectionTimeDownLayout(string startCell, string selectedRange, bool isCode, bool isLabel, bool isFlags, int expectedObservations)
		{
			var sheet = (Worksheet)TestBook.Worksheets["series vertical"];
			var range = sheet.Range[startCell].CurrentRegion;
			var selection = sheet.Range[selectedRange];

			var data = BaseDataAccess.ObservationReader(
					_dataset,
					new ExcelCellAccess(range, selection),
					OutputType.TimeSeriesDown,
					isCode,
					isLabel,
					isFlags,
					false,
					false
				)
				.ToArray();

			Assert.AreEqual(expectedObservations, data.Length);
		}

	}
}
