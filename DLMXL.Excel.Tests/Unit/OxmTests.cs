﻿using System;
using System.Collections.Generic;
using System.Linq;
using DLMXL.Models;
using DLMXL.Models.Interfaces;
using ExcelAddIn.DAL;
using ExcelAddIn.Domain;
using ExcelAddIn.Domain.Wizard;
using ExcelAddIn.Mappers;
using ExcelAddIn.Tests.Moq;
using NUnit.Framework;

namespace ExcelAddIn.Tests
{
    [TestFixture]
    public class OxmTests
    {
        [Test]
        public void ArrayMappingTest()
        {
            var dataAccess  = new TestDataAccess();
            var source      = OecdDataSource.Build("Test", "Test", dataAccess);
            var db          = source.Databases.First();
            var ds          = db.Datasets.First();

            var connection = new DataConnection()
            {
                Id = Guid.NewGuid().ToString(),
                Source = source,
                StartCell = "A1",
                EndCell = "C3",
                IsProduction = true,
                IsFlags = true,
                IsMetadata = true,
                IsActionColumn = true,
                IsUpdateData = true,
                LabelOutput = LabelOutput.French,
                Filter = new SdmxFilter(SWFilter.BuildFilter(ds))
                {
                    SourceType = FavoriteTreeItemType.Dataset, 
                    SourceId = ds.Uid,
                    HistoricVersionType = HistoricVersionType.AsOf
                },
                LastRefreshTime = DateTime.Now,
                OutputType = OutputType.TimeSeriesAcross
            };

            var xml = DataConnectionArrayMap.Serialize(new[] { connection, connection });

            Console.Write(xml);

            var output = DataConnectionArrayMap.Deserialize(xml);

            Assert.IsNotNull(output);
            Assert.AreEqual(2, output.Count());

            var connection2 = output.First();

            Assert.AreEqual(connection.Id, connection2.Id);
            Assert.AreEqual(connection.Source.Name, connection2.Source.Name);
            Assert.AreEqual(connection.StartCell, connection2.StartCell);
            Assert.AreEqual(connection.EndCell, connection2.EndCell);
            Assert.AreEqual(connection.IsProduction, connection2.IsProduction);
            Assert.AreEqual(connection.IsFlags, connection2.IsFlags);
            Assert.AreEqual(connection.IsMetadata, connection2.IsMetadata);
            Assert.AreEqual(connection.IsActionColumn, connection2.IsActionColumn);
            Assert.AreEqual(connection.IsUpdateData, connection2.IsUpdateData);
            Assert.AreEqual(connection.LabelOutput, connection2.LabelOutput);
            Assert.AreEqual(connection.OutputType, connection2.OutputType);

            Assert.IsNotNull(connection2.Filter.FilterBuilder);
            Assert.AreEqual(connection.Filter.SdmxQuery, connection2.Filter.SdmxQuery);
            Assert.AreEqual(connection.Filter.SWFilter, connection2.Filter.SWFilter);
            Assert.AreEqual(connection.Filter.StartTime, connection2.Filter.StartTime);
            Assert.AreEqual(connection.Filter.EndTime, connection2.Filter.EndTime);
            Assert.AreEqual(connection.Filter.UpdatedAfter, connection2.Filter.UpdatedAfter);
            Assert.AreEqual(connection.Filter.SourceId, connection2.Filter.SourceId);
            Assert.AreEqual(connection.Filter.SourceType, connection2.Filter.SourceType);
            Assert.AreEqual(connection.Filter.HistoricVersionType, connection2.Filter.HistoricVersionType);
        }

        [Test]
        public void FavoritesTest()
        {
            var collection = Enum
                .GetNames(typeof(Domain.FavoriteTreeItemType))
                .Select((level, i) => new KeyValuePair<string, HashSet<string>>(level, new HashSet<string>(Enumerable.Range(i * 5, 5).Select(x => Convert.ToString(x)))));

            var input   = new FavoritesCache(collection);
            var xml     = FavoritesMap.Serialize(input);

            Console.Write(xml);

            var output = FavoritesMap.Deserialize(xml);

            Assert.AreEqual(output.Count, input.Count);

            for(var i=0;i<input.Count;i++)
            {
                Assert.AreEqual(input[i].Key, output[i].Key);
                Assert.AreEqual(input[i].Value.Count, output[i].Value.Count);
                Assert.AreEqual(input[i].Value.First(), input[i].Value.First());
                Assert.AreEqual(input[i].Value.Last(), input[i].Value.Last());
            }
        }
    }
}
