﻿<?xml version="1.0" encoding="utf-8"?>
<configuration>
  <configSections>
    <section name="dlmxlConfig" type="DLMXL.DAL.Config.DlmExcelAddinConfigSection, DLMXL.DAL" />
  </configSections>
  <dlmxlConfig 
      title=".Stat DLM" 
      helpLink="https://sis-cc.gitlab.io/dotstatsuite-documentation/using-dlm-excel-addin" 
      languages="en,fr"
      authRedirectUri="https://dotstat-suite-addin.oecd.org"
  >
    <connections>
        <StatworksConnection 
        id="Statworks"
        name="Statworks"
        dsn="STATWORKS"
        server="statw-sql19-1.main.oecd.org"
        db="StatworksMain"/>
      
      <EurostatConnection 
        id="Eurostat" 
        name="Eurostat" 
        baseUri="https://ec.europa.eu/eurostat/api/dissemination/sdmx/2.1/"/>
    
      <IMFConnection 
        id="IMF" 
        name="IMF" 
        baseUri="http://dataservices.imf.org/REST/"/>

      <GenericRestConnection
        id="ILOStat"
        name="ILOStat"
        baseUri="https://sdmx.ilo.org/rest/"
        flowQuery="dataflow/all/all/latest"
        structureQuery="dataflow/{1}/{0}/{2}?references=all&amp;detail=referencepartial"
        dataQuery="data/{0}"
        isExternal="true"/>

      <DotstatV7SdmxConnection
        id="UnescoV2"
        name="Unesco"
        baseUri="http://data.uis.unesco.org/RestSDMX/sdmx.ashx/" />
      
      <GenericRestConnection
        id="UNSD"
        name="UNSD"
        baseUri="https://data.un.org/ws/rest/" 
        flowQuery="dataflow/all/all/latest" 
        structureQuery="dataflow/{1}/{0}/{2}?references=all&amp;detail=referencepartial" 
        dataQuery="data/{0}"
        isExternal="true"/>

      <GenericRestConnection
        id="ECB"
        name="ECB"
        baseUri="https://sdw-wsrest.ecb.europa.eu/service/"
        flowQuery="dataflow/all/all/latest"
        structureQuery="datastructure/all/{0}/{2}/?references=children"
        dataQuery="data/{0}" 
        isExternal="true"/>
        
        <WorldbankConnection
        id="Worldbank"
        name="Worldbank"
        baseUri="https://api.worldbank.org/v2/sdmx/rest/"
        flowQuery="dataflow"
        structureQuery="datastructure/{1}/{0}?references=children"
        dataQuery="data/{0}"
        isExternal="true"/>

      <GenericRestConnection
          id="oecd-disseminate-final"
          name="Disseminate final"
          baseUri="https://sdmx.oecd.org/public/rest/"
          flowQuery="dataflow/all/all/latest"
          structureQuery="dataflow/{1}/{0}/{2}?references=all&amp;detail=referencepartial"
          dataQuery="data/{0}"
          isExternal="false"
          transferUri="https://dotstat-store.oecd.org/transfer/2"
          saveDataspace="DisseminateFinal"
          authUri="https://signin.oecd.org/adfs"
          authClientId="dd05d370-38c3-4c06-b5b0-7b010daa1fbc"
          isMetadata="true"/>

      <GenericRestConnection
          id="oecd-disseminate-staging"
          name="Disseminate staging"
          baseUri="https://dotstat-store.oecd.org/disseminate-staging/rest/"
          flowQuery="dataflow/all/all/latest"
          structureQuery="dataflow/{1}/{0}/{2}?references=all&amp;detail=referencepartial"
          dataQuery="data/{0}"
          isExternal="false"
          transferUri="https://dotstat-store.oecd.org/transfer/2"
          saveDataspace="DisseminateStaging"
          authUri="https://signin.oecd.org/adfs"
          authClientId="dd05d370-38c3-4c06-b5b0-7b010daa1fbc"
          isMetadata="true"/>

      <GenericRestConnection
          id="oecd-disseminate-archive"
          name="Disseminate archive"
          baseUri="https://dotstat-store.oecd.org/disseminate-archive/rest/"
          flowQuery="dataflow/all/all/latest"
          structureQuery="dataflow/{1}/{0}/{2}?references=all&amp;detail=referencepartial"
          dataQuery="data/{0}"
          isExternal="false"
          transferUri="https://dotstat-store.oecd.org/transfer/2"
          saveDataspace="DisseminateArchive"
          authUri="https://signin.oecd.org/adfs"
          authClientId="dd05d370-38c3-4c06-b5b0-7b010daa1fbc"
          isMetadata="true"/>

      <GenericRestConnection
          id="oecd-design"
          name="Design"
          baseUri="https://dotstat-store.oecd.org/design/rest/"
          flowQuery="dataflow/all/all/latest"
          structureQuery="dataflow/{1}/{0}/{2}?references=all&amp;detail=referencepartial"
          dataQuery="data/{0}"
          isExternal="false"
          transferUri="https://dotstat-store.oecd.org/transfer/2"
          saveDataspace="Design"
          authUri="https://signin.oecd.org/adfs"
          authClientId="dd05d370-38c3-4c06-b5b0-7b010daa1fbc"
          isMetadata="true"/>

      <GenericRestConnection
          id="oecd-collect"
          name="Collect"
          baseUri="https://sdmx.oecd.org/collect/rest/"
          flowQuery="dataflow/all/all/latest"
          structureQuery="dataflow/{1}/{0}/{2}?references=all&amp;detail=referencepartial"
          dataQuery="data/{0}"
          isExternal="false"
          transferUri="https://dotstat-store.oecd.org/transfer/2"
          saveDataspace="Collect"
          authUri="https://signin.oecd.org/adfs"
          authClientId="dd05d370-38c3-4c06-b5b0-7b010daa1fbc"
          isMetadata="true"/>

      <GenericRestConnection
          id="oecd-process"
          name="Process"
          baseUri="https://dotstat-store.oecd.org/process/rest/"
          flowQuery="dataflow/all/all/latest"
          structureQuery="dataflow/{1}/{0}/{2}?references=all&amp;detail=referencepartial"
          dataQuery="data/{0}"
          isExternal="false"
          transferUri="https://dotstat-store.oecd.org/transfer/2"
          saveDataspace="Process"
          authUri="https://signin.oecd.org/adfs"
          authClientId="dd05d370-38c3-4c06-b5b0-7b010daa1fbc"
          isMetadata="true"/>

      <GenericRestConnection
          id="eco-collect"
          name="ECO Collect"
          baseUri="https://dotstat-eco.oecd.org/collect/rest/"
          flowQuery="dataflow/all/all/latest"
          structureQuery="dataflow/{1}/{0}/{2}?references=all&amp;detail=referencepartial"
          dataQuery="data/{0}"
          isExternal="false"
          transferUri="https://dotstat-eco.oecd.org/transfer/2"
          saveDataspace="EcoCollect"
          authUri="https://signin.oecd.org/adfs"
          authClientId="dd05d370-38c3-4c06-b5b0-7b010daa1fbc"
          isMetadata="true"/>

      <GenericRestConnection
          id="eco-process"
          name="ECO Process"
          baseUri="https://dotstat-eco.oecd.org/process/rest/"
          flowQuery="dataflow/all/all/latest"
          structureQuery="dataflow/{1}/{0}/{2}?references=all&amp;detail=referencepartial"
          dataQuery="data/{0}"
          isExternal="false"
          transferUri="https://dotstat-eco.oecd.org/transfer/2"
          saveDataspace="EcoProcess"
          authUri="https://signin.oecd.org/adfs"
          authClientId="dd05d370-38c3-4c06-b5b0-7b010daa1fbc"
          isMetadata="true"/>

        <GenericRestConnection
            id="dcd-disseminate-final"
            name="DCD Disseminate final"
            baseUri="https://sdmx.oecd.org/dcd-public/rest/"
            flowQuery="dataflow/all/all/latest"
            structureQuery="dataflow/{1}/{0}/{2}?references=all&amp;detail=referencepartial"
            dataQuery="data/{0}"
            isExternal="false"
            transferUri="https://dotstat-store.oecd.org/transfer/2"
            saveDataspace="DcdDisseminateFinal"
            authUri="https://signin.oecd.org/adfs"
            authClientId="dd05d370-38c3-4c06-b5b0-7b010daa1fbc"
            isMetadata="true"/>
        
        <GenericRestConnection
            id="sti-disseminate-final"
            name="STI Disseminate final"
            baseUri="https://sdmx.oecd.org/sti-public/rest/"
            flowQuery="dataflow/all/all/latest"
            structureQuery="dataflow/{1}/{0}/{2}?references=all&amp;detail=referencepartial"
            dataQuery="data/{0}"
            isExternal="false"
            transferUri="https://dotstat-store.oecd.org/transfer/2"
            saveDataspace="StiDisseminateFinal"
            authUri="https://signin.oecd.org/adfs"
            authClientId="dd05d370-38c3-4c06-b5b0-7b010daa1fbc"
            isMetadata="true"/>
        
    </connections>
  </dlmxlConfig>
  <startup>
      <supportedRuntime version="v4.0" sku=".NETFramework,Version=v4.7.2" />
  </startup>
    <runtime>
        <assemblyBinding xmlns="urn:schemas-microsoft-com:asm.v1">
            <dependentAssembly>
                <assemblyIdentity name="Newtonsoft.Json" publicKeyToken="30ad4fe6b2a6aeed" culture="neutral" />
                <bindingRedirect oldVersion="0.0.0.0-13.0.0.0" newVersion="13.0.0.0" />
            </dependentAssembly>
        </assemblyBinding>
    </runtime>
</configuration>