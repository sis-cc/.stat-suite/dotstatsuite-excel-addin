﻿using DLMXL.DAL.Config;
using ExcelAddIn.Domain;
using cfg = System.Configuration.ConfigurationManager;

namespace ExcelAddIn
{
    public static class Config
    {
        public static readonly DlmExcelAddinConfigSection DW = (DlmExcelAddinConfigSection) cfg.GetSection("dlmxlConfig");

        public static readonly UserSettings Settings = UserSettings.Load() ?? new UserSettings();
    }
}
