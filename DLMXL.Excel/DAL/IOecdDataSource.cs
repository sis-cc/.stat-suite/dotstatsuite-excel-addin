﻿using System.Collections.Generic;
using DLMXL.Models;
using DLMXL.Models.Interfaces;
using ExcelAddIn.Domain;

namespace ExcelAddIn.DAL
{
    public interface IOecdDataSource
    {
	    IDataAccess DataAccess { get; }
        string Code { get; }
        string Name { get; }
        bool IsExternal { get; }
        bool IsSaveAllowed { get; }
        bool IsDataSpace { get; }
        bool IsLoaded { get; }
        bool IsFlagsOutput { get; }
        bool IsDateOutput { get; }

        IList<IFavoriteTreeItem> GetAll();

        IFavoriteTreeItem this[int index] { get; }

        IFavoriteTreeItem FindTreeItemById(FavoriteTreeItemType type, string uid);

        void SetFavoritesCache(FavoritesCache cache);

        bool IsFavorite(IFavoriteTreeItem item);

        SdmxFilter BuildFilterFromSdmxQueryString(string query);

        SWFilter BuildSwFilter(string query);

        string GetSdmxQueryString(ISdmxFilter filter, List<KeyValuePair<string, string>> @params = null);

        void ClearCache();
    }
}
