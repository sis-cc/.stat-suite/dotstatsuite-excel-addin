﻿using System.Collections.Generic;
using System.Linq;
using DLMXL.DAL.Config;
using DLMXL.Models;
using DLMXL.Models.Interfaces;
using ExcelAddIn.Domain;

namespace ExcelAddIn.DAL
{
    public class OecdDataSource : Repository<OecdDataSource>, IOecdDataSource
    {
        private IList<IFavoriteTreeItem> _allItems;
        public static IEnumerable<IOecdDataSource> AllSources => _Repositories.Values.OrderBy(x=>x.Index);
        public static IOecdDataSource GetById(string id) => _Repositories[id];

        public readonly int Index;
        public bool IsExternal => DataAccess.IsExternal;
        public bool IsSaveAllowed => DataAccess.IsSaveAllowed;
        public bool IsDataSpace => DataAccess.IsDataSpace;
        public bool IsFlagsOutput => DataAccess.IsFlagsOutput;
        public bool IsDateOutput => DataAccess.IsDateOutput;

        public bool IsLoaded => this._allItems != null;

        protected OecdDataSource(string code, string name, IDataAccess dataAccess, int index) : base(code, name, dataAccess)
        {
            Index = index;
        }

        static OecdDataSource()
        {
            if (Config.DW == null) return;

            int index = 0;

            foreach (Connection c in Config.DW.Connections)
                Build(c.Id, c.Name, c.DataAccess, index++);
        }

        public static OecdDataSource Build(string code, string name, IDataAccess dataAccess, int index = 0)
        {
            return Build(new OecdDataSource(code, name, dataAccess, index));
        }

        public SdmxFilter BuildFilterFromSdmxQueryString(string query)
        {
            return base.BuildFilterFromSdmxQueryString<SdmxFilter>(query);
        }

        #region Favorites

        public IList<IFavoriteTreeItem> GetAll()
        {
            return _allItems ?? (_allItems = Load());
        }

        private IList<IFavoriteTreeItem> Load()
        {
            DataAccess.BulkPreLoad();

            var list    = new FavoritesList(this);
            var index   = 0;
            TraverseDbTree(this.Databases.Where(x => x.Parent == null).ToArray(), null, list, ref index);
            return list;
        }

        private void TraverseDbTree(IList<IDatabase> dbs, IFavoriteTreeItem parent, List<IFavoriteTreeItem> list, ref int index)
        {
            if (dbs==null || !dbs.Any())
                return;

            foreach (var db in dbs)
            {
                var fDb = db.Id > 0 ? new DatabaseItem(index++, db, parent) : null;

                if(fDb!=null)
                    list.Add(fDb);

                TraverseDbTree(db.Children, fDb, list, ref index);

                foreach (var ds in db.Datasets)
                {
                    var fDs = new DatasetItem(index++, ds, fDb);
                    list.Add(fDs);

                    if (ds.Queries == null)
                        continue;

                    foreach (var query in ds.Queries)
                        list.Add(new QueryItem(index++, query, fDs, BuildFilterFromSdmxQueryString));
                }
            }
        }

        public IFavoriteTreeItem this[int index] => _allItems?[index];

        public IFavoriteTreeItem FindTreeItemById(FavoriteTreeItemType type, string uid)
        {
            return _allItems?.FirstOrDefault(x => x.Type == type && x.Uid == uid);
        }

        private FavoritesCache _cache;

        public void SetFavoritesCache(FavoritesCache cache)
        {
            _cache = cache;
        }

        public bool IsFavorite(IFavoriteTreeItem item)
        {
            return item.Parent != null && item.Parent.IsFavorite && item.Parent.IsPropagateSelection
                   ||
                   _cache != null && _cache[(int) item.Type].Value.Contains(item.Uid);
        }

        protected class FavoritesList : List<IFavoriteTreeItem>
        {
            private readonly IOecdDataSource _source;

            public FavoritesList(IOecdDataSource source)
            {
                _source = source;
            }

            public new void Add(IFavoriteTreeItem item)
            {
                item.IsFavorite = _source.IsFavorite(item);
                base.Add(item);
            }

            public new void AddRange(IEnumerable<IFavoriteTreeItem> collection)
            {
                foreach (var item in collection) Add(item);
            }
        }

        #endregion

        public override void ClearCache()
        {
	        this._allItems = null;
			base.ClearCache();
        }
    }
}
