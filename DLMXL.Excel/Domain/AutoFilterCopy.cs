﻿using System;
using System.Collections.Generic;
using Microsoft.Office.Interop.Excel;
using xFilter = System.Tuple<int, object, Microsoft.Office.Interop.Excel.XlAutoFilterOperator, object>;


namespace ExcelAddIn.Domain
{
    public class AutoFilterHandler: IDisposable
    {
	    private Worksheet _sheet;
        private IList<xFilter>  _filters;
        private Range           _filterRange;
        private bool _screenUpdateState;
        private bool _statusBarState;
        private bool _eventsState;
        private XlCalculation _calcState;
        private bool _displayPageBreakState;

        public AutoFilterHandler(Worksheet sheet)
        {
	        _sheet = sheet;

            if (sheet.AutoFilterMode)
            {
                var autoFilter          = sheet.AutoFilter;
                _filters                = ExtractAutoFilter(sheet.AutoFilter);
                _filterRange            = autoFilter.Range;
            }

            _screenUpdateState = Globals.ThisAddIn.Application.ScreenUpdating;
            _statusBarState = Globals.ThisAddIn.Application.DisplayStatusBar;
            _eventsState = Globals.ThisAddIn.Application.EnableEvents;
            _calcState = Globals.ThisAddIn.Application.Calculation;
            

	        Globals.ThisAddIn.Application.ScreenUpdating    = false;
            Globals.ThisAddIn.Application.DisplayStatusBar	= false;
            Globals.ThisAddIn.Application.EnableEvents		= false;
            Globals.ThisAddIn.Application.Calculation		= XlCalculation.xlCalculationManual;

            _sheet.AutoFilterMode                           = false;
            _sheet.DisplayPageBreaks						= false;
        }

        private static IList<xFilter> ExtractAutoFilter(AutoFilter autoFilter)
        {
            if (autoFilter == null)
                return null;

            var list        = new List<xFilter>();

            for (var col = 1; col <= autoFilter.Filters.Count; col++)
            {
                var f = autoFilter.Filters.Item[col];

                if (!f.On) 
                    continue;

                var criteria1   = f.Criteria1;
                var @operator   = f.Operator;
                var criteria2   = @operator > 0 ? f.Criteria2 : Type.Missing;

                list.Add(new xFilter(col, criteria1, @operator, criteria2));
            }
            
            return list;
        }

        private static void ApplyAutoFilter(Range rng, IList<xFilter> filters)
        {
            foreach (var f in filters)
            {
                if (f.Item3 > 0)    rng.AutoFilter(f.Item1, f.Item2, f.Item3, f.Item4);
                else                rng.AutoFilter(f.Item1, f.Item2);
            }
        }

        public void Dispose()
        {
            if (_filters != null)
            {
                ApplyAutoFilter(_filterRange, _filters);
            }

            _filters        = null;
            _filterRange    = null;

            Globals.ThisAddIn.Application.ScreenUpdating    = _screenUpdateState;
            Globals.ThisAddIn.Application.DisplayStatusBar	= _statusBarState;
            Globals.ThisAddIn.Application.EnableEvents		= _eventsState;
            Globals.ThisAddIn.Application.Calculation		= _calcState;
            _sheet.DisplayPageBreaks = _displayPageBreakState;
        }
    }
}
