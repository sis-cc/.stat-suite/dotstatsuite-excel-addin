﻿using System;
using System.Collections.Generic;
using DLMXL.Models;
using DLMXL.Models.Interfaces;

namespace ExcelAddIn.Domain
{
    public interface IFavoriteTreeItem
    {
        string Uid { get; }
        int Index { get; }
        bool IsPropagateSelection { get; }
        IDlmXLAddinObject Item { get; }
        FavoriteTreeItemType Type { get; }
        bool IsFavorite { get; set; }
        IFavoriteTreeItem Parent { get; }
        IList<IFavoriteTreeItem> Children { get; }
        string Name();
        SdmxFilter GetFilter();
    }

    public interface IFavoriteTreeItem<out T> : IFavoriteTreeItem where T : IDlmXLAddinObject
    {
        new T Item { get;}
    }

    public abstract class FavoriteTreeItem<T> : IFavoriteTreeItem<T> where T : IDlmXLAddinObject
    {
        private Lazy<T> _item;
        public string Uid { get;}
        public int Index { get;}

        public bool IsPropagateSelection { get;}

        public T Item { get { return _item.Value; } }

        IDlmXLAddinObject IFavoriteTreeItem.Item
        {
            get { return Item; }
        }

        public abstract FavoriteTreeItemType Type { get; }

        public bool IsFavorite { get; set; }

        public IFavoriteTreeItem Parent { get;}

        public IList<IFavoriteTreeItem> Children { get;}

        protected FavoriteTreeItem(string uid, int index, bool isPropagate,  T item, IFavoriteTreeItem parent) : this(uid, index, isPropagate, new Lazy<T>(()=>item), parent)
        {}

        protected FavoriteTreeItem(string uid, int index, bool isPropagate,  Lazy<T> item, IFavoriteTreeItem parent)
        {
            Uid                     = uid;
            Index                   = index;
            IsPropagateSelection    = isPropagate;
            _item                   = item;
            Parent                  = parent;
            Children                = new List<IFavoriteTreeItem>();

            if (parent != null)
                parent.Children.Add(this);                
        }

        public abstract string Name();

        public virtual SdmxFilter GetFilter()
        {
            return null;
        }
    }

    public class DatabaseItem : FavoriteTreeItem<IDatabase>
    {
        public override FavoriteTreeItemType Type => FavoriteTreeItemType.Database;

        public override string Name()
        {
	        return Item.DefaultName;
        }

        public DatabaseItem(int index, IDatabase item, IFavoriteTreeItem parent) : base(item.Uid, index, true, item, parent)
        {}
    }

    public class DatasetItem : FavoriteTreeItem<IDataset>
    {
        public override FavoriteTreeItemType Type => FavoriteTreeItemType.Dataset;

        public override string Name()
        {
            return Item.GetLabel("{1} [{0}]", Config.Settings.Language);
        }

        public override SdmxFilter GetFilter()
        {
            return new SdmxFilter(SWFilter.BuildFilter(Item)) { SourceType = Type, SourceId = Uid };
        }

        public DatasetItem(int index, IDataset item, IFavoriteTreeItem parent) : base(item.Uid, index, false, item, parent)
        {}
    }

    public class QueryItem : FavoriteTreeItem<IQuery>
    {
        private Func<string, SdmxFilter> _loadFilter;

        public override FavoriteTreeItemType Type => FavoriteTreeItemType.Query;

        public override string Name()
        {
            return Item.DefaultName;
        }

        public override SdmxFilter GetFilter()
        {
            var filter          = _loadFilter(Item.SdmxQuery);
            filter.SourceId     = Uid;
            filter.SourceType   = Type;
            return filter;
        }

        public QueryItem(int index, IQuery item, IFavoriteTreeItem parent, Func<string, SdmxFilter> loadFilter) : base(item.Uid, index, false, item, parent)
        {
            _loadFilter = loadFilter;
        }
    }
}
