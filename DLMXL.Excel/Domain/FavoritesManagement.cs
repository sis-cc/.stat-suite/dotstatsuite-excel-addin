﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using DLMXL.DAL;
using ExcelAddIn.DAL;
using ExcelAddIn.Mappers;

namespace ExcelAddIn.Domain
{
    public enum FavoriteTreeItemType
    {
        Database,
        Dataset,
        Query
    }

    public class FavoritesCache : List<KeyValuePair<string, HashSet<string>>>
    {
        public FavoritesCache()
        {}

        public FavoritesCache(IEnumerable<KeyValuePair<string, HashSet<string>>> collection):base(collection)
        {}
    }

    public static class FavoritesManagement
    {
        public static void Load()
        {
            foreach (var dataSource in OecdDataSource.AllSources)
                dataSource.SetFavoritesCache(LoadFromStorage(dataSource.GetFileName()));
        }

        private static FavoritesCache LoadFromStorage(string fileName)
        {
            fileName = Path.Combine(Log.BasePath, fileName);

            if (!File.Exists(fileName))
                return null;

            return FavoritesMap.Deserialize(File.ReadAllText(fileName)); 
        }

        public static void Save()
        {
            if (!Directory.Exists(Log.BasePath))
                Directory.CreateDirectory(Log.BasePath);

            foreach (var dataSource in OecdDataSource.AllSources)
            {
                if(!dataSource.IsLoaded) 
                    continue;
                
                var selectedItems = GetEmptyList();
                ExtractFavorites(dataSource.GetAll(), selectedItems);
                Persist(dataSource.GetFileName(), selectedItems);
            }
        }

        private static void Persist(string fileName, FavoritesCache selectedItems)
        {
            var xml = FavoritesMap.Serialize(selectedItems);

            File.WriteAllText(Path.Combine(Log.BasePath, fileName), xml);
        }

        private static FavoritesCache GetEmptyList()
        {
            var names = Enum
                .GetNames(typeof (FavoriteTreeItemType))
                .Select(name => new KeyValuePair<string, HashSet<string>>(name, new HashSet<string>()));

            return new FavoritesCache(names);
        }

        private static void ExtractFavorites(IList<IFavoriteTreeItem> items, FavoritesCache selected)
        {
            if (items.IsEmpty())
                return;

            foreach (var item in items)
            {
                var selectedIds = selected[(int) item.Type].Value;

                if (item.IsFavorite && (item.Parent == null || !item.Parent.IsFavorite || !item.Parent.IsPropagateSelection))
                    selectedIds.Add(item.Uid);
            }
        }

        private static string GetFileName(this IOecdDataSource source)
        {
            return "{0}.xml".F(source.Code);
        }
    }
}
