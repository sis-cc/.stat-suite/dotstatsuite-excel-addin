﻿using System;
using DLMXL.Models;
using DLMXL.Models.Interfaces;

namespace ExcelAddIn.Domain
{
    /// <summary>
    /// Deals with common dimensions, time dimension and extra options
    /// </summary>
    public sealed class SdmxFilter : ISdmxFilter
    {
        private SWFilter    _filter;
        private string      _sdmx;

        public string Dataset => _filter?.Dataset.FriendlyName ?? _sdmx?.Split('/')[0];

        public string SourceId { get; set; }
        public FavoriteTreeItemType SourceType  { get; set; }
        public Func<string, SWFilter> FilterBuilder { get; set; }

        #region ISdmxFilter
        public Frequency Frequency { get; set; }
        public string StartTime { get; set; }
        public string EndTime { get; set; }
        public int? LastPeriods { get; set; }
        public DateTime? UpdatedAfter { get; set; }
        public HistoricVersionType? HistoricVersionType { get; set; }
        #endregion

        /// <summary>
        /// Common dimension filter
        /// </summary>
        public SWFilter SWFilter
        {
            get { return _filter ?? (_filter = Build(_sdmx));}
            set { _filter = value; }
        }

        /// <summary>
        /// Returns pure sdmx query for common dimensions without time dimension and extra options
        /// </summary>
        public string SdmxQuery
        {
            get { return _filter?.GetSdmxQueryString() ?? _sdmx; }
            set { _sdmx = value; }
        }

        public SdmxFilter()
        {}

        public SdmxFilter(SWFilter filter)
        {
            _filter = filter;
        }

        public SdmxFilter Clone()
        {
            var filter      = (SdmxFilter) this.MemberwiseClone();

            if (this.SWFilter!=null)
            {
                filter.SWFilter = (SWFilter) this.SWFilter.Clone();
            }

            return filter;
        }

        private SWFilter Build(string query)
        {
            return FilterBuilder?.Invoke(query);
        }
    }
}