﻿using System.IO;
using Newtonsoft.Json;

namespace ExcelAddIn.Domain
{
    public sealed class UserSettings
    {
        #region Settings

        public string Language { get; set; } = "en";

        #endregion

        #region Mngmt methods

        private static readonly string SettingsFilepath = Path.Combine(Log.BasePath, "user.settings");

        private static string Serialize(UserSettings settings)
        {
            return JsonConvert.SerializeObject(settings);
        }

        private static UserSettings Deserialize(string json)
        {
            return JsonConvert.DeserializeObject<UserSettings>(json);
        }

        public static void Save(UserSettings settings)
        {
            File.WriteAllText(SettingsFilepath, Serialize(settings));
        }

        public static UserSettings Load()
        {
            if (!File.Exists(SettingsFilepath))
                return null;

            return Deserialize(File.ReadAllText(SettingsFilepath));
        }

        #endregion
    }
}
