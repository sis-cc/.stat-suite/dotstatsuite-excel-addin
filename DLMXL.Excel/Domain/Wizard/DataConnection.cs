﻿using System;
using System.Linq;
using DLMXL.Models;
using ExcelAddIn.DAL;

namespace ExcelAddIn.Domain.Wizard
{
    public class DataConnection : IWizardDto
    {
	    public LabelOutput LabelOutput { get; set; }
		public string Language { get; set; }

        public string Sheet { get; set; }
        public string[] AllSheets { get; set; }

        public bool Resume { get; set; }
        public string Id { get; set; }
        public IOecdDataSource Source { get; set; }
        public bool IsProduction { get; set; }
        
        public string StartCell { get; set; }
        public string EndCell { get; set; }
        public bool IsFlags { get; set; }
        public bool IsMetadata { get; set; }
        public bool IsActionColumn { get; set; }
        public bool IsUpdateData { get; set; }
        public DateTime? LastRefreshTime { get; set; }
        public OutputType OutputType { get; set; }
        public SdmxFilter Filter { get; set; }

        public bool IsNew()
        {
            return string.IsNullOrEmpty(this.Id);
        }

        public bool IsSaveAllowed()
        {
            return Source.IsSaveAllowed 
                   && Filter.HistoricVersionType == null
                   && (IsMetadata || !Source.DataAccess.IsSaveWithAttributesOnly || IsFlags);
        }

        public object[,] GetData()
        {
            this.LastRefreshTime = DateTime.Now;

            return IsMetadata 
                ? Source.DataAccess.LoadMetadata(
                    Filter,
                    LabelOutput,
                    Language,
                    IsActionColumn
                  )
                : Source.DataAccess.LoadData(
                    this.OutputType,
                    this.Filter,
                    IsProduction,
                    LabelOutput,
				    Language,
                    IsFlags,
                    IsUpdateData,
                    IsActionColumn
                );
        }

        public DataConnection Clone()
        {
            var connection      =  (DataConnection) this.MemberwiseClone();
            connection.Filter   =  this.Filter.Clone();
            return connection;
        }
        public string GetDataset()
        {
            return this.Filter.Dataset;
        }

        public string GetDatasetName()
        {
            if (this.Filter.SWFilter == null)
                return null;

            var allNames = this.Filter.SWFilter.Dataset.Names;
            return !string.IsNullOrEmpty(this.Language) && allNames.TryGetValue(this.Language, out var name) 
                ? name 
                : allNames.Values.First();
        }
    }
}