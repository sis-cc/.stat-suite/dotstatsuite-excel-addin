﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using DLMXL.Models.Exceptions;
using ExcelAddIn.Forms;

namespace ExcelAddIn
{
    public static class Log
    {
        public static readonly string BasePath = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments), ".Stat DLM");

        public static void Handle(Exception ex)
        {
            var managed = ex as DlmXLManagedException;

            if (managed != null)
            {
                InfoDialog.Show(ex.Message);
            }
            else
            {
                InfoDialog.Show(ex.Message);
                Error(ex);
            }
        }
        
        
        private static void Error(Exception ex)
        {
            if (!Directory.Exists(Log.BasePath))
                Directory.CreateDirectory(Log.BasePath);
            
            var path = Path.Combine(BasePath, String.Format("{0:yyyy-MM-dd-HHmmssfff}.log", DateTime.Now));

            using (var sw = new StreamWriter(path)) 
            {
                sw.WriteLine(SystemInformation());
                sw.WriteLine("-------------------------------------");
                sw.WriteLine(StackTrace(ex));
            }
        }

        private static string StackTrace(Exception e)
        {
            var res = new List<string>();
            
            while (e != null)
            {
                res.Add("[" + e.GetType() + ": " + e.Message + "]\n" + e.StackTrace + "\n\n");
                var loadEx = e as ReflectionTypeLoadException;
                if(loadEx!=null) res.AddRange(loadEx.LoaderExceptions.Select(StackTrace));
                e = e.InnerException;
            }

            res.Reverse();
            return string.Concat(res.ToArray());
        }

        private static string SystemInformation()
        {
            return
                "Current Directory : ".PadRight(50) + Environment.CurrentDirectory + Environment.NewLine +
                "Mashine : ".PadRight(50) + Environment.MachineName + Environment.NewLine +
                "OS : ".PadRight(50) + Environment.OSVersion.VersionString + Environment.NewLine +
                "Framework : ".PadRight(50) + Environment.Version;
        }
    }
}
