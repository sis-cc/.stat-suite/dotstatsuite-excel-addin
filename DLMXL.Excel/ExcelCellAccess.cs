﻿using System;
using System.Collections.Generic;
using System.Globalization;
using DLMXL.Models.Interfaces;
using Excel = Microsoft.Office.Interop.Excel;

namespace ExcelAddIn
{
	public sealed class ExcelCellAccess : ICellAccess
	{
		private Excel.Range _range;
		private Excel.Range _selection;
		
		public int Columns => _range.Columns.Count;
		public int Rows => _range.Rows.Count;

		public string this[int x, int y] => GetCellValue(_range[x, y].Value);

		public ExcelCellAccess(Excel.Range  range, Excel.Range selection = null)
		{
			_range = range;
			_selection = selection;
		}

		public IEnumerable<Selection> Selection()
		{
			if (_selection == null)
				yield break;

			var data = new Selection(
				new Range(1, Rows), 
				new Range(1, Columns)
			);

			foreach (Excel.Range area in _selection.Areas)
			{
				var rowStart = area.Row - _range.Row + 1;
				var rowEnd = rowStart + area.Rows.Count - 1;

				var clmnStart = area.Column - _range.Column + 1;
				var clmnEnd = clmnStart + area.Columns.Count - 1;

				var select = new Selection(
					new Range(rowStart, rowEnd),
					new Range(clmnStart, clmnEnd)
				);

				// discard selections that don't overlap with observation data

				if (select.Rows.IsOverlapped(data.Rows) && select.Columns.IsOverlapped(data.Columns))
					yield return select;
			}
		}

        private static string GetCellValue(object o)
        {
            switch (o)
            {
                case DateTime time:
                    return time.ToString("yyyy-MM-dd");
                default:
                    return Convert.ToString(o, CultureInfo.InvariantCulture);
            }
        }
	}
}