﻿using System.Collections.Generic;
using System.Linq;
using ExcelAddIn.Domain.Wizard;
using ExcelAddIn.Mappers;
using Excel = Microsoft.Office.Interop.Excel;
using Microsoft.Office.Core;

namespace ExcelAddIn
{
    public static class ExcelMeta
    {
        private const string SheetId            = "SheetId";
        private const string BookId             = "BookId";
        private const string Connections        = "SheetConnections";

        private static Excel.CustomProperty GetCustomProperty(this Excel.Worksheet sheet, string name)
        {
            return sheet.CustomProperties
                .Cast<Excel.CustomProperty>()
                .FirstOrDefault(p => p.Name == name);
        }

        private static string GetValue(this Excel.CustomProperty property)
        {
            if (property == null)
                return null;

            try
            {
                return property.Value;
            }
            catch
            {
                property.Delete();
                return null;
            }
        }

        private static DocumentProperty GetCustomProperty(this Excel.Workbook book, string name)
        {
            foreach (DocumentProperty p in book.CustomDocumentProperties)
                if (p.Name == name)
                    return p;

            return null;
        }

        private static T GetValue<T>(this DocumentProperty property)
        {
            return property == null ? default(T) : property.Value;
        }

        public static string GetId(this Excel.Worksheet sheet)
        {
            return sheet.GetCustomProperty(SheetId).GetValue();
        }

        public static void SetId(this Excel.Worksheet sheet, string uid)
        {
            sheet.CustomProperties.Add(SheetId, uid);
        }

        public static string GetId(this Excel.Workbook book)
        {
            return book.GetCustomProperty(BookId).GetValue<string>();
        }

        public static void SetId(this Excel.Workbook book, string uid)
        {
            var property = book.GetCustomProperty(BookId);

            if (property == null)
            {
                ((DocumentProperties)book.CustomDocumentProperties).Add(
                    BookId,
                    false,
                    MsoDocProperties.msoPropertyTypeString,
                    uid
                );
            }
            else
            {
                property.Value = uid;
            }
        }

        public static Dictionary<string, DataConnection> GetConnections(this Excel.Worksheet sheet)
        {
            var xml = sheet.GetCustomProperty(Connections).GetValue();

            var collection = DataConnectionArrayMap.Deserialize(xml);

            return collection == null
                ? null
                : collection.ToDictionary(x => x.Id, x=>x);
        }

        public static void SetConnections(this Excel.Worksheet sheet, Dictionary<string, DataConnection> connections)
        {
            var xml = connections == null || !connections.Any()
                ? null
                : DataConnectionArrayMap.Serialize(connections.Values.ToArray());
            
            var property = sheet.GetCustomProperty(Connections);

            if (!string.IsNullOrEmpty(xml) && property == null)
            {
                sheet.CustomProperties.Add(Connections, xml);
            }
            else if (!string.IsNullOrEmpty(xml))
            {
                property.Value = xml;
            }
            else
            {
                property?.Delete();
            }
        }
    }
}
