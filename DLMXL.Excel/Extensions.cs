﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Windows.Forms;
using DLMXL.DAL;
using ExcelAddIn.Domain.Wizard;
using Excel = Microsoft.Office.Interop.Excel;

namespace ExcelAddIn
{
    public static class Extensions
    {
        private static int lightRed = ColorTranslator.ToOle(Color.FromArgb(232, 93, 101));

        #region Excel

        public static Excel.Worksheet GetActiveWorksheet(this ThisAddIn addIn)
        {
            var book    = addIn.GetActiveWorkbook();
            return book == null ? null : book.ActiveSheet as Excel.Worksheet;
        }

        public static Excel.Workbook GetActiveWorkbook(this ThisAddIn addIn)
        {
            return addIn.Application.ActiveWorkbook;
        }

        public static string RangeAddress(this Excel.Range rng)
        {
            return rng.get_AddressLocal(false,false, Excel.XlReferenceStyle.xlA1, Type.Missing, Type.Missing);
        }

        public static bool InRegion(this Excel.Range cell, DataConnection connection)
        {
            if (cell == null || connection == null)
                return false;

            var result = Globals.ThisAddIn.Application.Intersect(
                cell,
                connection.Region(cell.Worksheet)
            );

            return result != null;
        }

        public static bool HasConnections(this Excel.Worksheet sheet)
        {
            return !WorkbookConnectionBag.GetConnections(sheet).IsEmpty();
        }

        public static bool HasConnections(this Excel.Workbook book)
        {
            return book.Worksheets.Cast<Excel.Worksheet>().Any(HasConnections);
        }


        #endregion

        #region DataConnection
        public static Excel.Range Region(this DataConnection connection, Excel.Worksheet sheet)
        {
            return sheet.Range[connection.StartCell, connection.EndCell];
        }

        public static void ApplyFormating(this DataConnection connection, Excel.Worksheet sheet)
        {
            if(connection.IsActionColumn)
            {
                var range = connection.Region(sheet);
                range.FormatConditions.Delete();
                
                Excel.FormatCondition condition = range.FormatConditions.Add(
                    Excel.XlFormatConditionType.xlCellValue, 
                    Excel.XlFormatConditionOperator.xlEqual,
                    "Delete"   
                );

                condition.Interior.Color = lightRed;
            }
        }
        #endregion

        #region String
        public static string JoinMembers(this IEnumerable<string> codes)
        {
            return String.Join("+", codes);
        }

        public static string[] SplitMembers(this string text)
        {
            return text.Split(new[] {' ', '+', ','}, StringSplitOptions.RemoveEmptyEntries);
        }

        public static string ToDT(this DateTime? dt)
        {
            return dt.HasValue ? string.Format("{0:yyyy-MM-ddTHH:mm:ss}", dt) : null;
        }

        public static bool? ToBool(this string str)
        {
            bool temp;
            return bool.TryParse(str, out temp)
                        ? (bool?)temp
                        : null;
        }

        public static string F(this string format, params object[] args)
        {
            return string.IsNullOrEmpty(format) || args == null || args.Length == 0 
                ? format
                : string.Format(format, args);
        }

     
        #endregion

        #region DevExpress

        public static string Value(this DevExpress.XtraEditors.TextEdit input)
        {
            return input.Text == input.Properties.NullValuePrompt ? null : input.Text;
        }

        #endregion

        #region UI

        public static bool IsReallyDesignMode(this Control ctrl)
        {
            if (System.ComponentModel.LicenseManager.UsageMode == System.ComponentModel.LicenseUsageMode.Designtime)
                return true;

            while (ctrl != null)
            {
                if (ctrl.Site != null && ctrl.Site.DesignMode)
                    return true;
                ctrl = ctrl.Parent;
            }
            return System.Diagnostics.Process.GetCurrentProcess().ProcessName == "devenv";
        }
        #endregion
    }
}
