﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Text;

namespace ExcelAddIn
{
	public static class FormUpload  
    {  
        private static readonly Encoding encoding = Encoding.UTF8;

        public static HttpWebResponse Post(string postUrl, Action<HttpWebRequest> modifyRequest, params KeyValuePair<string, object>[] postParameters)  
        {  
	        var boundary = String.Format("----------{0:N}", Guid.NewGuid());
	        var request = WebRequest.Create(postUrl) as HttpWebRequest;  
  
            if (request == null)  
            {  
                throw new NullReferenceException("request is not a http request");  
            }  
  
            // Set up the request properties.  
            request.Method = "POST";
            request.ContentType = "multipart/form-data; boundary=" + boundary;
            request.CookieContainer = new CookieContainer(); 

			modifyRequest?.Invoke(request);

			// Send the form data to the request.  
			using (var requestStream = request.GetRequestStream())
			{
				WriteMultipartFormData(boundary, requestStream, postParameters);
			}  
  
            return request.GetResponse() as HttpWebResponse;  
        }  
  
        private static void WriteMultipartFormData(string boundary, Stream formDataStream, KeyValuePair<string, object>[] postParameters)  
        {
			var needsCLRF = false;

            foreach (var param in postParameters)  
            {  
                if (needsCLRF)  
                    formDataStream.Write(encoding.GetBytes("\r\n"), 0, encoding.GetByteCount("\r\n"));  
  
                needsCLRF = true;  
  
				if(param.Value == null)
					continue;

				var fileToUpload = param.Value as FileInfo;

				if (fileToUpload != null)
				{
					if(!fileToUpload.Exists)
						continue;

					// Add just the first part of this param, since we will write the file data directly to the Stream  
					var header = string.Format("--{0}\r\nContent-Disposition: form-data; name=\"{1}\"; filename=\"{2}\"\r\nContent-Type: {3}\r\n\r\n",  
						boundary,  
						param.Key,  
						fileToUpload.Name,  
						"application/octet-stream"
					);  
  
					formDataStream.Write(encoding.GetBytes(header), 0, encoding.GetByteCount(header));  
  
					var bytesRead = 0;
					var buffer = new byte[2048];

					using (FileStream fileStream = new FileStream(fileToUpload.FullName, FileMode.Open, FileAccess.Read))
						while ((bytesRead = fileStream.Read(buffer, 0, buffer.Length)) != 0)
							formDataStream.Write(buffer, 0, bytesRead);
				}
				else
				{
					var postField = string.Format("--{0}\r\nContent-Disposition: form-data; name=\"{1}\"\r\n\r\n{2}",  
						boundary,  
						param.Key,  
						param.Value);
                    
					formDataStream.Write(encoding.GetBytes(postField), 0, encoding.GetByteCount(postField));
				}
            }
  
            // End of request  
            var footer = "\r\n--" + boundary + "--\r\n";  
            formDataStream.Write(encoding.GetBytes(footer), 0, encoding.GetByteCount(footer));  
        }
    }
}
