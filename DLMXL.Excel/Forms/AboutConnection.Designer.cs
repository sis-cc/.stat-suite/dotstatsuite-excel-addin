﻿namespace ExcelAddIn.Forms
{
    partial class AboutConnection
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.pnlActions = new DevExpress.XtraEditors.PanelControl();
            this.OkButton = new ExcelAddIn.UI.Controls.MainButton();
            this.pnlMain = new DevExpress.XtraEditors.PanelControl();
            this.lblDataspace = new DevExpress.XtraEditors.LabelControl();
            this.labelControl4 = new DevExpress.XtraEditors.LabelControl();
            this.lblUpdateDate = new DevExpress.XtraEditors.LabelControl();
            this.lblSource = new DevExpress.XtraEditors.LabelControl();
            this.labelControl2 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl1 = new DevExpress.XtraEditors.LabelControl();
            this.lblDataset = new DevExpress.XtraEditors.LabelControl();
            this.labelControl5 = new DevExpress.XtraEditors.LabelControl();
            ((System.ComponentModel.ISupportInitialize)(this.pnlActions)).BeginInit();
            this.pnlActions.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pnlMain)).BeginInit();
            this.pnlMain.SuspendLayout();
            this.SuspendLayout();
            // 
            // pnlActions
            // 
            this.pnlActions.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.pnlActions.Controls.Add(this.OkButton);
            this.pnlActions.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.pnlActions.Location = new System.Drawing.Point(0, 110);
            this.pnlActions.Name = "pnlActions";
            this.pnlActions.Size = new System.Drawing.Size(284, 33);
            this.pnlActions.TabIndex = 6;
            // 
            // OkButton
            // 
            this.OkButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.OkButton.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.OkButton.Appearance.Options.UseFont = true;
            this.OkButton.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.OkButton.Location = new System.Drawing.Point(199, 6);
            this.OkButton.Name = "OkButton";
            this.OkButton.Size = new System.Drawing.Size(73, 20);
            this.OkButton.TabIndex = 3;
            this.OkButton.Text = "OK";
            // 
            // pnlMain
            // 
            this.pnlMain.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.pnlMain.Controls.Add(this.lblDataset);
            this.pnlMain.Controls.Add(this.labelControl5);
            this.pnlMain.Controls.Add(this.lblDataspace);
            this.pnlMain.Controls.Add(this.labelControl4);
            this.pnlMain.Controls.Add(this.lblUpdateDate);
            this.pnlMain.Controls.Add(this.lblSource);
            this.pnlMain.Controls.Add(this.labelControl2);
            this.pnlMain.Controls.Add(this.labelControl1);
            this.pnlMain.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnlMain.Location = new System.Drawing.Point(0, 0);
            this.pnlMain.Margin = new System.Windows.Forms.Padding(0);
            this.pnlMain.Name = "pnlMain";
            this.pnlMain.Padding = new System.Windows.Forms.Padding(12);
            this.pnlMain.Size = new System.Drawing.Size(284, 110);
            this.pnlMain.TabIndex = 13;
            // 
            // lblDataspace
            // 
            this.lblDataspace.Location = new System.Drawing.Point(134, 54);
            this.lblDataspace.Name = "lblDataspace";
            this.lblDataspace.Size = new System.Drawing.Size(51, 13);
            this.lblDataspace.TabIndex = 5;
            this.lblDataspace.Text = "Production";
            // 
            // labelControl4
            // 
            this.labelControl4.Location = new System.Drawing.Point(15, 54);
            this.labelControl4.Name = "labelControl4";
            this.labelControl4.Size = new System.Drawing.Size(55, 13);
            this.labelControl4.TabIndex = 4;
            this.labelControl4.Text = "Dataspace:";
            // 
            // lblUpdateDate
            // 
            this.lblUpdateDate.Location = new System.Drawing.Point(134, 74);
            this.lblUpdateDate.Name = "lblUpdateDate";
            this.lblUpdateDate.Size = new System.Drawing.Size(87, 13);
            this.lblUpdateDate.TabIndex = 3;
            this.lblUpdateDate.Text = "01.01.2015 10:10";
            // 
            // lblSource
            // 
            this.lblSource.Location = new System.Drawing.Point(134, 15);
            this.lblSource.Name = "lblSource";
            this.lblSource.Size = new System.Drawing.Size(48, 13);
            this.lblSource.TabIndex = 2;
            this.lblSource.Text = "Statworks";
            // 
            // labelControl2
            // 
            this.labelControl2.Location = new System.Drawing.Point(15, 74);
            this.labelControl2.Name = "labelControl2";
            this.labelControl2.Size = new System.Drawing.Size(101, 13);
            this.labelControl2.TabIndex = 1;
            this.labelControl2.Text = "Last extraction date:";
            // 
            // labelControl1
            // 
            this.labelControl1.Location = new System.Drawing.Point(15, 15);
            this.labelControl1.Name = "labelControl1";
            this.labelControl1.Size = new System.Drawing.Size(62, 13);
            this.labelControl1.TabIndex = 0;
            this.labelControl1.Text = "Source type:";
            // 
            // lblDataset
            // 
            this.lblDataset.Location = new System.Drawing.Point(134, 34);
            this.lblDataset.Name = "lblDataset";
            this.lblDataset.Size = new System.Drawing.Size(25, 13);
            this.lblDataset.TabIndex = 7;
            this.lblDataset.Text = "Code";
            // 
            // labelControl5
            // 
            this.labelControl5.Location = new System.Drawing.Point(15, 34);
            this.labelControl5.Name = "labelControl5";
            this.labelControl5.Size = new System.Drawing.Size(70, 13);
            this.labelControl5.TabIndex = 6;
            this.labelControl5.Text = "Dataset Code:";
            // 
            // AboutConnection
            // 
            this.AcceptButton = this.OkButton;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(284, 143);
            this.Controls.Add(this.pnlMain);
            this.Controls.Add(this.pnlActions);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "AboutConnection";
            this.ShowIcon = false;
            this.ShowInTaskbar = false;
            this.Text = "About connection";
            ((System.ComponentModel.ISupportInitialize)(this.pnlActions)).EndInit();
            this.pnlActions.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pnlMain)).EndInit();
            this.pnlMain.ResumeLayout(false);
            this.pnlMain.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraEditors.PanelControl pnlActions;
        private ExcelAddIn.UI.Controls.MainButton OkButton;
        private DevExpress.XtraEditors.PanelControl pnlMain;
        private DevExpress.XtraEditors.LabelControl lblUpdateDate;
        private DevExpress.XtraEditors.LabelControl lblSource;
        private DevExpress.XtraEditors.LabelControl labelControl2;
        private DevExpress.XtraEditors.LabelControl labelControl1;
        private DevExpress.XtraEditors.LabelControl lblDataspace;
        private DevExpress.XtraEditors.LabelControl labelControl4;
        private DevExpress.XtraEditors.LabelControl lblDataset;
        private DevExpress.XtraEditors.LabelControl labelControl5;
        
    }
}