﻿using DevExpress.XtraEditors;
using ExcelAddIn.Domain.Wizard;

namespace ExcelAddIn.Forms
{
    public partial class AboutConnection : XtraForm
    {
        /// <summary>
        /// Designer only constructor 
        /// </summary>
        private AboutConnection()
        {
            InitializeComponent();
        }

        public AboutConnection(DataConnection connection) : this()
        {
            if (connection == null)
                return;

            lblSource.Text      = connection.Source.Name;
            lblDataset.Text     = connection.GetDataset();
            lblDataspace.Text   = connection.Source.IsDataSpace ? (connection.IsProduction ? "Production" : "Import") : "";
            lblUpdateDate.Text  = string.Format("{0:dd.MM.yyyy HH:mm}", connection.LastRefreshTime);
        }
    }
}
