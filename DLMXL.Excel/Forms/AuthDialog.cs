﻿using System;
using System.IO;
using System.Net.Http;
using System.Threading.Tasks;
using System.Windows.Forms;
using DLMXL.DAL.Access;
using DLMXL.Models.Interfaces;
using Microsoft.Web.WebView2.Core;

namespace ExcelAddIn.Forms
{
    public partial class AuthDialog : Form
    {
        private readonly IAuth _auth;
        private Exception _exception;

        public AuthDialog(IAuth auth)
        {
            InitializeComponent();
            InitUi();
            _auth = auth;

            this.Load += AuthDialog_Load;
        }

        private async void AuthDialog_Load(object sender, EventArgs e)
        {
            var env = await CoreWebView2Environment.CreateAsync(userDataFolder: Path.GetTempPath());
            await webView2.EnsureCoreWebView2Async(env);

            webView2.Source = new Uri(_auth.GetAuthorizationUrl());
            webView2.NavigationStarting += NavigationStarting;
        }

        private void NavigationStarting(object sender, CoreWebView2NavigationStartingEventArgs e)
        {
            if (!e.Uri.StartsWith(OAuth.RedirectUri))
                return;

            e.Cancel = true;

            var query = new Uri(e.Uri).ParseQueryString();

            _auth.Authorize(query).ContinueWith(t =>
                {
                    if (t.Exception != null)
                    {
                        _exception = t.Exception.InnerException;
                        this.Close(false);
                    }
                    else
                    {
                        this.Close(true);
                    }
                },
                TaskScheduler.FromCurrentSynchronizationContext());
        }

        private void InitUi()
        {
            this.Text = Config.DW.Title;
        }

        private void Close(bool success)
        {
            if (success)
            {
                this.DialogResult = DialogResult.OK;
            }
            this.Close();
        }

        public void ThrowUnauthorizedException()
        {
            throw _exception ?? new Exception("Unauthorized");
        }
    }
}
