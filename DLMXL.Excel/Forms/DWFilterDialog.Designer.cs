﻿using ExcelAddIn.UI.Controls;

namespace ExcelAddIn.Forms
{
    partial class DWFilterDialog {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose( bool disposing ) {
            if( disposing && ( components != null ) ) {
                components.Dispose( );
            }
            base.Dispose( disposing );
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent( ) {
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(DWFilterDialog));
			this.OkButton = new ExcelAddIn.UI.Controls.MainButton();
			this.btnCancel = new DevExpress.XtraEditors.SimpleButton();
			this.panel1 = new DevExpress.XtraEditors.PanelControl();
			this.FilterHolderPanel = new DevExpress.XtraEditors.PanelControl();
			((System.ComponentModel.ISupportInitialize)(this.panel1)).BeginInit();
			this.panel1.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.FilterHolderPanel)).BeginInit();
			this.SuspendLayout();
			// 
			// OkButton
			// 
			this.OkButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
			this.OkButton.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
			this.OkButton.Appearance.Options.UseFont = true;
			this.OkButton.Location = new System.Drawing.Point(586, 8);
			this.OkButton.Name = "OkButton";
			this.OkButton.Size = new System.Drawing.Size(112, 20);
			this.OkButton.TabIndex = 1;
			this.OkButton.Text = "APPLY FILTERS";
			this.OkButton.Click += new System.EventHandler(this.OkButtonClick);
			// 
			// btnCancel
			// 
			this.btnCancel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
			this.btnCancel.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
			this.btnCancel.Appearance.Options.UseFont = true;
			this.btnCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
			this.btnCancel.Location = new System.Drawing.Point(481, 8);
			this.btnCancel.Name = "btnCancel";
			this.btnCancel.Size = new System.Drawing.Size(95, 20);
			this.btnCancel.TabIndex = 2;
			this.btnCancel.Text = "NO FILTER";
			this.btnCancel.Click += new System.EventHandler(this.CancelButtonClick);
			// 
			// panel1
			// 
			this.panel1.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
			this.panel1.Controls.Add(this.OkButton);
			this.panel1.Controls.Add(this.btnCancel);
			this.panel1.Dock = System.Windows.Forms.DockStyle.Bottom;
			this.panel1.Location = new System.Drawing.Point(0, 431);
			this.panel1.Name = "panel1";
			this.panel1.Size = new System.Drawing.Size(709, 35);
			this.panel1.TabIndex = 3;
			// 
			// FilterHolderPanel
			// 
			this.FilterHolderPanel.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
			this.FilterHolderPanel.Dock = System.Windows.Forms.DockStyle.Fill;
			this.FilterHolderPanel.Location = new System.Drawing.Point(0, 0);
			this.FilterHolderPanel.Name = "FilterHolderPanel";
			this.FilterHolderPanel.Size = new System.Drawing.Size(709, 431);
			this.FilterHolderPanel.TabIndex = 4;
			// 
			// DWFilterDialog
			// 
			this.AcceptButton = this.OkButton;
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.CancelButton = this.btnCancel;
			this.ClientSize = new System.Drawing.Size(709, 466);
			this.Controls.Add(this.FilterHolderPanel);
			this.Controls.Add(this.panel1);
			this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.SizableToolWindow;
			this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
			this.MaximizeBox = false;
			this.MinimizeBox = false;
			this.MinimumSize = new System.Drawing.Size(636, 500);
			this.Name = "DWFilterDialog";
			this.ShowInTaskbar = false;
			this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
			this.Text = "Filter";
			((System.ComponentModel.ISupportInitialize)(this.panel1)).EndInit();
			this.panel1.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.FilterHolderPanel)).EndInit();
			this.ResumeLayout(false);

        }

        #endregion

        private MainButton OkButton;
        private DevExpress.XtraEditors.SimpleButton btnCancel;
        private DevExpress.XtraEditors.PanelControl panel1;
        private DevExpress.XtraEditors.PanelControl FilterHolderPanel;
    }
}