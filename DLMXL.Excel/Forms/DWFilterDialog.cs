﻿using System;
using System.Drawing;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using DLMXL.Models;
using ExcelAddIn.Domain;
using ExcelAddIn.UI.Filter;

namespace ExcelAddIn.Forms
{
    public partial class DWFilterDialog: XtraForm {
        private SdmxFilter _filter;
        private SWFilterPanel _filterPanel;

        private DWFilterDialog( ) {
            _filter = null;
            InitializeComponent( );
        }

        private DWFilterDialog(SdmxFilter filter, bool showDataExistanceCheck = false) : this()
        {
            try
            {
                SuspendLayout();
                
//                if (showDataExistanceCheck)
//                    filter.SWFilter.Dataset.UpdateDimensionMembersWithData(true);

                _filter = filter.Clone();

                _filterPanel = new SWFilterPanel(_filter, showDataExistanceCheck)
                {
                    Dock        = DockStyle.Fill,
                    Location    = new Point(0, 0),
                    Name        = "FilterPanel",
                    TabIndex    = 0
                };

                FilterHolderPanel.Controls.Add(_filterPanel);
                _filterPanel.SendToBack();
            }
            finally
            {
                ResumeLayout();
            }
        }

        public static SdmxFilter ShowFilterDialog(string title, SdmxFilter filter, bool showDataExistanceCheck = false)
        {
            var fd = new DWFilterDialog(filter, showDataExistanceCheck)
            {
                Text = title
            };

            if( fd.ShowDialog() == DialogResult.OK )
                return fd._filter;

            return null;
        }

        private void CancelButtonClick( object sender, EventArgs e ) {

            this._filter = new SdmxFilter(SWFilter.BuildFilter(_filter.SWFilter.Dataset)); // reset filter;
            DialogResult = DialogResult.OK;
            Close( );
        }

        private void OkButtonClick( object sender, EventArgs e )
        {
            if (!_filterPanel.IsValid())
                return;
             
            _filterPanel.Apply();
            DialogResult = DialogResult.OK;
            Close( );
        }
    }
}