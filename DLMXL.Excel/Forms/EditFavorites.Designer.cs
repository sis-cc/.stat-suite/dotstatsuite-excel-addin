﻿using ExcelAddIn.UI;
using ExcelAddIn.UI.Controls;

namespace ExcelAddIn.Forms
{
    partial class EditFavorites
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.pnlActions = new DevExpress.XtraEditors.PanelControl();
            this.btnClear = new DevExpress.XtraEditors.SimpleButton();
            this.chkFavoritesOnly = new DevExpress.XtraEditors.CheckEdit();
            this.OkButton = new ExcelAddIn.UI.Controls.MainButton();
            this.panelControl2 = new DevExpress.XtraEditors.PanelControl();
            this.SearchText = new ExcelAddIn.UI.IncrementalSearchTextBox();
            this.lblCounter = new DevExpress.XtraEditors.LabelControl();
            this.labelControl1 = new DevExpress.XtraEditors.LabelControl();
            this.pnlTree = new DevExpress.XtraEditors.PanelControl();
            ((System.ComponentModel.ISupportInitialize)(this.pnlActions)).BeginInit();
            this.pnlActions.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chkFavoritesOnly.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl2)).BeginInit();
            this.panelControl2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.SearchText.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pnlTree)).BeginInit();
            this.SuspendLayout();
            // 
            // pnlActions
            // 
            this.pnlActions.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.pnlActions.Controls.Add(this.btnClear);
            this.pnlActions.Controls.Add(this.chkFavoritesOnly);
            this.pnlActions.Controls.Add(this.OkButton);
            this.pnlActions.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.pnlActions.Location = new System.Drawing.Point(0, 379);
            this.pnlActions.Name = "pnlActions";
            this.pnlActions.Size = new System.Drawing.Size(564, 47);
            this.pnlActions.TabIndex = 6;
            // 
            // btnClear
            // 
            this.btnClear.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnClear.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.btnClear.Appearance.Options.UseFont = true;
            this.btnClear.Location = new System.Drawing.Point(341, 15);
            this.btnClear.Name = "btnClear";
            this.btnClear.Size = new System.Drawing.Size(101, 20);
            this.btnClear.TabIndex = 5;
            this.btnClear.Text = "CLEAR";
            this.btnClear.Click += new System.EventHandler(this.btnClear_Click);
            // 
            // chkFavoritesOnly
            // 
            this.chkFavoritesOnly.Location = new System.Drawing.Point(12, 16);
            this.chkFavoritesOnly.Name = "chkFavoritesOnly";
            this.chkFavoritesOnly.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 10F);
            this.chkFavoritesOnly.Properties.Appearance.Options.UseFont = true;
            this.chkFavoritesOnly.Properties.Caption = "Show favorites only";
            this.chkFavoritesOnly.Size = new System.Drawing.Size(138, 20);
            this.chkFavoritesOnly.TabIndex = 4;
            this.chkFavoritesOnly.CheckedChanged += new System.EventHandler(this.chkFavoritesOnly_CheckedChanged);
            // 
            // OkButton
            // 
            this.OkButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.OkButton.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.OkButton.Appearance.Options.UseFont = true;
            this.OkButton.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.OkButton.Location = new System.Drawing.Point(448, 15);
            this.OkButton.Name = "OkButton";
            this.OkButton.Size = new System.Drawing.Size(101, 20);
            this.OkButton.TabIndex = 3;
            this.OkButton.Text = "SAVE";
            this.OkButton.Click += new System.EventHandler(this.OkButton_Click);
            // 
            // panelControl2
            // 
            this.panelControl2.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.panelControl2.Controls.Add(this.SearchText);
            this.panelControl2.Controls.Add(this.lblCounter);
            this.panelControl2.Controls.Add(this.labelControl1);
            this.panelControl2.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelControl2.Location = new System.Drawing.Point(0, 0);
            this.panelControl2.Name = "panelControl2";
            this.panelControl2.Size = new System.Drawing.Size(564, 71);
            this.panelControl2.TabIndex = 12;
            // 
            // SearchText
            // 
            this.SearchText.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.SearchText.IsPopupDisabled = false;
            this.SearchText.Location = new System.Drawing.Point(12, 39);
            this.SearchText.Name = "SearchText";
            this.SearchText.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 10F);
            this.SearchText.Properties.Appearance.Options.UseFont = true;
            this.SearchText.SearchTextHint = "Type here to search";
            this.SearchText.Size = new System.Drawing.Size(540, 22);
            this.SearchText.TabIndex = 13;
            this.SearchText.SearchTextChanged += new System.EventHandler(this.SearchText_SearchTextChanged);
            // 
            // lblCounter
            // 
            this.lblCounter.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.lblCounter.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.lblCounter.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.lblCounter.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.lblCounter.Location = new System.Drawing.Point(452, 18);
            this.lblCounter.Name = "lblCounter";
            this.lblCounter.Size = new System.Drawing.Size(100, 13);
            this.lblCounter.TabIndex = 12;
            this.lblCounter.Text = "0 / 0";
            // 
            // labelControl1
            // 
            this.labelControl1.Appearance.Font = new System.Drawing.Font("Tahoma", 10F, System.Drawing.FontStyle.Bold);
            this.labelControl1.Location = new System.Drawing.Point(13, 15);
            this.labelControl1.Name = "labelControl1";
            this.labelControl1.Size = new System.Drawing.Size(190, 16);
            this.labelControl1.TabIndex = 11;
            this.labelControl1.Text = "Select your favorite sources:";
            // 
            // pnlTree
            // 
            this.pnlTree.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.pnlTree.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnlTree.Location = new System.Drawing.Point(0, 71);
            this.pnlTree.Margin = new System.Windows.Forms.Padding(0);
            this.pnlTree.Name = "pnlTree";
            this.pnlTree.Padding = new System.Windows.Forms.Padding(12);
            this.pnlTree.Size = new System.Drawing.Size(564, 308);
            this.pnlTree.TabIndex = 13;
            // 
            // EditFavorites
            // 
            this.AcceptButton = this.OkButton;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(564, 426);
            this.Controls.Add(this.pnlTree);
            this.Controls.Add(this.panelControl2);
            this.Controls.Add(this.pnlActions);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.SizableToolWindow;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "EditFavorites";
            this.ShowIcon = false;
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Edit favorites";
            ((System.ComponentModel.ISupportInitialize)(this.pnlActions)).EndInit();
            this.pnlActions.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.chkFavoritesOnly.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl2)).EndInit();
            this.panelControl2.ResumeLayout(false);
            this.panelControl2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.SearchText.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pnlTree)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraEditors.PanelControl pnlActions;
        private MainButton OkButton;
        private DevExpress.XtraEditors.CheckEdit chkFavoritesOnly;
        private DevExpress.XtraEditors.PanelControl panelControl2;
        private IncrementalSearchTextBox SearchText;
        private DevExpress.XtraEditors.LabelControl lblCounter;
        private DevExpress.XtraEditors.LabelControl labelControl1;
        private DevExpress.XtraEditors.PanelControl pnlTree;
        private DevExpress.XtraEditors.SimpleButton btnClear;
        
    }
}