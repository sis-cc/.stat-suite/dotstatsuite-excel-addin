﻿using System;
using System.Linq;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using DevExpress.XtraTreeList.Nodes;
using DLMXL.Models;
using ExcelAddIn.DAL;
using ExcelAddIn.UI;

namespace ExcelAddIn.Forms
{
    public partial class EditFavorites : XtraForm
    {
        private readonly FavoriteDisplayTree DisplayTree;
        private int totalNodes;
        
        /// <summary>
        /// Designer only constructor 
        /// </summary>
        public EditFavorites()
        {
            InitializeComponent();
        }

        public EditFavorites(string title, IOecdDataSource source, ImageList checkImages = null) : this()
        {
            this.Text           = title;
            totalNodes          = source.GetAll().Count;
            var selectedCount   = source.GetAll().Count(x => x.IsFavorite);

            UpdateFavoriteCount(selectedCount);

            DisplayTree = new FavoriteDisplayTree(source, selectedCount, checkImages)
            {
                Dock = DockStyle.Fill,
                IsNodeVisible = IsNodeVisible
            };

            DisplayTree.FavoriteCountChanged += FavoriteCountChanged;
            pnlTree.Controls.Add(DisplayTree);
        }

        

        private void UpdateFavoriteCount(int selectedCount)
        {
            this.lblCounter.Text = selectedCount + @"/" + totalNodes;
        }

        #region Event handlers

        private void FavoriteCountChanged(object sender, EventArgs<int> e)
        {
            UpdateFavoriteCount(e.Item);
        }

        private void chkFavoritesOnly_CheckedChanged(object sender, EventArgs e)
        {
            DisplayTree.UpdateListVisibility(SearchText.Text.Trim());
        }

        private void SearchText_SearchTextChanged(object sender, EventArgs e)
        {
            DisplayTree.UpdateListVisibility(SearchText.Text.Trim());
        }

        private void OkButton_Click(object sender, EventArgs e)
        {
            DisplayTree.SaveFavorites();
            this.Close();
        }

        private void btnClear_Click(object sender, EventArgs e)
        {
            if (YesNoDialog.Show("Are you sure to clear all favorites ?"))
                DisplayTree.ClearFavorites();
        }

        private bool IsNodeVisible(TreeListNode node)
        {
            return !chkFavoritesOnly.Checked || node.Checked;
        }

        #endregion


    }
}
