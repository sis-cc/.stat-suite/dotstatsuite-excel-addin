﻿using System;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using ExcelAddIn.Domain.Wizard;
using ExcelAddIn.UI.Wizard;

namespace ExcelAddIn.Forms
{
    public partial class GetDataForm : XtraForm
    {
        public GetDataForm()
        {
            InitializeComponent();
        }

        private GetDataForm(DataConnection dto) : this()
        {
            InitUi(dto);

            var wizard = new WizardBase<DataConnection, GetDataWizzardStep>();
            wizard.AddStep("Select data", new SelectDataStep(){NextButtonText = @"NEXT STEP"});
            wizard.AddStep("Specify output", new SpecifyOutputStep() { NextButtonText = @"GET DATA" });
            wizard.Dock         = DockStyle.Fill;
            wizard.Completed    += WizardCompleted;
            wizard.AutoSize     = true;

            this.Controls.Add(wizard);

            wizard.Init(dto, dto.Resume ? wizard.TotalSteps : 1);
        }

        private void InitUi(DataConnection dto)
        {
            this.Text = $@"{Config.DW.Title} › {dto.Source.Name}";
        }

        public static bool ShowFilterDialog(DataConnection dto)
        {
            using (var form = new GetDataForm(dto))
                return form.ShowDialog() == DialogResult.OK;
        }

        #region Event Handlers

        private void WizardCompleted(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.OK;
            this.Close();
        }

        #endregion
    }
}
