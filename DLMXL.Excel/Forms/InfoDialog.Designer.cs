﻿namespace ExcelAddIn.Forms
{
    partial class InfoDialog
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
			this.pnlActions = new System.Windows.Forms.FlowLayoutPanel();
			this.OkButton = new ExcelAddIn.UI.Controls.MainButton();
			this.pnlMain = new DevExpress.XtraEditors.PanelControl();
			this.lblMessage = new DevExpress.XtraEditors.MemoEdit();
			this.CancelButton = new ExcelAddIn.UI.Controls.MainButton();
			this.pnlActions.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.pnlMain)).BeginInit();
			this.pnlMain.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.lblMessage.Properties)).BeginInit();
			this.SuspendLayout();
			// 
			// pnlActions
			// 
			this.pnlActions.Controls.Add(this.OkButton);
			this.pnlActions.Controls.Add(this.CancelButton);
			this.pnlActions.Dock = System.Windows.Forms.DockStyle.Bottom;
			this.pnlActions.FlowDirection = System.Windows.Forms.FlowDirection.RightToLeft;
			this.pnlActions.Location = new System.Drawing.Point(0, 222);
			this.pnlActions.Name = "pnlActions";
			this.pnlActions.Padding = new System.Windows.Forms.Padding(3);
			this.pnlActions.Size = new System.Drawing.Size(509, 29);
			this.pnlActions.TabIndex = 14;
			// 
			// OkButton
			// 
			this.OkButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
			this.OkButton.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
			this.OkButton.Appearance.Options.UseFont = true;
			this.OkButton.DialogResult = System.Windows.Forms.DialogResult.OK;
			this.OkButton.Location = new System.Drawing.Point(427, 6);
			this.OkButton.Name = "OkButton";
			this.OkButton.Size = new System.Drawing.Size(73, 20);
			this.OkButton.TabIndex = 4;
			this.OkButton.Text = "OK";
			// 
			// pnlMain
			// 
			this.pnlMain.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
			this.pnlMain.Controls.Add(this.lblMessage);
			this.pnlMain.Dock = System.Windows.Forms.DockStyle.Fill;
			this.pnlMain.Location = new System.Drawing.Point(0, 0);
			this.pnlMain.Margin = new System.Windows.Forms.Padding(0);
			this.pnlMain.Name = "pnlMain";
			this.pnlMain.Padding = new System.Windows.Forms.Padding(12);
			this.pnlMain.Size = new System.Drawing.Size(509, 222);
			this.pnlMain.TabIndex = 15;
			// 
			// lblMessage
			// 
			this.lblMessage.Dock = System.Windows.Forms.DockStyle.Fill;
			this.lblMessage.Location = new System.Drawing.Point(12, 12);
			this.lblMessage.Name = "lblMessage";
			this.lblMessage.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 10F);
			this.lblMessage.Properties.Appearance.Options.UseFont = true;
			this.lblMessage.Properties.AppearanceReadOnly.BackColor = System.Drawing.Color.White;
			this.lblMessage.Properties.AppearanceReadOnly.Options.UseBackColor = true;
			this.lblMessage.Properties.ReadOnly = true;
			this.lblMessage.Size = new System.Drawing.Size(485, 198);
			this.lblMessage.TabIndex = 0;
			// 
			// CancelButton
			// 
			this.CancelButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
			this.CancelButton.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
			this.CancelButton.Appearance.Options.UseFont = true;
			this.CancelButton.DialogResult = System.Windows.Forms.DialogResult.Cancel;
			this.CancelButton.Location = new System.Drawing.Point(348, 6);
			this.CancelButton.Name = "CancelButton";
			this.CancelButton.Size = new System.Drawing.Size(73, 20);
			this.CancelButton.TabIndex = 5;
			this.CancelButton.Text = "Cancel";
			this.CancelButton.Visible = false;
			// 
			// InfoDialog
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.CancelButton = this.CancelButton;
			this.ClientSize = new System.Drawing.Size(509, 251);
			this.Controls.Add(this.pnlMain);
			this.Controls.Add(this.pnlActions);
			this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
			this.MaximizeBox = false;
			this.MinimizeBox = false;
			this.Name = "InfoDialog";
			this.ShowIcon = false;
			this.ShowInTaskbar = false;
			this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
			this.Text = "Title";
			this.pnlActions.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.pnlMain)).EndInit();
			this.pnlMain.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.lblMessage.Properties)).EndInit();
			this.ResumeLayout(false);

        }

        #endregion
		private System.Windows.Forms.FlowLayoutPanel pnlActions;
		private UI.Controls.MainButton OkButton;
		private DevExpress.XtraEditors.PanelControl pnlMain;
		private DevExpress.XtraEditors.MemoEdit lblMessage;
		private UI.Controls.MainButton CancelButton;
	}
}