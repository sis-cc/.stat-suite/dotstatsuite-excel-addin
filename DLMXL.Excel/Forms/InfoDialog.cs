﻿using System;
using System.Windows.Forms;
using DevExpress.XtraEditors;

namespace ExcelAddIn.Forms
{
    public enum MessageType
    {
        Info,
        Error,
		Wait
    }
    
    public partial class InfoDialog : XtraForm
    {
        /// <summary>
        /// Designer only constructor 
        /// </summary>
        private InfoDialog()
        {
            InitializeComponent();
            InitUi();
        }

        private void InitUi()
        {
            this.Text = Config.DW.Title;
        }

        public InfoDialog(string message, MessageType type): this()
        {
	        
            lblMessage.Text = message;

            if (type == MessageType.Wait)
            {
	            this.OkButton.Visible = false;
	            this.CancelButton.Visible = true;
            }
        }

        public static void Show(string message, MessageType type = MessageType.Info)
        {
            using (var form = new InfoDialog(message, type))
                form.ShowDialog();
        }

        public void ThreadSafeException(Exception e)
        {
	        if (this.InvokeRequired)
	        {
		        this.BeginInvoke(new Action<Exception>(this.ShowException), e);
	        }
	        else
	        {
		        this.ShowException(e);
	        }
        }

        public void ThreadSafeClose()
        {
	        if (this.InvokeRequired)
	        {
		        this.Invoke(new Action(this.GracefulClose));
	        }
	        else
	        {
		        this.GracefulClose();
	        }
        }

        private void GracefulClose()
        {
	        DialogResult = DialogResult.OK;
			this.Close();
        }

        private void ShowException(Exception e)
        {
	        while (e.InnerException != null)
		        e = e.InnerException;

	        this.lblMessage.Text = e.Message + @"\n\n" + e.StackTrace;
        }
    }
}
