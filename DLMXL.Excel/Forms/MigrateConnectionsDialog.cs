﻿using System;
using System.Collections.Generic;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using Microsoft.Office.Interop.Excel;
using DevExpress.XtraEditors.Controls;
using ExcelAddIn.DAL;
using ExcelAddIn.Domain.Wizard;
using ExcelAddIn.UI;

namespace ExcelAddIn.Forms
{
    public partial class MigrateConnectionsDialog : XtraForm
    {
        private readonly Workbook Book;

        public delegate void WorkbookConnectionToMigrateEventHandler(WorkbookConnection workbookConnection);

        public event WorkbookConnectionToMigrateEventHandler WorkbookConnectionToMigrate;

        //public event 
        /// <summary>
        /// Designer only constructor 
        /// </summary>
        private MigrateConnectionsDialog()
        {
            InitializeComponent();
        }

        public MigrateConnectionsDialog(Workbook book) : this()
        {
            Book = book;

            if (WorkbookConnectionBag.WorkbooksConnectionsToMigrate.ContainsKey(book))
            {
                foreach (var wc in WorkbookConnectionBag.WorkbooksConnectionsToMigrate[book])
                {
                    ListObject listObject;
                    if (WorkbookConnectionBag.IsWorkbookConnectionLinkedToTable(wc.Value, out listObject))
                    {
                        string description = "";

                        description = string.IsNullOrEmpty(wc.Value.Description)
                            ? "{0} ({1})".F(wc.Key, listObject.Name)
                            : "{0} ({1})".F(wc.Key, wc.Value.Description);

                        clbConnections.Items.Add(new CheckedListBoxItem(wc.Key, description));
                    }
                }
            }
        }

        private void btnMigate_Click(object sender, System.EventArgs e)
        {
            if (WorkbookConnectionBag.WorkbooksConnectionsToMigrate.ContainsKey(Book))
            {
                using (new LongRunningOperation())
                {
                    foreach (CheckedListBoxItem clBoxItem in clbConnections.CheckedItems)
                    {
                        if (
                            WorkbookConnectionBag.WorkbooksConnectionsToMigrate[Book].ContainsKey(
                                clBoxItem.Value.ToString()))
                        {
                            string connectionName = clBoxItem.Value.ToString();

                            WorkbookConnection wc =
                                WorkbookConnectionBag.WorkbooksConnectionsToMigrate[Book][connectionName];

                            WorkbookConnectionToMigrateEventHandler handler = WorkbookConnectionToMigrate;
                            if (handler != null)
                                handler(wc);
                        }
                    }
                }
            }
        }
    }
}
