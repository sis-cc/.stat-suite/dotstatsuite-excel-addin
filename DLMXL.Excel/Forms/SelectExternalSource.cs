﻿using System.Linq;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using ExcelAddIn.DAL;

namespace ExcelAddIn.Forms
{
    public partial class SelectExternalSource : XtraForm
    {
        /// <summary>
        /// Designer only constructor 
        /// </summary>
        private SelectExternalSource()
        {
            InitializeComponent();
            BindSources();
            InitUi();
        }

        private void InitUi()
        {
            this.Text = $@"{Config.DW.Title} › Select external source";
        }

        private void BindSources()
        {
            foreach (var source in OecdDataSource.AllSources.Where(x => x.IsExternal))
                this.cbSource.Items.Add(source);
        }

        private IOecdDataSource Source => this.cbSource.SelectedItem as IOecdDataSource;

        public static IOecdDataSource ShowFilterDialog()
        {
            using (var f = new SelectExternalSource())
            {
                var result = f.ShowDialog();

                return result == DialogResult.OK
                    ? f.Source
                    : null;
            }
        }
    }
}
