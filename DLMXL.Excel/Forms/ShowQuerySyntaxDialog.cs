﻿using System.Windows.Forms;
using DevExpress.XtraEditors;
using ExcelAddIn.Domain.Wizard;
using ExcelAddIn.UI;

namespace ExcelAddIn.Forms
{
    public sealed partial class ShowQuerySyntaxDialog : XtraForm
    {
        /// <summary>
        /// Designer only constructor 
        /// </summary>
        public ShowQuerySyntaxDialog()
        {
            InitializeComponent();
        }

        public ShowQuerySyntaxDialog(DataConnection connection) : this()
        {
            if (connection == null)
                return;

            var queries = connection.Source.DataAccess.GetConnectionStrings(
                connection.Filter,
                connection.IsProduction,
                connection.IsUpdateData
            );

            foreach (var query in queries)
            {
                var ctrl = new ExternalSystemQuery(query) {Dock = DockStyle.Bottom};
                pnlMain.Controls.Add(ctrl);
            }
        }
    }
}
