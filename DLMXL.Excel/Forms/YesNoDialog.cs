﻿using System.Windows.Forms;
using DevExpress.XtraEditors;

namespace ExcelAddIn.Forms
{    
    public partial class YesNoDialog : XtraForm
    {
        /// <summary>
        /// Designer only constructor 
        /// </summary>
        private YesNoDialog()
        {
            InitializeComponent();
            InitUi();
        }

        private void InitUi()
        {
            this.Text = Config.DW.Title;
        }

        public YesNoDialog(string message): this()
        {
            lblMessage.Text = message;
        }

        public static bool Show(string message)
        {
            using (var form = new YesNoDialog(message) {StartPosition = FormStartPosition.CenterParent})
                return form.ShowDialog() == DialogResult.OK;
        }
    }
}
