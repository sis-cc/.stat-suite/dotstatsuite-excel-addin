﻿using System.IO;
using System.Xml;
using System.Xml.Linq;
using OXM;

namespace ExcelAddIn.Mappers
{
    public abstract class BaseMap<T, M> : RootElementMap<T>
        where T : class
        where M : BaseMap<T, M>, new()
    {
        public static string Serialize(T item)
        {
            if (item == null)
                return null;

            var map = new M();
            var doc = new XDocument();

            using (var writer = doc.CreateWriter())
            {
                map.WriteXml(writer, item);
            }

            return doc.ToString();
        }

        public static T Deserialize(string xml)
        {
            if (string.IsNullOrEmpty(xml))
                return null;

            var map = new M();

            using (var reader = XmlReader.Create(new StringReader(xml)))
                return map.ReadXml(reader);
        }
    }
}
