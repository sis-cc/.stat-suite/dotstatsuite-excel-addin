﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Xml.Linq;
using DLMXL.Models;
using ExcelAddIn.DAL;
using ExcelAddIn.Domain.Wizard;
using OXM;

namespace ExcelAddIn.Mappers
{
    public class DataConnectionMap : ClassMap<DataConnection>
    {
        private readonly DataConnection _conn;

        public DataConnectionMap()
        {
            _conn = new DataConnection();

            Map(o => o.Id)
                .ToSimpleElement("Id", true)
                .Set(v => _conn.Id = v)
                .Converter(new StringConverter());
            
            Map(o => o.Source.Code)
                .ToSimpleElement("Type", true)
                .Set(v => _conn.Source = GetSource(v))
                .Converter(new StringConverter());

            Map(o => o.Filter)
                .ToElement("Filter", true)
                .Set(v => _conn.Filter = v)
                .ClassMap(() => new SdmxFilterMap());

            Map(o => o.StartCell)
                .ToSimpleElement("StartCell", true)
                .Set(v => _conn.StartCell = v)
                .Converter(new StringConverter());

            Map(o => o.EndCell)
                .ToSimpleElement("EndCell", true)
                .Set(v => _conn.EndCell = v)
                .Converter(new StringConverter());

            Map(o => o.IsProduction)
                .ToSimpleElement("IsProduction", true)
                .Set(v => _conn.IsProduction = v)
                .Converter(new BooleanConverter());

            Map(o => o.IsFlags)
                .ToSimpleElement("IsFlags", true)
                .Set(v => _conn.IsFlags = v)
                .Converter(new BooleanConverter());

            Map(o => o.IsMetadata)
                .ToSimpleElement("IsMetadata", false)
                .Set(v => _conn.IsMetadata = v)
                .Converter(new BooleanConverter());

            Map(o => o.IsActionColumn)
                .ToSimpleElement("IsActionColumn", false)
                .Set(v => _conn.IsActionColumn = v)
                .Converter(new BooleanConverter());

            Map(o => o.IsUpdateData)
                .ToSimpleElement("IsUpdateData", true)
                .Set(v => _conn.IsUpdateData = v)
                .Converter(new BooleanConverter());

            Map(o => o.Language)
                .ToSimpleElement("Language2", false)
                .Set(v => _conn.Language = v)
                .Converter(new StringConverter());

            Map(o => (int) o.LabelOutput)
	            .ToSimpleElement("Language", true)
	            .Set(v =>
	            {
		            _conn.LabelOutput = (LabelOutput) v;

		            if ((_conn.LabelOutput & LabelOutput.English) > 0)
			            _conn.Language = "en";

		            if ((_conn.LabelOutput & LabelOutput.French) > 0)
			            _conn.Language = "fr";

	                if ((_conn.LabelOutput & LabelOutput.Label) > 0 && string.IsNullOrEmpty(_conn.Language))
	                {
	                    _conn.Language = "en";
	                }
	            })
	            .Converter(new Int32Converter());

            Map(o => string.Format("{0:yyyy-MM-ddTHH:mm:ss}", o.LastRefreshTime))
                .ToSimpleElement("LastRefreshTime", true)
                .Set(v => _conn.LastRefreshTime = DateTime.Parse(v))
                .Converter(new StringConverter());

            Map(o => o.OutputType.ToString())
                .ToSimpleElement("OutputType", true)
                .Set(v => _conn.OutputType = (OutputType)Enum.Parse(typeof(OutputType), v))
                .Converter(new StringConverter());
        }

        private static IOecdDataSource GetSource(string type)
        {
            if (type == ".STAT")
                type = "DotStat";

			return OecdDataSource
			 .AllSources
			 .FirstOrDefault(x => x.Code.Equals(type, StringComparison.InvariantCulture));
		}

        protected override DataConnection Return()
        {
			if (_conn.Source == null)
				return null;

			_conn.Filter.FilterBuilder = _conn.Source.BuildSwFilter;
            return _conn;
        }
    }

    public class DataConnectionArrayMap : BaseMap<IEnumerable<DataConnection>, DataConnectionArrayMap>
    {
        private readonly List<DataConnection> _connections;

        public DataConnectionArrayMap()
        {
            _connections = new List<DataConnection>();

            MapCollection(o => o)
                .ToElement("Connection", true)
                .Set(x =>
                {
	                if (x != null)
	                {
		                _connections.Add(x);
	                }
                })
                .ClassMap(() => new DataConnectionMap());
        }

        protected override IEnumerable<DataConnection> Return()
        {
            return _connections;
        }

        public override XName Name
        {
            get { return "Connections"; }
        }
    }
}
