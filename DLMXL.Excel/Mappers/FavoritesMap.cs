﻿using System.Collections.Generic;
using System.Xml.Linq;
using ExcelAddIn.Domain;
using OXM;

namespace ExcelAddIn.Mappers
{
    public class FavoritesMap : BaseMap<FavoritesCache, FavoritesMap>
    {
        private readonly FavoritesCache _list;

        public FavoritesMap()
        {
            _list = new FavoritesCache();

            MapCollection(o => o)
                .ToElement("Item", true)
                .Set(v => _list.Add(v))
                .ClassMap(() => new FavoritesHierarchyMap());
        }

        protected override FavoritesCache Return()
        {
            return _list;
        }

        public override XName Name
        {
            get { return "Favorties"; }
        }
    }

    public class FavoritesHierarchyMap : ClassMap<KeyValuePair<string, HashSet<string>>>
    {
        private string _level;
        private HashSet<string> _ids; 

        public FavoritesHierarchyMap()
        {
            Map(o => o.Key)
                .ToSimpleElement("Level", true)
                .Set(v => _level=v)
                .Converter(new StringConverter());

            Map(o => o.Value)
                .ToElement("Set", false)
                .Set(v => _ids = v)
                .ClassMap(()=>new FavoritesSetMap());
        }

        protected override KeyValuePair<string, HashSet<string>> Return()
        {
            return new KeyValuePair<string, HashSet<string>>(_level, _ids);
        }
    }

    public class FavoritesSetMap : ClassMap<HashSet<string>>
    {
        private HashSet<string> _set = new HashSet<string>();
 
        public FavoritesSetMap()
        {
            MapCollection(o => o)
                .ToElement("Item", false)
                .Set(v => _set.Add(v))
                .ClassMap(() => new IdMap());
        }

        protected override HashSet<string> Return()
        {
            return _set;
        }
    }

    public class IdMap : ClassMap<string>
    {
        private string _id;

        public IdMap()
        {
            Map(o => o)
                .ToAttribute("id", true)
                .Set(v => _id = v)
                .Converter(new StringConverter());
        }

        protected override string Return()
        {
            return _id;
        }
    }
}
