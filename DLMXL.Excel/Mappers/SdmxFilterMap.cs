﻿using System;
using DLMXL.Models.Interfaces;
using ExcelAddIn.Domain;
using OXM;

namespace ExcelAddIn.Mappers
{
    public class SdmxFilterMap : ClassMap<SdmxFilter>
    {
        private readonly SdmxFilter _filter;

        public SdmxFilterMap()
        {
            _filter = new SdmxFilter();

            Map(o => o.SdmxQuery)
                .ToSimpleElement("Sdmx", true)
                .Set(v => _filter.SdmxQuery = GetSdmxString(v))
                .Converter(new StringConverter());

            Map(o => (int) o.Frequency)
                .ToSimpleElement("Frequency", true)
                .Set(v => _filter.Frequency = (Frequency) v)
                .Converter(new Int32Converter());

            Map(o => o.LastPeriods ?? 0)
                .ToSimpleElement("LastPeriods", false)
                .Set(v => _filter.LastPeriods = v > 0 ? (int?) v : null)
                .Converter(new Int32Converter());

            Map(o => o.StartTime)
                .ToSimpleElement("StartTime", false)
                .Set(v => _filter.StartTime = v)
                .Converter(new StringConverter());

            Map(o => o.EndTime)
                .ToSimpleElement("EndTime", false)
                .Set(v => _filter.EndTime = v)
                .Converter(new StringConverter());

            Map(o => o.UpdatedAfter.ToDT())
                .ToSimpleElement("UpdatedAfter", false)
                .Set(v => _filter.UpdatedAfter = DateTime.Parse(v))
                .Converter(new StringConverter());

            Map(o => o.HistoricVersionType.HasValue ? (int) o.HistoricVersionType.Value : 0)
                .ToSimpleElement("HistoricVersionType", false)
                .Set(v => _filter.HistoricVersionType = v > 0 ? (HistoricVersionType?) v : null)
                .Converter(new Int32Converter());

            Map(o => o.SourceId)
                .ToSimpleElement("SourceId", false)
                .Set(v => _filter.SourceId = v)
                .Converter(new StringConverter());

            Map(o => (int) o.SourceType)
                .ToSimpleElement("SourceType", false)
                .Set(v => _filter.SourceType = (FavoriteTreeItemType)v)
                .Converter(new Int32Converter());
        }

        private static string GetSdmxString(string sdmx)
        {
            if (string.IsNullOrEmpty(sdmx))
                return null;

            return sdmx.StartsWith("DOTSTAT.") ? sdmx.Substring(8) : sdmx;
        }

        protected override SdmxFilter Return()
        {
            return _filter;
        }
    }
}
