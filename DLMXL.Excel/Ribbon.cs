﻿using System;
using System.Collections.Generic;
using System.Deployment.Application;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using DLMXL.DAL;
using DLMXL.DAL.Access;
using DLMXL.Models;
using DLMXL.Models.Exceptions;
using DLMXL.Models.Interfaces;
using ExcelAddIn.DAL;
using ExcelAddIn.Domain;
using ExcelAddIn.Domain.Wizard;
using ExcelAddIn.Forms;
using ExcelAddIn.UI;
using ExcelAddIn.UI.Meta;
using Microsoft.Office.Interop.Excel;
using Office = Microsoft.Office.Core;
using Excel = Microsoft.Office.Interop.Excel;

namespace ExcelAddIn
{
    [ComVisible(true)]
    public class Ribbon : Office.IRibbonExtensibility
    {
        // https://support.office.com/en-nz/article/Excel-specifications-and-limits-1672b34d-7043-467e-8e27-269d656771c3
        private const int MaxWidth = 16384;    // 2^14
        private const int MaxHeight = 1048576;  // 2^20

        private Office.IRibbonUI ribbon;
        private bool _pnlDataVisible;
        private bool _pnlLoginVisible;
        private bool _pnlManagementVisible;
        private bool _pnlMetaVisible;
        private bool _pnlInfoVisible;
        private bool _btnChangeVisible;
        private bool _btnRefreshSheetVisible;
        private bool _btnRefreshAllVisible;
        private bool _btnHowToDeleteVisible;
        private string _lblSource;
        private string _lblUpdateDate;
        private bool _isIdLabel = true;
        private bool _isNonExistingDataflow = false;
        private string _lblDatasetId;
        private string _lblDatasetName;

        private Dictionary<string, KeyValuePair<string, bool>> _logOutDict = new Dictionary<string, KeyValuePair<string, bool>>();
        private DataConnection activeConnection;
        private List<Office.CommandBarButton> connectionContextButtons;
        private CloseRequestInfo _pendingCloseRequest;
        private Dictionary<string, int> _customTaskPanes = new Dictionary<string, int>();

        private int AmountOfOpenWorkbooks => Globals.ThisAddIn.Application.Workbooks.Count;
        private int Hwnd => Globals.ThisAddIn.Application.Hwnd;

        #region IRibbonExtensibility Members

        public string GetCustomUI(string ribbonID)
        {
            return GetResourceText("ExcelAddIn.Ribbon.xml");
        }

        #endregion

        #region Ribbon Callbacks

        public void Ribbon_Load(Office.IRibbonUI ribbonUI)
        {
            this.ribbon = ribbonUI;

            Globals.ThisAddIn.Application.SheetActivate += OnSheetActivation;
            Globals.ThisAddIn.Application.WorkbookOpen += OnWorkbookOpen;
            Globals.ThisAddIn.Application.WorkbookBeforeSave += OnWorkbookSave;
            Globals.ThisAddIn.Application.WorkbookBeforeClose += OnBeforeWorkbookClosed;
            Globals.ThisAddIn.Application.WorkbookActivate += OnWorkbookActivate;
            Globals.ThisAddIn.Application.WorkbookDeactivate += OnWorkbookDeactivate;
            Globals.ThisAddIn.Application.SheetSelectionChange += OnSelectionChange;
            Globals.ThisAddIn.Application.SheetBeforeRightClick += OnRightClick;
            Globals.ThisAddIn.Application.SheetChange += OnSheetCellChange;

            UpdateButtonsForActiveSheet(Globals.ThisAddIn.GetActiveWorksheet());
        }

        public System.Drawing.Image GetImage(string image)
        {
            return (System.Drawing.Image) Resources.ResourceManager.GetObject(image);
        }

        public string GetRibbonTitle(Office.IRibbonControl ctrl)
        {
            return Config.DW.Title;
        }

        public string GetAppVersion(Office.IRibbonControl ctrl)
        {
            return ApplicationDeployment.IsNetworkDeployed
                ? $"v{ApplicationDeployment.CurrentDeployment.CurrentVersion}"
                : null;
        }

        public bool GetVisibility(Office.IRibbonControl ctrl)
        {
            switch (ctrl.Id)
            {
                case "pnlData": return _pnlDataVisible;
                case "pnlLogin": return _pnlLoginVisible;
                case "pnlManagement": return _pnlManagementVisible;
                case "pnlMeta": return _pnlMetaVisible;
                case "pnlInfo": return _pnlInfoVisible;
                case "btnChange": return _btnChangeVisible;
                case "btnRefreshSheet": return _btnRefreshSheetVisible;
                case "btnRefreshAll": return _btnRefreshAllVisible;
                case "btnHowToDelete": return _btnHowToDeleteVisible;
            }

            return true;
        }

        public string GetInfoLabels(Office.IRibbonControl ctrl)
        {
            switch (ctrl.Id)
            {
                case "lblDataset": return _isIdLabel ? _lblDatasetId : _lblDatasetName;
                case "lblSource": return _lblSource;
                case "lblUpdateDate": return _lblUpdateDate;
            }

            throw new NotImplementedException();
        }

        public string GetHowToDeleteLabel(Office.IRibbonControl ctrl)
        {
            return activeConnection != null
                ? $"How to delete {(activeConnection.IsMetadata ? "ref. metadata" : "data")}?"
                : null;
        }

        public void Label_Change(Office.IRibbonControl ctrl)
        {
            this._isIdLabel = ctrl.Id == "item_id";

            this.ribbon.InvalidateControl("lblDataset");
            this.ribbon.InvalidateControl("mDataset");
        }

        public System.Drawing.Image GetLabelImage(Office.IRibbonControl ctrl)
        {
            var labelImage = _isNonExistingDataflow 
                ? "img_lbl_alert"
                : this._isIdLabel ? "img_lbl_id" : "img_lbl_tag";

            return (System.Drawing.Image) Resources.ResourceManager.GetObject(labelImage);
        }

        #region Logout
        public string AddLogoutButtons(Office.IRibbonControl ctrl)
        {
            var sb = new StringBuilder("<menu xmlns='http://schemas.microsoft.com/office/2006/01/customui'>");
            foreach (var group in OecdDataSource.AllSources.Where(x => x.DataAccess.IsAuthRequired).GroupBy(x => x.DataAccess.AuthId))
            {
                if (!_logOutDict.ContainsKey(group.Key))
                {
                    _logOutDict.Add(group.Key, new KeyValuePair<string, bool>());
                }
                sb.Append($"<button id='{group.Key}' tag='{group.First().Code}' screentip='{string.Join(", ", group.Select(x => x.Code))}' getLabel='GetLogOutLabel' getVisible='GetLogOutVisibility' onAction='LogOut' />");
            }
            sb.Append("</menu>");

            return sb.ToString();
        }
        public void LogOut(Office.IRibbonControl ctrl)
        {
            var authId = ctrl.Id;
            var sourceId = ctrl.Tag;
            
            try
            {
                using (new LongRunningOperation())
                {
                    OecdDataSource.GetById(sourceId).DataAccess.LogOut();
                }

                _logOutDict[authId] = new KeyValuePair<string, bool>();
                _pnlLoginVisible = _logOutDict.Values.Any(x => x.Value);

                ribbon.InvalidateControl(authId);
                
                if (!_pnlLoginVisible)
                {
                    ribbon.InvalidateControl("pnlLogin");
                }
            }
            catch (Exception ex)
            {
                Log.Handle(ex);
            }
        }
        public string GetLogOutLabel(Office.IRibbonControl ctrl)
        {
            return _logOutDict[ctrl.Id].Key;
        }
        public bool GetLogOutVisibility(Office.IRibbonControl ctrl)
        {
            return _logOutDict[ctrl.Id].Value;
        }
        #endregion

        #region Get data
        public string AddDataSources(Office.IRibbonControl ctrl)
        {
            var i = 0;
            var sb = new StringBuilder("<menu xmlns='http://schemas.microsoft.com/office/2006/01/customui' itemSize='large'>");

            foreach (var source in OecdDataSource.AllSources.Where(x => !x.IsExternal))
            {
                sb.Append($"<button id='dataBtn{i++}' label='{source.Name}' tag='{source.Code}' onAction='GetDataClick' />");
            }

            sb.Append($"<button id='dataBtn{i++}' label='External Sources...' onAction='ExternalSourcesClick' />")
                .Append("</menu>");

            return sb.ToString();
        }

        public void GetDataClick(Office.IRibbonControl ctrl)
        {
            try
            {
                SelectSource(OecdDataSource.GetById(ctrl.Tag));
            }
            catch (Exception ex)
            {
                Log.Handle(ex);
            }
        }
        public void ExternalSourcesClick(Office.IRibbonControl ctrl)
        {
            try
            {
                var dataSource = SelectExternalSource.ShowFilterDialog();

                if (dataSource != null)
                    SelectSource(dataSource);
            }
            catch (Exception ex)
            {
                Log.Handle(ex);
            }
        }

        #endregion

        public void btnHelp_Click(Office.IRibbonControl ctrl)
        {
            var link = ctrl.Id == "btnAbout"
                ? Config.DW.HelpLink
                : activeConnection.IsMetadata
                    ? "https://sis-cc.gitlab.io/dotstatsuite-documentation/using-dlm-excel-addin/edit-data/#deleting-reference-metadata"
                    : "https://sis-cc.gitlab.io/dotstatsuite-documentation/using-dlm-excel-addin/edit-data/#deleting-data";

            if (string.IsNullOrEmpty(link))
                return;

            try
            {
                var book = Globals.ThisAddIn.GetActiveWorkbook();

                book.FollowHyperlink(link);
            }
            catch { }
        }
        public void btnChange_Click(Office.IRibbonControl ctrl)
        {
            if (activeConnection == null)
                return;

            try
            {
                Authenticate(activeConnection.Source.DataAccess);

                var connection = activeConnection.Clone();
                var currentRegion = connection.Region(Globals.ThisAddIn.GetActiveWorksheet());

                InvokeWizard(connection, currentRegion);
            }
            catch (Exception ex)
            {
                Log.Handle(ex);
            }
        }
        public void btnRefreshWorkbook_Click(Office.IRibbonControl ctrl)
        {
            try
            {
                using (new LongRunningOperation())
                {
                    ClearCache();
                    RefreshConnectionsForWorkbook(Globals.ThisAddIn.GetActiveWorkbook());
                }
            }
            catch (Exception ex)
            {
                Log.Handle(ex);
            }
        }
        public void btnRefreshSheet_Click(Office.IRibbonControl ctrl)
        {
            try
            {
                using (new LongRunningOperation())
                {
                    ClearCache();
                    RefreshConnectionsForWorksheet(Globals.ThisAddIn.GetActiveWorksheet());
                }

            }
            catch (Exception ex)
            {
                Log.Handle(ex);
            }
        }
        public void btnSaveSelected_Click(Office.IRibbonControl ctrl)
        {
            Save(true);
        }
        public void btnSaveAll_Click(Office.IRibbonControl ctrl)
        {
            Save(false);
        }

        #region Meta edit

        public async void btnMeta_Click(Office.IRibbonControl ctrl)
        {
            if (MetaUtils.IsCellReferencingMetadata(activeConnection, Globals.ThisAddIn.Application.ActiveCell, out var metaAttribute))
            {
                await ShowMetadataEditor(metaAttribute, Globals.ThisAddIn.Application.ActiveCell);
            }
        }

        private async Task ShowMetadataEditor(IMetadataAttribute metadataAttribute, Excel.Range target)
        {
            var pane = GetTaskPane();
            var metadataEditor = (MetadataEditor)pane.Control;

            pane.Visible = true;
            metadataEditor.ShowEditorForRange(
                metadataAttribute,
                target
            );
        }

        private void HideMetadataEditor()
        {
            var pane = GetTaskPane();
            pane.Visible = false;
        }

        private Microsoft.Office.Tools.CustomTaskPane GetTaskPane()
        {
            if (_customTaskPanes.TryGetValue(MetaSidePaneId, out var paneIndex))
            {
                return Globals.ThisAddIn.CustomTaskPanes[paneIndex];
            }

            var pane = Globals.ThisAddIn.CustomTaskPanes.Add(
                new MetadataEditor(),
                "Metadata editor",
                Globals.ThisAddIn.Application.ActiveWindow
            );
            pane.DockPosition = Office.MsoCTPDockPosition.msoCTPDockPositionRight;
            pane.Width = 650;
            pane.VisibleChanged += MetaPaneVisibility_Changed;

            _customTaskPanes.Add(MetaSidePaneId, Globals.ThisAddIn.CustomTaskPanes.Count - 1);

            return pane;
        }

        private void MetaPaneVisibility_Changed(object sender, EventArgs e)
        {
            var pane = (Microsoft.Office.Tools.CustomTaskPane)sender;

            if (!pane.Visible)
            {
                ((MetadataEditor)pane.Control).EnsureSaved();
                pane.Control.Visible = false;
            }
        }

        private string MetaSidePaneId => "Meta_" + Hwnd;

        private bool IsMetaSidePaneVisible => _customTaskPanes.TryGetValue(MetaSidePaneId, out var paneIndex)
                                                        &&
                                                    Globals.ThisAddIn.CustomTaskPanes[paneIndex].Visible;

        #endregion

        #endregion

        #region Events
        private void Ribbon_Close(object sender, EventArgs e)
        {
            RemoveContextButtons();
        }

        private void OnRightClick(object sh, Excel.Range target, ref bool cancel)
        {
            RemoveContextButtons();
            CreateContextButtons(activeConnection);
        }

        private void CreateContextButtons(DataConnection connection)
        {
            if (connection == null)
                return;

            var contextMenu = Globals.ThisAddIn.Application.CommandBars["Cell"];

            connectionContextButtons = new List<Office.CommandBarButton>
            {
                CreateContextButton(contextMenu, "Query syntax", 1000, OnShowQuerySyntax),
                CreateContextButton(contextMenu, "About connection", 487, OnAboutConnection),
            };

            if (connection.Filter.SWFilter != null)
            {
                connectionContextButtons.Add(CreateContextButton(contextMenu, "Refresh connection", 3817, OnRefreshConnection));
            }
            
            connectionContextButtons.Add(CreateContextButton(contextMenu, "Break connection", 2087, OnBreakConnection));

            if (
                activeConnection.IsMetadata
                && MetaUtils.IsCellReferencingMetadata(activeConnection, Globals.ThisAddIn.Application.ActiveCell, out var metaAttribute)
                && metaAttribute.IsExternalEditor
            )
            {
                connectionContextButtons.Add(CreateContextButton(contextMenu, "Edit metadata", 205, EditMeta));
            }
        }

        private void EditMeta(Office.CommandBarButton ctrl, ref bool canceldefault)
        {
            if (MetaUtils.IsCellReferencingMetadata(activeConnection, Globals.ThisAddIn.Application.ActiveCell, out var metaAttribute))
            {
                ShowMetadataEditor(metaAttribute, Globals.ThisAddIn.Application.ActiveCell);
            }
        }

        private void RemoveContextButtons()
        {
            if (connectionContextButtons == null)
                return;

            for (var i = 0; i < connectionContextButtons.Count; i++)
                if (connectionContextButtons[i] != null)
                {
                    connectionContextButtons[i].Delete(true);
                    connectionContextButtons[i] = null;
                }

            connectionContextButtons = null;
        }

        private void OnSelectionChange(object sh, Excel.Range target)
        {
            try
            {
                OnCellActivation(target);
            }
            catch (Exception ex)
            {
                Log.Handle(ex);
            }
        }

        private void OnSheetCellChange(object sender, Excel.Range target)
        {
            var connection = FindConnectionForRange(target, true);

            if (connection != null)
            {
                var region = connection.Region(target.Worksheet).CurrentRegion;
                var start = target.Worksheet.Range[connection.StartCell];
                var end = (Excel.Range)start.Cells[region.Rows.Count, region.Columns.Count];
                connection.EndCell = end.RangeAddress();

                connection.ApplyFormating(target.Worksheet);
            }
        }

        private void OnSheetActivation(object sh)
        {
            try
            {
                UpdateButtonsForActiveSheet(sh as Excel.Worksheet);
            }
            catch (Exception ex)
            {
                Log.Handle(ex);
            }
        }

        private void OnWorkbookOpen(Workbook book)
        {
            try
            {
                WorkbookConnectionBag.ExtractConnections(book);
            }
            catch (Exception ex)
            {
                Log.Handle(ex);
            }
        }

        private void OnWorkbookSave(Workbook book, bool saveasui, ref bool cancel)
        {
            try
            {
                WorkbookConnectionBag.SaveConnections(book, saveasui);
            }
            catch (Exception ex)
            {
                Log.Handle(ex);
            }
        }

        private void OnBeforeWorkbookClosed(Workbook wb, ref bool cancel)
        {
            if (!cancel)
            {
                _pendingCloseRequest = new CloseRequestInfo(wb.GetId(), AmountOfOpenWorkbooks);
            }
        }

        private void OnWorkbookDeactivate(Workbook wb)
        {
            if (AmountOfOpenWorkbooks != 1)
                return;

            try
            {
                if (_pendingCloseRequest != null)
                {
                    WorkbookConnectionBag.CloseWorkbook(_pendingCloseRequest.Id);
                    _pendingCloseRequest = null;
                }

                UpdateButtonsForActiveSheet();
            }
            catch (Exception ex)
            {
                Log.Handle(ex);
            }
        }

        private void OnWorkbookActivate(Workbook wb)
        {
            try
            {
                if (_pendingCloseRequest != null && _pendingCloseRequest.IsClosed(AmountOfOpenWorkbooks))
                {
                    WorkbookConnectionBag.CloseWorkbook(_pendingCloseRequest.Id);
                }

                _pendingCloseRequest = null;

                UpdateButtonsForActiveSheet(wb.ActiveSheet as Excel.Worksheet);

            }
            catch (Exception ex)
            {
                Log.Handle(ex);
            }
        }

        private void OnRefreshConnection(Office.CommandBarButton ctrl, ref bool canceldefault)
        {
            try
            {
                using (new LongRunningOperation())
                    RefreshConnection(activeConnection);
            }
            catch (Exception ex)
            {
                Log.Handle(ex);
            }
        }

        private void OnBreakConnection(Office.CommandBarButton ctrl, ref bool canceldefault)
        {
            if (activeConnection == null)
                return;

            try
            {
                var activeSheet = Globals.ThisAddIn.GetActiveWorksheet();

                WorkbookConnectionBag.BreakConnection(activeSheet, activeConnection);
                UpdateButtonsForActiveSheet(activeSheet);
            }
            catch (Exception ex)
            {
                Log.Handle(ex);
            }
        }

        private void OnShowQuerySyntax(Office.CommandBarButton ctrl, ref bool canceldefault)
        {
            if (activeConnection == null)
                return;

            try
            {
                using (var dlg = new ShowQuerySyntaxDialog(activeConnection))
                    dlg.ShowDialog();
            }
            catch (Exception ex)
            {
                Log.Handle(ex);
            }
        }

        private void OnAboutConnection(Office.CommandBarButton ctrl, ref bool canceldefault)
        {
            if (activeConnection == null)
                return;

            try
            {
                using (var form = new AboutConnection(activeConnection))
                    form.ShowDialog();
            }
            catch (Exception ex)
            {
                Log.Handle(ex);
            }
        }

        private void Save(bool saveSelectedOnly)
        {
            try
            {
                if (activeConnection == null || !activeConnection.IsSaveAllowed())
                    return;

                var dataAccess = activeConnection.Source.DataAccess;

                Authenticate(dataAccess);

                using (new LongRunningOperation())
                {
                    exitEditMode();

                    var activeSheet = Globals.ThisAddIn.GetActiveWorksheet();
                    var dataset = activeConnection.Filter.SWFilter.Dataset;
                    var currentRegion = activeConnection.Region(activeSheet).CurrentRegion;
                    var selection = saveSelectedOnly
                        ? Globals.ThisAddIn.Application.ActiveWindow.RangeSelection
                        : null;

                    var result = dataAccess.Save(
                        dataset,
                        new ExcelCellAccess(currentRegion, selection),
                        activeConnection.OutputType,
                        (activeConnection.LabelOutput & LabelOutput.Code) > 0,
                        activeConnection.LabelOutput > LabelOutput.Code,
                        activeConnection.IsFlags,
                        activeConnection.IsMetadata,
                        activeConnection.IsActionColumn,
                        SaveDialogInterrupt
                    );

                    var msgTemplate = activeConnection.IsMetadata
                            ? "A referential metadata change request with ID [{0}] containing referential metadata for [{1}] attachment(s) was successfully registered. Please check the notification e-mail for the outcome of this request."
                            : "A data change request with ID [{0}] containing [{1}] observation(s) was successfully registered. Please check the notification e-mail for the outcome of this request.";

                    InfoDialog.Show(string.Format(msgTemplate, result.Item1, result.Item2));
                }
            }
            catch(InterruptCodeException)
            {}
            catch (Exception ex)
            {
                Log.Handle(ex);
            }
        }

        private InterruptCode SaveDialogInterrupt(string[] row, IDataset dataset, int index, InterruptCode interrupt)
        {
            if (interrupt != InterruptCode.BulkDelete && row[0] == "D")
            {
                for (var i = 1; i < dataset.Dimensions.Count + 1; i++)
                {
                    if (string.IsNullOrEmpty(row[i]))
                    {
                        var emptyDim = dataset.Dimensions.OrderBy(dim => dim.TimeLastOrder()).ToArray()[i - 1].Code;
                        var message = $"Warning: You are bulk deleting [data|metadata] when not specifying a dimension [{emptyDim}] value, in line [{index}]. Did you intend doing this?";
                        
                        if (!YesNoDialog.Show(message))
                        {
                            throw new InterruptCodeException();
                        }
                        
                        return InterruptCode.BulkDelete;
                    }
                }
            }

            return 0;
        }

        #endregion

        #region Methods

        private void ClearCache()
        {
            try
            {
                // reset persisting structure and data cache 
                BaseDataAccess.ClearCache();

                // reset session object cache
                foreach (var repo in OecdDataSource.AllSources)
                    repo.ClearCache();
            }
            catch { }
        }

        private void NotImplemented()
        {
            MessageBox.Show(@"Not yet implemented");
        }

        private void SelectSource(IOecdDataSource source)
        {
            Authenticate(source.DataAccess);

            var cell = Globals.ThisAddIn.Application.ActiveCell;

            if (cell == null)
            {
                InfoDialog.Show("Please ensure you have an open workbook");
                return;
            }

            var connection = new DataConnection()
            {
                Source = source,
                StartCell = cell.RangeAddress(),
            };

            InvokeWizard(connection);
        }

        private void InvokeWizard(DataConnection connection, Excel.Range currentRegion = null)
        {
            var book = Globals.ThisAddIn.GetActiveWorkbook();
            connection.Sheet = book.ActiveSheet.Name;
            connection.AllSheets = book.Worksheets.Cast<Worksheet>().Select(x => x.Name).ToArray();

            var activeSheet = book.ActiveSheet as Worksheet;
            Worksheet destSheet = null;

            do
            {
                if (!GetDataForm.ShowFilterDialog(connection))
                    return;

                destSheet = book.Worksheets.Cast<Worksheet>().First(x => x.Name == connection.Sheet);

                try
                {
                    using (new LongRunningOperation())
                        OutputData(connection, destSheet, currentRegion);

                    connection.Resume = false;
                }
                catch (DlmXlResumeActionException ex)
                {
                    InfoDialog.Show(ex.Message);

                    connection.Resume = true;
                }
            }
            while (connection.Resume);

            destSheet.Activate();
            destSheet.Range[connection.StartCell].Activate();

            if (!connection.IsNew())
                WorkbookConnectionBag.BreakConnection(activeSheet, connection);

            if (connection.OutputType != OutputType.Pivot)
                WorkbookConnectionBag.SaveConnection(destSheet, connection);

            UpdateButtonsForActiveSheet(destSheet);
        }

        private void OutputData(DataConnection connection, Worksheet sheet, Excel.Range currentRegion = null)
        {
            using (new AutoFilterHandler(sheet))
            {
                if (connection.OutputType == OutputType.Pivot)
                {
                    OutputPivot(connection, sheet, currentRegion);
                }
                else
                {
                    OutputFlat(connection, sheet, currentRegion);
                }
            }
        }

        private void OutputFlat(DataConnection connection, Worksheet sheet, Excel.Range currentRegion = null)
        {
            object[,] data;

            try
            {
                data = connection.GetData();
            }
            catch (Exception ex)
            {
                throw new DlmXLManagedException($"{ex.Message}\r\nConnection {connection.GetDataset()} ({connection.StartCell}:{connection.EndCell}) on sheet <{sheet.Name}>");
            }

            if (data == null)
                return;

            var start = sheet.Range[connection.StartCell];
            var height = data.GetLength(0);
            var width = data.GetLength(1);

            if (height == 0 || width == 0)
                throw new InvalidOperationException("Data is empty");

            if ((start.Row + height) > MaxHeight || (start.Column + width) > MaxWidth)
                throw new DlmXLManagedException("The size of data ({0}x{1}) exceeds MS Excel limits".F(width, height));

            // Show no data message, but still output empty table
            if (height == 1)
                InfoDialog.Show($"{connection.Filter.Dataset}: No (meta)data available for this selection.");

            var end = (Excel.Range)start.Cells[height, width];
            var endCell = end.RangeAddress();
            var resultRange = sheet.Range[start, end];
            var connections = WorkbookConnectionBag.GetConnections(sheet);
            var overlapping = connections.IsEmpty()
                                    ? null
                                    : connections.FirstOrDefault(c => !c.Id.Equals(connection.Id) && resultRange.InRegion(c));

            if (overlapping != null)
                throw new DlmXLManagedException(
                    $"Connection {connection.GetDataset()} ({connection.StartCell}:{endCell}) " +
                    $"will overlap existing connection {overlapping.GetDataset()} ({overlapping.StartCell}:{overlapping.EndCell}) " +
                    $"on sheet <{sheet.Name}>"
                );

            // 2. Clear current region
            if (currentRegion != null)
                currentRegion.Clear();

            resultRange.Value2 = data;
            connection.EndCell = endCell;
        }

        private void OutputPivot(DataConnection connection, Worksheet sheet, Excel.Range currentRegion = null)
        {
            var activeBook = sheet.Parent as Workbook;
            var tempSheet = (Worksheet)activeBook.Worksheets.Add();
            tempSheet.Visible = XlSheetVisibility.xlSheetHidden;

            OutputFlat(connection, tempSheet, currentRegion);

            var pivotCache = activeBook.PivotCaches().Create(
                XlPivotTableSourceType.xlDatabase,
                connection.Region(tempSheet),
                XlPivotTableVersionList.xlPivotTableVersion12
            );

            var destRange = sheet.Range[connection.StartCell];
            var pivotTable = pivotCache.CreatePivotTable(destRange, connection.Id);
            pivotTable.ManualUpdate = true;

            //http://blog.jerrynixon.com/2008/11/use-vsto-3-to-create-pivottable-in.html

            Globals.ThisAddIn.Application.DisplayAlerts = false;
            tempSheet.Delete();
            Globals.ThisAddIn.Application.DisplayAlerts = true;
        }

        private void RefreshConnectionsForWorkbook(Workbook book)
        {
            foreach (Worksheet sheet in book.Worksheets)
                RefreshConnectionsForWorksheet(sheet);
        }

        private void RefreshConnectionsForWorksheet(Worksheet sheet)
        {
            var connections = WorkbookConnectionBag.GetConnections(sheet);

            if (connections == null)
                return;

            foreach (var connection in connections)
                RefreshConnection(connection, sheet);
        }

        private void RefreshConnection(DataConnection connection, Worksheet sheet = null)
        {
            if (connection == null || connection.Filter.SWFilter == null)
                return;

            if (sheet == null)
                sheet = Globals.ThisAddIn.GetActiveWorksheet();

            Authenticate(connection.Source.DataAccess);
            OutputData(connection, sheet, connection.Region(sheet));
        }

        private void UpdateButtonsForActiveSheet(Excel.Worksheet sheet = null)
        {
            _pnlDataVisible = sheet != null;
            _btnChangeVisible = false;
            _btnRefreshSheetVisible = false;
            _btnRefreshAllVisible = false;
            _pnlManagementVisible = false;
            _pnlInfoVisible = false;
            _btnHowToDeleteVisible = false;

            if (sheet != null)
            {
                var wb = (sheet.Parent as Workbook);

                _btnRefreshAllVisible = wb.HasConnections();
                _btnRefreshSheetVisible = _btnRefreshAllVisible && sheet.HasConnections();

                OnCellActivation(Globals.ThisAddIn.Application.ActiveCell);
            }

            this.ribbon.Invalidate();
        }

        private void OnCellActivation(Excel.Range target)
        {
            var connection = FindConnectionForRange(target);

            SetActiveConnection(connection, target);
        }

        private void SetActiveConnection(DataConnection connection, Excel.Range target)
        {
            this.activeConnection = connection;
            _btnChangeVisible = connection != null;
            _pnlManagementVisible = connection != null;

            _pnlInfoVisible = connection != null;
            _btnHowToDeleteVisible = connection != null;

            if (connection != null)
            {
                Authenticate(connection.Source.DataAccess);
                
                _lblDatasetId = connection.GetDataset();
                _lblDatasetName = connection.GetDatasetName();
                _lblSource = " Source: " + connection.Source.Name;
                _lblUpdateDate = $" Last extraction date: {connection.LastRefreshTime:dd.MM.yyyy HH:mm}";
                _isNonExistingDataflow = connection.Filter.SWFilter == null;

                _pnlManagementVisible = connection.IsSaveAllowed() && connection.Filter.SWFilter != null;
            }

            // metadata

            _pnlMetaVisible= false;

            if (connection == null
                || !connection.IsMetadata
                || !MetaUtils.IsCellReferencingMetadata(connection, target, out var metaAttribute)
                || !metaAttribute.IsExternalEditor
            )
            {
                if (IsMetaSidePaneVisible)
                {
                    HideMetadataEditor();
                }
            }
            else
            {
                _pnlMetaVisible = true;

                if (IsMetaSidePaneVisible)
                {
                    ShowMetadataEditor(metaAttribute, target);
                }
            }

            this.ribbon.Invalidate();
        }

        private DataConnection FindConnectionForRange(Excel.Range cell, bool expand = false)
        {
            if (cell == null)
                return null;

            var connections = WorkbookConnectionBag.GetConnections(cell.Worksheet);

            if (connections.IsEmpty())
                return null;

            if (expand)
            {
                var currentRegion = cell.CurrentRegion;

                cell = cell.Count >= currentRegion.Count
                    ? cell
                    : currentRegion;
            }

            return connections.FirstOrDefault(cell.InRegion);
        }

        // http://stackoverflow.com/questions/9705335/outlook-2007-add-in-how-to-add-an-icon-to-an-msocontrolbutton
        // Office Icons Gallery https://bettersolutions.com/vba/ribbon/face-ids-2003.htm

        private Office.CommandBarButton CreateContextButton(Office.CommandBar contextMenu, string title, int icon, Microsoft.Office.Core._CommandBarButtonEvents_ClickEventHandler handler)
        {
            var menuItem = (Office.CommandBarButton) contextMenu.Controls.Add(
                Office.MsoControlType.msoControlButton,
                Type.Missing,
                Type.Missing,
                Type.Missing,
                true
            );

            menuItem.Style = Office.MsoButtonStyle.msoButtonIconAndCaption;
            menuItem.Caption = title;
            menuItem.FaceId = icon;
            menuItem.Click += handler;

            return menuItem;
        }

        private void Authenticate(IDataAccess dataAccess)
        {
            if (!dataAccess.IsAuthRequired || dataAccess.IsAuthenticated)
                return;

            using (var form = new AuthDialog(dataAccess.Auth))
            {
                var result = form.ShowDialog();

                if (result != DialogResult.OK)
                {
                    form.ThrowUnauthorizedException();
                }
            }

            _logOutDict[dataAccess.AuthId] = new KeyValuePair<string, bool>(dataAccess.Userinfo, true);
            ribbon.InvalidateControl(dataAccess.AuthId);

            if (!_pnlLoginVisible)
            {
                _pnlLoginVisible = true;
                ribbon.InvalidateControl("pnlLogin");
            }
        }

        #endregion

        #region Helpers
        private class CloseRequestInfo
        {
            public CloseRequestInfo(string id, int count)
            {
                this.Id = id;
                this.WorkbookCount = count;
            }

            public string Id { get; private set; }

            public int WorkbookCount { get; private set; }

            public bool IsClosed(int actualCount)
            {
                return WorkbookCount != actualCount;
            }
        }

        private bool isExcelInteractive()
        {
            try
            {
                // this line does nothing if Excel is not
                // in edit mode. However, trying to set
                // this property while Excel is in edit
                // cell mdoe will cause an exception
                Globals.ThisAddIn.Application.Interactive = Globals.ThisAddIn.Application.Interactive;
                return true; // no exception, ecel is
                // interactive
            }
            catch
            {
                return false; // in edit mode
            }
        }

        //https://theofficecontext.com/2011/05/03/exceptions-occur-when-automating-excel-detecting-cell-edit-mode/
        private void exitEditMode()
        {
            if (!isExcelInteractive())
            {
                // get the current range
                var r = Globals.ThisAddIn.Application.ActiveCell;
                // bring Excel to the foreground, with focus
                // and issue keys to exit the cell
                //xlBringToFront();
                Globals.ThisAddIn.Application.ActiveWindow.Activate();
                SendKeys.Flush();
                SendKeys.SendWait("{ENTER}");
                // now make sure the original cell is
                // selected…
                r.Select();
            }
        }

        private static string GetResourceText(string resourceName)
        {
            Assembly asm = Assembly.GetExecutingAssembly();
            string[] resourceNames = asm.GetManifestResourceNames();
            for (int i = 0; i < resourceNames.Length; ++i)
            {
                if (string.Compare(resourceName, resourceNames[i], StringComparison.OrdinalIgnoreCase) == 0)
                {
                    using (StreamReader resourceReader = new StreamReader(asm.GetManifestResourceStream(resourceNames[i])))
                    {
                        if (resourceReader != null)
                        {
                            return resourceReader.ReadToEnd();
                        }
                    }
                }
            }
            return null;
        }

        #endregion
    }
}
