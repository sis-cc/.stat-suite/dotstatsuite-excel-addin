﻿using System;
using System.Net;
using DevExpress.Skins;
using DLMXL.DAL.Access;
using ExcelAddIn.Domain;

namespace ExcelAddIn
{
    public partial class ThisAddIn
    {
        private void ThisAddIn_Startup(object sender, System.EventArgs e)
        {
            try
            {
                this.SetSkin();
                FavoritesManagement.Load();
                ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;
                OAuth.Init(Config.DW.AuthRedirectUri);
            }
            catch(Exception ex)
            {
                Log.Handle(ex);
            }
        }

        private void ThisAddIn_Shutdown(object sender, System.EventArgs e)
        {
            try
            {
                FavoritesManagement.Save();
                UserSettings.Save(Config.Settings);
                BaseDataAccess.ClearCache();
            }
            catch (Exception ex)
            {
                Log.Handle(ex);
            }
        }

        #region VSTO generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InternalStartup()
        {
            this.Startup += new System.EventHandler(ThisAddIn_Startup);
            this.Shutdown += new System.EventHandler(ThisAddIn_Shutdown);
        }

        protected override Microsoft.Office.Core.IRibbonExtensibility CreateRibbonExtensibilityObject()
        {
            return new Ribbon();
        }

        #endregion

        #region Methods

        private void SetSkin()
        {
            SkinManager.EnableFormSkins();
            SkinManager.Default.RegisterAssembly(typeof(DevExpress.UserSkins.DataWizard).Assembly);
            var lookAndFeelSetter                           = new DevExpress.LookAndFeel.DefaultLookAndFeel();
            lookAndFeelSetter.LookAndFeel.SkinName          = "DataWizard";
        }

        #endregion
    }
}
