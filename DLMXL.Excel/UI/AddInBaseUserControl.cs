﻿using System;
using System.Drawing;
using System.Windows.Forms;
using DevExpress.XtraEditors;

namespace ExcelAddIn.UI
{
    public class AddInBaseUserControl : UserControl
    {
        public StyleController InputStyleController { get; private set; }
        public StyleController LabelStyleController { get; private set; }
        private ErrorProvider _errorProvider;

        private ErrorProvider GetErrorProvider
        {
            get { return _errorProvider ?? (_errorProvider = InitErrorProvider()); }
        }

        public AddInBaseUserControl()
        {
            InputStyleController = new StyleController();
            InputStyleController.Appearance.Font = new Font("Tahoma", 10);

            LabelStyleController = new StyleController();
            LabelStyleController.Appearance.Font = new Font("Tahoma", 10);
        }

        public void SetStyleController(StyleController controller, params BaseControl[] controls)
        {
            foreach (var ctrl in controls)
                ctrl.StyleController = controller;
        }

        #region Validation

        private ErrorProvider InitErrorProvider()
        {
            return new ErrorProvider(this)
            {
                BlinkStyle =  ErrorBlinkStyle.NeverBlink
            };
        }

        protected bool IsValid(Control ctrl, bool condition, string errorMessage)
        {
            return condition || SetError(ctrl, errorMessage);
        }

        protected bool IsValid(DevExpress.XtraEditors.TextEdit txt, Func<string, bool> validator, string errorMessage)
        {
            return IsValid(txt, validator(txt.Text), errorMessage);
        }

        protected bool IsValid(DevExpress.XtraEditors.TextEdit txt, Func<DevExpress.XtraEditors.TextEdit, string> validator)
        {
            var errorMessage = validator(txt);

            return IsValid(txt, string.IsNullOrEmpty(errorMessage), errorMessage);
        }

        protected bool IsGreatedThan(DevExpress.XtraEditors.TextEdit txt, DevExpress.XtraEditors.TextEdit compareTo, string errorMessage)
        {
            var text    = txt.Value();
            var text2   = compareTo.Value();

            return string.IsNullOrEmpty(text)
                   || string.IsNullOrEmpty(text2)
                   || IsValid(txt, String.CompareOrdinal(text, text2) >= 0, errorMessage);
        }

        protected bool IsLessThan(DevExpress.XtraEditors.TextEdit txt, DevExpress.XtraEditors.TextEdit compareTo, string errorMessage)
        {
            var text    = txt.Value();
            var text2   = compareTo.Value();

            return string.IsNullOrEmpty(text)
                   || string.IsNullOrEmpty(text2)
                   || IsValid(txt, String.CompareOrdinal(text, text2) <= 0, errorMessage);
        }

        protected bool IsValid(DevExpress.XtraEditors.ComboBoxEdit ddl, string errorMessage)
        {
            return IsValid(ddl, ddl.SelectedIndex >= 0, errorMessage);
        }

        protected bool SetError(Control ctrl, string errorMessage)
        {
            //GetErrorProvider.SetIconAlignment(ctrl, ErrorIconAlignment.MiddleRight);
            GetErrorProvider.SetError(ctrl, errorMessage);
            return false;
        }

        protected void ClearErrors(Control ctrl = null)
        {
            if(ctrl == null)    GetErrorProvider.Clear();
            else                GetErrorProvider.SetError(ctrl, null);
        }

        #endregion
    }
}
