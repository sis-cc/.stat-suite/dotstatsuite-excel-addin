﻿namespace ExcelAddIn.UI
{
    partial class CommonDimensionFilter
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lblCode = new DevExpress.XtraEditors.LabelControl();
            this.txtDimension = new DevExpress.XtraEditors.TextEdit();
            this.lblDimension = new DevExpress.XtraEditors.LabelControl();
            ((System.ComponentModel.ISupportInitialize)(this.txtDimension.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // lblCode
            // 
            this.lblCode.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.lblCode.Location = new System.Drawing.Point(0, 5);
            this.lblCode.Name = "lblCode";
            this.lblCode.Size = new System.Drawing.Size(59, 13);
            this.lblCode.TabIndex = 0;
            this.lblCode.Text = "Dimension";
            // 
            // txtDimension
            // 
            this.txtDimension.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtDimension.EditValue = "";
            this.txtDimension.Location = new System.Drawing.Point(120, 2);
            this.txtDimension.Name = "txtDimension";
            this.txtDimension.Size = new System.Drawing.Size(395, 20);
            this.txtDimension.TabIndex = 1;
            // 
            // lblDimension
            // 
            this.lblDimension.AllowHtmlString = true;
            this.lblDimension.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lblDimension.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(249)))), ((int)(((byte)(248)))), ((int)(((byte)(245)))));
            this.lblDimension.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.lblDimension.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Flat;
            this.lblDimension.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.lblDimension.Location = new System.Drawing.Point(120, 2);
            this.lblDimension.Name = "lblDimension";
            this.lblDimension.Padding = new System.Windows.Forms.Padding(2, 1, 2, 1);
            this.lblDimension.Size = new System.Drawing.Size(395, 22);
            this.lblDimension.TabIndex = 1;
            // 
            // CommonDimensionFilter
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.lblDimension);
            this.Controls.Add(this.txtDimension);
            this.Controls.Add(this.lblCode);
            this.Name = "CommonDimensionFilter";
            this.Size = new System.Drawing.Size(516, 26);
            ((System.ComponentModel.ISupportInitialize)(this.txtDimension.Properties)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private DevExpress.XtraEditors.LabelControl lblCode;
        private DevExpress.XtraEditors.TextEdit txtDimension;
        private DevExpress.XtraEditors.LabelControl lblDimension;
    }
}
