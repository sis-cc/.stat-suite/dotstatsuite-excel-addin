﻿using System;
using System.Linq;
using System.Text.RegularExpressions;
using System.Windows.Forms;
using DLMXL.Models;

namespace ExcelAddIn.UI
{
    public sealed partial class CommonDimensionFilter : BaseDimensionFilter
    {
        private SWFilterDimension _filter;
        private bool _isAll;
        private int ProgrammaticChange;
        private static readonly Regex Format = new Regex(@"<b><color=red>(.*)</color></b>", RegexOptions.Compiled);

        public CommonDimensionFilter()
        {
            InitializeComponent();

            SetStyleController(InputStyleController,
                lblDimension,
                txtDimension
            );

            lblDimension.Click += lblDimension_Click;
            txtDimension.Leave += txtDimension_Leave;
            txtDimension.GotFocus += Activate_Textbox;
        }

        public CommonDimensionFilter(SWFilterDimension filter, int index) : this()
        {
            this.IsValid            = true;
            _filter                 = filter;
            this.TabIndex           = index;
            lblCode.Text            = filter.Dimension.Code;
            Dock                    = DockStyle.Top;
            var members             = filter.SelectedMembers.Select(x => x.Code).JoinMembers();
            SetLabelText(members);
        }

        private string GetUnformatedMembers(string text)
        {
            return text.SplitMembers()
                        .Select(x => Format.Replace(x, @"$1"))
                        .JoinMembers();
        }

        private void UpdateSelectedMembers(string text)
        {
            _filter.ResetSelectedMembers();

            IsValid         = true;
            var inputCodes  = text.SplitMembers();
            var outputCodes = new string[inputCodes.Count()];

            for (var i = 0; i < inputCodes.Count(); i++)
            {
                var code        = inputCodes[i];
                var dimMember   = _filter.SelectableMembers.FirstOrDefault(x => x.Code == code);
                outputCodes[i]  = GetCodeFormated(code, dimMember != null);

                if (dimMember!=null)
                {
                    _filter.AddSelectedMember(dimMember);
                }
                else
                {
                    IsValid = false;
                }
            }

            lblDimension.Visible    = true;
            var newDimString        = outputCodes.JoinMembers();
            var oldDimString        = _isAll ? string.Empty : lblDimension.Text;

            SetLabelText(newDimString);

            if (IsValid && !oldDimString.Equals(newDimString))
                OnDimensionChanged(null);
        }

        private string GetCodeFormated(string code, bool isValid)
        {
            return isValid ? code : string.Format("<b><color=red>{0}</color></b>", code);
        }

        private void SetLabelText(string text)
        {
            _isAll                  = string.IsNullOrEmpty(text);
            lblDimension.Text       = _isAll ? "(all)" : text;
        }

        #region Event handler
        private void lblDimension_Click(object sender, EventArgs e)
        {
            txtDimension.Focus();
        }

        private void Activate_Textbox(object sender, EventArgs e)
        {
            lblDimension.Visible            = false;
            txtDimension.Text               = _isAll ? string.Empty : GetUnformatedMembers(lblDimension.Text);
            txtDimension.Select(txtDimension.Text.Length, 0);
        }

        private void txtDimension_Leave(object sender, EventArgs e)
        {
            UpdateSelectedMembers(txtDimension.Text);
        }

        #endregion
    }

    public class BaseDimensionFilter : AddInBaseUserControl
    {
        public event EventHandler DimensionHasChanged;
        public virtual bool IsValid { get; protected set; }

        public void OnDimensionChanged(EventArgs args)
        {
            DimensionHasChanged?.Invoke(null, args);
        }
    }
}
