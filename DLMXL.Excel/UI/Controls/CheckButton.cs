﻿using System.Drawing;
using System.Windows.Forms;

namespace ExcelAddIn.UI.Controls
{
    public sealed class CheckButton : DevExpress.XtraEditors.CheckButton
    {
        protected override void OnPaint(PaintEventArgs e)
        {
            this.ForeColor = this.Checked ? Color.FromArgb(68,68,68) : Color.White;
            base.OnPaint(e);
        }
    }
}
