﻿
namespace ExcelAddIn.UI.Controls
{
    public sealed class MainButton : DevExpress.XtraEditors.SimpleButton
    {
        public MainButton()
        {
            this.LookAndFeel.UseDefaultLookAndFeel = false;
            this.LookAndFeel.SkinName = "DataWizard2";
        }
    }
}
