﻿using System.Windows.Forms;
using DevExpress.Utils;
using DevExpress.Utils.Win;

namespace ExcelAddIn.UI
{
    public class CustomFlyoutPanel : FlyoutPanel
    {
        protected override FlyoutPanelToolForm CreateToolFormCore(Control owner, FlyoutPanel content, FlyoutPanelOptions options)
        {
            return new MyFlyoutPanelToolForm(owner, content, options);
        }
    }

    class MyFlyoutPanelToolForm : FlyoutPanelToolForm
    {
        public MyFlyoutPanelToolForm(Control owner, FlyoutPanel flyoutPanel, FlyoutPanelOptions options) : base(owner, flyoutPanel, options)
        {}

        protected override ToolWindowHookPopup CreateHook()
        {
            return new MyToolWindowHookPopup(this, MessageRedirector, PopupManager);
        }
    }

    class MyToolWindowHookPopup : ToolWindowHookPopup
    {
        public MyToolWindowHookPopup(IPopupControl popup, DevExpress.Utils.Win.Hook.IMessageRedirector msgRedirector, ISupportsExternalPopupManagement target) : base(popup, msgRedirector, target)
        {}
        
        protected override bool ClosePopupIfNeeded()
        {
            var panel       = (this.Popup as FlyoutPanelToolForm).Content as FlyoutPanel;
            var ownerPoint  = panel.OwnerControl.PointToClient(Control.MousePosition);
            ownerPoint.X    += panel.OwnerControl.Bounds.X;
            ownerPoint.Y    += panel.OwnerControl.Bounds.Y;
            if (!panel.OptionsBeakPanel.CloseOnOuterClick || ToolWindow.Contains(Control.MousePosition) || panel.OwnerControl.Bounds.Contains(ownerPoint))
                return false;

            panel.HidePopup(true);
            
            return true;
        }
    }
}
