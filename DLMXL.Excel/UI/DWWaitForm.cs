﻿using DevExpress.XtraWaitForm;

namespace ExcelAddIn.UI {
    public partial class DWWaitForm: WaitForm {
        public DWWaitForm( ) {
            InitializeComponent( );
            InitUi();
            this.progressPanel1.AutoHeight = true;
        }

        private void InitUi()
        {
            this.SetDescription($@"{Config.DW.Title} is executing an operation...");
        }

        public override void SetCaption( string caption ) {
            base.SetCaption( caption );
            this.progressPanel1.Caption = caption;
        }
        public override void SetDescription( string description ) {
            base.SetDescription( description );
            this.progressPanel1.Description = description;
        }
    }
}