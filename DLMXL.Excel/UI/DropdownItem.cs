﻿namespace ExcelAddIn.UI
{
    public sealed class DropdownItem<T>
    {
        public string Name { get; }

        public T Value { get; }

        public DropdownItem(string name, T value)
        {
            Name = name;
            Value = value;
        }

        public override string ToString()
        {
            return Name;
        }

        public static implicit operator T(DropdownItem<T> item)
        {
            return item != null
                ? item.Value
                : default(T);
        }
    }
}