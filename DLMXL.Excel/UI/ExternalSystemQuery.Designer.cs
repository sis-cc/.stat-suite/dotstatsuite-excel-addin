﻿namespace ExcelAddIn.UI
{
    partial class ExternalSystemQuery
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.txtQuery = new DevExpress.XtraEditors.MemoEdit();
            this.lblQueryName = new DevExpress.XtraEditors.LabelControl();
            ((System.ComponentModel.ISupportInitialize)(this.txtQuery.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // txtQuery
            // 
            this.txtQuery.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtQuery.Location = new System.Drawing.Point(15, 27);
            this.txtQuery.Name = "txtQuery";
            this.txtQuery.Properties.AppearanceReadOnly.BackColor = System.Drawing.Color.White;
            this.txtQuery.Properties.AppearanceReadOnly.Options.UseBackColor = true;
            this.txtQuery.Properties.ReadOnly = true;
            this.txtQuery.Size = new System.Drawing.Size(470, 58);
            this.txtQuery.TabIndex = 3;
            // 
            // lblQueryName
            // 
            this.lblQueryName.Location = new System.Drawing.Point(15, 8);
            this.lblQueryName.Name = "lblQueryName";
            this.lblQueryName.Size = new System.Drawing.Size(59, 13);
            this.lblQueryName.TabIndex = 2;
            this.lblQueryName.Text = "Query name";
            // 
            // ExternalSystemQuery
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.txtQuery);
            this.Controls.Add(this.lblQueryName);
            this.Name = "ExternalSystemQuery";
            this.Size = new System.Drawing.Size(500, 90);
            ((System.ComponentModel.ISupportInitialize)(this.txtQuery.Properties)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private DevExpress.XtraEditors.MemoEdit txtQuery;
        private DevExpress.XtraEditors.LabelControl lblQueryName;
    }
}
