﻿using System.Collections.Generic;
using System.Windows.Forms;

namespace ExcelAddIn.UI
{
    public partial class ExternalSystemQuery : UserControl
    {
        public ExternalSystemQuery(KeyValuePair<string, string> query)
        {
            InitializeComponent();

            lblQueryName.Text   = query.Key;
            txtQuery.Text       = query.Value;
        }
    }
}
