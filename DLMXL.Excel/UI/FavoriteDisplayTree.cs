﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using DevExpress.XtraGrid;
using DevExpress.XtraTreeList;
using DevExpress.XtraTreeList.Columns;
using DevExpress.XtraTreeList.Data;
using DevExpress.XtraTreeList.Nodes;
using ExcelAddIn.DAL;
using ExcelAddIn.Domain;

namespace ExcelAddIn.UI
{
    public class EventArgs<T> : EventArgs
    {
        public T Item {get; private set; }

        public EventArgs(T item)
        {
            Item = item;
        }
    }

    public sealed class FavoriteDisplayTree : SWTreeList
    {
        private readonly Color _checked     = Color.FromArgb(2, 171, 242);
        private readonly Color _unchecked   = Color.FromArgb(107, 107, 107);

        private readonly System.Windows.Forms.ImageList _checkImages;
        private bool    _showOnlySelected;
        private int     _selectedCount       = 0;
        public event EventHandler<EventArgs<int>> FavoriteCountChanged;

        public Func<TreeListNode, bool> IsNodeVisible { get; set; }

        public IOecdDataSource Source           { get; private set; }
        
        public FavoriteDisplayTree(IOecdDataSource source, int selectedCount,  System.Windows.Forms.ImageList checkImages = null)
        {
            Source                          = source;
            this._selectedCount             = selectedCount;
            OptionsSelection.MultiSelect    = true;
            OptionsView.ShowCheckBoxes      = true;           
            InitComplete                    = true;
            this._checkImages               = checkImages;

            if (checkImages != null)
            {
                this.CustomDrawNodeCheckBox += FavoriteDisplayTree_CustomDrawNodeCheckBox;
                this.NodeCellStyle+=FavoriteDisplayTree_NodeCellStyle;
            }
        }

        private void FavoriteDisplayTree_NodeCellStyle(object sender, GetCustomNodeCellStyleEventArgs e)
        {
            e.Appearance.ForeColor  = e.Node.Checked ? _checked : _unchecked;
            e.Appearance.Font       = new Font(e.Appearance.Font, FontStyle.Bold);
        }

        private void FavoriteDisplayTree_CustomDrawNodeCheckBox(object sender, CustomDrawNodeCheckBoxEventArgs e)
        {
            var rect        = new System.Drawing.Rectangle(e.Bounds.Left, e.Bounds.Top + 1, 16, 16);
            var imgIndex    = e.Node.Checked ? 1 : 0;
            e.Graphics.DrawImage(_checkImages.Images[imgIndex], rect);
            e.Handled = true; 
        }

        protected override TreeListColumnCollection CreateColumns()
        {
            var cl = new[] {
                    new TreeListColumn {
                            Name = "Name",
                            Caption = "Name",
                            AllowIncrementalSearch = true,
                            FilterMode = ColumnFilterMode.DisplayText,
                            MinWidth = 140,
                            UnboundType = UnboundColumnType.String,
                            Visible = true,
                            VisibleIndex = 1,
                    }
            };

            var res = new TreeListColumnCollection(this);
            res.AddRange(cl);
            return res;
        }

        protected override TreeListData CreateData()
        {
            return new FavoriteTreeData(this);
        }

        protected override void RaiseAfterCheckNode(TreeListNode node)
        {
            base.RaiseAfterCheckNode(node);

            if (ProgrammaticChange != 0) return;

            _selectedCount += node.Checked ? 1 : -1;

            try
            {
                ProgrammaticChange++;
                BeginUpdate();

                var item = Source[node.Id];

                if (item.IsPropagateSelection)
                {
                    PropageSelection(node);
                }
                
                if (!node.Checked)
                {
                    while (node.ParentNode != null && node.ParentNode.Checked && Source[node.Id].Parent.IsPropagateSelection)
                    {
                        node.ParentNode.Checked = false;
                        _selectedCount--;
                        node = node.ParentNode;
                    }
                }

                if (FavoriteCountChanged != null)
                    FavoriteCountChanged(this, new EventArgs<int>(_selectedCount));
            }
            finally
            {
                EndUpdate();
                ProgrammaticChange--;
            }
        }

        private void PropageSelection(TreeListNode node)
        {
            foreach (TreeListNode child in node.Nodes.Where(x => x.Checked != node.Checked))
            {
                child.Checked   = node.Checked;
                _selectedCount  += node.Checked ? 1 : -1;

                if (Source[child.Id].IsPropagateSelection)
                    PropageSelection(child);
            }
        }

        public void UpdateListVisibility(string searchPattern)
        {
            try
            {
                ProgrammaticChange++;
                BeginUpdate();
                UpdateListVisibility(this.Nodes, searchPattern);
                this.TopVisibleNodeIndex = 0;
            }
            finally
            {
                EndUpdate();
                ProgrammaticChange--;
            }
        }

        private bool UpdateListVisibility(TreeListNodes nodes, string searchPattern)
        {
            var expand = !string.IsNullOrEmpty(searchPattern);
            var result = false;
            
            foreach (TreeListNode node in nodes)
            {
                var isVisible =
                    (string.IsNullOrEmpty(searchPattern) || (node.GetDisplayText(0) ?? "").IndexOf(searchPattern, StringComparison.InvariantCultureIgnoreCase) >= 0)
                        &&
                    (IsNodeVisible == null || IsNodeVisible(node));

                var isChildVisible  = UpdateListVisibility(node.Nodes, searchPattern);
                node.Visible        = isVisible || isChildVisible;

                if (expand && !isVisible && isChildVisible)
                    node.Expanded = true;

                result |= (isVisible || isChildVisible);
            }

            return result;
        }

        public void SaveFavorites()
        {
            this.ForEach((node)=> { Source[node.Id].IsFavorite = node.Checked; });
        }

        public void ClearFavorites()
        {
            this.UncheckAll();
            this._selectedCount = 0;

            FavoriteCountChanged?.Invoke(this, new EventArgs<int>(_selectedCount));
        }

        private class FavoriteTreeData : ReadOnlyTreeListData
        {
            private FavoriteDisplayTree Tree;
            private List<IFavoriteTreeItem> _DataList;

            public FavoriteTreeData(FavoriteDisplayTree tl) : base(tl)
            {}

            public override object GetValue(int nodeID, object columnID)
            {
                return ((FavoriteDisplayTree)DisplayTree).Source[nodeID].Name();
            }

            public override void CreateNodes(TreeListNode parent)
            {
                var items = ((FavoriteDisplayTree) DisplayTree).Source
                    .GetAll()
                    .Where(x=>x.Parent == null)
                    .ToArray();

                BuildTree(null, items);
            }

            private void BuildTree(TreeListNode parent, IList<IFavoriteTreeItem> items)
            {
                if (items == null || items.Count == 0) return;

                foreach (var item in items)
                {
                    var node            = DataHelper.CreateNode(item.Index, parent, item);
                    node.HasChildren    = item.Children.Count > 0;
                    node.Visible        = true;
                    node.Checked        = item.IsFavorite;

                    if(item.Children.Count > 0)
                        BuildTree(node, item.Children);
                }
            }

            public override IList DataList
            {
                get
                {
                    return _DataList
                         ?? (_DataList =
                                 new List<IFavoriteTreeItem>
                                         (DisplayTree == null || DisplayTree.IsDesignMode
                                                 ? Enumerable.Empty<IFavoriteTreeItem>()
                                                 : ((FavoriteDisplayTree)DisplayTree).Source.GetAll()));
                }
            }
        }
    }
}
