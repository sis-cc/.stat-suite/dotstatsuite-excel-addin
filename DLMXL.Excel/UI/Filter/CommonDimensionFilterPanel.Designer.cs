﻿namespace ExcelAddIn.UI.Filter
{
    partial class CommonDimensionFilterPanel {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent( ) {
            this.panelControl1 = new DevExpress.XtraEditors.PanelControl();
            this.SearchText = new IncrementalSearchTextBox();
            this.btnSelectAll = new DevExpress.XtraEditors.SimpleButton();
            this.btnNoFilter = new DevExpress.XtraEditors.SimpleButton();
            this.lblTitle = new DevExpress.XtraEditors.LabelControl();
            this.btnHierarchy = new ExcelAddIn.UI.Controls.CheckButton();
            this.chkExpand = new DevExpress.XtraEditors.CheckEdit();
            this.chkOnlyMembersWithData = new DevExpress.XtraEditors.CheckEdit();
            this.chkOnlySelectedMembers = new DevExpress.XtraEditors.CheckEdit();
            this.panelControl3 = new DevExpress.XtraEditors.PanelControl();
            this.pnlTree = new DevExpress.XtraEditors.PanelControl();
            this.pnlHierarchyOuter = new DevExpress.XtraEditors.PanelControl();
            this.pnlHierarchyInner = new DevExpress.XtraEditors.PanelControl();
            this.flowLayoutPanel1 = new System.Windows.Forms.FlowLayoutPanel();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).BeginInit();
            this.panelControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.SearchText.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkExpand.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkOnlyMembersWithData.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkOnlySelectedMembers.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl3)).BeginInit();
            this.panelControl3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pnlTree)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pnlHierarchyOuter)).BeginInit();
            this.pnlHierarchyOuter.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pnlHierarchyInner)).BeginInit();
            this.flowLayoutPanel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // panelControl1
            // 
            this.panelControl1.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.panelControl1.Appearance.Options.UseBackColor = true;
            this.panelControl1.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.panelControl1.Controls.Add(this.SearchText);
            this.panelControl1.Controls.Add(this.btnSelectAll);
            this.panelControl1.Controls.Add(this.btnNoFilter);
            this.panelControl1.Controls.Add(this.lblTitle);
            this.panelControl1.Controls.Add(this.btnHierarchy);
            this.panelControl1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelControl1.Location = new System.Drawing.Point(5, 0);
            this.panelControl1.LookAndFeel.UseDefaultLookAndFeel = false;
            this.panelControl1.Name = "panelControl1";
            this.panelControl1.Size = new System.Drawing.Size(671, 91);
            this.panelControl1.TabIndex = 0;
            // 
            // SearchText
            // 
            this.SearchText.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.SearchText.IsPopupDisabled = false;
            this.SearchText.Location = new System.Drawing.Point(5, 30);
            this.SearchText.Name = "SearchText";
            this.SearchText.SearchTextHint = "type here to search";
            this.SearchText.Size = new System.Drawing.Size(660, 20);
            this.SearchText.TabIndex = 12;
            this.SearchText.SearchTextChanged += new System.EventHandler(this.ApplyFilter);
            // 
            // btnSelectAll
            // 
            this.btnSelectAll.Location = new System.Drawing.Point(85, 63);
            this.btnSelectAll.Name = "btnSelectAll";
            this.btnSelectAll.Size = new System.Drawing.Size(81, 23);
            this.btnSelectAll.TabIndex = 11;
            this.btnSelectAll.Text = "SELECT ALL";
            this.btnSelectAll.Click += new System.EventHandler(this.btnSelectAll_Click);
            // 
            // btnNoFilter
            // 
            this.btnNoFilter.Location = new System.Drawing.Point(5, 63);
            this.btnNoFilter.Name = "btnNoFilter";
            this.btnNoFilter.Size = new System.Drawing.Size(73, 23);
            this.btnNoFilter.TabIndex = 10;
            this.btnNoFilter.Text = "NO FILTER";
            this.btnNoFilter.Click += new System.EventHandler(this.btnNoFilter_Click);
            // 
            // lblTitle
            // 
            this.lblTitle.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.lblTitle.Location = new System.Drawing.Point(5, 5);
            this.lblTitle.Name = "lblTitle";
            this.lblTitle.Size = new System.Drawing.Size(88, 19);
            this.lblTitle.TabIndex = 9;
            this.lblTitle.Text = "YEA - Year";
            // 
            // btnHierarchy
            // 
            this.btnHierarchy.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnHierarchy.Appearance.ForeColor = System.Drawing.Color.White;
            this.btnHierarchy.Appearance.Options.UseForeColor = true;
            this.btnHierarchy.Location = new System.Drawing.Point(523, 61);
            this.btnHierarchy.Name = "btnHierarchy";
            this.btnHierarchy.Size = new System.Drawing.Size(142, 23);
            this.btnHierarchy.TabIndex = 8;
            this.btnHierarchy.Text = "HIERARCHY SELECTION";
            this.btnHierarchy.Visible = false;
            this.btnHierarchy.Click += new System.EventHandler(this.btnHierarchy_Click);
            // 
            // chkExpand
            // 
            this.chkExpand.AutoSizeInLayoutControl = true;
            this.chkExpand.Location = new System.Drawing.Point(3, 3);
            this.chkExpand.Name = "chkExpand";
            this.chkExpand.Properties.AutoWidth = true;
            this.chkExpand.Properties.Caption = "Expand all";
            this.chkExpand.Size = new System.Drawing.Size(71, 19);
            this.chkExpand.TabIndex = 7;
            this.chkExpand.Visible = false;
            this.chkExpand.CheckedChanged += new System.EventHandler(this.chkExpand_CheckedChanged);
            // 
            // chkOnlyMembersWithData
            // 
            this.chkOnlyMembersWithData.AutoSizeInLayoutControl = true;
            this.chkOnlyMembersWithData.Location = new System.Drawing.Point(200, 3);
            this.chkOnlyMembersWithData.Name = "chkOnlyMembersWithData";
            this.chkOnlyMembersWithData.Properties.AutoWidth = true;
            this.chkOnlyMembersWithData.Properties.Caption = "Show only items with data";
            this.chkOnlyMembersWithData.Size = new System.Drawing.Size(147, 19);
            this.chkOnlyMembersWithData.TabIndex = 6;
            this.chkOnlyMembersWithData.Visible = false;
            this.chkOnlyMembersWithData.CheckedChanged += new System.EventHandler(this.ApplyFilter);
            // 
            // chkOnlySelectedMembers
            // 
            this.chkOnlySelectedMembers.AutoSizeInLayoutControl = true;
            this.chkOnlySelectedMembers.Location = new System.Drawing.Point(80, 3);
            this.chkOnlySelectedMembers.Name = "chkOnlySelectedMembers";
            this.chkOnlySelectedMembers.Properties.AutoWidth = true;
            this.chkOnlySelectedMembers.Properties.Caption = "Show selected only";
            this.chkOnlySelectedMembers.Size = new System.Drawing.Size(114, 19);
            this.chkOnlySelectedMembers.TabIndex = 5;
            this.chkOnlySelectedMembers.CheckedChanged += new System.EventHandler(this.ApplyFilter);
            // 
            // panelControl3
            // 
            this.panelControl3.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.panelControl3.Controls.Add(this.pnlTree);
            this.panelControl3.Controls.Add(this.pnlHierarchyOuter);
            this.panelControl3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelControl3.Location = new System.Drawing.Point(5, 91);
            this.panelControl3.LookAndFeel.UseDefaultLookAndFeel = false;
            this.panelControl3.Name = "panelControl3";
            this.panelControl3.Padding = new System.Windows.Forms.Padding(5);
            this.panelControl3.Size = new System.Drawing.Size(671, 234);
            this.panelControl3.TabIndex = 2;
            // 
            // pnlTree
            // 
            this.pnlTree.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.pnlTree.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnlTree.Location = new System.Drawing.Point(5, 5);
            this.pnlTree.Name = "pnlTree";
            this.pnlTree.Size = new System.Drawing.Size(494, 224);
            this.pnlTree.TabIndex = 1;
            // 
            // pnlHierarchyOuter
            // 
            this.pnlHierarchyOuter.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.pnlHierarchyOuter.Controls.Add(this.pnlHierarchyInner);
            this.pnlHierarchyOuter.Dock = System.Windows.Forms.DockStyle.Right;
            this.pnlHierarchyOuter.Location = new System.Drawing.Point(499, 5);
            this.pnlHierarchyOuter.LookAndFeel.UseDefaultLookAndFeel = false;
            this.pnlHierarchyOuter.Margin = new System.Windows.Forms.Padding(10, 3, 3, 3);
            this.pnlHierarchyOuter.Name = "pnlHierarchyOuter";
            this.pnlHierarchyOuter.Padding = new System.Windows.Forms.Padding(10, 0, 0, 0);
            this.pnlHierarchyOuter.Size = new System.Drawing.Size(167, 224);
            this.pnlHierarchyOuter.TabIndex = 0;
            this.pnlHierarchyOuter.Visible = false;
            // 
            // pnlHierarchyInner
            // 
            this.pnlHierarchyInner.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(249)))), ((int)(((byte)(248)))), ((int)(((byte)(245)))));
            this.pnlHierarchyInner.Appearance.Options.UseBackColor = true;
            this.pnlHierarchyInner.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.HotFlat;
            this.pnlHierarchyInner.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnlHierarchyInner.Location = new System.Drawing.Point(10, 0);
            this.pnlHierarchyInner.LookAndFeel.Style = DevExpress.LookAndFeel.LookAndFeelStyle.Flat;
            this.pnlHierarchyInner.LookAndFeel.UseDefaultLookAndFeel = false;
            this.pnlHierarchyInner.Name = "pnlHierarchyInner";
            this.pnlHierarchyInner.Size = new System.Drawing.Size(157, 224);
            this.pnlHierarchyInner.TabIndex = 0;
            // 
            // flowLayoutPanel1
            // 
            this.flowLayoutPanel1.AutoSize = true;
            this.flowLayoutPanel1.Controls.Add(this.chkExpand);
            this.flowLayoutPanel1.Controls.Add(this.chkOnlySelectedMembers);
            this.flowLayoutPanel1.Controls.Add(this.chkOnlyMembersWithData);
            this.flowLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.flowLayoutPanel1.Location = new System.Drawing.Point(5, 325);
            this.flowLayoutPanel1.Name = "flowLayoutPanel1";
            this.flowLayoutPanel1.Size = new System.Drawing.Size(671, 25);
            this.flowLayoutPanel1.TabIndex = 8;
            // 
            // CommonDimensionFilterPanel
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.panelControl3);
            this.Controls.Add(this.flowLayoutPanel1);
            this.Controls.Add(this.panelControl1);
            this.MinimumSize = new System.Drawing.Size(400, 200);
            this.Name = "CommonDimensionFilterPanel";
            this.Padding = new System.Windows.Forms.Padding(5, 0, 0, 0);
            this.Size = new System.Drawing.Size(676, 350);
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).EndInit();
            this.panelControl1.ResumeLayout(false);
            this.panelControl1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.SearchText.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkExpand.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkOnlyMembersWithData.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkOnlySelectedMembers.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl3)).EndInit();
            this.panelControl3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pnlTree)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pnlHierarchyOuter)).EndInit();
            this.pnlHierarchyOuter.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pnlHierarchyInner)).EndInit();
            this.flowLayoutPanel1.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private DevExpress.XtraEditors.PanelControl panelControl1;
        private ExcelAddIn.UI.Controls.CheckButton btnHierarchy;
        private DevExpress.XtraEditors.PanelControl panelControl3;
        private DevExpress.XtraEditors.CheckEdit chkOnlyMembersWithData;
        private DevExpress.XtraEditors.CheckEdit chkOnlySelectedMembers;
        private DevExpress.XtraEditors.LabelControl lblTitle;
        private DevExpress.XtraEditors.PanelControl pnlTree;
        private DevExpress.XtraEditors.PanelControl pnlHierarchyOuter;
        private DevExpress.XtraEditors.PanelControl pnlHierarchyInner;
        private DevExpress.XtraEditors.SimpleButton btnSelectAll;
        private DevExpress.XtraEditors.SimpleButton btnNoFilter;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel1;
        private DevExpress.XtraEditors.CheckEdit chkExpand;
        private IncrementalSearchTextBox SearchText;


    }
}
