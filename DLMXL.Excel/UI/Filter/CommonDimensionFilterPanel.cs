﻿using System;
using System.Drawing;
using System.Linq;
using System.Windows.Forms;
using DevExpress.XtraTreeList;
using DevExpress.XtraTreeList.Nodes;
using DLMXL.DAL;
using DLMXL.Models;

namespace ExcelAddIn.UI.Filter
{
    public interface IDimensionFilterPanel
    {
        SWFilterDimension FilterDimension { get; }
        bool IsValid { get; }
        void Apply();
    }


    public partial class CommonDimensionFilterPanel : AddInBaseUserControl, IDimensionFilterPanel
    {
        public SWFilterDimension FilterDimension { get;}

        public bool IsValid => true;

        public void Apply()
        {}

        private readonly DimensionDisplayTree _DimensionDisplayTree;
        private readonly int _maxLevel;
        private int _programmaticChange;
        private bool isDataExistanceCheck; 

        public CommonDimensionFilterPanel(SWFilterDimension fdim,  bool isDataExistanceCheck = false)
        {
            this.isDataExistanceCheck = isDataExistanceCheck;
            
            InitializeComponent( );

            SetStyleController(InputStyleController,
                SearchText,
                chkExpand,
                chkOnlySelectedMembers,
                chkOnlyMembersWithData
            );
            
            try 
            {
                _programmaticChange++;
                
                SearchText.SearchTextHint = "type here to filter members";
                SearchText.SearchEventTriggerDelayInMilliseconds = 300;

                this.chkOnlyMembersWithData.Visible = isDataExistanceCheck;    
                
                lblTitle.Text                       = fdim.Dimension.GetLabel("{0} - {1}", Config.Settings.Language);
                FilterDimension                     = fdim;

                _DimensionDisplayTree = new DimensionDisplayTree(fdim) {
                    Dock                = DockStyle.Fill,
                    CheckIfNodeVisible  = IsNodeVisible
                };
                _DimensionDisplayTree.AfterCheckNode += DimensionDisplayTreeAfterCheckNode;
                
                pnlTree.Controls.Add(_DimensionDisplayTree);

                _maxLevel               = FilterDimension.SelectableMembers.MaxOrDefault( mem => mem.Depth, 1 );

                if (_maxLevel > 1)
                {
                    btnHierarchy.Visible    = true;
                    chkExpand.Visible       = true;
                    BuildHierarchyLevel(_maxLevel);
                }

                this.Load += OnLoad;
            }

            finally {
                _programmaticChange--;
            }
        }

        #region Methods

        private void BuildHierarchyLevel(int levels)
        {
            for (var i = 0; i < levels; i++)
                AddLevelOption("Select level {0}".F(i + 1), i);

            AddLevelOption("Lowest level", levels, true);
        }

        private void AddLevelOption(string name, int index, bool last=false)
        {
            var chk = new DevExpress.XtraEditors.CheckEdit()
            {
                StyleController = InputStyleController,
                Text            = name,
                Size            = new Size(150, 15),
                Location        = new Point(10, 5 + 20 * index),
                Tag             = last ? -1 : index 
            };

            chk.CheckedChanged+=HierarchyLevelChecked;
            pnlHierarchyInner.Controls.Add(chk);
        }

        #region Hierarchy levels

        private void UpdateHierarchyLevels(bool? @explicit = null)
        {
            if (_maxLevel < 2)
                return;

            var levels = Enumerable.Repeat(@explicit ?? true, _maxLevel + 1).ToArray();
            
            if(!@explicit.HasValue) 
                UpdateLevelsFromTree(_DimensionDisplayTree.Nodes, levels);

            UpdateLevelsSelection(levels);
        }

        private void UpdateLevelsFromTree(TreeListNodes nodes, bool[] levels)
        {
            foreach (TreeListNode node in nodes)
            {
                levels[node.Level] &= node.Checked;
                if (node.HasChildren) UpdateLevelsFromTree(node.Nodes, levels);
                else levels[levels.Length - 1] &= node.Checked;
            }
        }

        private void UpdateLevelsSelection(bool[] levels)
        {
            for (var i = 0; i < pnlHierarchyInner.Controls.Count; i++)
            {
                var chk = pnlHierarchyInner.Controls[i] as DevExpress.XtraEditors.CheckEdit;

                if (chk != null)
                    chk.Checked = levels[i];
            }
        }

        #endregion

        private void ApplyFilter()
        {
            if (chkExpand.Visible)
                this.chkExpand.Checked = false;

            _DimensionDisplayTree.ApplyFilter(SearchText.Text.Trim(), chkOnlySelectedMembers.Checked);
        }

        private void ChangeNodeState(TreeListNodes nodes, int level, int changeLevel, bool @checked)
        {
            foreach (TreeListNode node in nodes)
            {
                if (node.Checked != @checked && (level == changeLevel || changeLevel == -1 && !node.HasChildren))
                {
                    node.Checked = @checked;
                    _DimensionDisplayTree.DoNodeSelection(node, DimensionDisplayTree.SelectionMode.Single);
                }

                if(node.HasChildren)
                    ChangeNodeState(node.Nodes, level + 1, changeLevel, @checked);
            }
        }

        

        private bool IsNodeVisible(TreeListNode node)
        {
            if (!isDataExistanceCheck || !chkOnlyMembersWithData.Checked)
                return true;

            if (node == null)
                return false;

            return true;

//            var mem = node.Tag as SWDimensionMember;
//
//            return mem != null
//                        &&
//                   (mem.DataStatus.Value.HasImportData || mem.DataStatus.Value.HasProdData);
        }

        #endregion

        #region Event handlers
        private void OnLoad(object sender, EventArgs e)
        {
            UpdateHierarchyLevels();
        }

        private void btnSelectAll_Click(object sender, EventArgs e)
        {
            if (_programmaticChange > 0)
                return;

            try
            {
                _programmaticChange++;
                _DimensionDisplayTree.CheckAll();
                UpdateHierarchyLevels(true);
            }
            finally
            {
                _programmaticChange--;
            }
        }

        private void btnNoFilter_Click(object sender, EventArgs e)
        {
            try
            {
                _programmaticChange++;
                _DimensionDisplayTree.UncheckAll();
                UpdateHierarchyLevels(false);
            }
            finally
            {
                _programmaticChange--;
            }
        }

        private void btnHierarchy_Click(object sender, EventArgs e)
        {
            pnlHierarchyOuter.Visible = !pnlHierarchyOuter.Visible;
        }

        private void DimensionDisplayTreeAfterCheckNode(object sender, NodeEventArgs e)
        {
            if (_programmaticChange > 0)
                return;

            try
            {
                _programmaticChange++;

                UpdateHierarchyLevels();
            }
            finally
            {
                _programmaticChange--;
            }            
        }

        private void chkExpand_CheckedChanged(object sender, EventArgs e)
        {
            if (chkExpand.Checked)  _DimensionDisplayTree.ExpandAll();
            else                    _DimensionDisplayTree.CollapseAll();
        }

        private void ApplyFilter(object sender, EventArgs e)
        {
            ApplyFilter();
        }

        private void HierarchyLevelChecked(object sender, EventArgs e)
        {
            if (_programmaticChange > 0)
                return;

            try
            {
                _programmaticChange++;

                var chk = sender as DevExpress.XtraEditors.CheckEdit;

                if (chk == null)
                    return;

                ChangeNodeState(_DimensionDisplayTree.Nodes, 0, Convert.ToInt32(chk.Tag), chk.Checked);
                UpdateHierarchyLevels();
            }
            finally
            {
                _programmaticChange--;
            }
        }

        #endregion


    }
}
