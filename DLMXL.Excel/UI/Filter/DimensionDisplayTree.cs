﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Windows.Forms;
using DevExpress.XtraGrid;
using DevExpress.XtraTreeList.Columns;
using DevExpress.XtraTreeList.Data;
using DevExpress.XtraTreeList.Nodes;
using DLMXL.Models;
using DLMXL.Models.Interfaces;

namespace ExcelAddIn.UI.Filter {
    public class DimensionDisplayTree: SWTreeList {
        private readonly SWFilterDimension FilterDimension;
        private readonly Timer clickWaitingTimer;
        private TreeListNode clickedNode;
        private bool doubleClickHasBeenHandled;
        private bool isNotClickOnLabel;
        private bool isUsingMultiSelectionKey;

        public DimensionDisplayTree( SWFilterDimension fdim)  {
            FilterDimension                 = fdim;
            OptionsSelection.MultiSelect    = true;
            OptionsView.ShowCheckBoxes      = true;

            clickWaitingTimer = new Timer {
                                                  Interval = SystemInformation.DoubleClickTime + 1
                                          };
            clickWaitingTimer.Tick += ClickWaitingTimerOnTick;

            InitComplete = true;
        }

        public Func<TreeListNode, bool> CheckIfNodeVisible { get; set; }

        public new void CheckAll( ) {
            try {
                ProgrammaticChange++;
                BeginUpdate( );
                FilterDimension.ResetSelectedMembers(FilterDimension.SelectableMembers);
                base.CheckAll( );
            } finally {
                EndUpdate( );
                ProgrammaticChange--;
            }
        }

        public new void UncheckAll( ) {
            try {
                ProgrammaticChange++;
                BeginUpdate( );
                FilterDimension.ResetSelectedMembers();
                base.UncheckAll( );
            } finally {
                EndUpdate( );
                ProgrammaticChange--;
            }
        }

        // This event is raised when the user click on a member label, but also on the member checkbox, 
        // even if it doesn't pass through this event handler when setting breakpoints.
        protected override void OnMouseClick( MouseEventArgs e ) {
            if( isNotClickOnLabel ) {
                isNotClickOnLabel = false;
            }
            else {
                clickWaitingTimer.Start( );
                doubleClickHasBeenHandled = false;
                isUsingMultiSelectionKey = ModifierKeys.HasFlag( Keys.Shift ) || ModifierKeys.HasFlag( Keys.Control );
                clickedNode = CalcHitInfo(new Point( e.X, e.Y)).Node;
            }
            base.OnMouseClick( e );
        }

        protected override void OnMouseDoubleClick( MouseEventArgs e ) {
            if( clickedNode != null ) {
                doubleClickHasBeenHandled = true;
                clickedNode.Checked = !clickedNode.Checked;
                SelectionMode selectionMode = ModifierKeys.HasFlag( Keys.Control ) ? SelectionMode.AllDescendents : SelectionMode.AllChildren;
                DoNodeSelection( clickedNode, selectionMode );
                clickedNode = null;
            }
            base.OnMouseDoubleClick( e );
        }

        private void ClickWaitingTimerOnTick( object sender, EventArgs eventArgs ) {
            if( clickedNode != null ) {
                clickWaitingTimer.Stop( );
                if( doubleClickHasBeenHandled || isUsingMultiSelectionKey ) {
                    return;
                }
                clickedNode.Checked = !clickedNode.Checked;
                DoNodeSelection( clickedNode, SelectionMode.Single );
                clickedNode = null;
            }
        }

        protected override void RaiseAfterCheckNode( TreeListNode node ) {
            isNotClickOnLabel = true;
            DoNodeSelection( node, SelectionMode.Single );
            base.RaiseAfterCheckNode( node );
        }

        protected override void OnSelectionChanged( ) {
            if( Selection.Count > 1 ) {
                List< TreeListNode > selectedNodes = Selection.Cast< TreeListNode >( )
                        .ToList( );
                bool isLastChecked = selectedNodes[ selectedNodes.Count - 1 ].Checked;
                foreach( TreeListNode node in selectedNodes ) {
                    node.Checked = !isLastChecked;
                    DoNodeSelection( node, SelectionMode.AllDescendents );
                }
            }
            base.OnSelectionChanged( );
        }

        protected override bool OnBeforeChangeExpanded( TreeListNode node, bool newVal ) {
            isNotClickOnLabel = true;
            return base.OnBeforeChangeExpanded( node, newVal );
        }

        protected override void ApplyFilterCore(TreeListNodes nl, params object[] searchCriteria)
        {
            var srchTxt = searchCriteria[0] as string;
            var showOnlySelected = (bool)searchCriteria[1];
            if (string.IsNullOrWhiteSpace(srchTxt) && !showOnlySelected && (CheckIfNodeVisible == null || CheckIfNodeVisible(null) ))
            {
                ClearFilter();
                return;
            }
            foreach (TreeListNode sn in nl)
            {
                var curText = sn.GetDisplayText(0);
                var matchSrchTxt = string.IsNullOrWhiteSpace(srchTxt) ||
                    (!string.IsNullOrEmpty(curText) && (curText.IndexOf(srchTxt, StringComparison.InvariantCultureIgnoreCase) >= 0));
                var matchSelected = !showOnlySelected || sn.Checked;
                var match = matchSrchTxt && matchSelected && (CheckIfNodeVisible == null || CheckIfNodeVisible(sn));
                sn.Visible = match;
                ApplyFilterCore(sn.Nodes, searchCriteria);
                if (!match)
                {
                    continue;
                }
                var pn = sn;
                while (pn.ParentNode != null)
                {
                    pn.ParentNode.Visible = true;
                    pn.ParentNode.Expanded = true;
                    pn = pn.ParentNode;
                }
            }
        }

        public void DoNodeSelection( TreeListNode node, SelectionMode selectionMode ) {
            if( ProgrammaticChange > 0 )
                return;

            try {
                ProgrammaticChange++;

                var mem = FilterDimension.Dimension.FindMember( node.Id );

                if (mem == null) return;

                if ( node.Checked ) {

                    FilterDimension.AddSelectedMember( mem );

                    if( selectionMode == SelectionMode.AllChildren ) {
                        FilterDimension.AddSelectedMembers( mem.ChildMembers.Where( FilterDimension.IsMemberSelectable ) );
                        UpdateCheckedNodesFromFilter( );
                    }
                    else if( selectionMode == SelectionMode.AllDescendents ) {
                        FilterDimension.AddSelectedMembers( mem.AllDescendents.Where( FilterDimension.IsMemberSelectable ) );
                        UpdateCheckedNodesFromFilter( );
                    }
                }
                else {
                    FilterDimension.RemoveSelectedMember( mem );
                    if( selectionMode == SelectionMode.AllChildren ) {
                        FilterDimension.RemoveSelectedMembers( mem.ChildMembers.Where( FilterDimension.IsMemberSelectable ) );
                        UpdateCheckedNodesFromFilter( );
                    }
                    else if( selectionMode == SelectionMode.AllDescendents ) {
                        FilterDimension.RemoveSelectedMembers( mem.AllDescendents.Where( FilterDimension.IsMemberSelectable ) );
                        UpdateCheckedNodesFromFilter( );
                    }
                }

            }
            finally
            {
                ProgrammaticChange--;
            }
        }

        protected override TreeListColumnCollection CreateColumns( ) {
            var cl = new[ ] {
                                new TreeListColumn {
                                        Name = "Code",
                                        Caption = "Code",
                                        AllowIncrementalSearch = true,
                                        FilterMode = ColumnFilterMode.DisplayText,
                                        MinWidth = 140,
                                        UnboundType = UnboundColumnType.String,
                                        Visible = true,
                                        VisibleIndex = 1,
                                },
                            };

            var res = new TreeListColumnCollection( this );
            res.AddRange( cl );
            return res;
        }

        protected override TreeListData CreateData( ) {
            return new DimensionTreeData( this );
        }

        private void UpdateCheckedNodesFromFilter( ) {
            BeginUpdate( );
            Selection.Clear( );
            base.UncheckAll( );
            foreach( var mem in FilterDimension.SelectedMembers ) {
                var node = FindNodeByID(mem.Id);
                if( node != null )
                    node.Checked = true;
            }
            EndUpdate( );
        }

        private sealed class DimensionTreeData: ReadOnlyTreeListData {
            private List< ICodelistMember > _DataList;

            public DimensionTreeData( DimensionDisplayTree tl )
                    : base( tl ) {
            }

            public override IList DataList {
                get {
                    return _DataList
                           ?? ( _DataList =
                                   new List<ICodelistMember>
                                           ( DisplayTree == null || DisplayTree.IsDesignMode
                                                   ? Enumerable.Empty<ICodelistMember>( )
                                                   : ( ( DimensionDisplayTree ) DisplayTree ).FilterDimension.SelectableMembers));
                }
            }

            public override void CreateNodes( TreeListNode parent ) {
                if( parent == null ) {
                    DataHelper.ClearNodes( );
                }
                var dt = ( DimensionDisplayTree )DisplayTree;
                var ml = parent == null
                        ? dt.FilterDimension.SelectableMembers.Where( mem => mem.ParentMember == null )
                        : dt.FilterDimension.Dimension.FindMember(parent.Id).ChildMembers.Where( dt.FilterDimension.IsMemberSelectable );

                foreach ( var mem in ml ) {
                    var node = DataHelper.CreateNode( mem.Id, parent, mem );
                    node.HasChildren = mem.ChildMembers.Where(dt.FilterDimension.IsMemberSelectable).Any();
                    node.Visible = true;
                    node.CheckState = dt.FilterDimension.IsMemberSelected( mem ) ? CheckState.Checked : CheckState.Unchecked;
                    CreateNodes( node );
                }
            }

            public override object GetValue( int nodeId, object columnId ) {

                var mem = (( DimensionDisplayTree )DisplayTree ).FilterDimension.Dimension.FindMember(nodeId);

                if ( mem == null ) 
                    throw new InvalidProgramException();

                return mem.Names.ContainsKey(Config.Settings.Language)
                    ? $"{mem.Names[Config.Settings.Language]} ({mem.Code})"
                    : mem.Code;
            }
        }

        public enum SelectionMode {
            Single,
            AllChildren,
            AllDescendents
        }
    }
}
