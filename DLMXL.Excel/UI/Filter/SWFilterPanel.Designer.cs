﻿namespace ExcelAddIn.UI.Filter
{
    partial class SWFilterPanel {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose( bool disposing ) {
            if( disposing && ( components != null ) ) {
                components.Dispose( );
            }
            base.Dispose( disposing );
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent( ) {
            this.DimensionsTabs = new DevExpress.XtraTab.XtraTabControl();
            this.xtraTabPage1 = new DevExpress.XtraTab.XtraTabPage();
            ((System.ComponentModel.ISupportInitialize)(this.DimensionsTabs)).BeginInit();
            this.DimensionsTabs.SuspendLayout();
            this.SuspendLayout();
            // 
            // DimensionsTabs
            // 
            this.DimensionsTabs.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(103)))), ((int)(((byte)(205)))), ((int)(((byte)(244)))));
            this.DimensionsTabs.Appearance.Options.UseBackColor = true;
            this.DimensionsTabs.AppearancePage.Header.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.DimensionsTabs.AppearancePage.Header.Options.UseFont = true;
            this.DimensionsTabs.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.DimensionsTabs.BorderStylePage = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.DimensionsTabs.Dock = System.Windows.Forms.DockStyle.Fill;
            this.DimensionsTabs.HeaderLocation = DevExpress.XtraTab.TabHeaderLocation.Left;
            this.DimensionsTabs.HeaderOrientation = DevExpress.XtraTab.TabOrientation.Horizontal;
            this.DimensionsTabs.Location = new System.Drawing.Point(0, 0);
            this.DimensionsTabs.Name = "DimensionsTabs";
            this.DimensionsTabs.SelectedTabPage = this.xtraTabPage1;
            this.DimensionsTabs.Size = new System.Drawing.Size(724, 367);
            this.DimensionsTabs.TabIndex = 2;
            this.DimensionsTabs.TabPages.AddRange(new DevExpress.XtraTab.XtraTabPage[] {
            this.xtraTabPage1});
            this.DimensionsTabs.TabPageWidth = 180;
            // 
            // xtraTabPage1
            // 
            this.xtraTabPage1.Font = new System.Drawing.Font("Tahoma", 10F);
            this.xtraTabPage1.Name = "xtraTabPage1";
            this.xtraTabPage1.Size = new System.Drawing.Size(524, 361);
            this.xtraTabPage1.Text = "Dimension";
            // 
            // SWFilterPanel
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.DimensionsTabs);
            this.Name = "SWFilterPanel";
            this.Size = new System.Drawing.Size(724, 367);
            ((System.ComponentModel.ISupportInitialize)(this.DimensionsTabs)).EndInit();
            this.DimensionsTabs.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraTab.XtraTabControl DimensionsTabs;
        private DevExpress.XtraTab.XtraTabPage xtraTabPage1;
    }
}
