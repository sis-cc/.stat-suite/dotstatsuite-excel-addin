﻿using System;
using System.ComponentModel;
using System.Linq;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using DevExpress.XtraTab;
using DLMXL.Models;
using ExcelAddIn.Domain;

namespace ExcelAddIn.UI.Filter
{
    public partial class SWFilterPanel: XtraUserControl
    {
        private int _ProgrammaticChange;

        private SWFilterPanel( ) {
            InitializeComponent( );
        }

        public SWFilterPanel(SdmxFilter filter, bool showDataExistanceCheck = false)
            : this() 
        {
            try {
                _ProgrammaticChange++;
                SuspendLayout( );
   
                DimensionsTabs.TabPages.Clear();
                
                foreach( var fdim in filter.SWFilter.FilterDimensions )
                {
                    if (fdim.Dimension.IsFrequency)
                        continue;
                    
                    DimensionsTabs.TabPages.Add(CreateDimensionTab(filter, fdim, showDataExistanceCheck));
                    fdim.PropertyChanged += FilterDimPropertyChanged;
                }

            } finally {
                ResumeLayout( );
                _ProgrammaticChange--;
            }
        }

        public bool IsValid()
        {
            foreach (XtraTabPage tab in DimensionsTabs.TabPages)
                if (!((IDimensionFilterPanel)tab.Controls[0]).IsValid)
                    return false;

            return true;
        }

        public void Apply()
        {
            foreach (XtraTabPage tab in DimensionsTabs.TabPages)
                ((IDimensionFilterPanel) tab.Controls[0]).Apply();
        }

        private XtraTabPage FindTab( SWFilterDimension fdim ) {
            foreach( XtraTabPage tab in DimensionsTabs.TabPages ) {
                var fp = (IDimensionFilterPanel) tab.Controls[0];
                // ReSharper disable once PossibleUnintendedReferenceComparison
                if( fp.FilterDimension == fdim ) {
                    return tab;
                }
            }
            throw new InvalidProgramException( );
        }

        private void FilterDimPropertyChanged( object sender, PropertyChangedEventArgs e ) {
            var fdim = ( SWFilterDimension )sender;

            var tab     = FindTab(fdim);
            tab.Text    = BuildTabCaption(fdim);
        }

        private static string BuildTabCaption(SWFilterDimension fdim)
        {
            if (fdim.Dimension.IsTimeSeries)
                return fdim.Dimension.GetLabel("{0} - {1}", Config.Settings.Language);

            var selected    = fdim.SelectedMembers.Count();
            var total       = fdim.SelectableMembers.Count();

            return "{0}\n{1} / {2}".F(
                fdim.Dimension.GetLabel("{0} - {1}", Config.Settings.Language),
                selected == 0 || selected == total ? "All" : selected.ToString(),
                total
            );
        }

        private static XtraTabPage CreateDimensionTab(SdmxFilter filter, SWFilterDimension fdim,  bool showDataExistanceCheck = false) 
        {
            var tab = new XtraTabPage { Text = BuildTabCaption( fdim)};

            var fp = fdim.Dimension.IsTimeSeries
                ? (UserControl) new TimeDimensionFilterPanel(filter, fdim, filter.SWFilter.FilterDimensions.FirstOrDefault(x=>x.Dimension.IsFrequency))
                : new CommonDimensionFilterPanel(fdim, showDataExistanceCheck);

            fp.Dock = DockStyle.Fill;
            tab.Controls.Add( fp );
            
            return tab;
        }
    }
}
