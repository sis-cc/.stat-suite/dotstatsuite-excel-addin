﻿namespace ExcelAddIn.UI.Filter
{
    partial class TimeDimensionFilterPanel
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.panelControl1 = new DevExpress.XtraEditors.PanelControl();
            this.btnNoFilter = new DevExpress.XtraEditors.SimpleButton();
            this.lblTitle = new DevExpress.XtraEditors.LabelControl();
            this.ddlFrequency = new DevExpress.XtraEditors.ComboBoxEdit();
            this.pnlMain = new DevExpress.XtraEditors.PanelControl();
            this.lblFrequency = new DevExpress.XtraEditors.LabelControl();
            this.txtTo = new DevExpress.XtraEditors.TextEdit();
            this.txtFrom = new DevExpress.XtraEditors.TextEdit();
            this.lblTo = new DevExpress.XtraEditors.LabelControl();
            this.lblFrom = new DevExpress.XtraEditors.LabelControl();
            this.pnlLastPeriods = new DevExpress.XtraEditors.PanelControl();
            this.lblLastPeriods = new DevExpress.XtraEditors.LabelControl();
            this.txtLastPeriods = new DevExpress.XtraEditors.TextEdit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).BeginInit();
            this.panelControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ddlFrequency.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pnlMain)).BeginInit();
            this.pnlMain.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtTo.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtFrom.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pnlLastPeriods)).BeginInit();
            this.pnlLastPeriods.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtLastPeriods.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // panelControl1
            // 
            this.panelControl1.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.panelControl1.Controls.Add(this.btnNoFilter);
            this.panelControl1.Controls.Add(this.lblTitle);
            this.panelControl1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelControl1.Location = new System.Drawing.Point(0, 0);
            this.panelControl1.LookAndFeel.UseDefaultLookAndFeel = false;
            this.panelControl1.Name = "panelControl1";
            this.panelControl1.Size = new System.Drawing.Size(444, 58);
            this.panelControl1.TabIndex = 1;
            // 
            // btnNoFilter
            // 
            this.btnNoFilter.Location = new System.Drawing.Point(10, 29);
            this.btnNoFilter.Name = "btnNoFilter";
            this.btnNoFilter.Size = new System.Drawing.Size(75, 23);
            this.btnNoFilter.TabIndex = 14;
            this.btnNoFilter.Text = "NO FILTER";
            this.btnNoFilter.Click += new System.EventHandler(this.btnNoFilter_Click);
            // 
            // lblTitle
            // 
            this.lblTitle.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.lblTitle.Location = new System.Drawing.Point(10, 5);
            this.lblTitle.Name = "lblTitle";
            this.lblTitle.Size = new System.Drawing.Size(88, 19);
            this.lblTitle.TabIndex = 13;
            this.lblTitle.Text = "YEA - Year";
            // 
            // ddlFrequency
            // 
            this.ddlFrequency.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.ddlFrequency.EditValue = "";
            this.ddlFrequency.Location = new System.Drawing.Point(127, 6);
            this.ddlFrequency.Name = "ddlFrequency";
            this.ddlFrequency.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.ddlFrequency.Properties.NullValuePrompt = "-- choose type --";
            this.ddlFrequency.Properties.NullValuePromptShowForEmptyValue = true;
            this.ddlFrequency.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            this.ddlFrequency.Size = new System.Drawing.Size(241, 20);
            this.ddlFrequency.TabIndex = 26;
            this.ddlFrequency.SelectedIndexChanged += new System.EventHandler(this.ddlFrequency_SelectedIndexChanged);
            // 
            // pnlMain
            // 
            this.pnlMain.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.pnlMain.Controls.Add(this.lblFrequency);
            this.pnlMain.Controls.Add(this.ddlFrequency);
            this.pnlMain.Controls.Add(this.txtTo);
            this.pnlMain.Controls.Add(this.txtFrom);
            this.pnlMain.Controls.Add(this.lblTo);
            this.pnlMain.Controls.Add(this.lblFrom);
            this.pnlMain.Dock = System.Windows.Forms.DockStyle.Top;
            this.pnlMain.Location = new System.Drawing.Point(0, 58);
            this.pnlMain.LookAndFeel.UseDefaultLookAndFeel = false;
            this.pnlMain.Name = "pnlMain";
            this.pnlMain.Size = new System.Drawing.Size(444, 67);
            this.pnlMain.TabIndex = 2;
            // 
            // lblFrequency
            // 
            this.lblFrequency.Location = new System.Drawing.Point(10, 9);
            this.lblFrequency.Name = "lblFrequency";
            this.lblFrequency.Size = new System.Drawing.Size(51, 13);
            this.lblFrequency.TabIndex = 27;
            this.lblFrequency.Text = "Frequency";
            // 
            // txtTo
            // 
            this.txtTo.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.txtTo.Location = new System.Drawing.Point(322, 38);
            this.txtTo.Name = "txtTo";
            this.txtTo.Properties.NullValuePromptShowForEmptyValue = true;
            this.txtTo.Size = new System.Drawing.Size(100, 20);
            this.txtTo.TabIndex = 15;
            this.txtTo.Modified += new System.EventHandler(this.txtTo_Modified);
            this.txtTo.Validating += new System.ComponentModel.CancelEventHandler(this.txtTo_Validating);
            // 
            // txtFrom
            // 
            this.txtFrom.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.txtFrom.Location = new System.Drawing.Point(127, 38);
            this.txtFrom.Name = "txtFrom";
            this.txtFrom.Properties.NullValuePromptShowForEmptyValue = true;
            this.txtFrom.Size = new System.Drawing.Size(100, 20);
            this.txtFrom.TabIndex = 14;
            this.txtFrom.Modified += new System.EventHandler(this.txtFrom_Modified);
            this.txtFrom.Validating += new System.ComponentModel.CancelEventHandler(this.txtFrom_Validating);
            // 
            // lblTo
            // 
            this.lblTo.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.lblTo.Location = new System.Drawing.Point(289, 41);
            this.lblTo.Name = "lblTo";
            this.lblTo.Size = new System.Drawing.Size(12, 13);
            this.lblTo.TabIndex = 2;
            this.lblTo.Text = "To";
            // 
            // lblFrom
            // 
            this.lblFrom.Location = new System.Drawing.Point(10, 41);
            this.lblFrom.Name = "lblFrom";
            this.lblFrom.Size = new System.Drawing.Size(24, 13);
            this.lblFrom.TabIndex = 0;
            this.lblFrom.Text = "From";
            // 
            // pnlLastPeriods
            // 
            this.pnlLastPeriods.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.pnlLastPeriods.Controls.Add(this.lblLastPeriods);
            this.pnlLastPeriods.Controls.Add(this.txtLastPeriods);
            this.pnlLastPeriods.Dock = System.Windows.Forms.DockStyle.Top;
            this.pnlLastPeriods.Location = new System.Drawing.Point(0, 125);
            this.pnlLastPeriods.LookAndFeel.UseDefaultLookAndFeel = false;
            this.pnlLastPeriods.Name = "pnlLastPeriods";
            this.pnlLastPeriods.Size = new System.Drawing.Size(444, 42);
            this.pnlLastPeriods.TabIndex = 3;
            // 
            // lblLastPeriods
            // 
            this.lblLastPeriods.Location = new System.Drawing.Point(10, 9);
            this.lblLastPeriods.Name = "lblLastPeriods";
            this.lblLastPeriods.Size = new System.Drawing.Size(66, 13);
            this.lblLastPeriods.TabIndex = 28;
            this.lblLastPeriods.Text = "Last period(s)";
            // 
            // txtLastPeriods
            // 
            this.txtLastPeriods.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.txtLastPeriods.Location = new System.Drawing.Point(127, 6);
            this.txtLastPeriods.Name = "txtLastPeriods";
            this.txtLastPeriods.Properties.Mask.EditMask = "\\d+";
            this.txtLastPeriods.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.RegEx;
            this.txtLastPeriods.Properties.Mask.SaveLiteral = false;
            this.txtLastPeriods.Properties.Mask.ShowPlaceHolders = false;
            this.txtLastPeriods.Properties.MaxLength = 5;
            this.txtLastPeriods.Properties.NullValuePromptShowForEmptyValue = true;
            this.txtLastPeriods.Size = new System.Drawing.Size(100, 20);
            this.txtLastPeriods.TabIndex = 30;
            this.txtLastPeriods.Modified += new System.EventHandler(this.txtLastPeriods_Modified);
            // 
            // TimeDimensionFilterPanel
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.pnlLastPeriods);
            this.Controls.Add(this.pnlMain);
            this.Controls.Add(this.panelControl1);
            this.Name = "TimeDimensionFilterPanel";
            this.Size = new System.Drawing.Size(444, 238);
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).EndInit();
            this.panelControl1.ResumeLayout(false);
            this.panelControl1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ddlFrequency.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pnlMain)).EndInit();
            this.pnlMain.ResumeLayout(false);
            this.pnlMain.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtTo.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtFrom.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pnlLastPeriods)).EndInit();
            this.pnlLastPeriods.ResumeLayout(false);
            this.pnlLastPeriods.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtLastPeriods.Properties)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraEditors.PanelControl panelControl1;
        private DevExpress.XtraEditors.PanelControl pnlMain;
        private DevExpress.XtraEditors.LabelControl lblTo;
        private DevExpress.XtraEditors.LabelControl lblFrom;
        private DevExpress.XtraEditors.TextEdit txtTo;
        private DevExpress.XtraEditors.TextEdit txtFrom;
        private DevExpress.XtraEditors.ComboBoxEdit ddlFrequency;
        private DevExpress.XtraEditors.LabelControl lblFrequency;
        private DevExpress.XtraEditors.SimpleButton btnNoFilter;
        private DevExpress.XtraEditors.LabelControl lblTitle;
        private DevExpress.XtraEditors.PanelControl pnlLastPeriods;
        private DevExpress.XtraEditors.TextEdit txtLastPeriods;
        private DevExpress.XtraEditors.LabelControl lblLastPeriods;
    }
}
