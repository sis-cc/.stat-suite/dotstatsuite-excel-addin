﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text.RegularExpressions;
using DevExpress.XtraEditors;
using DLMXL.Models;
using DLMXL.Models.Interfaces;
using ExcelAddIn.Domain;
using Org.Sdmxsource.Sdmx.Util.Date;

namespace ExcelAddIn.UI.Filter
{
    public partial class TimeDimensionFilterPanel : AddInBaseUserControl, IDimensionFilterPanel
    {
        private static readonly Regex Quarter   = new Regex(@"Q[1-4]", RegexOptions.IgnoreCase | RegexOptions.Compiled);
        private static readonly Regex Month     = new Regex(@"\d{4}-?M?\d{1,2}",RegexOptions.IgnoreCase | RegexOptions.Compiled);

        private readonly SWFilterDimension _time;
        private readonly SWFilterDimension _freq;

        public SWFilterDimension FilterDimension => _time;
        private SdmxFilter Filter { get;}

        public bool IsValid => ValidFrom() && ValidTo();

        public void Apply()
        {
            Filter.Frequency    = Frequency;
            Filter.StartTime    = txtFrom.Value();
            Filter.EndTime      = txtTo.Value();
            Filter.LastPeriods = txtLastPeriods.Text.Any() ? (int?) int.Parse(txtLastPeriods.Text) : null;

            if (_freq == null)
                return;

            IEnumerable<ICodelistMember> freqMembers = null;

            switch (Filter.Frequency)
            {
                case Frequency.Anually:
                    freqMembers = _freq.SelectableMembers.Where(x => x.IsYear);
                    break;
                case Frequency.Quarterly:
                    freqMembers = _freq.SelectableMembers.Where(x => x.IsQuarter);
                    break;
                case Frequency.Monthly:
                    freqMembers = _freq.SelectableMembers.Where(x => x.IsMonth);
                    break;
                case Frequency.Daily:
                    freqMembers = _freq.SelectableMembers.Where(x => x.IsDaily);
                    break;
                case Frequency.Weekly:
                    freqMembers = _freq.SelectableMembers.Where(x => x.IsWeekly);
                    break;
                case Frequency.Semestrial:
                    freqMembers = _freq.SelectableMembers.Where(x => x.IsSemestrial);
                    break;
                case Frequency.Business:
                    freqMembers = _freq.SelectableMembers.Where(x => x.IsBusiness);
                    break;
            }

            _freq.ResetSelectedMembers(freqMembers);
        }

        public TimeDimensionFilterPanel(SdmxFilter filter, SWFilterDimension time, SWFilterDimension freq)
        {
            InitializeComponent();

            lblTitle.Text = time.Dimension.GetLabel("{0} - {1}", Config.Settings.Language);

            SetStyleController(InputStyleController,
                txtFrom,
                txtTo,
                txtLastPeriods
            );

            SetStyleController(LabelStyleController,
                lblFrom,
                lblTo,
                lblFrequency,
                lblLastPeriods
            );

            _time           = time;
            _freq           = freq;
            Filter          = filter;

            this.BindFrequency(Filter.Frequency);

            txtFrom.Text    = Filter.StartTime;
            txtTo.Text      = Filter.EndTime;
            txtLastPeriods.Text = Filter.LastPeriods?.ToString();

            ApplyTimeConstraints(time.Dimension.Constraint?.TimeRange);
        }

        #region Methods
        private void SetPeriodOrDates(bool period)
        {
            if (period)
            {
                this.txtFrom.Text = null;
                this.txtTo.Text = null;
            }
            else
            {
                this.txtLastPeriods.Text = null;
            }
        }

        private void ApplyTimeConstraints(TimeRange range)
        {
            if (range == null)
                return;

            if (range.StartDate != null)
            {
                this.txtFrom.Properties.NullValuePrompt = $"({range.StartDate.Date:yyyy})";
            }

            if (range.EndDate != null)
            {
                this.txtTo.Properties.NullValuePrompt = $"({range.EndDate.Date:yyyy})";
            }
        }

        private Frequency Frequency => ddlFrequency.SelectedItem as DropdownItem<Frequency>;



        private string Validate(TextEdit input)
        {
            var text = input.Value();

            try
            {
                DateUtil.GetTimeFormatOfDate(text);
            }
            catch
            {
                return "Invalid date format";
            }

            return null;
        }

        private bool ValidFrom()
        {
            ClearErrors(txtFrom);

            return string.IsNullOrEmpty(txtFrom.Value()) || IsValid(txtFrom, Validate);
        }

        private bool ValidTo()
        {
            ClearErrors(txtTo);

            return string.IsNullOrEmpty(txtTo.Value()) || IsValid(txtTo, Validate);
        }
        private bool IsYearly()
        {
            return _freq?.SelectableMembers.Any(x=>x.IsYear) ?? _time.Dimension.Members.Any(x => x.Code.Length == 4);
        }

        private bool IsQuartely()
        {
            return _freq?.SelectableMembers.Any(x => x.IsQuarter) ?? _time.Dimension.Members.Any(x => Quarter.IsMatch(x.Code));
        }

        private bool IsMonthly()
        {
            return _freq?.SelectableMembers.Any(x => x.IsMonth) ?? _time.Dimension.Members.Any(x => Month.IsMatch(x.Code));
        }

        private bool IsDaily()
        {
            return _freq?.SelectableMembers.Any(x => x.IsDaily) ?? false;
        }

        private bool IsWeekly()
        {
            return _freq?.SelectableMembers.Any(x => x.IsWeekly) ?? false;
        }

        private bool IsSemestrial()
        {
            return _freq?.SelectableMembers.Any(x => x.IsSemestrial) ?? false;
        }

        private bool IsBusiness()
        {
            return _freq?.SelectableMembers.Any(x => x.IsBusiness) ?? false;
        }

        #endregion

        #region Event handlers

        private void txtFrom_Validating(object sender, System.ComponentModel.CancelEventArgs e)
        {
            if (!txtFrom.IsModified)
                return;
            
            ValidFrom();
        }

        private void txtTo_Validating(object sender, System.ComponentModel.CancelEventArgs e)
        {
            if (!txtTo.IsModified)
                return;
            
            ValidTo();
        }

        private void txtFrom_Modified(object sender, EventArgs e)
        {
            SetPeriodOrDates(false);
        }

        private void txtTo_Modified(object sender, EventArgs e)
        {
            SetPeriodOrDates(false);
        }

        private void txtLastPeriods_Modified(object sender, EventArgs e)
        {
            SetPeriodOrDates(true);
        }

        private void ddlFrequency_SelectedIndexChanged(object sender, EventArgs e)
        {
            pnlLastPeriods.Visible = Frequency != Frequency.All;

            if (!pnlLastPeriods.Visible)
            {
                txtLastPeriods.Text = null;
            }
        }

        private void btnNoFilter_Click(object sender, EventArgs e)
        {
            txtFrom.EditValue       = null;
            txtTo.EditValue         = null;
            this.ddlFrequency.SelectedIndex = 0;
        }

        #endregion

        private void BindFrequency(Frequency freq)
        {
            this.ddlFrequency.Properties.Items.Clear();
            var frequencies = GetFrequencyList().ToList();
            this.ddlFrequency.Properties.Items.AddRange(frequencies);
            this.ddlFrequency.SelectedItem = frequencies.FirstOrDefault(x => x.Value == freq);
        }

        private IEnumerable<DropdownItem<Frequency>> GetFrequencyList()
        {
            yield return new DropdownItem<Frequency>("All", Frequency.All);

            if (IsYearly())
                yield return new DropdownItem<Frequency>("Yearly", Frequency.Anually);

            if (IsSemestrial())
                yield return new DropdownItem<Frequency>("Semestrial", Frequency.Semestrial);

            if (IsQuartely())
                yield return new DropdownItem<Frequency>("Quarterly", Frequency.Quarterly);

            if (IsMonthly())
                yield return new DropdownItem<Frequency>("Monthly", Frequency.Monthly);

            if (IsWeekly())
                yield return new DropdownItem<Frequency>("Weekly", Frequency.Weekly);

            if (IsDaily())
                yield return new DropdownItem<Frequency>("Daily", Frequency.Daily);

            if (IsBusiness())
                yield return new DropdownItem<Frequency>("Business", Frequency.Business);
        }


    }
}
