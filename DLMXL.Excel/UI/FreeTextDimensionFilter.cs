﻿using System;
using System.Linq;
using System.Windows.Forms;
using DLMXL.DAL;
using DLMXL.Models;
using ExcelAddIn.Domain;

namespace ExcelAddIn.UI
{
    public sealed partial class FreeTextDimensionFilter : UserControl
    {
        public event EventHandler FilterHasChanged; 

        public FreeTextDimensionFilter()
        {
            InitializeComponent();
        }

        public bool IsValid
        {
            get { return panel.Controls.Cast<BaseDimensionFilter>().All(x => x.IsValid); }
        }

        public int ApplyFilter(SdmxFilter filter)
        {
            this.panel.Controls.Clear();

            if (filter?.SWFilter == null)
                return 0;

            filter.SWFilter.FilterDimensions
                .Where(dim =>!dim.Dimension.IsFrequency)
                .Select((dim, index) => GetFilterControl(filter,dim,index))
                .Reverse()
                .ForEach(AddControl);

            return panel.Controls.Count;
        }

        private BaseDimensionFilter GetFilterControl(SdmxFilter filter, SWFilterDimension dim, int index)
        {
            return dim.Dimension.IsTimeSeries
                ? (BaseDimensionFilter)new TimeDimensionFilter(filter, dim, index)
                : new CommonDimensionFilter(dim, index);
        }

        private void AddControl(BaseDimensionFilter ctrl)
        {
            ctrl.DimensionHasChanged += BindFilterHasChanged;
            panel.Controls.Add(ctrl);
        }

        private void BindFilterHasChanged(object sender, EventArgs e)
        {
            FilterHasChanged?.Invoke(this, null);
        }
    }
}
