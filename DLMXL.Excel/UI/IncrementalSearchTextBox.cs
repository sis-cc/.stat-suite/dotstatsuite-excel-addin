﻿using System;
using System.ComponentModel;
using System.Threading;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using DevExpress.XtraEditors.Controls;
using DevExpress.XtraEditors.Drawing;
using DevExpress.XtraEditors.Repository;
using Timer = System.Threading.Timer;

namespace ExcelAddIn.UI {
    public class IncrementalSearchTextBox: MRUEdit {
        private int _SearchEventTriggerDelayInMilliseconds;
        public event EventHandler SearchTextChanged;
        public event EventHandler ClearButtonClicked;
        private readonly Timer _Timer;
        private RepositoryItemMRUEdit _DummyProperties;

        public IncrementalSearchTextBox() : this( 800, "type here to search") {
            Width = 150;
            MinimumSearchTextLength = 2;
        }

        //to prevent properties be serialized by winforms designer
        [Browsable(false)]
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        protected new RepositoryItemMRUEdit Properties {
            get { return _DummyProperties??(_DummyProperties = new RepositoryItemMRUEdit(  ) ); }
        }

        [DefaultValue(2)]
        public int MinimumSearchTextLength { get; set; }

        public bool IsPopupDisabled { get; set; }

        public IncrementalSearchTextBox( int searchEventTriggerDelayInMilliseconds = 400, string searchHintText = "type here to search") {
            _SearchEventTriggerDelayInMilliseconds = searchEventTriggerDelayInMilliseconds;
            SearchTextHint = searchHintText;

            _Timer = new Timer( DelayedEditValueChanged, null, Timeout.Infinite, Timeout.Infinite );
        }


        protected override void OnCreateControl( ) {
            base.OnCreateControl( );

#if DEBUG
            if (this.IsReallyDesignMode())
                return;
#endif

            base.Properties.AutoComplete = true;
            base.Properties.NullValuePrompt = SearchTextHint + " (minimum " + MinimumSearchTextLength + " characters)";
            base.Properties.NullValuePromptShowForEmptyValue = true;
            var clearButton = new EditorButton( ButtonPredefines.Delete ) {
                                                                                  Image = Resources.Close_Normal
                                                                          };
            var cmb = base.Properties.Buttons.Count == 0
                      ? new EditorButton(ButtonPredefines.DropDown)
                      : base.Properties.Buttons[0];
            
            base.Properties.Buttons.Clear( );
            base.Properties.Buttons.Add( clearButton );
            base.Properties.Buttons.Add( cmb );
        }

        private void DelayedEditValueChanged( object state ) {
            if( InvokeRequired ) {
                Invoke( ( MethodInvoker )( ( ) => DelayedEditValueChanged( state ) ) );
                return;
            }
            try {
                if( IsDisposed || Disposing ) {
                    return;
                }
                OnSearchTextChanged( );
            } catch {
                //noop;
            }
        }

        [Browsable(false)]
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        public int SearchEventTriggerDelayInMilliseconds {
            get {
                return _SearchEventTriggerDelayInMilliseconds;
            } set {
                _SearchEventTriggerDelayInMilliseconds = value;
                _Timer.Change( value, Timeout.Infinite );
            }
        }

        public string SearchTextHint { get; set; }

        protected virtual void OnSearchTextChanged()
        {
            var editValue = EditValue as string;

            if (SearchTextChanged == null || String.IsNullOrEmpty(editValue))
            {
                return;
            }

            var editValueLength = editValue.Trim().Length;

            if (editValueLength > 0 && editValueLength < MinimumSearchTextLength)
            {
                return;
            }

            if (editValueLength >= MinimumSearchTextLength && !base.Properties.Items.Contains(editValue.Trim()))
            {
                base.Properties.Items.Add(editValue.Trim());
            }

            SelectionStart = editValue.Length;
            SearchTextChanged(this, EventArgs.Empty);
        }

        protected override void OnEditValueChanged( ) {
            _Timer.Change( _SearchEventTriggerDelayInMilliseconds, Timeout.Infinite );

            base.OnEditValueChanged( );
        }

        protected override void OnPopupSelectedIndexChanged( ) {
            OnSearchTextChanged( );
            
            //todo: below is needed for a devexpress bug
            if( PopupForm == null ) {
                GetPopupForm( );
            }
            
            base.OnPopupSelectedIndexChanged( );
        }

        protected override void OnPressButton(EditorButtonObjectInfoArgs buttonInfo)
        {
            if (buttonInfo.Button.Kind == ButtonPredefines.Delete)
            {
                Text = "";
                var cbc = ClearButtonClicked;
                if (cbc != null)
                {
                    cbc(this, EventArgs.Empty);
                }
            }
            else if (!IsPopupDisabled)
            {
                ShowPopup();
            }
        }

        protected override void Dispose( bool disposing ) {
            if( disposing ) {
                _Timer.Dispose( );
            }
            base.Dispose( disposing );
        }
    }
}
