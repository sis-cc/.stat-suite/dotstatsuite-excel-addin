﻿namespace ExcelAddIn.UI
{
    partial class LabelBar
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lblLeftLine = new DevExpress.XtraEditors.LabelControl();
            this.panel1 = new System.Windows.Forms.Panel();
            this.lblText = new DevExpress.XtraEditors.LabelControl();
            this.lblRightLine = new DevExpress.XtraEditors.LabelControl();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // lblLeftLine
            // 
            this.lblLeftLine.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.lblLeftLine.Dock = System.Windows.Forms.DockStyle.Left;
            this.lblLeftLine.LineColor = System.Drawing.Color.Gainsboro;
            this.lblLeftLine.LineVisible = true;
            this.lblLeftLine.Location = new System.Drawing.Point(0, 0);
            this.lblLeftLine.Name = "lblLeftLine";
            this.lblLeftLine.ShowLineShadow = false;
            this.lblLeftLine.Size = new System.Drawing.Size(23, 28);
            this.lblLeftLine.TabIndex = 5;
            // 
            // panel1
            // 
            this.panel1.AutoSize = true;
            this.panel1.Controls.Add(this.lblText);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel1.Location = new System.Drawing.Point(23, 0);
            this.panel1.Name = "panel1";
            this.panel1.Padding = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.panel1.Size = new System.Drawing.Size(42, 28);
            this.panel1.TabIndex = 6;
            // 
            // lblText
            // 
            this.lblText.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.lblText.Appearance.Font = new System.Drawing.Font("Tahoma", 8F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblText.Appearance.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(100)))), ((int)(((byte)(100)))), ((int)(((byte)(100)))));
            this.lblText.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.Horizontal;
            this.lblText.Location = new System.Drawing.Point(8, 8);
            this.lblText.Margin = new System.Windows.Forms.Padding(0);
            this.lblText.Name = "lblText";
            this.lblText.ShowLineShadow = false;
            this.lblText.Size = new System.Drawing.Size(29, 13);
            this.lblText.TabIndex = 9;
            this.lblText.Text = "label";
            // 
            // lblRightLine
            // 
            this.lblRightLine.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.lblRightLine.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblRightLine.LineColor = System.Drawing.Color.Gainsboro;
            this.lblRightLine.LineVisible = true;
            this.lblRightLine.Location = new System.Drawing.Point(65, 0);
            this.lblRightLine.Name = "lblRightLine";
            this.lblRightLine.ShowLineShadow = false;
            this.lblRightLine.Size = new System.Drawing.Size(334, 28);
            this.lblRightLine.TabIndex = 10;
            // 
            // LabelBar
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.lblRightLine);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.lblLeftLine);
            this.Name = "LabelBar";
            this.Size = new System.Drawing.Size(399, 28);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private DevExpress.XtraEditors.LabelControl lblLeftLine;
        private System.Windows.Forms.Panel panel1;
        private DevExpress.XtraEditors.LabelControl lblText;
        private DevExpress.XtraEditors.LabelControl lblRightLine;
    }
}
