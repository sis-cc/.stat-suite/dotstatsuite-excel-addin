﻿using System.Windows.Forms;

namespace ExcelAddIn.UI
{
    public partial class LabelBar : UserControl
    {
        public string LabelText
        {
            get { return lblText.Text; }
            set { lblText.Text = value; }
        }

        public LabelBar()
        {
            InitializeComponent();
        }
    }
}
