﻿using System;
using System.Windows.Forms;
using DevExpress.XtraSplashScreen;

namespace ExcelAddIn.UI
{
    public class LongRunningOperation :IDisposable
    {
        private readonly bool   _isActive;

        public LongRunningOperation()
        {
            if (SplashScreenManager.Default != null)
                return;
            
            _isActive       = true;
            Cursor.Current  = Cursors.WaitCursor;
            SplashScreenManager.ShowForm(typeof(DWWaitForm), false, false);
        }

        public void Dispose()
        {
            if (!_isActive) return;

            Cursor.Current  = Cursors.Arrow;
            SplashScreenManager.CloseForm(false);
        }
    }
}
