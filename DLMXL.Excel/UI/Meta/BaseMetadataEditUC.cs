﻿using System;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ExcelAddIn.UI.Meta
{
    public class BaseMetadataEditUC :  UserControl
    {
        public virtual void LoadContent(string content, bool isHtml) => throw new NotImplementedException();
        public virtual Task<string> GetContent() => throw new NotImplementedException();
    }
}