﻿using System;
using System.Diagnostics;
using System.IO;
using System.Reflection;
using System.Threading.Tasks;
using System.Windows.Forms;
using Microsoft.Web.WebView2.Core;
using Newtonsoft.Json;

namespace ExcelAddIn.UI.Meta
{
    public partial class HtmlEditor : UserControl
    {
        public event EventHandler TextChanged;
        private bool _isEditorReady;
        private string _editorContent;

        public HtmlEditor()
        {
            InitializeComponent();

            this.Load += HtmlEditor_Load;
        }

        private async void HtmlEditor_Load(object sender, EventArgs e)
        {
            var env = await CoreWebView2Environment.CreateAsync(userDataFolder: Path.GetTempPath());
            await webView1.EnsureCoreWebView2Async(env);

            webView1.NavigationCompleted += NavigationCompleted;
            webView1.WebMessageReceived += WebMessageReceived;
            webView1.CoreWebView2.AddWebResourceRequestedFilter("http://tinymce/*", CoreWebView2WebResourceContext.All);
            webView1.CoreWebView2.WebResourceRequested += CoreWebView2_WebResourceRequested;
            webView1.Source = new Uri("http://tinymce/index.html");
        }

        private void CoreWebView2_WebResourceRequested(object sender, CoreWebView2WebResourceRequestedEventArgs e)
        {
            Debug.WriteLine(e.Request.Uri);

            var stream = Assembly
                .GetExecutingAssembly()
                .GetManifestResourceStream("ExcelAddIn.tinymce." + e.Request.Uri.Substring(15).Replace('/', '.'));
            
            e.Response = webView1.CoreWebView2.Environment.CreateWebResourceResponse(stream, 200, "OK", null);
        }

        private void WebMessageReceived(object sender, CoreWebView2WebMessageReceivedEventArgs e)
        {
            TextChanged?.Invoke(null, null);
        }

        private void NavigationCompleted(object sender, CoreWebView2NavigationCompletedEventArgs e)
        {
            _isEditorReady = true;

            if (!string.IsNullOrEmpty(_editorContent))
            {
                SetText(_editorContent);
            }
        }

        public async Task<string> GetText()
        {
            return _isEditorReady 
                ? JsonConvert.DeserializeObject<string>(await webView1.ExecuteScriptAsync("GetContent()")) 
                : _editorContent;
        }

        public void SetText(string text)
        {
            if (_isEditorReady)
            {
                webView1.ExecuteScriptAsync("SetContent(" + JsonConvert.SerializeObject(text) + ")");
            }
            else
            {
                _editorContent = text;
            }
        }
        
    }
}
