﻿using System.Collections.Generic;
using System.IO;
using System.Linq;
using DLMXL.Models;
using DLMXL.Models.Interfaces;
using ExcelAddIn.Domain.Wizard;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace ExcelAddIn.UI.Meta
{
    public static class MetaUtils
    {
        public static string ToJson(IEnumerable<KeyValuePair<string, string>> keys)
        {
            using(var strWriter = new StringWriter())
            using (var writer = new JsonTextWriter(strWriter){QuoteName = false})
            {
                foreach(var key in keys)
                {
                    writer.WritePropertyName(key.Key);
                    writer.WriteValue(key.Value);
                }

                return strWriter.ToString();
            }
        }

        public static IEnumerable<KeyValuePair<string, string>> Parse(string content, string[] locales)
        {
            var json = ParseMetaAsJson(content);
            var values = locales.ToDictionary(s => s, s => string.Empty);

            if (json != null)
            {
                foreach (var p in json.Properties())
                {
                    if (values.ContainsKey(p.Name))
                    {
                        values[p.Name] = p.Value.Value<string>();
                    }
                }
            }
            else
            {
                values[locales[0]] = content;
            }

            return values;
        }

        private static JObject ParseMetaAsJson(string content)
        {
            if (string.IsNullOrEmpty(content))
            {
                return null;
            }

            try
            {
                return JObject.Parse("{" + content + "}");
            }
            catch
            {
                return null;
            }
        }

        public static bool IsCellReferencingMetadata(
            DataConnection connection,
            Microsoft.Office.Interop.Excel.Range range, 
            out IMetadataAttribute attribute)
        {
            attribute = null;

            if (connection == null || !connection.IsMetadata || range.Rows.Count > 1 || range.Columns.Count > 1)
                return false;

            if (range.Row <= RangeAddressToRowIndex(connection.StartCell))
                return false;

            var ds   = connection.Filter.SWFilter.Dataset;
            var coef = connection.LabelOutput == (LabelOutput.Code | LabelOutput.Label) ? 2 : 1;
            var nonTimeDimColumns = ds.Dimensions.Count(x => !x.IsTimeSeries) * coef;
            var timeColumns = ds.HasATimeDimension ? 1 : 0;
            var systemColumns = connection.IsActionColumn ? 2 : 1; // SID + Action
            var index = range.Column - RangeAddressToColumnIndex(connection.StartCell) - systemColumns - nonTimeDimColumns - timeColumns;

            attribute = index >= 0 && index < ds.Msd.MetadataAttributes.Count 
                            ? ds.Msd.MetadataAttributes[index] 
                            : null;

            return attribute!=null;
        }

        public static int RangeAddressToColumnIndex(string range)
        {
            int sum = 0;

            foreach (var c in range)
            {
                if (char.IsDigit(c))
                    break;

                sum *= 26;
                sum += (c - 'A' + 1);
            }

            return sum;
        }

        public static int RangeAddressToRowIndex(string range)
        {
            return int.Parse(new string(range.Where(char.IsDigit).ToArray()));
        }

    }
}