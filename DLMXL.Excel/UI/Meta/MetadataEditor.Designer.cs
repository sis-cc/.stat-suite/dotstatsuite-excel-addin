﻿
namespace ExcelAddIn.UI
{
    partial class MetadataEditor
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.topPnl = new System.Windows.Forms.Panel();
            this.btmPnl = new System.Windows.Forms.Panel();
            this.btnSave = new ExcelAddIn.UI.Controls.MainButton();
            this.middlePnl = new System.Windows.Forms.Panel();
            this.multilingualEdit = new ExcelAddIn.UI.Meta.MultilingualEdit();
            this.btmPnl.SuspendLayout();
            this.middlePnl.SuspendLayout();
            this.SuspendLayout();
            // 
            // topPnl
            // 
            this.topPnl.Dock = System.Windows.Forms.DockStyle.Top;
            this.topPnl.Location = new System.Drawing.Point(0, 0);
            this.topPnl.Name = "topPnl";
            this.topPnl.Size = new System.Drawing.Size(555, 19);
            this.topPnl.TabIndex = 1;
            // 
            // btmPnl
            // 
            this.btmPnl.Controls.Add(this.btnSave);
            this.btmPnl.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.btmPnl.Location = new System.Drawing.Point(0, 427);
            this.btmPnl.Name = "btmPnl";
            this.btmPnl.Size = new System.Drawing.Size(555, 28);
            this.btmPnl.TabIndex = 2;
            // 
            // btnSave
            // 
            this.btnSave.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnSave.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.btnSave.Appearance.Options.UseFont = true;
            this.btnSave.Enabled = false;
            this.btnSave.Location = new System.Drawing.Point(431, 4);
            this.btnSave.LookAndFeel.SkinName = "Office 2013";
            this.btnSave.LookAndFeel.UseDefaultLookAndFeel = false;
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(113, 20);
            this.btnSave.TabIndex = 4;
            this.btnSave.Text = "Update table cell";
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
            // 
            // middlePnl
            // 
            this.middlePnl.BackColor = System.Drawing.SystemColors.Control;
            this.middlePnl.Controls.Add(this.multilingualEdit);
            this.middlePnl.Dock = System.Windows.Forms.DockStyle.Fill;
            this.middlePnl.Location = new System.Drawing.Point(0, 19);
            this.middlePnl.Name = "middlePnl";
            this.middlePnl.Size = new System.Drawing.Size(555, 408);
            this.middlePnl.TabIndex = 3;
            // 
            // multilingualEdit
            // 
            this.multilingualEdit.AutoScroll = true;
            this.multilingualEdit.AutoSize = true;
            this.multilingualEdit.Dock = System.Windows.Forms.DockStyle.Fill;
            this.multilingualEdit.Location = new System.Drawing.Point(0, 0);
            this.multilingualEdit.Name = "multilingualEdit";
            this.multilingualEdit.Size = new System.Drawing.Size(555, 408);
            this.multilingualEdit.TabIndex = 0;
            // 
            // MetadataEditor
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.middlePnl);
            this.Controls.Add(this.btmPnl);
            this.Controls.Add(this.topPnl);
            this.Name = "MetadataEditor";
            this.Size = new System.Drawing.Size(555, 455);
            this.btmPnl.ResumeLayout(false);
            this.middlePnl.ResumeLayout(false);
            this.middlePnl.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel topPnl;
        private System.Windows.Forms.Panel btmPnl;
        private System.Windows.Forms.Panel middlePnl;
        private Controls.MainButton btnSave;
        private Meta.MultilingualEdit multilingualEdit;
    }
}
