﻿using System;
using System.Threading.Tasks;
using System.Windows.Forms;
using DLMXL.Models.Interfaces;
using ExcelAddIn.Forms;

namespace ExcelAddIn.UI
{
    public partial class MetadataEditor : UserControl
    {
        private Microsoft.Office.Interop.Excel.Range Range;

        public MetadataEditor()
        {
            InitializeComponent();
            multilingualEdit.ContentChanged += MultilingualEdit_ContentChanged;
            ActivateSaveButton(false);
        }

        private async void MultilingualEdit_ContentChanged(object sender, EventArgs e)
        {
            ActivateSaveButton(await multilingualEdit.HasChanges());
        }

        private void ActivateSaveButton(bool active)
        {
            this.btnSave.Enabled = active;
            this.btnSave.LookAndFeel.SkinName = active ? "DataWizard2" : "Office 2013";
        }

        public async Task ShowEditorForRange(IMetadataAttribute metadataAttribute, Microsoft.Office.Interop.Excel.Range range)
        {
            await EnsureSaved();

            Range = range;

            multilingualEdit.LoadContent(
                range.Value2 != null ? range.Value2.ToString() : null, 
                metadataAttribute.IsHtml
            );

            this.Visible = true;
        }

        public async Task EnsureSaved()
        {
            if (this.Visible && await multilingualEdit.HasChanges() && YesNoDialog.Show("Would you like to save your changes ?"))
            {
                await Save();
            }
        }

        private async Task Save()
        {
            Range.Value2 = await multilingualEdit.GetContent();
            ActivateSaveButton(false);
        }

        private async void btnSave_Click(object sender, EventArgs e)
        {
            await Save();
        }
    }
}
