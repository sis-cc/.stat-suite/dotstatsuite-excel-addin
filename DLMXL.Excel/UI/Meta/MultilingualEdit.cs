﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ExcelAddIn.UI.Meta
{
    public partial class MultilingualEdit : BaseMetadataEditUC
    {
        public event EventHandler ContentChanged;
        private RowStyle _minimizedRowStyle = new RowStyle(SizeType.Absolute, 35);
        private RowStyle _normalRowStyle = new RowStyle(SizeType.Percent, 0);
        private int _lastIndex = -1;
        private bool _isInited = false;

        public MultilingualEdit()
        {
            InitializeComponent();
            table.RowStyles.Clear();
        }


        public override void LoadContent(string content, bool isHtml)
        {
            table.SuspendLayout();

            var index = 0;

            foreach (var kv in MetaUtils.Parse(content, Config.DW.Languages.Select(x=>x.Key).ToArray()))
            {
                if (_isInited)
                {
                    ((MultilingualItem) this.table.Controls[kv.Key]).LoadContent(kv, isHtml);
                }
                else
                {
                    var item = new MultilingualItem(kv, isHtml, index++)
                    {
                        Dock = DockStyle.Fill,
                        Name = kv.Key
                    };

                    item.Toggle += Item_Toggle;
                    item.ContentChanged += Item_ContentChanged;

                    this.AddRow(item);
                }
            }

            if (!_isInited)
            {
                _normalRowStyle.Height = 100f / this.table.Controls.Count;

                foreach (RowStyle row in table.RowStyles)
                    row.Height = _normalRowStyle.Height;

                _isInited = true;
            }

            table.ResumeLayout();
        }

        private void Item_ContentChanged(object sender, EventArgs e)
        {
            ContentChanged?.Invoke(null, null);
        }

        private void Item_Toggle(object sender, int index)
        {
            var maximize = _lastIndex != index;
            _lastIndex = maximize ? index : -1;

            for (var i = 0; i < this.table.RowStyles.Count; i++)
            {
                var rowStyle = i != index && maximize 
                    ? _minimizedRowStyle 
                    : _normalRowStyle;

                ((MultilingualItem) this.table.Controls[i]).ToggleState(maximize ? i == index : (bool?) null);
                
                this.table.RowStyles[i].Height = rowStyle.Height;
                this.table.RowStyles[i].SizeType = rowStyle.SizeType;
            }
        }

        private void AddRow(MultilingualItem item)
        {
           table.RowCount = table.Controls.Count + 1;
           table.Controls.Add(item, 0, table.Controls.Count);
           table.RowStyles.Add(new RowStyle(SizeType.Percent, 25));
           
        }

        public override async Task<string> GetContent()
        {
            return MetaUtils.ToJson(await this.GetAllLanguages());
        }

        private async Task<IEnumerable<KeyValuePair<string, string>>> GetAllLanguages()
        {
            var result = new List<KeyValuePair<string, string>>();
            foreach(var multilingualItem in GetItems()) result.Add(await multilingualItem.Save());
            return result;
        }

        private IEnumerable<MultilingualItem> GetItems()
        {
            for (var i = 0; i < this.table.Controls.Count; i++)
            {
                yield return (MultilingualItem) this.table.Controls[i];
            }
        }

        public async Task<bool> HasChanges()
        {
            if (_isInited)
            {
                foreach (var item in GetItems())
                {
                    if (await item.HasChanges())
                        return true;
                }
            }

            return false;
        }
    }
}