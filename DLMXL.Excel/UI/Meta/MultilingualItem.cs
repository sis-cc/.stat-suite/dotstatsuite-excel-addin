﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ExcelAddIn.UI.Meta
{
    public sealed partial class MultilingualItem : UserControl
    {
        public delegate void EventHandler<T>(object sender, T e);
        public event EventHandler<int> Toggle;
        public event EventHandler ContentChanged;

        private bool _isHtml;
        private string _lang;
        private string _initialContent;
        private int _index;

        public MultilingualItem(KeyValuePair<string, string> content, bool isHtml, int index)
        {
            InitializeComponent();

            _index = index;

            LoadContent(content, isHtml);

            this.textBox.TextChanged += ContentChangedHandler;
            this.htmlEditor.TextChanged += ContentChangedHandler;
        }

        private void ContentChangedHandler(object sender, EventArgs e)
        {
            ContentChanged?.Invoke(this, null);
        }

        public void LoadContent(KeyValuePair<string, string> content, bool isHtml)
        {
            this._lang = content.Key;
            this._initialContent = content.Value ?? "";
            this.lblLanguage.Text = System.Globalization.CultureInfo.GetCultureInfo(content.Key).EnglishName;

            this._isHtml = isHtml;
            this.htmlEditor.Visible = isHtml;
            this.textBox.Visible = !isHtml;

            if (isHtml)
            {
                this.htmlEditor.SetText(this._initialContent);
            }
            else
            {
                this.textBox.Text = this._initialContent;
            }
        }

        public async Task<bool> HasChanges() => !string.Equals(this._initialContent, await this.GetInput());

        private async Task<string> GetInput() => this._isHtml ? await this.htmlEditor.GetText() : this.textBox.Text;

        public async Task<KeyValuePair<string, string>> Save() => new KeyValuePair<string, string>(
            this._lang,
            this._initialContent = await this.GetInput()
        );

        private void Fullscreen_Click(object sender, EventArgs e)
        {
            Toggle?.Invoke(this, this._index);
        }

        public void ToggleState(bool? state)
        {
            this.lblLanguage.ForeColor = state == false ? Color.DarkGray : Color.Black;
            this.btnFullScreen.ImageIndex = state == true ? 1 : 0;
        }
    }
}