﻿using System;
using System.Collections;
using System.Windows.Forms;
using DevExpress.XtraTreeList.Columns;
using DevExpress.XtraTreeList.Data;
using DevExpress.XtraTreeList.Nodes;
using DLMXL.Models;

namespace ExcelAddIn.UI
{
    public class SWTreeListDataInfoCollection : DataColumnInfoCollection
    {
        private readonly SWTreeList _DisplayTree;

        public SWTreeListDataInfoCollection(TreeListData data, SWTreeList displayTree): base(data)
        {
            _DisplayTree = displayTree;
         
            //below is needed to prevent a bootstrapping problem in devexpress treelist
            if (_DisplayTree.TreeData == null)
                _DisplayTree.TreeData = data;
        }

        protected override void AddColumnDefinitions(IList dataSource)
        {

            foreach (TreeListColumn col in _DisplayTree.Columns)
            {
                List.Add(new DataColumnInfo(col));
            }
        }
    }

    public class SWTreeListDataHelper : TreeListDataHelper
    {
        public readonly SWTreeList Tree;

        public SWTreeListDataHelper(SWTreeList tree)
            : base(tree)
        {
            Tree = tree;
        }
    }

    public abstract class ReadOnlyTreeListData: TreeListData {
        protected readonly SWTreeList DisplayTree;
        private DataColumnInfoCollection _Columns;

        protected ReadOnlyTreeListData( SWTreeList tl ) : base( new SWTreeListDataHelper( tl ) ) {
            DisplayTree = tl;
        }

        public override bool IsUnboundMode {
            get {
                return true;
            }
        }

        public override bool CanAdd {
            get {
                return false;
            }
        }

        public override bool CanClear {
            get {
                return false;
            }
        }

        public override bool CanRemove( TreeListNode node ) {
            return false;
        }

        protected override void ClearCore( ) {
            throw new InvalidOperationException( "read-only" );
        }

        protected override void RemoveFromSource( int nodeId ) {
            throw new InvalidOperationException( "read-only" );
        }

        protected override void OnRemoveNode( TreeListNode node ) {
            //noop
        }

        protected override int AppendCore( ) {
            throw new InvalidOperationException( "read-only" );
        }

        protected override bool CanSetValue( object columnId ) {
            return false;
        }

        public override bool CanPopulate( DataColumnInfo info ) {
            return true;
        }

        protected override DataColumnInfoCollection CreateColumns( ) {
            return _Columns ?? ( _Columns = new SWTreeListDataInfoCollection( this, (( SWTreeListDataHelper )DataHelper).Tree ) );
        }

        public override object GetDataRow( int nodeId ) {
            TreeListNode node = DisplayTree.FindNodeByID( nodeId );
            return node.Tag;
        }

        public override object GetValues( int nodeId ) {
            TreeListNode node = DisplayTree.FindNodeByID( nodeId );
            var no = ( SWBase )node.Tag;
            var res = new string[ DisplayTree.Columns.Count ];
            if( no == null ) {
                return res;
            }
            for( int ii = 0; ii < res.Length; ii++ ) {
                res[ ii ] = ( string )GetValue( nodeId, ii );
            }
            return res;
        }

        protected override bool SetValueCore( int nodeId, object columnId, object value ) {
            throw new InvalidOperationException( "read-only" );
        }

        protected override void SetValuesCore( int nodeId, object[ ] values ) {
            throw new InvalidOperationException( "read-only" );
        }

        public override bool CurrencyManagerWasReset( CurrencyManager manager, ItemChangedEventArgs e ) {
            return true;
        }

        public override bool IsValidColumnHandle( int columnHandle ) {
            return true;
        }

        public override void OnColumnTypeChanged( TreeListColumn column ) {
            //noop;
        }

    }
}
