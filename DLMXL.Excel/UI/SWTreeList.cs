using System;
using System.Collections.Generic;
using System.Drawing;
using System.Windows.Forms;
using DevExpress.Skins;
using DevExpress.XtraGrid;
using DevExpress.XtraTreeList;
using DevExpress.XtraTreeList.Columns;
using DevExpress.XtraTreeList.Data;
using DevExpress.XtraTreeList.Nodes;
using DLMXL.Models;

namespace ExcelAddIn.UI {
    public abstract class SWTreeList: TreeList
    {
        private TreeListHitInfo _HitInfo;

        protected bool InitComplete;
        protected int ProgrammaticChange;
        private readonly ImageList _ImageList;

        public override bool IsDesignMode {
            get {
                return base.IsDesignMode || this.IsReallyDesignMode( );
            }
        }

        protected SWTreeList() : base(  ) {

            _ImageList = new ImageList( );
            SelectImageList = _ImageList;

            OptionsBehavior.AllowQuickHideColumns = false;
            OptionsBehavior.DragNodes = false;
            OptionsBehavior.AllowIndeterminateCheckState = false;
            OptionsBehavior.AllowRecursiveNodeChecking = false;
            OptionsBehavior.AllowIncrementalSearch = true;
            OptionsBehavior.Editable = false;
            OptionsBehavior.ResizeNodes = false;
            OptionsBehavior.ExpandNodesOnIncrementalSearch = true;

            OptionsSelection.MultiSelect = false;

            OptionsView.ShowCheckBoxes = false;
            OptionsView.AutoWidth = true;
            OptionsView.ShowAutoFilterRow = false;
            OptionsView.ShowHorzLines = false;
            OptionsView.ShowColumns = false;
            OptionsView.ShowVertLines = false;
            OptionsView.ShowButtons = true;
            OptionsView.ShowIndicator = false;

            OptionsLayout.AddNewColumns = false;
            OptionsLayout.RemoveOldColumns = false;
            OptionsLayout.StoreAppearance = false;

            TreeLineStyle = LineStyle.None;
            Appearance.FocusedRow.BackColor = Color.FromArgb(20, 0, 100, 75);
            Appearance.FocusedRow.Options.UseBackColor = true;
            Appearance.FocusedRow.Options.UseBorderColor = true;

            Appearance.HideSelectionRow.BackColor = Color.FromArgb(25, 0, 100, 75);
            Appearance.HideSelectionRow.Options.UseBackColor = true;
            Appearance.HideSelectionRow.Options.UseBorderColor = true;
            
            InitComplete = true;
            ProgrammaticChange = 0;
        }

       protected override void OnStyleChanged( EventArgs e ) {
            var skin = GridSkins.GetSkin( LookAndFeel );
            skin.Properties[ GridSkins.OptShowTreeLine ] = true;
            base.OnStyleChanged( e );
        }

        protected override void InternalColumnChanged( TreeListColumn column ) {
            if( !InitComplete ) {
                return;
            }
            base.InternalColumnChanged( column );
        }

        protected virtual void ApplyFilterCore( TreeListNodes nl, params object[ ] searchCriteria ) {
        }

        public override int SetFocusedNode( TreeListNode node ) {
            var res = base.SetFocusedNode( node );
            if ( node == null ) {
                return res;
            }
            var pn = node.ParentNode;
            while ( pn != null && !pn.Expanded ) {
                pn.Expanded = true;
                pn = pn.ParentNode;
            }
            MakeNodeVisible( node );
            if( TopVisibleNode == node ) {
                TopVisibleNodeIndex--;
            }
            return res;
        }
        


        public static void ApplyFilterCoreImpl( TreeList tree, TreeListNodes nl, params object[ ] searchCriteria ) {
            try {
                tree.BeginUpdate( );
                var text = ( string )searchCriteria[ 0 ];
                if ( string.IsNullOrEmpty( text ) ) {
                    return;
                }
                foreach ( TreeListNode sn in nl ) {
                    var match = false;
                    // ReSharper disable once LoopCanBeConvertedToQuery
                    for ( var ii = 0; ii < tree.Columns.Count; ii++ ) {
                        var col = tree.Columns[ ii ];
                        if ( col.FilterMode == ColumnFilterMode.DisplayText ) {
                            var val = sn.GetDisplayText( col );
                            if ( val != null && val.IndexOf( text, StringComparison.InvariantCultureIgnoreCase ) >= 0 ) {
                                match = true;
                                break;
                            }
                        }
                    }
                    sn.Visible = match;
                    if ( match ) {
                        var pn = sn.ParentNode;
                        while ( pn != null ) {
                            pn.Visible = true;
                            pn.Expanded = true;
                            pn = pn.ParentNode;
                        }
                    } else {
                        ApplyFilterCoreImpl( tree, sn.Nodes, text );
                    }
                }
            } finally {
                tree.EndUpdate( );
            }
        }

        protected virtual void ClearFilterCore( TreeListNodes nl ) {
            try {
                BeginUpdate( );
            	LoadNodes();
            } finally {
                EndUpdate( );
            }
        }

        public void LoadNodes( ) {
            DoDataSourceChanged( );
        }

        /// <summary>
        /// Hides all nodes that does not start with the given text
        /// </summary>
        /// <param name="searchCriteria">The criteria for the search</param>
        public void ApplyFilter( params object[] searchCriteria ) {
            try {
                BeginUpdate( );
                ClearFilter( );
                ApplyFilterCore( Nodes, searchCriteria );
            } finally {
                EndUpdate(  );
            }
        }

        public void ClearFilter( ) {
            try {
                BeginUpdate( );
                var sn = FocusedNode;
                ClearFilterCore( Nodes );
                SetFocusedNode( sn );
            } finally {
                EndUpdate( );
            }
        }

        internal TreeListData TreeData {
            get {
                return Data;
            }
            set {
                Data = value;
            }
        }

        public event EventHandler OnObjectSelected;

        public delegate void NodeEventHandler(object sender, TreeListNode e);

        public event NodeEventHandler OnNodeClicked;

        protected override void OnClick(EventArgs e)
        {
            base.OnClick(e);
            var info = CalcHitInfo(PointToClient(MousePosition));
            var node = info.Node;

            if (node == null)
                return;

            OnNodeClicked?.Invoke(this, node);
        }




        protected override void RaiseHiddenEditor( ) {
            base.RaiseHiddenEditor( );
            OptionsBehavior.Editable = false;
        }

        public void ForEach( Action<TreeListNode> action ) {
            ForEach( Nodes, action );
        }

        private static void ForEach( IEnumerable<TreeListNode> nodes, Action<TreeListNode> action ) {
            foreach ( var node in nodes ) {
                action( node );
                ForEach( node.Nodes, action );
            }
        }

        private void InitializeComponent()
        {
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            this.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();
            this.ResumeLayout(false);

        }

    }
}