﻿namespace ExcelAddIn.UI
{
    partial class SelectDatasetQuery
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.SearchText = new IncrementalSearchTextBox();
            this.pnlTree = new ExcelAddIn.UI.CustomFlyoutPanel();
            this.lblDummyFocus = new DevExpress.XtraEditors.LabelControl();
            ((System.ComponentModel.ISupportInitialize)(this.SearchText.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pnlTree)).BeginInit();
            this.SuspendLayout();
            // 
            // SearchText
            // 
            this.SearchText.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.SearchText.Location = new System.Drawing.Point(0, 17);
            this.SearchText.Name = "SearchText";
            this.SearchText.SearchTextHint = "-- Type here to search a dataset or a query --";
            this.SearchText.Size = new System.Drawing.Size(483, 20);
            this.SearchText.TabIndex = 14;
            this.SearchText.SearchTextChanged += new System.EventHandler(this.SearchText_SearchTextChanged);
            this.SearchText.Click += new System.EventHandler(this.SearchText_Click);
            // 
            // pnlTree
            // 
            this.pnlTree.Location = new System.Drawing.Point(0, 40);
            this.pnlTree.Name = "pnlTree";
            this.pnlTree.Options.AnchorType = DevExpress.Utils.Win.PopupToolWindowAnchor.TopLeft;
            this.pnlTree.Options.CloseOnOuterClick = true;
            this.pnlTree.Options.VertIndent = 20;
            this.pnlTree.OwnerControl = this.SearchText;
            this.pnlTree.Size = new System.Drawing.Size(483, 300);
            this.pnlTree.TabIndex = 15;
            // 
            // lblDummyFocus
            // 
            this.lblDummyFocus.Location = new System.Drawing.Point(4, 0);
            this.lblDummyFocus.Name = "lblDummyFocus";
            this.lblDummyFocus.Size = new System.Drawing.Size(0, 13);
            this.lblDummyFocus.TabIndex = 16;
            // 
            // SelectDatasetQuery
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.lblDummyFocus);
            this.Controls.Add(this.pnlTree);
            this.Controls.Add(this.SearchText);
            this.Name = "SelectDatasetQuery";
            this.Size = new System.Drawing.Size(483, 60);
            ((System.ComponentModel.ISupportInitialize)(this.SearchText.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pnlTree)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private IncrementalSearchTextBox SearchText;
        private ExcelAddIn.UI.CustomFlyoutPanel pnlTree;
        private DevExpress.XtraEditors.LabelControl lblDummyFocus;
    }
}
