﻿using System;
using System.ComponentModel;
using System.Drawing;
using System.Windows.Forms;
using DevExpress.Utils;
using DevExpress.XtraTreeList;
using DevExpress.XtraTreeList.Nodes;
using DLMXL.Models;
using ExcelAddIn.DAL;
using ExcelAddIn.Domain;

namespace ExcelAddIn.UI
{
    [ToolboxItem(true)]
    public partial class SelectDatasetQuery : AddInBaseUserControl
    {
        public event EventHandler<EventArgs<IFavoriteTreeItem>> SelectionChanged;

        private FavoriteDisplayTree _displayTree;
        private bool _showFavoritesOnly;
        private int _ProgrammaticChange;

        public SelectDatasetQuery()
        {
            InitializeComponent();

            SetStyleController(InputStyleController,
                SearchText
            );

            this.SearchText.IsPopupDisabled = true;
            this.pnlTree.Hidden             += pnlTree_Hidden;
        }

        private void pnlTree_Hidden(object sender, FlyoutPanelEventArgs e)
        {
            SetSelectedSource(this.Selection);
        }

        private IOecdDataSource _source = null;

        public IOecdDataSource Source
        {
            get { return _source; }
            set
            {
                _source = value;

                if (_source != null)
                {
                    // Preload meta objects
                    _source.GetAll();
                    
                    _displayTree = new FavoriteDisplayTree(_source, 0)
                    {
                        Dock = DockStyle.Fill,
                    };

                    _displayTree.IsNodeVisible = IsNodeVisible;
                    _displayTree.OptionsSelection.MultiSelect = false;
                    _displayTree.OptionsView.ShowCheckBoxes = false;
                    _displayTree.FocusedNodeChanged += OnNodeSelected;
                    _displayTree.NodeCellStyle += displayTree_NodeCellStyle;

                    pnlTree.Controls.Clear();
                    pnlTree.Controls.Add(_displayTree);
                }
            }
        }

        public IFavoriteTreeItem Selection { get; private set; }

        private void ShowFilter()
        {
            try
            {
                _ProgrammaticChange++;

                pnlTree.Width = SearchText.Width;
                pnlTree.ShowPopup(true);
                
                UpdateTree();
                UpdateSelectionInTree(this.Selection);
            }
            finally
            {
                _ProgrammaticChange--;
            }
        }

        private void SearchText_SearchTextChanged(object sender, EventArgs e)
        {
            if (!pnlTree.IsPopupOpen)
                return;

            UpdateTree();
        }

        private void SearchText_Click(object sender, EventArgs e)
        {
            if (pnlTree.IsPopupOpen)
                return;

            ShowFilter();
        }

        private void OnNodeSelected(object sender, FocusedNodeChangedEventArgs e)
        {
            if (_ProgrammaticChange > 0)
                return;
            
            var selection = Source[e.Node.Id];

            if (selection == null || selection.Type == FavoriteTreeItemType.Database)
                return;

            Selection = selection;
            pnlTree.HidePopup(true);

            if (SelectionChanged != null)
                SelectionChanged(this, new EventArgs<IFavoriteTreeItem>(selection));
        }

        private void displayTree_NodeCellStyle(object sender, DevExpress.XtraTreeList.GetCustomNodeCellStyleEventArgs e)
        {
            var item = e.Node.Tag as IFavoriteTreeItem;

            if (item != null && item.Type == FavoriteTreeItemType.Database)
            {
                e.Appearance.ForeColor = Color.FromArgb(174,174,174);
            }
        }

        public void SetVisibility(bool showFavoritesOnly)
        {
            this._showFavoritesOnly = showFavoritesOnly;
        }

        private bool IsNodeVisible(TreeListNode node)
        {
            return !_showFavoritesOnly || Source[node.Id].IsFavorite;
        }

        private void UpdateTree()
        {
            _displayTree.UpdateListVisibility(SearchText.Text.Trim());
        }

        public void SetSelectedSource(SdmxFilter filter)
        {
            var selection = filter != null
                ? Source.FindTreeItemById(filter.SourceType, filter.SourceId)
                : null;

            SetSelectedSource(selection);
        }

        private void SetSelectedSource(IFavoriteTreeItem selection)
        {
            Selection = selection;
            this.SearchText.EditValue = null;
            this.ActiveControl = lblDummyFocus;

            this.SearchText.Properties.NullValuePrompt = selection == null
                ? "-- Type here to search a dataset or a query --  (minimum " + SearchText.MinimumSearchTextLength + " characters)"
                : "-- {0} --".F(selection.Name());
        }

        private void UpdateSelectionInTree(IFavoriteTreeItem selection)
        {
            this._displayTree.Selection.Clear();
            this._displayTree.SetFocusedNode(null);

            if (selection == null) return;

            var node = this._displayTree.FindNodeByID(selection.Index);

            if (node == null || !node.Visible) return;

            this._displayTree.Selection.Add(node);
            this._displayTree.SetFocusedNode(node);
        }

        public void UpdateLanguage()
        {
            this.SetSelectedSource(this.Selection);
        }

        /// <summary>
        /// to address an issue with IncrementalSearchTextBox overwrite of NullValuePrompt on OnCreateControl
        /// </summary>
        protected override void OnCreateControl()
        {
            base.OnCreateControl();

            SetSelectedSource(Selection);
        }
    }
}
