﻿using System.Windows.Forms;
using DevExpress.XtraEditors;
using DLMXL.Models;
using ExcelAddIn.Domain;
using Org.Sdmxsource.Sdmx.Util.Date;

namespace ExcelAddIn.UI
{
    public sealed partial class TimeDimensionFilter : BaseDimensionFilter
    {
        private SdmxFilter _filter;

        public TimeDimensionFilter()
        {
            InitializeComponent();

            SetStyleController(InputStyleController,
                txtFrom,
                txtTo
            );
        }

        public override bool IsValid => ValidFrom() && ValidTo();

        public TimeDimensionFilter(SdmxFilter filter, SWFilterDimension timeDim, int index) : this()
        {
            _filter             = filter;
            this.IsValid        = true;
            Dock                = DockStyle.Top;
            this.lblName.Text   = timeDim.Dimension.Code;
            this.txtFrom.Text   = filter.StartTime;
            this.txtTo.Text     = filter.EndTime;

            ApplyTimeConstraints(timeDim.Dimension.Constraint?.TimeRange);
        }

        private void ApplyTimeConstraints(TimeRange range)
        {
            if (range == null)
                return;

            if (range.StartDate != null)
            {
                this.txtFrom.Properties.NullValuePrompt = $"({range.StartDate.Date:yyyy})";
            }

            if (range.EndDate != null)
            {
                this.txtTo.Properties.NullValuePrompt = $"({range.EndDate.Date:yyyy})";
            }
        }

        private void txtFrom_Validating(object sender, System.ComponentModel.CancelEventArgs e)
        {
            if (!txtFrom.IsModified) 
                return;

            UpdateTime();
        }

        private void txtTo_Validating(object sender, System.ComponentModel.CancelEventArgs e)
        {
            if (!txtTo.IsModified)
                return;

            UpdateTime();
        }

        private void UpdateTime()
        {
            _filter.StartTime   = ValidFrom()   ? txtFrom.Value() : null;
            _filter.EndTime     = ValidTo()     ? txtTo.Value() : null;

            if (!string.IsNullOrEmpty(_filter.StartTime) || !string.IsNullOrEmpty(_filter.EndTime))
            {
                _filter.LastPeriods = null;
            }

            OnDimensionChanged(null);
        }

        private bool ValidFrom()
        {
            ClearErrors(txtFrom);

            return string.IsNullOrEmpty(txtFrom.Value()) || IsValid(txtFrom, Validate);
        }

        private bool ValidTo()
        {
            ClearErrors(txtTo);

            return string.IsNullOrEmpty(txtTo.Value()) || IsValid(txtTo, Validate);
        }

        private string Validate(TextEdit input)
        {
            var text = input.Value();

            try
            {
                DateUtil.GetTimeFormatOfDate(text);
            }
            catch
            {
                return "Invalid date format";
            }

            return null;
        }
    }
}
