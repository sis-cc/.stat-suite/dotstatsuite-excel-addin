﻿using System.Windows.Forms;
using ExcelAddIn.Domain.Wizard;

namespace ExcelAddIn.UI.Wizard
{
    public interface IWizardBase<T, R>
        where T : IWizardDto
        where R : UserControl, IWizardStep<T>
    {
        void Init(T dto, int step = 1);

        void AddStep(string name, R step);
    }

    public interface IWizardStep<T> where T : IWizardDto
    {
        bool IsValid { get; }

        void Init(T dto);

        void Save(T dto);

        string NextButtonText { get; }

        void AttachButtonPanel(Control parent);
    }
}
