﻿namespace ExcelAddIn.UI.Wizard
{
    partial class SelectDataStep
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(SelectDataStep));
            this.pnlFilters = new DevExpress.XtraEditors.PanelControl();
            this.txtSdmxQuery = new DevExpress.XtraEditors.MemoEdit();
            this.btnApply = new DevExpress.XtraEditors.SimpleButton();
            this.pnlWorkspace = new DevExpress.XtraEditors.PanelControl();
            this.btnProductionValues = new ExcelAddIn.UI.Controls.CheckButton();
            this.btnImportValues = new ExcelAddIn.UI.Controls.CheckButton();
            this.labelBar2 = new ExcelAddIn.UI.LabelBar();
            this.pnlDatasets = new DevExpress.XtraEditors.PanelControl();
            this.btnClearCache = new System.Windows.Forms.Button();
            this.UCselectDatasetQuery = new ExcelAddIn.UI.SelectDatasetQuery();
            this.btnFavorites = new System.Windows.Forms.Button();
            this.formImgList = new System.Windows.Forms.ImageList(this.components);
            this.lblDatasets = new DevExpress.XtraEditors.LabelControl();
            this.rgSelection = new DevExpress.XtraEditors.RadioGroup();
            this.labelBar1 = new ExcelAddIn.UI.LabelBar();
            this.btnEditFavorites = new DevExpress.XtraEditors.SimpleButton();
            this.panelControl1 = new DevExpress.XtraEditors.PanelControl();
            this.lblCurrentFilters = new DevExpress.XtraEditors.LabelControl();
            this.labelBar3 = new ExcelAddIn.UI.LabelBar();
            this.btnEditFilters = new DevExpress.XtraEditors.SimpleButton();
            this.pnlDynamic = new DevExpress.XtraEditors.PanelControl();
            this.dimensionFilter = new ExcelAddIn.UI.FreeTextDimensionFilter();
            this.pnlLanguages = new DevExpress.XtraEditors.PanelControl();
            this.ddlLanguages = new DevExpress.XtraEditors.ComboBoxEdit();
            this.labelBar4 = new ExcelAddIn.UI.LabelBar();
            this.lblWarning = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.pnlMain)).BeginInit();
            this.pnlMain.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pnlButtons)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pnlFilters)).BeginInit();
            this.pnlFilters.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtSdmxQuery.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pnlWorkspace)).BeginInit();
            this.pnlWorkspace.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pnlDatasets)).BeginInit();
            this.pnlDatasets.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.rgSelection.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).BeginInit();
            this.panelControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pnlDynamic)).BeginInit();
            this.pnlDynamic.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pnlLanguages)).BeginInit();
            this.pnlLanguages.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ddlLanguages.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // pnlMain
            // 
            this.pnlMain.Controls.Add(this.pnlDynamic);
            this.pnlMain.Controls.Add(this.panelControl1);
            this.pnlMain.Controls.Add(this.pnlFilters);
            this.pnlMain.Controls.Add(this.pnlWorkspace);
            this.pnlMain.Controls.Add(this.pnlDatasets);
            this.pnlMain.Controls.Add(this.pnlLanguages);
            this.pnlMain.Size = new System.Drawing.Size(703, 526);
            // 
            // pnlButtons
            // 
            this.pnlButtons.Location = new System.Drawing.Point(0, 526);
            this.pnlButtons.Size = new System.Drawing.Size(703, 35);
            // 
            // pnlFilters
            // 
            this.pnlFilters.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.pnlFilters.Controls.Add(this.lblWarning);
            this.pnlFilters.Controls.Add(this.txtSdmxQuery);
            this.pnlFilters.Controls.Add(this.btnApply);
            this.pnlFilters.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.pnlFilters.Location = new System.Drawing.Point(0, 407);
            this.pnlFilters.Name = "pnlFilters";
            this.pnlFilters.Size = new System.Drawing.Size(703, 119);
            this.pnlFilters.TabIndex = 11;
            // 
            // txtSdmxQuery
            // 
            this.txtSdmxQuery.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtSdmxQuery.Location = new System.Drawing.Point(15, 8);
            this.txtSdmxQuery.Name = "txtSdmxQuery";
            this.txtSdmxQuery.Properties.EditValueChangedDelay = 400;
            this.txtSdmxQuery.Properties.EditValueChangedFiringMode = DevExpress.XtraEditors.Controls.EditValueChangedFiringMode.Buffered;
            this.txtSdmxQuery.Properties.NullValuePrompt = "Paste SDMX Rest query selection here";
            this.txtSdmxQuery.Properties.NullValuePromptShowForEmptyValue = true;
            this.txtSdmxQuery.Size = new System.Drawing.Size(552, 65);
            this.txtSdmxQuery.TabIndex = 14;
            this.txtSdmxQuery.EditValueChanged += new System.EventHandler(this.txtSdmxQuery_TextChanged);
            // 
            // btnApply
            // 
            this.btnApply.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnApply.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.btnApply.Appearance.Options.UseFont = true;
            this.btnApply.Location = new System.Drawing.Point(587, 6);
            this.btnApply.Name = "btnApply";
            this.btnApply.Size = new System.Drawing.Size(101, 23);
            this.btnApply.TabIndex = 0;
            this.btnApply.Text = "APPLY";
            this.btnApply.Visible = false;
            this.btnApply.Click += new System.EventHandler(this.btnApply_Click);
            // 
            // pnlWorkspace
            // 
            this.pnlWorkspace.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.pnlWorkspace.Controls.Add(this.btnProductionValues);
            this.pnlWorkspace.Controls.Add(this.btnImportValues);
            this.pnlWorkspace.Controls.Add(this.labelBar2);
            this.pnlWorkspace.Dock = System.Windows.Forms.DockStyle.Top;
            this.pnlWorkspace.Location = new System.Drawing.Point(0, 182);
            this.pnlWorkspace.Name = "pnlWorkspace";
            this.pnlWorkspace.Size = new System.Drawing.Size(703, 65);
            this.pnlWorkspace.TabIndex = 10;
            // 
            // btnProductionValues
            // 
            this.btnProductionValues.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnProductionValues.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.btnProductionValues.Appearance.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(68)))), ((int)(((byte)(68)))), ((int)(((byte)(68)))));
            this.btnProductionValues.Appearance.Options.UseFont = true;
            this.btnProductionValues.Appearance.Options.UseForeColor = true;
            this.btnProductionValues.Checked = true;
            this.btnProductionValues.GroupIndex = 1;
            this.btnProductionValues.Location = new System.Drawing.Point(281, 31);
            this.btnProductionValues.Name = "btnProductionValues";
            this.btnProductionValues.Size = new System.Drawing.Size(200, 23);
            this.btnProductionValues.TabIndex = 6;
            this.btnProductionValues.Text = "PRODUCTION VALUES";
            // 
            // btnImportValues
            // 
            this.btnImportValues.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnImportValues.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.btnImportValues.Appearance.ForeColor = System.Drawing.Color.White;
            this.btnImportValues.Appearance.Options.UseFont = true;
            this.btnImportValues.Appearance.Options.UseForeColor = true;
            this.btnImportValues.GroupIndex = 1;
            this.btnImportValues.Location = new System.Drawing.Point(488, 31);
            this.btnImportValues.Name = "btnImportValues";
            this.btnImportValues.Size = new System.Drawing.Size(200, 23);
            this.btnImportValues.TabIndex = 5;
            this.btnImportValues.TabStop = false;
            this.btnImportValues.Text = "IMPORT VALUES";
            // 
            // labelBar2
            // 
            this.labelBar2.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.labelBar2.LabelText = "Workspace";
            this.labelBar2.Location = new System.Drawing.Point(15, 5);
            this.labelBar2.Name = "labelBar2";
            this.labelBar2.Size = new System.Drawing.Size(673, 20);
            this.labelBar2.TabIndex = 4;
            // 
            // pnlDatasets
            // 
            this.pnlDatasets.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.pnlDatasets.Controls.Add(this.btnClearCache);
            this.pnlDatasets.Controls.Add(this.UCselectDatasetQuery);
            this.pnlDatasets.Controls.Add(this.btnFavorites);
            this.pnlDatasets.Controls.Add(this.lblDatasets);
            this.pnlDatasets.Controls.Add(this.rgSelection);
            this.pnlDatasets.Controls.Add(this.labelBar1);
            this.pnlDatasets.Controls.Add(this.btnEditFavorites);
            this.pnlDatasets.Dock = System.Windows.Forms.DockStyle.Top;
            this.pnlDatasets.Location = new System.Drawing.Point(0, 65);
            this.pnlDatasets.Name = "pnlDatasets";
            this.pnlDatasets.Size = new System.Drawing.Size(703, 117);
            this.pnlDatasets.TabIndex = 9;
            // 
            // btnClearCache
            // 
            this.btnClearCache.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnClearCache.FlatAppearance.BorderSize = 0;
            this.btnClearCache.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnClearCache.Image = ((System.Drawing.Image)(resources.GetObject("btnClearCache.Image")));
            this.btnClearCache.Location = new System.Drawing.Point(625, 82);
            this.btnClearCache.Margin = new System.Windows.Forms.Padding(0);
            this.btnClearCache.Name = "btnClearCache";
            this.btnClearCache.Size = new System.Drawing.Size(24, 24);
            this.btnClearCache.TabIndex = 15;
            this.btnClearCache.UseVisualStyleBackColor = false;
            this.btnClearCache.Click += new System.EventHandler(this.btnClearCache_Click);
            // 
            // UCselectDatasetQuery
            // 
            this.UCselectDatasetQuery.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.UCselectDatasetQuery.Location = new System.Drawing.Point(15, 69);
            this.UCselectDatasetQuery.Name = "UCselectDatasetQuery";
            this.UCselectDatasetQuery.Size = new System.Drawing.Size(606, 41);
            this.UCselectDatasetQuery.Source = null;
            this.UCselectDatasetQuery.TabIndex = 14;
            // 
            // btnFavorites
            // 
            this.btnFavorites.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnFavorites.Enabled = false;
            this.btnFavorites.FlatAppearance.BorderSize = 0;
            this.btnFavorites.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnFavorites.ImageIndex = 0;
            this.btnFavorites.ImageList = this.formImgList;
            this.btnFavorites.Location = new System.Drawing.Point(664, 82);
            this.btnFavorites.Name = "btnFavorites";
            this.btnFavorites.Size = new System.Drawing.Size(24, 24);
            this.btnFavorites.TabIndex = 12;
            this.btnFavorites.UseVisualStyleBackColor = true;
            this.btnFavorites.Click += new System.EventHandler(this.btnFavorites_Click);
            // 
            // formImgList
            // 
            this.formImgList.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("formImgList.ImageStream")));
            this.formImgList.TransparentColor = System.Drawing.Color.Transparent;
            this.formImgList.Images.SetKeyName(0, "star_off.png");
            this.formImgList.Images.SetKeyName(1, "star_on.png");
            // 
            // lblDatasets
            // 
            this.lblDatasets.Appearance.Font = new System.Drawing.Font("Tahoma", 10F);
            this.lblDatasets.Location = new System.Drawing.Point(46, 33);
            this.lblDatasets.Name = "lblDatasets";
            this.lblDatasets.Size = new System.Drawing.Size(37, 16);
            this.lblDatasets.TabIndex = 11;
            this.lblDatasets.Text = "Show:";
            // 
            // rgSelection
            // 
            this.rgSelection.EditValue = 0;
            this.rgSelection.Location = new System.Drawing.Point(351, 24);
            this.rgSelection.Name = "rgSelection";
            this.rgSelection.Properties.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.rgSelection.Properties.Appearance.Options.UseBackColor = true;
            this.rgSelection.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.rgSelection.Properties.Columns = 2;
            this.rgSelection.Properties.Items.AddRange(new DevExpress.XtraEditors.Controls.RadioGroupItem[] {
            new DevExpress.XtraEditors.Controls.RadioGroupItem(1, "favorites"),
            new DevExpress.XtraEditors.Controls.RadioGroupItem(0, "all")});
            this.rgSelection.Size = new System.Drawing.Size(198, 24);
            this.rgSelection.TabIndex = 10;
            // 
            // labelBar1
            // 
            this.labelBar1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.labelBar1.LabelText = "Datasets and queries";
            this.labelBar1.Location = new System.Drawing.Point(15, 5);
            this.labelBar1.Name = "labelBar1";
            this.labelBar1.Size = new System.Drawing.Size(673, 20);
            this.labelBar1.TabIndex = 5;
            // 
            // btnEditFavorites
            // 
            this.btnEditFavorites.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnEditFavorites.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.btnEditFavorites.Appearance.Options.UseFont = true;
            this.btnEditFavorites.Location = new System.Drawing.Point(568, 26);
            this.btnEditFavorites.Name = "btnEditFavorites";
            this.btnEditFavorites.Size = new System.Drawing.Size(120, 23);
            this.btnEditFavorites.TabIndex = 2;
            this.btnEditFavorites.Text = "EDIT FAVORITES";
            this.btnEditFavorites.Click += new System.EventHandler(this.btnEditFavorites_Click);
            // 
            // panelControl1
            // 
            this.panelControl1.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.panelControl1.Controls.Add(this.lblCurrentFilters);
            this.panelControl1.Controls.Add(this.labelBar3);
            this.panelControl1.Controls.Add(this.btnEditFilters);
            this.panelControl1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelControl1.Location = new System.Drawing.Point(0, 247);
            this.panelControl1.Name = "panelControl1";
            this.panelControl1.Size = new System.Drawing.Size(703, 65);
            this.panelControl1.TabIndex = 12;
            // 
            // lblCurrentFilters
            // 
            this.lblCurrentFilters.Appearance.Font = new System.Drawing.Font("Tahoma", 10F);
            this.lblCurrentFilters.Location = new System.Drawing.Point(46, 37);
            this.lblCurrentFilters.Name = "lblCurrentFilters";
            this.lblCurrentFilters.Size = new System.Drawing.Size(84, 16);
            this.lblCurrentFilters.TabIndex = 15;
            this.lblCurrentFilters.Text = "Current filters:";
            // 
            // labelBar3
            // 
            this.labelBar3.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.labelBar3.LabelText = "Data filters";
            this.labelBar3.Location = new System.Drawing.Point(15, 9);
            this.labelBar3.Name = "labelBar3";
            this.labelBar3.Size = new System.Drawing.Size(673, 20);
            this.labelBar3.TabIndex = 14;
            // 
            // btnEditFilters
            // 
            this.btnEditFilters.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnEditFilters.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.btnEditFilters.Appearance.Options.UseFont = true;
            this.btnEditFilters.Location = new System.Drawing.Point(568, 32);
            this.btnEditFilters.Name = "btnEditFilters";
            this.btnEditFilters.Size = new System.Drawing.Size(120, 23);
            this.btnEditFilters.TabIndex = 13;
            this.btnEditFilters.Text = "EDIT FILTERS";
            this.btnEditFilters.Click += new System.EventHandler(this.btnEditFilters_Click);
            // 
            // pnlDynamic
            // 
            this.pnlDynamic.AutoSize = true;
            this.pnlDynamic.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.pnlDynamic.Controls.Add(this.dimensionFilter);
            this.pnlDynamic.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnlDynamic.Location = new System.Drawing.Point(0, 312);
            this.pnlDynamic.Name = "pnlDynamic";
            this.pnlDynamic.Padding = new System.Windows.Forms.Padding(45, 0, 15, 0);
            this.pnlDynamic.Size = new System.Drawing.Size(703, 95);
            this.pnlDynamic.TabIndex = 13;
            // 
            // dimensionFilter
            // 
            this.dimensionFilter.AutoScroll = true;
            this.dimensionFilter.AutoSize = true;
            this.dimensionFilter.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dimensionFilter.Location = new System.Drawing.Point(45, 0);
            this.dimensionFilter.Margin = new System.Windows.Forms.Padding(0);
            this.dimensionFilter.Name = "dimensionFilter";
            this.dimensionFilter.Size = new System.Drawing.Size(643, 95);
            this.dimensionFilter.TabIndex = 14;
            // 
            // pnlLanguages
            // 
            this.pnlLanguages.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.pnlLanguages.Controls.Add(this.ddlLanguages);
            this.pnlLanguages.Controls.Add(this.labelBar4);
            this.pnlLanguages.Dock = System.Windows.Forms.DockStyle.Top;
            this.pnlLanguages.Location = new System.Drawing.Point(0, 0);
            this.pnlLanguages.Name = "pnlLanguages";
            this.pnlLanguages.Size = new System.Drawing.Size(703, 65);
            this.pnlLanguages.TabIndex = 14;
            // 
            // ddlLanguages
            // 
            this.ddlLanguages.EditValue = "";
            this.ddlLanguages.Location = new System.Drawing.Point(15, 35);
            this.ddlLanguages.Name = "ddlLanguages";
            this.ddlLanguages.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.ddlLanguages.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            this.ddlLanguages.Size = new System.Drawing.Size(205, 20);
            this.ddlLanguages.TabIndex = 15;
            // 
            // labelBar4
            // 
            this.labelBar4.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.labelBar4.LabelText = "Content language";
            this.labelBar4.Location = new System.Drawing.Point(15, 9);
            this.labelBar4.Name = "labelBar4";
            this.labelBar4.Size = new System.Drawing.Size(673, 20);
            this.labelBar4.TabIndex = 14;
            // 
            // lblWarning
            // 
            this.lblWarning.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lblWarning.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(186)));
            this.lblWarning.ForeColor = System.Drawing.Color.Red;
            this.lblWarning.Location = new System.Drawing.Point(15, 76);
            this.lblWarning.Name = "lblWarning";
            this.lblWarning.Size = new System.Drawing.Size(677, 40);
            this.lblWarning.TabIndex = 15;
            this.lblWarning.Text = "Error: The dataset <> doesn\'t exist. Please update the selection.";
            this.lblWarning.Visible = false;
            // 
            // SelectDataStep
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Name = "SelectDataStep";
            this.Size = new System.Drawing.Size(703, 561);
            ((System.ComponentModel.ISupportInitialize)(this.pnlMain)).EndInit();
            this.pnlMain.ResumeLayout(false);
            this.pnlMain.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pnlButtons)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pnlFilters)).EndInit();
            this.pnlFilters.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.txtSdmxQuery.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pnlWorkspace)).EndInit();
            this.pnlWorkspace.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pnlDatasets)).EndInit();
            this.pnlDatasets.ResumeLayout(false);
            this.pnlDatasets.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.rgSelection.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).EndInit();
            this.panelControl1.ResumeLayout(false);
            this.panelControl1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pnlDynamic)).EndInit();
            this.pnlDynamic.ResumeLayout(false);
            this.pnlDynamic.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pnlLanguages)).EndInit();
            this.pnlLanguages.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.ddlLanguages.Properties)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private DevExpress.XtraEditors.PanelControl pnlFilters;
        private DevExpress.XtraEditors.SimpleButton btnApply;
        private DevExpress.XtraEditors.PanelControl pnlWorkspace;
        private LabelBar labelBar2;
        private DevExpress.XtraEditors.PanelControl pnlDatasets;
        private System.Windows.Forms.Button btnFavorites;
        private DevExpress.XtraEditors.LabelControl lblDatasets;
        private DevExpress.XtraEditors.RadioGroup rgSelection;
        private LabelBar labelBar1;
        private DevExpress.XtraEditors.SimpleButton btnEditFavorites;
        private System.Windows.Forms.ImageList formImgList;
        private ExcelAddIn.UI.Controls.CheckButton btnProductionValues;
        private ExcelAddIn.UI.Controls.CheckButton btnImportValues;
        private DevExpress.XtraEditors.MemoEdit txtSdmxQuery;
        private SelectDatasetQuery UCselectDatasetQuery;
        private DevExpress.XtraEditors.PanelControl pnlDynamic;
        private FreeTextDimensionFilter dimensionFilter;
        private DevExpress.XtraEditors.PanelControl panelControl1;
        private DevExpress.XtraEditors.LabelControl lblCurrentFilters;
        private LabelBar labelBar3;
        private DevExpress.XtraEditors.SimpleButton btnEditFilters;
		private System.Windows.Forms.Button btnClearCache;
        private DevExpress.XtraEditors.PanelControl pnlLanguages;
        private LabelBar labelBar4;
        private DevExpress.XtraEditors.ComboBoxEdit ddlLanguages;
        private System.Windows.Forms.Label lblWarning;
    }
}
