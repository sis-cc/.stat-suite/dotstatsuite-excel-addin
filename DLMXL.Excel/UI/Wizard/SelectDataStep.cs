﻿using System;
using System.Linq;
using DLMXL.DAL.Access;
using ExcelAddIn.Domain;
using ExcelAddIn.Domain.Wizard;
using ExcelAddIn.Forms;

namespace ExcelAddIn.UI.Wizard
{
    public partial class SelectDataStep : GetDataWizzardStep
    {
        private int _ProgrammaticChange;

        public SelectDataStep()
        {
            InitializeComponent();

            SetStyleController(InputStyleController,
                rgSelection,
                txtSdmxQuery
            );

            SetStyleController(LabelStyleController,
                lblDatasets,
                lblCurrentFilters
            );

            rgSelection.SelectedIndexChanged            += rgSelection_SelectedIndexChanged;
            dimensionFilter.FilterHasChanged            += FilterHasChanged;
            this.UCselectDatasetQuery.SelectionChanged  += SourceSelectionChanged;
            ddlLanguages.SelectedIndexChanged           += ContentLanguageChanged;
        }



        #region IWizardStep
        public override bool IsValid
        {
            get
            {
                ClearErrors();
                var ok      = IsValid(UCselectDatasetQuery, Dto.Filter?.SWFilter != null, "Datasource undefined");
                ok          &= dimensionFilter.IsValid;          
                return ok;
            }
        }

        public override void Init(DataConnection dto)
        {
            base.Init(dto);

            if (IsLoaded) return;

            BindContentLanguages();

            rgSelection.SelectedIndex           = 1;
            btnProductionValues.Checked         = dto.IsProduction;
            btnImportValues.Checked             = !dto.IsProduction;
            pnlWorkspace.Visible                = dto.Source.IsDataSpace;
            this.UCselectDatasetQuery.Source    = Dto.Source;

            if (!dto.IsNew() || dto.Resume)
            {
                UCselectDatasetQuery.SetSelectedSource(dto.Filter);
                ApplyNewFilter(dto.Filter);
            }

            IsLoaded = true;
        }



        public override void Save(DataConnection dto)
        {
            dto.IsProduction    = btnProductionValues.Checked;            
        }

        #endregion

        #region Methods

        private void BindContentLanguages()
        {
            var langs = Config.DW.Languages
                .Select(l => new DropdownItem<string>(l.Value, l.Key))
                .ToList();

            this.ddlLanguages.Properties.Items.Clear();
            this.ddlLanguages.Properties.Items.AddRange(langs);

            this.ddlLanguages.SelectedItem = langs.FirstOrDefault(x => x.Value == Config.Settings.Language);

            if (this.ddlLanguages.SelectedIndex < 0)
                this.ddlLanguages.SelectedIndex = 0;
        }

        private void DatasourceItemHasChanged(IFavoriteTreeItem item)
        {
            if (item == null) return;

            btnFavorites.Enabled    = true;
            btnFavorites.ImageIndex = item.IsFavorite ? 1 : 0;
            
            using(new LongRunningOperation())
                ApplyNewFilter(item.GetFilter());
        }

        private void ApplyNewFilter(SdmxFilter filter)
        {
            Dto.Filter = filter;

            var form    = this.ParentForm;
            var height  = 0;

            this.SuspendLayout();

            if(form!=null)
                height  = form.Height - dimensionFilter.Height;

            var size    = dimensionFilter.ApplyFilter(filter);

            UpdateSdmxQuery(filter);
            SetWarning(filter);

            if (form!=null)
                form.Height = height + (size * 28);

            this.ResumeLayout();
        }

        private bool IsValidSdmxQuery(string query)
        {
            return Dto.Source.BuildFilterFromSdmxQueryString(query) != null;
        }

        private void UpdateSdmxQuery(SdmxFilter filter)
        {
            if (filter == null)
                return;

            _ProgrammaticChange++;
            
            this.txtSdmxQuery.EditValue = filter.SWFilter == null
                ? filter.SdmxQuery
                : Dto.Source.GetSdmxQueryString(filter);
            
            _ProgrammaticChange--;
        }

        private void SetWarning(SdmxFilter filter)
        {
            if (filter != null && filter.SWFilter == null)
            {
                this.lblWarning.Text = $@"Error: The dataset <{filter.Dataset}> doesn't exist. Please update the selection.";
                this.lblWarning.Visible = true;
            }
            else
            {
                this.lblWarning.Visible = false;
            }
        }

        #endregion

        #region Event handlers

        private void ContentLanguageChanged(object sender, EventArgs e)
        {
            Config.Settings.Language = ddlLanguages.SelectedItem as DropdownItem<string>;
            UCselectDatasetQuery.UpdateLanguage();
        }

        private void btnClearCache_Click(object sender, EventArgs e)
        {
	        try
	        {
		        using (new LongRunningOperation())
		        {
			        BaseDataAccess.ClearCache(Dto.Source.Code);
			        Dto.Source.ClearCache();

			        this.IsLoaded = false;
			        this.Init(Dto);

			        this.txtSdmxQuery.Text = "";
			        UCselectDatasetQuery.SetSelectedSource(null);
			        ApplyNewFilter(null);
		        }
	        }
			catch {}
        }

        private void SourceSelectionChanged(object sender, EventArgs<IFavoriteTreeItem> e)
        {
            ClearErrors(UCselectDatasetQuery);
            
            this.Refresh();
            
            DatasourceItemHasChanged(e.Item);
        }

        private void FilterHasChanged(object sender, EventArgs eventArgs)
        {
            UpdateSdmxQuery(Dto.Filter);
        }

        private void rgSelection_SelectedIndexChanged(object sender, EventArgs e)
        {
            this.UCselectDatasetQuery.SetVisibility(rgSelection.SelectedIndex == 0);                       
        }

        private void btnEditFavorites_Click(object sender, EventArgs e)
        {
            using (var f = new EditFavorites($@"{Config.DW.Title} › {Dto.Source.Name} › Edit favorites", Dto.Source, formImgList))
            {
                f.ShowDialog();
            }
        }

        private void btnFavorites_Click(object sender, EventArgs e)
        {
            var currentTreeSelection = this.UCselectDatasetQuery.Selection;

            if (currentTreeSelection != null)
            {
                currentTreeSelection.IsFavorite     = !currentTreeSelection.IsFavorite;
                btnFavorites.ImageIndex             = currentTreeSelection.IsFavorite ? 1 : 0;

                if (currentTreeSelection.Parent!=null 
                    && currentTreeSelection.Type == FavoriteTreeItemType.Dataset 
                    && !currentTreeSelection.IsFavorite 
                    && currentTreeSelection.Parent.IsFavorite
                )
                    currentTreeSelection.Parent.IsFavorite = false;
            }
        }

        private void btnEditFilters_Click(object sender, EventArgs e)
        {
            if (Dto.Filter?.SWFilter == null)
	            return;

            var filter = DWFilterDialog.ShowFilterDialog(
                    $@"{Config.DW.Title} › {Dto.Source.Name} › Edit filters",
                    Dto.Filter,
                    Dto.Source.IsDataSpace
            );

            if (filter == null)
	            return;

            ApplyNewFilter(filter);
        }

        private void txtSdmxQuery_TextChanged(object sender, EventArgs e)
        {
            if (_ProgrammaticChange > 0)
                return;

            using (new LongRunningOperation())
            {
                var query = txtSdmxQuery.Text.Trim();
                
                this.btnApply.Visible = !string.IsNullOrEmpty(query) && IsValidSdmxQuery(query);
            }
        }

        private void btnApply_Click(object sender, EventArgs e)
        {
            using (new LongRunningOperation())
            {
                var query   = txtSdmxQuery.Text.Trim();
                var filter  = Dto.Source.BuildFilterFromSdmxQueryString(query);

                if (filter.SWFilter != null)
                {
                    var ds              = filter.SWFilter.Dataset;
                    filter.SourceType   = FavoriteTreeItemType.Dataset;
                    filter.SourceId     = ds.Uid;
                }
                else
                {
                    filter.SdmxQuery = query;
                }

                this.ApplyNewFilter(filter);
                UCselectDatasetQuery.SetSelectedSource(filter);
  
                this.btnApply.Visible = false;
            }
        }
		#endregion


	}

	// class to support VS designer
	public class GetDataWizzardStep : WizardStep<DataConnection>
    {}
}
