﻿namespace ExcelAddIn.UI.Wizard
{
    partial class SpecifyOutputStep
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(SpecifyOutputStep));
            this.ddlTableType = new DevExpress.XtraEditors.ComboBoxEdit();
            this.lblTableType = new DevExpress.XtraEditors.LabelControl();
            this.btnShowQuery = new DevExpress.XtraEditors.SimpleButton();
            this.txtStartCell = new DevExpress.XtraEditors.TextEdit();
            this.lblStartCell = new DevExpress.XtraEditors.LabelControl();
            this.lblReturn = new DevExpress.XtraEditors.LabelControl();
            this.ddlLanguage = new DevExpress.XtraEditors.ComboBoxEdit();
            this.chkLabels = new DevExpress.XtraEditors.CheckEdit();
            this.chkDate = new DevExpress.XtraEditors.CheckEdit();
            this.ddlSheets = new DevExpress.XtraEditors.ComboBoxEdit();
            this.chkExcludeCodes = new DevExpress.XtraEditors.CheckEdit();
            this.rbValues = new System.Windows.Forms.RadioButton();
            this.rbAttributes = new System.Windows.Forms.RadioButton();
            this.rbMetadata = new System.Windows.Forms.RadioButton();
            this.btnMetadataHelp = new DevExpress.XtraEditors.SimpleButton();
            this.chkPreviousVersions = new DevExpress.XtraEditors.CheckEdit();
            this.pnlPreviousVersions = new System.Windows.Forms.Panel();
            this.rbUpdatedAfter = new System.Windows.Forms.RadioButton();
            this.rbIncludeHistory = new System.Windows.Forms.RadioButton();
            this.rbAsOf = new System.Windows.Forms.RadioButton();
            this.updateTime = new DevExpress.XtraEditors.TimeEdit();
            this.updateDate = new DevExpress.XtraEditors.DateEdit();
            ((System.ComponentModel.ISupportInitialize)(this.pnlMain)).BeginInit();
            this.pnlMain.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pnlButtons)).BeginInit();
            this.pnlButtons.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ddlTableType.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtStartCell.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ddlLanguage.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkLabels.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkDate.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ddlSheets.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkExcludeCodes.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkPreviousVersions.Properties)).BeginInit();
            this.pnlPreviousVersions.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.updateTime.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.updateDate.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.updateDate.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // pnlMain
            // 
            this.pnlMain.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.pnlMain.Appearance.Options.UseBackColor = true;
            this.pnlMain.Controls.Add(this.pnlPreviousVersions);
            this.pnlMain.Controls.Add(this.chkPreviousVersions);
            this.pnlMain.Controls.Add(this.btnMetadataHelp);
            this.pnlMain.Controls.Add(this.rbMetadata);
            this.pnlMain.Controls.Add(this.rbAttributes);
            this.pnlMain.Controls.Add(this.rbValues);
            this.pnlMain.Controls.Add(this.chkExcludeCodes);
            this.pnlMain.Controls.Add(this.ddlSheets);
            this.pnlMain.Controls.Add(this.chkDate);
            this.pnlMain.Controls.Add(this.chkLabels);
            this.pnlMain.Controls.Add(this.ddlLanguage);
            this.pnlMain.Controls.Add(this.lblReturn);
            this.pnlMain.Controls.Add(this.lblStartCell);
            this.pnlMain.Controls.Add(this.txtStartCell);
            this.pnlMain.Controls.Add(this.lblTableType);
            this.pnlMain.Controls.Add(this.ddlTableType);
            this.pnlMain.Size = new System.Drawing.Size(700, 465);
            // 
            // pnlButtons
            // 
            this.pnlButtons.Controls.Add(this.btnShowQuery);
            this.pnlButtons.Location = new System.Drawing.Point(0, 465);
            this.pnlButtons.Size = new System.Drawing.Size(700, 35);
            // 
            // ddlTableType
            // 
            this.ddlTableType.EditValue = "";
            this.ddlTableType.Location = new System.Drawing.Point(135, 107);
            this.ddlTableType.Name = "ddlTableType";
            this.ddlTableType.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.ddlTableType.Properties.NullValuePrompt = "-- choose type --";
            this.ddlTableType.Properties.NullValuePromptShowForEmptyValue = true;
            this.ddlTableType.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            this.ddlTableType.Size = new System.Drawing.Size(505, 20);
            this.ddlTableType.TabIndex = 9;
            // 
            // lblTableType
            // 
            this.lblTableType.Appearance.Font = new System.Drawing.Font("Tahoma", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTableType.Location = new System.Drawing.Point(15, 108);
            this.lblTableType.Name = "lblTableType";
            this.lblTableType.Size = new System.Drawing.Size(73, 16);
            this.lblTableType.TabIndex = 12;
            this.lblTableType.Text = "Table type:";
            // 
            // btnShowQuery
            // 
            this.btnShowQuery.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.btnShowQuery.Appearance.Options.UseFont = true;
            this.btnShowQuery.Location = new System.Drawing.Point(15, 8);
            this.btnShowQuery.Name = "btnShowQuery";
            this.btnShowQuery.Size = new System.Drawing.Size(158, 20);
            this.btnShowQuery.TabIndex = 1;
            this.btnShowQuery.Text = "SHOW QUERY SYNTAX";
            this.btnShowQuery.Click += new System.EventHandler(this.btnShowQuery_Click);
            // 
            // txtStartCell
            // 
            this.txtStartCell.Location = new System.Drawing.Point(382, 72);
            this.txtStartCell.Name = "txtStartCell";
            this.txtStartCell.Size = new System.Drawing.Size(59, 20);
            this.txtStartCell.TabIndex = 13;
            // 
            // lblStartCell
            // 
            this.lblStartCell.Appearance.Font = new System.Drawing.Font("Tahoma", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblStartCell.Location = new System.Drawing.Point(15, 73);
            this.lblStartCell.Name = "lblStartCell";
            this.lblStartCell.Size = new System.Drawing.Size(56, 16);
            this.lblStartCell.TabIndex = 14;
            this.lblStartCell.Text = "Start cell:";
            // 
            // lblReturn
            // 
            this.lblReturn.Appearance.Font = new System.Drawing.Font("Tahoma", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblReturn.Location = new System.Drawing.Point(15, 142);
            this.lblReturn.Name = "lblReturn";
            this.lblReturn.Size = new System.Drawing.Size(43, 16);
            this.lblReturn.TabIndex = 15;
            this.lblReturn.Text = "Return:";
            // 
            // ddlLanguage
            // 
            this.ddlLanguage.Enabled = false;
            this.ddlLanguage.Location = new System.Drawing.Point(221, 141);
            this.ddlLanguage.Name = "ddlLanguage";
            this.ddlLanguage.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.ddlLanguage.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            this.ddlLanguage.Size = new System.Drawing.Size(220, 20);
            this.ddlLanguage.TabIndex = 20;
            // 
            // chkLabels
            // 
            this.chkLabels.Location = new System.Drawing.Point(135, 140);
            this.chkLabels.Name = "chkLabels";
            this.chkLabels.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 10F);
            this.chkLabels.Properties.Appearance.Options.UseFont = true;
            this.chkLabels.Properties.Caption = "Labels";
            this.chkLabels.Size = new System.Drawing.Size(75, 20);
            this.chkLabels.TabIndex = 21;
            this.chkLabels.CheckedChanged += new System.EventHandler(this.chkLabels_CheckedChanged);
            // 
            // chkDate
            // 
            this.chkDate.Location = new System.Drawing.Point(451, 166);
            this.chkDate.Name = "chkDate";
            this.chkDate.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 10F);
            this.chkDate.Properties.Appearance.Options.UseFont = true;
            this.chkDate.Properties.Caption = "Update date";
            this.chkDate.Size = new System.Drawing.Size(100, 20);
            this.chkDate.TabIndex = 24;
            // 
            // ddlSheets
            // 
            this.ddlSheets.EditValue = "";
            this.ddlSheets.Location = new System.Drawing.Point(135, 72);
            this.ddlSheets.Name = "ddlSheets";
            this.ddlSheets.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.ddlSheets.Properties.NullValuePrompt = "-- choose type --";
            this.ddlSheets.Properties.NullValuePromptShowForEmptyValue = true;
            this.ddlSheets.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            this.ddlSheets.Size = new System.Drawing.Size(241, 20);
            this.ddlSheets.TabIndex = 25;
            // 
            // chkExcludeCodes
            // 
            this.chkExcludeCodes.EditValue = true;
            this.chkExcludeCodes.Location = new System.Drawing.Point(451, 140);
            this.chkExcludeCodes.Name = "chkExcludeCodes";
            this.chkExcludeCodes.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 10F);
            this.chkExcludeCodes.Properties.Appearance.Options.UseFont = true;
            this.chkExcludeCodes.Properties.Caption = "Exclude Codes";
            this.chkExcludeCodes.Size = new System.Drawing.Size(119, 20);
            this.chkExcludeCodes.TabIndex = 26;
            this.chkExcludeCodes.Visible = false;
            // 
            // rbValues
            // 
            this.rbValues.AutoSize = true;
            this.rbValues.Checked = true;
            this.rbValues.Location = new System.Drawing.Point(135, 171);
            this.rbValues.Name = "rbValues";
            this.rbValues.Size = new System.Drawing.Size(57, 17);
            this.rbValues.TabIndex = 27;
            this.rbValues.TabStop = true;
            this.rbValues.Text = "Values";
            this.rbValues.UseVisualStyleBackColor = true;
            // 
            // rbAttributes
            // 
            this.rbAttributes.AutoSize = true;
            this.rbAttributes.Location = new System.Drawing.Point(135, 194);
            this.rbAttributes.Name = "rbAttributes";
            this.rbAttributes.Size = new System.Drawing.Size(125, 17);
            this.rbAttributes.TabIndex = 28;
            this.rbAttributes.Text = "Values and Attributes";
            this.rbAttributes.UseVisualStyleBackColor = true;
            // 
            // rbMetadata
            // 
            this.rbMetadata.AutoSize = true;
            this.rbMetadata.Location = new System.Drawing.Point(135, 217);
            this.rbMetadata.Name = "rbMetadata";
            this.rbMetadata.Size = new System.Drawing.Size(123, 17);
            this.rbMetadata.TabIndex = 29;
            this.rbMetadata.Text = "Referential metadata";
            this.rbMetadata.UseVisualStyleBackColor = true;
            this.rbMetadata.CheckedChanged += new System.EventHandler(this.rbMetadata_CheckedChanged);
            // 
            // btnMetadataHelp
            // 
            this.btnMetadataHelp.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.btnMetadataHelp.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.btnMetadataHelp.Appearance.ForeColor = System.Drawing.Color.Transparent;
            this.btnMetadataHelp.Appearance.Options.UseBackColor = true;
            this.btnMetadataHelp.Appearance.Options.UseFont = true;
            this.btnMetadataHelp.Appearance.Options.UseForeColor = true;
            this.btnMetadataHelp.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.UltraFlat;
            this.btnMetadataHelp.Image = ((System.Drawing.Image)(resources.GetObject("btnMetadataHelp.Image")));
            this.btnMetadataHelp.Location = new System.Drawing.Point(262, 211);
            this.btnMetadataHelp.Name = "btnMetadataHelp";
            this.btnMetadataHelp.Size = new System.Drawing.Size(25, 26);
            this.btnMetadataHelp.TabIndex = 30;
            // 
            // chkPreviousVersions
            // 
            this.chkPreviousVersions.Location = new System.Drawing.Point(135, 251);
            this.chkPreviousVersions.Name = "chkPreviousVersions";
            this.chkPreviousVersions.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 10F);
            this.chkPreviousVersions.Properties.Appearance.Options.UseFont = true;
            this.chkPreviousVersions.Properties.Caption = "Previous data versions";
            this.chkPreviousVersions.Size = new System.Drawing.Size(241, 20);
            this.chkPreviousVersions.TabIndex = 31;
            this.chkPreviousVersions.CheckedChanged += new System.EventHandler(this.chkPreviousVersions_CheckedChanged);
            // 
            // pnlPreviousVersions
            // 
            this.pnlPreviousVersions.Controls.Add(this.updateTime);
            this.pnlPreviousVersions.Controls.Add(this.updateDate);
            this.pnlPreviousVersions.Controls.Add(this.rbAsOf);
            this.pnlPreviousVersions.Controls.Add(this.rbIncludeHistory);
            this.pnlPreviousVersions.Controls.Add(this.rbUpdatedAfter);
            this.pnlPreviousVersions.Location = new System.Drawing.Point(134, 273);
            this.pnlPreviousVersions.Name = "pnlPreviousVersions";
            this.pnlPreviousVersions.Size = new System.Drawing.Size(505, 84);
            this.pnlPreviousVersions.TabIndex = 32;
            // 
            // rbUpdatedAfter
            // 
            this.rbUpdatedAfter.AutoSize = true;
            this.rbUpdatedAfter.Checked = true;
            this.rbUpdatedAfter.Location = new System.Drawing.Point(3, 12);
            this.rbUpdatedAfter.Name = "rbUpdatedAfter";
            this.rbUpdatedAfter.Size = new System.Drawing.Size(124, 17);
            this.rbUpdatedAfter.TabIndex = 33;
            this.rbUpdatedAfter.TabStop = true;
            this.rbUpdatedAfter.Text = "Data changed since:";
            this.rbUpdatedAfter.UseVisualStyleBackColor = true;
            // 
            // rbIncludeHistory
            // 
            this.rbIncludeHistory.AutoSize = true;
            this.rbIncludeHistory.Location = new System.Drawing.Point(3, 35);
            this.rbIncludeHistory.Name = "rbIncludeHistory";
            this.rbIncludeHistory.Size = new System.Drawing.Size(135, 17);
            this.rbIncludeHistory.TabIndex = 33;
            this.rbIncludeHistory.Text = "All data changes since:";
            this.rbIncludeHistory.UseVisualStyleBackColor = true;
            // 
            // rbAsOf
            // 
            this.rbAsOf.AutoSize = true;
            this.rbAsOf.Location = new System.Drawing.Point(3, 58);
            this.rbAsOf.Name = "rbAsOf";
            this.rbAsOf.Size = new System.Drawing.Size(77, 17);
            this.rbAsOf.TabIndex = 34;
            this.rbAsOf.Text = "Data as of:";
            this.rbAsOf.UseVisualStyleBackColor = true;
            // 
            // updateTime
            // 
            this.updateTime.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.updateTime.EditValue = new System.DateTime(2015, 4, 9, 0, 0, 0, 0);
            this.updateTime.Enabled = false;
            this.updateTime.Location = new System.Drawing.Point(258, 34);
            this.updateTime.Name = "updateTime";
            this.updateTime.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.updateTime.Size = new System.Drawing.Size(99, 20);
            this.updateTime.TabIndex = 36;
            // 
            // updateDate
            // 
            this.updateDate.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.updateDate.EditValue = null;
            this.updateDate.Location = new System.Drawing.Point(153, 34);
            this.updateDate.Name = "updateDate";
            this.updateDate.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.updateDate.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.updateDate.Properties.DisplayFormat.FormatString = "yyy-MM-dd";
            this.updateDate.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.updateDate.Properties.Mask.EditMask = "yyyy-MM-dd";
            this.updateDate.Properties.NullValuePrompt = "yyyy-mm-dd";
            this.updateDate.Properties.NullValuePromptShowForEmptyValue = true;
            this.updateDate.Size = new System.Drawing.Size(100, 20);
            this.updateDate.TabIndex = 35;
            this.updateDate.DateTimeChanged += new System.EventHandler(this.updateDate_DateTimeChanged);
            // 
            // SpecifyOutputStep
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Name = "SpecifyOutputStep";
            this.Size = new System.Drawing.Size(700, 500);
            ((System.ComponentModel.ISupportInitialize)(this.pnlMain)).EndInit();
            this.pnlMain.ResumeLayout(false);
            this.pnlMain.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pnlButtons)).EndInit();
            this.pnlButtons.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.ddlTableType.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtStartCell.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ddlLanguage.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkLabels.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkDate.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ddlSheets.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkExcludeCodes.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkPreviousVersions.Properties)).EndInit();
            this.pnlPreviousVersions.ResumeLayout(false);
            this.pnlPreviousVersions.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.updateTime.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.updateDate.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.updateDate.Properties)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private DevExpress.XtraEditors.ComboBoxEdit ddlTableType;
        private DevExpress.XtraEditors.LabelControl lblTableType;
        private DevExpress.XtraEditors.SimpleButton btnShowQuery;
        private DevExpress.XtraEditors.TextEdit txtStartCell;
        private DevExpress.XtraEditors.LabelControl lblReturn;
        private DevExpress.XtraEditors.LabelControl lblStartCell;
        private DevExpress.XtraEditors.ComboBoxEdit ddlLanguage;
        private DevExpress.XtraEditors.CheckEdit chkDate;
        private DevExpress.XtraEditors.CheckEdit chkLabels;
        private DevExpress.XtraEditors.ComboBoxEdit ddlSheets;
        private DevExpress.XtraEditors.CheckEdit chkExcludeCodes;
        private System.Windows.Forms.RadioButton rbMetadata;
        private System.Windows.Forms.RadioButton rbAttributes;
        private System.Windows.Forms.RadioButton rbValues;
        private DevExpress.XtraEditors.SimpleButton btnMetadataHelp;
        private DevExpress.XtraEditors.CheckEdit chkPreviousVersions;
        private System.Windows.Forms.Panel pnlPreviousVersions;
        private System.Windows.Forms.RadioButton rbAsOf;
        private System.Windows.Forms.RadioButton rbIncludeHistory;
        private System.Windows.Forms.RadioButton rbUpdatedAfter;
        private DevExpress.XtraEditors.TimeEdit updateTime;
        private DevExpress.XtraEditors.DateEdit updateDate;
    }
}
