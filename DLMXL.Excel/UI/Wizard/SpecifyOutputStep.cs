﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using ExcelAddIn.Domain.Wizard;
using System.Text.RegularExpressions;
using DevExpress.Utils;
using DLMXL.Models;
using DLMXL.Models.Interfaces;
using ExcelAddIn.Forms;

namespace ExcelAddIn.UI.Wizard
{
    public partial class SpecifyOutputStep : GetDataWizzardStep
    {
        private static Regex ValidExcelCell = new Regex("^([a-z]{1,3})([0-9]{1,7})$", RegexOptions.IgnoreCase | RegexOptions.Compiled);

        static SpecifyOutputStep()
        {
            // NB! devexpress specific, needs refactoring when devexpress dependency is removed
            ToolTipController.DefaultController.HyperlinkClick += delegate(object sender, HyperlinkClickEventArgs e)
            {
                var process = new Process();
                process.StartInfo.FileName = (e.Link);
                process.StartInfo.Verb = "open";
                process.StartInfo.WindowStyle = ProcessWindowStyle.Normal;
                try
                {
                    process.Start();
                }
                catch { }
            };
        }

        public SpecifyOutputStep()
        {
            InitializeComponent();

            SetStyleController(InputStyleController, 
                ddlSheets, 
                txtStartCell,
                ddlTableType,
                ddlLanguage,
                chkLabels,
                chkDate
            );

            SetStyleController(LabelStyleController,
                lblStartCell,
                lblTableType,
                lblReturn
            );

            
            var sTooltip = new SuperToolTip();
            sTooltip.Setup(new SuperToolTipSetupArgs()
            {
                
                Title =
                {
                    Text = " See <href=\"https://sis-cc.gitlab.io/dotstatsuite-documentation/using-dlm/manage-structures/link-dsd-msd/\">here</href> for information on how to define referential metadata",
                }
            });
            sTooltip.AllowHtmlText = DefaultBoolean.True;
            btnMetadataHelp.SuperTip = sTooltip;
        }

        #region IWizardStep

        public override bool IsValid
        {
            get
            {
                ClearErrors();
                var ok  = IsValid(txtStartCell, IsValidExcelAddress, "Cell address is not valid");
                ok      &= IsValid(ddlTableType, "Table type undefined");
                ok      &= IsValid(updateTime, !chkPreviousVersions.Checked || rbIncludeHistory.Checked || UpdatedAfter != null, "Date must have a selection");
                return ok;
            }
        }

        public override void Init(DataConnection dto)
        {
            base.Init(dto);

            BindOutputTypes();
            BindLanguages();
            SetUpdatedAfterControls();

            txtStartCell.Text               = dto.StartCell;
            
            this.ddlSheets.Properties.Items.Clear();
            this.ddlSheets.Properties.Items.AddRange(dto.AllSheets);
            this.ddlSheets.SelectedItem = dto.Sheet;
            this.chkDate.Visible = dto.Source.IsDateOutput;
            this.rbAttributes.Visible = dto.Source.IsFlagsOutput;

            this.rbMetadata.Visible = dto.Source.DataAccess.IsMetadata;
            this.rbMetadata.Enabled = dto.Source.DataAccess.IsMetadata && dto.Filter.SWFilter.Dataset.Msd != null;
            this.btnMetadataHelp.Visible = dto.Source.DataAccess.IsMetadata && dto.Filter.SWFilter.Dataset.Msd == null;

            if (!rbMetadata.Visible && rbMetadata.Checked)
            {
                rbValues.Checked = true;
            }

            if (!dto.IsNew() || dto.Resume)
            {
				chkLabels.Checked		= (dto.LabelOutput & LabelOutput.Label) > 0;
				ddlLanguage.Enabled		= chkLabels.Checked;
				chkExcludeCodes.Visible = chkLabels.Checked;
				chkExcludeCodes.Checked = chkLabels.Checked && (dto.LabelOutput & LabelOutput.Code) == 0;

                rbAttributes.Checked    = dto.IsFlags;
                rbMetadata.Checked      = dto.IsMetadata;
                chkDate.Checked         = dto.IsUpdateData;

                if (dto.Source.DataAccess.IsHistoricVersion && dto.Filter.HistoricVersionType!=null)
                {
                    chkPreviousVersions.Checked = true;
                    HistoricVersionOutputType = dto.Filter.HistoricVersionType;
                    UpdatedAfter = dto.Filter.UpdatedAfter;
                }
            }

            this.chkPreviousVersions.Visible = dto.Source.DataAccess.IsHistoricVersion;
            this.pnlPreviousVersions.Visible = dto.Source.DataAccess.IsHistoricVersion && this.chkPreviousVersions.Checked;
        }

        public override void Save(DataConnection dto)
        {
	        dto.StartCell       = txtStartCell.Text.Trim();
	        dto.IsFlags         = rbAttributes.Checked;
            dto.IsMetadata      = rbMetadata.Checked;
	        dto.IsUpdateData    = chkDate.Checked;
            
            dto.OutputType = rbMetadata.Checked
                ? OutputType.Flat
                : ddlTableType.SelectedItem as DropdownItem<OutputType>;

            dto.IsActionColumn = dto.Source.DataAccess.IsActionColumn && dto.OutputType == OutputType.Flat;

            dto.Sheet           = ddlSheets.SelectedItem as String;

	        if (!chkLabels.Checked)
	        {
		        dto.LabelOutput = LabelOutput.Code;
		        dto.Language = null;
	        }
	        else
	        {
		        dto.LabelOutput = LabelOutput.Label | (chkExcludeCodes.Checked ? 0 : LabelOutput.Code);
		        dto.Language = ddlLanguage.SelectedItem as DropdownItem<string>;
	        }


            if (chkPreviousVersions.Checked)
            {
                dto.Filter.HistoricVersionType = HistoricVersionOutputType;
                dto.Filter.UpdatedAfter = UpdatedAfter;
            }
            else
            {
                dto.Filter.HistoricVersionType = null;
                dto.Filter.UpdatedAfter = null;
            }
        }

        #endregion

        private bool IsValidExcelAddress(string address)
        {
            if (string.IsNullOrEmpty(address) || address.Length > 10)
                return false;

            var match = ValidExcelCell.Match(address);

            if (!match.Success)
                return false;

            if (String.Compare(match.Groups[1].Value.PadLeft(3, 'A'), "XAA", StringComparison.OrdinalIgnoreCase) > 0) //XFD
                return false;

            if (int.Parse(match.Groups[2].Value) > 1000000) //1048576
                return false;
            
            return true;
        }

        private void BindLanguages()
        {
	        var langs = Dto.Source.DataAccess.GetLanguages(Dto.Filter.SWFilter.Dataset)
		        .Select(l => new DropdownItem<string>(l.Value, l.Key))
		        .ToList();

            this.ddlLanguage.Properties.Items.Clear();
            this.ddlLanguage.Properties.Items.AddRange(langs);

            var selectedLanguage = Dto.Language ?? Config.Settings.Language;
            
            this.ddlLanguage.SelectedItem = langs.FirstOrDefault(x => x.Value == selectedLanguage);

			if(this.ddlLanguage.SelectedIndex < 0)
				this.ddlLanguage.SelectedIndex = 0;
        }

        private void BindOutputTypes()
        {
            var index       = this.ddlTableType.SelectedIndex;
            var outputType  = new List<DropdownItem<OutputType>>(new[] {new DropdownItem<OutputType>("Flat", OutputType.Flat)});

            if (Dto.Filter.SWFilter.Dataset.HasATimeDimension)
               outputType.AddRange(new[]
               {
                   new DropdownItem<OutputType>("Time Series Down", OutputType.TimeSeriesDown), 
                   new DropdownItem<OutputType>("Time Series across", OutputType.TimeSeriesAcross),
               });

            this.ddlTableType.Properties.Items.Clear();
            this.ddlTableType.Properties.Items.AddRange(outputType);

            if (outputType.Count == 1)
            {
                this.ddlTableType.SelectedIndex = 0;
            }
            else if (index > -1)
            {
                this.ddlTableType.SelectedIndex = index;
            }
            else if (!Dto.IsNew() || Dto.Resume)
            {
                foreach (var item in outputType)
                    if (item.Value == Dto.OutputType)
                    {
                        ddlTableType.SelectedItem = item;
                        break;
                    }
            }
        }

        private void SetOuputSelection()
        {
            this.ddlTableType.SelectedIndex = 0;
            this.ddlTableType.Enabled = !this.rbMetadata.Checked && !this.chkPreviousVersions.Checked;
        }

        #region Updated after

        /// <summary>
        /// gets UTC datetime, sets UTC and converts to local
        /// </summary>
        private DateTime? UpdatedAfter
        {
            get
            {
                if (updateDate.DateTime == DateTime.MinValue)
                    return null;

                var dt = updateDate.DateTime.Date;

                if (updateTime.Time != DateTime.MinValue)
                {
                    dt = dt
                        .AddHours(updateTime.Time.Hour)
                        .AddMinutes(updateTime.Time.Minute)
                        .AddSeconds(updateTime.Time.Second);
                }

                return dt.ToUniversalTime();
            }
            set
            {
                if (value == null || value == DateTime.MinValue)
                    return;

                var dt = value.Value.ToLocalTime();

                updateDate.DateTime = dt;
                updateTime.Time = dt;
                updateDate.Enabled = true;
            }
        }

        private HistoricVersionType? HistoricVersionOutputType
        {
            get
            {
                if (rbUpdatedAfter.Checked) 
                    return HistoricVersionType.UpdatedAfter;
                
                if(rbIncludeHistory.Checked)
                    return HistoricVersionType.IncludeHistory;

                if (rbAsOf.Checked)
                    return HistoricVersionType.AsOf;

                return null;
            }
            set
            {
                if (value.HasValue)
                {
                    rbUpdatedAfter.Checked = value == HistoricVersionType.UpdatedAfter;
                    rbIncludeHistory.Checked = value == HistoricVersionType.IncludeHistory;
                    rbAsOf.Checked = value == HistoricVersionType.AsOf;
                }
            }
        }

        private void SetUpdatedAfterControls()
        {
            updateDate.Properties.MaxValue = DateTime.Today;
            updateDate.Properties.MinValue = new DateTime(1900, 1, 1);
        }

        #endregion

        #region Event handlers

        private void btnShowQuery_Click(object sender, EventArgs e)
        {
            this.Save(Dto);
            
            using (var dlg = new ShowQuerySyntaxDialog(Dto))
            {
                dlg.ShowDialog();
            }
        }

        private void chkLabels_CheckedChanged(object sender, EventArgs e)
        {
            ddlLanguage.Enabled     = chkLabels.Checked;
            chkExcludeCodes.Visible = chkLabels.Checked;
        }

        private void rbMetadata_CheckedChanged(object sender, EventArgs e)
        {
            this.SetOuputSelection();
        }

        private void chkPreviousVersions_CheckedChanged(object sender, EventArgs e)
        {
            this.pnlPreviousVersions.Visible = chkPreviousVersions.Checked;
            this.SetOuputSelection();
        }
        private void updateDate_DateTimeChanged(object sender, EventArgs e)
        {
            if (updateDate.DateTime == DateTime.MinValue)
            {
                updateTime.Enabled = false;
                updateTime.Time = DateTime.MinValue;
            }
            else
            {
                updateTime.Enabled = true;
            }
        }
        #endregion

    }
}
