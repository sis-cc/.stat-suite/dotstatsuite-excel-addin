﻿namespace ExcelAddIn.UI.Wizard
{
    partial class WizardBar
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(WizardBar));
            this.imgList = new System.Windows.Forms.ImageList(this.components);
            this.lblSeparator = new System.Windows.Forms.Label();
            this.lblArrow = new System.Windows.Forms.Label();
            this.lblText = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // imgList
            // 
            this.imgList.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("imgList.ImageStream")));
            this.imgList.TransparentColor = System.Drawing.Color.Transparent;
            this.imgList.Images.SetKeyName(0, "arrow_unactive.png");
            this.imgList.Images.SetKeyName(1, "arrow_active.png");
            // 
            // lblSeparator
            // 
            this.lblSeparator.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lblSeparator.BackColor = System.Drawing.SystemColors.ControlDark;
            this.lblSeparator.ForeColor = System.Drawing.SystemColors.ControlText;
            this.lblSeparator.Location = new System.Drawing.Point(0, 29);
            this.lblSeparator.Margin = new System.Windows.Forms.Padding(0);
            this.lblSeparator.Name = "lblSeparator";
            this.lblSeparator.Size = new System.Drawing.Size(247, 4);
            this.lblSeparator.TabIndex = 8;
            // 
            // lblArrow
            // 
            this.lblArrow.ImageAlign = System.Drawing.ContentAlignment.TopLeft;
            this.lblArrow.ImageIndex = 0;
            this.lblArrow.ImageList = this.imgList;
            this.lblArrow.Location = new System.Drawing.Point(7, 23);
            this.lblArrow.Margin = new System.Windows.Forms.Padding(0);
            this.lblArrow.Name = "lblArrow";
            this.lblArrow.Size = new System.Drawing.Size(50, 10);
            this.lblArrow.TabIndex = 10;
            // 
            // lblText
            // 
            this.lblText.AutoSize = true;
            this.lblText.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblText.ForeColor = System.Drawing.SystemColors.ControlDark;
            this.lblText.Location = new System.Drawing.Point(0, 6);
            this.lblText.Margin = new System.Windows.Forms.Padding(0);
            this.lblText.Name = "lblText";
            this.lblText.Size = new System.Drawing.Size(133, 15);
            this.lblText.TabIndex = 11;
            this.lblText.Text = "Step 1 - Select data";
            // 
            // WizardBar
            // 
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None;
            this.Controls.Add(this.lblText);
            this.Controls.Add(this.lblSeparator);
            this.Controls.Add(this.lblArrow);
            this.Font = new System.Drawing.Font("Tahoma", 8.25F);
            this.Margin = new System.Windows.Forms.Padding(0);
            this.Name = "WizardBar";
            this.Size = new System.Drawing.Size(247, 42);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ImageList imgList;
        private System.Windows.Forms.Label lblSeparator;
        private System.Windows.Forms.Label lblArrow;
        private System.Windows.Forms.Label lblText;
    }
}
