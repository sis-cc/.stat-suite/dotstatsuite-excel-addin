﻿using System;
using System.Drawing;
using System.Windows.Forms;

namespace ExcelAddIn.UI.Wizard
{
    public sealed partial class WizardBar : UserControl
    {
        private bool _isActive;
        private Color _activeColor      = Color.FromArgb(137,189,125);
        private Color _unactiveColor    = Color.FromArgb(192,192,192);

        public bool IsActive
        {
            get { return this._isActive; }
            set
            {
                this._isActive = value;
                this.lblSeparator.BackColor = this.lblText.ForeColor = value ? _activeColor : _unactiveColor;
                this.lblArrow.ImageIndex = value ? 1 : 0;
            }
        }

        public string Text
        {
            get { return this.lblText.Text; }
            set { this.lblText.Text = value; }
        }

        public WizardBar()
        {
            InitializeComponent();
        }

        public WizardBar(string text) : this()
        {
            this.Text = text;
            this.Dock = DockStyle.Fill;
        }
    }
}
