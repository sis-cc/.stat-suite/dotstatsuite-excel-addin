﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Windows.Forms;
using DevExpress.LookAndFeel;
using ExcelAddIn.Domain.Wizard;

namespace ExcelAddIn.UI.Wizard
{
    public partial class WizardBase<T, R> : UserControl, IWizardBase<T, R>
        where T : class, IWizardDto
        where R : UserControl, IWizardStep<T>
    {
        public event EventHandler Completed;
        public int TotalSteps { get; private set; }

        private int _currentStep;
        private bool _isInitialized             = false;
        private List<R> Steps                   = new List<R>(10);
        private List<WizardBar> ProgressBars    = new List<WizardBar>(10);

        public WizardBase()
        {
            InitializeComponent();

            btnNext.Click += MoveForward;
            btnBack.Click += MoveBack;

//            btnNext.LookAndFeel.UseDefaultLookAndFeel = false;
//            btnNext.LookAndFeel.Style = LookAndFeelStyle.Flat;
//            btnNext.BackColor = Color.FromArgb(137, 189, 125);
        }

        private T Dto { get; set; }

        public void Init(T dto, int currentStep = 1)
        {
            if(_isInitialized) 
                throw new InvalidOperationException("Wizard is already initialized");

            if(Steps.Count == 0)
                throw new InvalidOperationException("Add some wizard steps before initialize");

            var width = 100F / TotalSteps; 
            pnlProgressBar.ColumnCount = TotalSteps;
            pnlProgressBar.ColumnStyles.Clear();

            for(var i=0;i<ProgressBars.Count;i++)
            {
                pnlProgressBar.ColumnStyles.Add(new ColumnStyle(SizeType.Percent, width));
                pnlProgressBar.Controls.Add(ProgressBars[i], i, 0);
            }

            if (currentStep > 0 && currentStep <= TotalSteps)
                _currentStep = currentStep - 1;

            Dto = dto;
            LoadControl(GetCurrentStep());
            _isInitialized = true;
        }

        public void AddStep(string name, R step)
        {
            if (_isInitialized)
                throw new InvalidOperationException("Wizard is already initialized");

            if(step == null)
                throw new ArgumentException("Step can't be null");

            Steps.Add(step);
            ProgressBars.Add(new WizardBar($"Step {++TotalSteps} - {name}"));
        }

        private void LoadControl(R ctrl)
        {
            ctrl.Dock   = DockStyle.Fill;
            using(new LongRunningOperation()) ctrl.Init(Dto);
            ctrl.AttachButtonPanel(this.pnlStepButtons);
            
            this.pnlContent.Controls.Clear();
            this.pnlContent.Controls.Add(ctrl);

            this.btnBack.Visible    = IsPreviosStep();
            this.btnNext.Text       = ctrl.NextButtonText;
            
            SetProgressBar();
        }

        private R GetCurrentStep()
        {
            return this.Steps[this._currentStep];
        }

        private R GetNextStep()
        {
            return IsNextStep()
                ? this.Steps[++_currentStep]
                : null;
        }

        private R GetPreviousStep()
        {
            return IsPreviosStep()
                ? this.Steps[--_currentStep]
                : null;
        }

        private bool IsNextStep()
        {
            return (_currentStep + 1)  < TotalSteps;
        }

        private bool IsPreviosStep()
        {
            return _currentStep > 0;
        }

        private void SetProgressBar()
        {
            foreach (var bar in ProgressBars)
                bar.IsActive = false;

            ProgressBars[_currentStep].IsActive = true;
        }

        #region Event handlers
        private void MoveBack(object sender, EventArgs e)
        {
            var previousStep = GetPreviousStep();

            if (previousStep != null)
            {
                LoadControl(previousStep);
            }
        }

        private void MoveForward(object sender, EventArgs e)
        {
            var ctrl = GetCurrentStep();
            
            if (!ctrl.IsValid) return;

            btnNext.Enabled = false;
            
            ctrl.Save(Dto);

            var nextStep = GetNextStep();

            if (nextStep != null)
            {
                LoadControl(nextStep); 
            }
            else
            {
                Completed?.Invoke(this, null);
            }

            btnNext.Enabled = true;
        }
        #endregion
    }
}
