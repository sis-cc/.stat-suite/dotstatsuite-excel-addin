﻿using System.Windows.Forms;
using ExcelAddIn.Domain.Wizard;

namespace ExcelAddIn.UI.Wizard
{
    public partial class WizardStep<T> : AddInBaseUserControl, IWizardStep<T> where T : IWizardDto
    {
        public WizardStep()
        {
            InitializeComponent();
        }

        protected T Dto { get; set; }

        protected bool IsLoaded { get; set; }

        public virtual bool IsValid
        {
            get { return true; }
        }

        public virtual void Init(T dto)
        {
            Dto = dto;
        }

        public virtual void Save(T dto)
        {}

        public string NextButtonText { get; set; }

        public void AttachButtonPanel(Control parent)
        {
            if (parent == null) return;

            pnlButtons.Dock = DockStyle.Fill;
            this.Controls.Remove(pnlButtons);

            parent.Controls.Clear();
            parent.Controls.Add(pnlButtons);
        }
    }
}
