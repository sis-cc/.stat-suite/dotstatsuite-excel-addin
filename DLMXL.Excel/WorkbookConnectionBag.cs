﻿using System;
using System.Collections.Generic;
using ExcelAddIn.Domain.Wizard;
using Microsoft.Office.Interop.Excel;
using System.Linq;

namespace ExcelAddIn
{
    public class WorkbookConnectionBag
    {
        private static readonly Dictionary<string, WorkbookConnectionBag> Workbooks = new Dictionary<string, WorkbookConnectionBag>();
        public readonly Dictionary<string, Dictionary<string, DataConnection>> Sheets = new Dictionary<string, Dictionary<string, DataConnection>>();
        public static readonly Dictionary<Workbook, Dictionary<string, WorkbookConnection>> WorkbooksConnectionsToMigrate = new Dictionary<Workbook, Dictionary<string, WorkbookConnection>>();

        private static string Uid()
        {
            return Guid.NewGuid().ToString();
        }

        public static IList<DataConnection> GetConnections(Worksheet sheet)
        {
            var book                = sheet.Parent as Workbook;

            if (book == null)
                return null;

            var bookId      = book.GetId();
            var sheetId     = sheet.GetId();

            if (string.IsNullOrEmpty(bookId) || string.IsNullOrEmpty(sheetId) || !Workbooks.ContainsKey(bookId))
                return null;

            var bag = Workbooks[bookId];

            return bag.Sheets.ContainsKey(sheetId)
                ? bag.Sheets[sheetId].Values.ToArray()
                : null;
        }
       
        public static void SaveConnection(Worksheet sheet, DataConnection connection)
        {
            if(sheet == null)
                throw new ArgumentException("Worksheet");

            if (connection == null)
                throw new ArgumentException("Connection");

            var book                = sheet.Parent as Workbook;

            if (book == null)
                return;
            
            var bookId      = book.GetId();
            var sheetId     = sheet.GetId();

            if (string.IsNullOrEmpty(bookId) || !Workbooks.ContainsKey(bookId))
            {
                book.SetId(bookId = Uid());
                Workbooks.Add(bookId, new WorkbookConnectionBag());
            }

            var bag = Workbooks[bookId];

            if (sheetId == null)
            {
                sheet.SetId(sheetId = Uid());
                bag.Sheets.Add(sheetId, new Dictionary<string, DataConnection>());
            }

            if (connection.IsNew())
                connection.Id = Uid();

            bag.Sheets[sheetId][connection.Id] = connection;
        }

        public static void ExtractConnections(Workbook book)
        {
            string bookId;

            if (book == null || string.IsNullOrEmpty(bookId = book.GetId()))
                return;

            var bag = new WorkbookConnectionBag();

            // possible if workbook's physical copy created;
            if (Workbooks.ContainsKey(bookId))
            {
                book.SetId(bookId = Uid());
            }

            foreach (Worksheet sheet in book.Worksheets)
            {
                var sheetId  = sheet.GetId();

                if(string.IsNullOrEmpty(sheetId))
                    continue;
               
                bag.Sheets.Add(sheetId, sheet.GetConnections() ?? new Dictionary<string, DataConnection>());
            }

            Workbooks[bookId] = bag;
        }

   

        public static bool IsWorkbookConnectionLinkedToTable(WorkbookConnection wc, out ListObject listObject)
        {
            foreach (Worksheet ws in wc.Parent.Worksheets)
            {
                foreach (ListObject lo in ws.ListObjects)
                {
                    if (lo.QueryTable != null)
                    {
                        if (ReferenceEquals(lo.QueryTable.WorkbookConnection, wc))
                        {
                            listObject = lo;
                            return true;
                        }
                        
                    }
                }
            }
            listObject = null;
            return false;
        }

        public static void SaveConnections(Workbook book, bool withNewId)
        {
            string bookId;

            if (book == null || string.IsNullOrEmpty(bookId = book.GetId()) || !Workbooks.ContainsKey(bookId))
                return;

            var bag = Workbooks[bookId];

            if (book.Worksheets.Count == 0 || bag.Sheets.Count == 0)
                return;

            if (withNewId)
            {
                Workbooks.Remove(bookId);
                book.SetId(bookId = Uid());
                Workbooks.Add(bookId, bag);
            }

            foreach (Worksheet sheet in book.Worksheets)
            {
                var sheetId = sheet.GetId();

                if(string.IsNullOrEmpty(sheetId) || !bag.Sheets.ContainsKey(sheetId))
                    continue;

                sheet.SetConnections(bag.Sheets[sheetId]);
            }
        }

        public static void CloseWorkbook(string bookId)
        {
            if (string.IsNullOrEmpty(bookId) || !Workbooks.ContainsKey(bookId))
                return;

            Workbooks.Remove(bookId);
        }

        public static void BreakConnection(Worksheet sheet, DataConnection connection)
        {
            if (sheet == null)
                throw new ArgumentException("Worksheet");

            if (connection == null)
                throw new ArgumentException("Connection");

            var book    = sheet.Parent as Workbook;
            var bag     = Workbooks[ book.GetId()];
            
            bag.Sheets[sheet.GetId()].Remove(connection.Id);
        }
    }
}
