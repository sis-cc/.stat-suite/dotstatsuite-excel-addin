﻿using System.Collections.Generic;
using System.Linq;
using DLMXL.Models.Interfaces;

namespace DLMXL.Models
{
	public class Attribute : SWBase, IAttribute
	{
		public IList<ICodelistMember> Members { get; protected set; }

		public Attribute(
			string code, 
			string defaultName,
			Dictionary<string, string> names
		) 
			: base(-1, -1, code, defaultName, names)
		{}

		public void ResetMembers(IEnumerable<ICodelistMember> ml)
		{
			Members = ml.ToArray();
		}
	}
}