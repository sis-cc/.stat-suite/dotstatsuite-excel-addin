﻿using System;
using System.Collections.Generic;
using System.Linq;
using DLMXL.Models.Interfaces;

namespace DLMXL.Models
{
    /// <summary>
    /// Statworks specific
    /// </summary>
    public enum SWValueType
    {
        /// <summary>
        /// Value type
        /// </summary>
        Value = 0,
        /// <summary>
        /// Binary type (qualitative)
        /// </summary>
        ///Binary = 1,
        /// <summary>
        /// Multiple simple type (qualitative)
        /// </summary>
        SimpleMultipleChoice = 2,
        /// <summary>
        /// Multiple alternative type (qualitative)
        /// </summary>
        AlternativeMultipleChoice = 3,
        /// <summary>
        /// Multiple ranking type (qualitative)
        /// </summary>
        OrderedMultipleChoice = 4,
        /// <summary>
        /// Text type (qualitative)
        /// </summary>
        Text = 5,
        /// <summary>
        /// Date type (qualitative)
        /// </summary>
        Date = 6
    }

    public class CodelistMember : SWBase, ICodelistMember
	{
        private static readonly HashSet<string> AnnualCodes = new HashSet<string>(StringComparer.OrdinalIgnoreCase) {"A", "ANNUAL", "ANNUALY", "YEARLY", "Y"};
        private static readonly HashSet<string> MonthCodes = new HashSet<string>(StringComparer.OrdinalIgnoreCase)  { "M", "MONTH", "MONTHLY"};
        private static readonly HashSet<string> QuorterCodes = new HashSet<string>(StringComparer.OrdinalIgnoreCase)  { "Q", "QUORTER", "QUORTERLY"};
        
        private static readonly HashSet<string> DailyCodes = new HashSet<string>()  { "D", "Daily", "Day" };
        private static readonly HashSet<string> WeeklyCodes = new HashSet<string>()  { "W", "Weekly", "Week" };
        private static readonly HashSet<string> SemestrialCodes = new HashSet<string>()  { "S", "Half-yearly", "Semester" };
        private static readonly HashSet<string> BusinessCodes = new HashSet<string>()  { "B"};

        public ICodelistMember ParentMember { get; protected set; }
        public IList<ICodelistMember> ChildMembers { get; protected set; }

        public bool IsYear=> AnnualCodes.Contains(this.Code);
        public bool IsMonth => MonthCodes.Contains(this.Code);
        public bool IsQuarter => QuorterCodes.Contains(this.Code);

        public bool IsDaily => DailyCodes.Contains(this.Code);
        public bool IsWeekly => WeeklyCodes.Contains(this.Code);
        public bool IsSemestrial => SemestrialCodes.Contains(this.Code);
        public bool IsBusiness => BusinessCodes.Contains(this.Code);

        public IEnumerable<ICodelistMember> AllDescendents
        {
            get
            {
                foreach (var cm in ChildMembers)
                {
                    yield return cm;

                    foreach (var ccm in cm.AllDescendents)
                        yield return ccm;
                }
            }
        }

        // ----------------

        public SWValueType ValueType { get; protected set; }

        public bool IsMultipleChoice { get; protected set; }

        // ----------------

        public int Depth { get; protected set; }


        #region Constructor

        protected CodelistMember(
            CodelistMember parent
            , int id
            , int index
            , string code
			, string defaultName
			, Dictionary<string, string> names
            , SWValueType valueType = SWValueType.Value
            ) 
	        : base(id, index, code, defaultName, names)
        {
            ParentMember        = parent;
            ChildMembers        = new List<ICodelistMember>(100);
            ValueType           = valueType;
            Depth               = parent?.Depth + 1 ?? 1;
            IsMultipleChoice    = ValueType == SWValueType.AlternativeMultipleChoice
                                    || ValueType == SWValueType.SimpleMultipleChoice
                                    || ValueType == SWValueType.OrderedMultipleChoice;
        }

        public static CodelistMember Build(
            CodelistMember parent
            , string code
			, string defaultName
            , Dictionary<string, string> names
            , int? id = null
            , int? index = null
            , SWValueType valueType = SWValueType.Value
        )
        {
            return new CodelistMember(
                parent,
                id ?? GetNextId(),
                index ?? GetNextId(),
                code,
				defaultName,
                names,
                valueType
            );
        }

        #endregion

        public void AddChildMember(ICodelistMember mem)
        {
            ChildMembers.Add(mem);
        }

        public int CompareTo(object obj)
        {
            var oth = obj as CodelistMember;

            return oth == null 
                ? 1 
                : string.Compare(Uid, oth.Uid, StringComparison.Ordinal);
        }
    }
}
