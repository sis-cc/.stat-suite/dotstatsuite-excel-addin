﻿using System.Collections.Generic;
using System.Linq;

namespace DLMXL.Models
{
    public class Constraint
    {
        public readonly bool Include;
        public readonly HashSet<string> Codes;
        public readonly TimeRange TimeRange;

        public Constraint(bool include, IList<string> codes, TimeRange timeRange)
        {
            Include = include;
            Codes = new HashSet<string>(codes ?? Enumerable.Empty<string>());
            TimeRange = timeRange;
        }

        public bool IsCodeListBased => Codes.Any();
    }
}
