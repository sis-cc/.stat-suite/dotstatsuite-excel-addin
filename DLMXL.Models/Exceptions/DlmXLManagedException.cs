using System;

namespace DLMXL.Models.Exceptions
{
    public class DlmXLManagedException : Exception
    {
        public DlmXLManagedException(string message) :base(message)
        {}
    }
}