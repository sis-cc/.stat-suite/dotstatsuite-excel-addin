﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DLMXL.Models.Exceptions
{
    public class DlmXLNoData : DlmXLManagedException
    {
        public DlmXLNoData(string message) : base(message)
        {}
    }
}
