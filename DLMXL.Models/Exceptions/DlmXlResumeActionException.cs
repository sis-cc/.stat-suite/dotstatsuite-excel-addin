﻿namespace DLMXL.Models.Exceptions
{
    public class DlmXlResumeActionException : DlmXLManagedException
    {
        public DlmXlResumeActionException(string message, params object[] args) : base(string.Format(message, args))
        {}
    }
}
