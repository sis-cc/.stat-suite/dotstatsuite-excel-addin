﻿using System;
using System.Collections.Generic;
using DLMXL.Models.Interfaces;

namespace DLMXL.Models
{
    public enum OutputType
    {
        Flat,
        TimeSeriesDown,
        TimeSeriesAcross,
        Pivot
    }

    public interface IDataAccess
    {
		string Userinfo { get; }
		string AuthId { get; }
		bool IsAuthenticated { get; }
		bool IsAuthRequired { get; }
	    bool IsExternal { get; }
        bool IsMetadata { get; }
        bool IsHistoricVersion { get; }
        bool IsActionColumn { get; }
        bool IsSaveWithAttributesOnly { get; }
        bool IsSaveAllowed { get; }
        bool IsDataSpace { get; }
        bool IsFlagsOutput { get; }
        bool IsDateOutput { get; }
        bool IsQueryWithFullId { get; }
        bool IsQueryWithAllAsEmpty { get; }
        void BulkPreLoad();

        IList<IDatabase> LoadDatabases();
        IList<IDataset> LoadDatasets(IDatabase db);
        IList<IDimension> FetchDimensions(IDataset ds);
        IList<IAttribute> FetchAttributes(IDataset ds);
        IList<IQuery> LoadQueries(IDataset ds);
        IMsd LoadMsd(IDataset ds);

        object[,] LoadData(
	        OutputType outputType, 
	        ISdmxFilter sdmxFilter, 
	        bool production = true, 
	        LabelOutput labelOutput = LabelOutput.Code,
	        string language = null,
	        bool flags = false, 
	        bool updateDate = false,
            bool withActionColumn = false
        );

        object[,] LoadMetadata(
            ISdmxFilter sdmxFilter,
            LabelOutput labelOutput = LabelOutput.Code,
            string language = null,
            bool withActionColumn = false
        );

        string GetSdmxQueryString(ISdmxFilter filter, List<KeyValuePair<string, string>> @params = null);
        string GetMetaDataQueryString(ISdmxFilter filter);

        T InitFilter<T>(T filter, IList<KeyValuePair<string, string>> @params = null) where T : ISdmxFilter;
        IList<KeyValuePair<string, string>> GetLanguages(IDataset ds);

        Tuple<string, int> Save(
	        IDataset dataset,
	        ICellAccess source,
	        OutputType outputType,
	        bool withCodes,
	        bool withLabels,
	        bool withFlags,
            bool isMetadata,
            bool withActionColumn,
            SaveInterruptHander dialogInterrupt
        );

        void ClearObjectCache();

		#region Auth

        void LogOut();

        IAuth Auth { get; }

        #endregion

        #region Query Dialog

        IList<KeyValuePair<string, string>> GetConnectionStrings(ISdmxFilter filter, bool production, bool updateDate);

        #endregion
    }
}
