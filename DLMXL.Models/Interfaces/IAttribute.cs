﻿using System.Collections.Generic;

namespace DLMXL.Models.Interfaces
{
	public interface IAttribute : IDlmXLAddinObject
	{
		IList<ICodelistMember> Members { get; }
	}
}
