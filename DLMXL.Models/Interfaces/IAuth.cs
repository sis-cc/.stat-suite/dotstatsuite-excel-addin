﻿using System.Collections.Specialized;
using System.Threading.Tasks;

namespace DLMXL.Models.Interfaces
{
    public interface IAuth
    {
        string GetAuthorizationUrl();
        Task Authorize(NameValueCollection authQueryString);
    }
}