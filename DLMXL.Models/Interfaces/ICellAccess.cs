﻿using System;
using System.Collections.Generic;

namespace DLMXL.Models.Interfaces
{
	public interface ICellAccess
	{
		int Rows {get; }
		int Columns {get; }
		string this[int x, int y] {get; }
		IEnumerable<Selection> Selection();
	}

	public class Range
	{
		public readonly int Start;
		public readonly int End;

		public Range(int start, int end)
		{
			Start = start;
			End = end;
		}

		public bool IsOverlapped(Range range)
		{
			return Start <= range.End && range.Start <= End;
		}
	}

	public class Selection
	{
		public readonly Range Rows;
		public readonly Range Columns;

		public Selection(Range rows, Range columns)
		{
			Rows = rows;
			Columns = columns;
		}

		public int Count => (Rows.End - Rows.Start + 1) * (Columns.End - Columns.Start + 1);
	}
}