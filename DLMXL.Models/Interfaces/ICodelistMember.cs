﻿using System;
using System.Collections.Generic;

namespace DLMXL.Models.Interfaces
{
    public interface ICodelistMember : IDlmXLAddinObject, IComparable
    {
        ICodelistMember ParentMember { get;}
        IList<ICodelistMember> ChildMembers { get;}
        IEnumerable<ICodelistMember> AllDescendents { get; }
        int Depth { get;}

        bool IsYear { get; }
        bool IsMonth { get; }
        bool IsQuarter { get; }
        bool IsDaily { get; }
        bool IsWeekly { get; }
        bool IsSemestrial { get; }
        bool IsBusiness { get; }
    }
}