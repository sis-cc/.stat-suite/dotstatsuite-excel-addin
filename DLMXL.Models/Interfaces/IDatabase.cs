﻿using System.Collections.Generic;

namespace DLMXL.Models.Interfaces
{
    public interface IDatabase : IDlmXLAddinObject
    {
        IList<IDataset> Datasets { get; }
        IDatabase Parent { get; }
        IList<IDatabase> Children { get; }
    }
}