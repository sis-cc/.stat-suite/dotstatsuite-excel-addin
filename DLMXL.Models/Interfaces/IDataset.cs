﻿using System.Collections.Generic;

namespace DLMXL.Models.Interfaces
{
    public interface IDataset : IDlmXLAddinObject
    {
        IDataAccess DataAccess { get; }
        SdmxId DsdId { get; }
        SdmxId DfId { get; }
        IDatabase Database { get; set; }

        IMsd Msd { get;}

        bool HasATimeDimension { get; }

        IList<IDimension> Dimensions { get; }
        IList<IAttribute> Attributes { get; }

        IList<IQuery> Queries { get; }

        string FullCode { get; }
        string FriendlyName { get; }
    }
}