﻿using System.Collections.Generic;

namespace DLMXL.Models.Interfaces
{
    public interface IDimension : IDlmXLAddinObject
    {
        IDataset Dataset { get;}
        bool IsTimeSeries { get;}
        bool IsLocation { get;}
        bool IsFrequency { get;}
        IList<ICodelistMember> Members { get;}
        ICodelistMember FindMember(int id);
        Constraint Constraint { get; }
    }
}