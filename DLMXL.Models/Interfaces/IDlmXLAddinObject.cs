﻿using System.Collections.Generic;

namespace DLMXL.Models.Interfaces
{
    public interface IDlmXLAddinObject
    {
        string Uid { get; }
        int Id { get;}
        int Index { get;}
        string Code { get;}

		/// <summary>
		/// All Labels
		/// </summary>
        Dictionary<string, string> Names { get; }
		
        /// <summary>
		/// Default Label
		/// </summary>
        string DefaultName { get;}

        /// <summary>
        /// Returns label according to a pattern
        /// </summary>
        /// <param name="language"></param>
        /// <param name="pattern">{0} - FriendlyName (Code), {1} - Localized name</param>
        /// <returns></returns>
        string GetLabel(string pattern, string language = null);
    }
}