﻿namespace DLMXL.Models.Interfaces
{
    public interface IMetadataAttribute : IDlmXLAddinObject
    {
        string Path { get;}
        int? MinOccurs { get; set; }
        int? MaxOccurs { get; set; }
        string LocalRepresentation { get; set; }
        bool IsHtml { get; }
        bool IsExternalEditor { get; }
    }
}
