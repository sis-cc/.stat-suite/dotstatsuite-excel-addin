﻿using System;
using System.Collections.Generic;

namespace DLMXL.Models.Interfaces
{
    public interface IMsd : IDlmXLAddinObject
    {
        SdmxId SdmxId { get; }
        IList<IMetadataAttribute> MetadataAttributes { get; set; }
        IMetadataAttribute GetByPath(string path);
    }
}
