﻿namespace DLMXL.Models.Interfaces
{
    public interface IQuery : IDlmXLAddinObject
    {
        string SdmxQuery { get; }
    }
}
