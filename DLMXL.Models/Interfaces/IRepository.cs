﻿using System.Collections.Generic;

namespace DLMXL.Models.Interfaces
{
    public interface IRepository
    {
	    IDataAccess DataAccess { get; }
        string Code { get; }
        IList<IDatabase> Databases { get; }
        T BuildFilterFromSdmxQueryString<T>(string query) where T : ISdmxFilter, new();
    }
}