﻿using System;

namespace DLMXL.Models.Interfaces
{
    public enum Frequency
    {
        All = 0,
        Anually,
        Quarterly,
        Monthly,
        Daily,
        Weekly,
        Semestrial,
        Business
    }

    public enum HistoricVersionType
    {
        UpdatedAfter = 1,
        IncludeHistory = 2,
        AsOf=3
    }

    public interface ISdmxFilter
    {
        Frequency Frequency { get; set; }
        string StartTime { get; set; }
        string EndTime { get; set; }
        DateTime? UpdatedAfter { get; set; }
        int? LastPeriods { get; set; }
        HistoricVersionType? HistoricVersionType { get; set; }
        SWFilter SWFilter { get; set; }
        string SdmxQuery { get; }
    }
}
