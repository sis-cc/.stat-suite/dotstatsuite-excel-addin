﻿using System;
using DLMXL.Models.Interfaces;

namespace DLMXL.Models
{
    public delegate InterruptCode SaveInterruptHander(string[] row, IDataset dataset, int index, InterruptCode interrupt);

    public class InterruptCodeException : Exception
    {}

    [Flags]
    public enum InterruptCode
    {
        BulkDelete = 1
    }
}
