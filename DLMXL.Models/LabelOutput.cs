﻿using System;

namespace DLMXL.Models
{
    [Flags]
    public enum LabelOutput
    {
        Code = 0x01,
		Label = 0x08,

        // Legacy
        English = 0x02,
        French = 0x04,
    }
}
