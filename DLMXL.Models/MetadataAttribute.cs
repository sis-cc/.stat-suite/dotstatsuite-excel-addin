﻿using System;
using System.Collections.Generic;
using DLMXL.Models.Interfaces;

namespace DLMXL.Models
{
    public class MetadataAttribute : SWBase, IMetadataAttribute
    {
        private static HashSet<string> _externalEditorTypes = new HashSet<string>(new[]
            {
                "XHTML",
                "STRING"
            }, 
            StringComparer.InvariantCultureIgnoreCase
        );

        public MetadataAttribute(string code) : base(-1, -1, code, null, null)
        {}

        public string Path { get; set; }
        public int? MinOccurs { get; set; }
        public int? MaxOccurs { get; set; }
        public string LocalRepresentation { get; set; }
        public bool IsHtml => LocalRepresentation.Equals("XHTML", StringComparison.InvariantCultureIgnoreCase);
        public bool IsExternalEditor => _externalEditorTypes.Contains(LocalRepresentation);
    }
}
