﻿using System;
using System.Collections.Generic;
using System.Linq;
using DLMXL.Models.Interfaces;

namespace DLMXL.Models
{
    public class Msd : SWBase , IMsd
    {
        private readonly Lazy<Dictionary<string, IMetadataAttribute>> _attributeDictionary;

        private Dictionary<string, IMetadataAttribute> Hash()
        {
            return MetadataAttributes.ToDictionary(x => x.Path, x => x);
        }

        public Msd(SdmxId sdmxId) : base(-1, -1, sdmxId.Id, null, null)
        {
            SdmxId = sdmxId;
            _attributeDictionary = new Lazy<Dictionary<string, IMetadataAttribute>>(Hash);
        }

        public SdmxId SdmxId { get; }

        public IList<IMetadataAttribute> MetadataAttributes { get; set; }

        public IMetadataAttribute GetByPath(string path)
        {
            return _attributeDictionary.Value.TryGetValue(path, out var metadataAttribute) ? metadataAttribute : null;
        }
    }
}