﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using DLMXL.Models.Interfaces;

namespace DLMXL.Models
{
    public abstract class Repository<T> : IRepository where T: IRepository
    {
	    public IDataAccess DataAccess { get; }
	    protected static ConcurrentDictionary<string, T> _Repositories = new ConcurrentDictionary<string, T>();
        private Lazy<IList<IDatabase>> _loadDatabases;

        public IList<IDatabase> Databases => _loadDatabases.Value;

        public string Code { get;}

        public string Name { get;}

        protected Repository(string code, string name, IDataAccess dataAccess)
        {
	        DataAccess     = dataAccess;
            _loadDatabases  = new Lazy<IList<IDatabase>>(dataAccess.LoadDatabases);
            Code            = code;
            Name            = name;
        }

        public override string ToString()
        {
            return this.Name;
        }

        #region Filters

        public SWFilter BuildSwFilter(string query)
        {
            // 'REFSERIES/AUS+BEL+CAN.GDPA.A'

            var sections    = query.Split('/');
            var ds          = FindDataset(sections[0]);

            if (ds == null)
                return null;

            var dimString   = sections.Length > 1 ? sections[1] : null;
            var res         = SWFilter.BuildFilter( ds );

            if (!string.IsNullOrEmpty(dimString) && dimString != "all")
            {
                var temp    = dimString.Split('.');
                var dims    = ds.Dimensions.Where(d => !d.IsTimeSeries).ToArray();

                if (temp.Length !=  dims.Length)
                    throw new InvalidOperationException($"The number of dimensions in the SDMX query string {dimString} does not match with the dataset {ds}'s dimensions");

                for( var i = 0; i < temp.Length; i++ )
                {
                    var memString = temp[i];

                    if ( string.IsNullOrWhiteSpace(memString))
                        continue;

                    var selected    = new List<ICodelistMember>();
                    var err         = new List<string>();
                    var dim         = dims[i];
                    var fdim        = res.FilterDimensions.First(x=>x.Dimension.Id == dim.Id);
                    var codes       = memString.Split(new[] {'+'}, StringSplitOptions.RemoveEmptyEntries);

                    foreach( var code in codes ) {
                        var mem = dim.Members.FirstOrDefault(m=>m.Code == code);

                        if ( mem == null )      err.Add( code );
                        else                    selected.Add( mem );
                    }

                    if( err.Count > 0 && selected.Count == 0 )
                        throw new Exception($"Unrecognized members: {ds.Code}.{dim.Code}: {string.Join(",", err)}");

                    if ( selected.Count > 0 )
                        fdim.AddSelectedMembers( selected );

                    // All selected
                    if( fdim.SelectedMembers.Count == fdim.SelectableMembers.Count)
                        fdim.ResetSelectedMembers();
                }
            }
            
            return res;
        }

        private IDataset FindDataset(string code)
        {
            IDataset ds = null;

            if (code.IndexOf(',') > 0)
            {
                foreach (var db in Databases)
                    if ((ds = db.Datasets.FirstOrDefault(x => x.DfId.FullId.Equals(code, StringComparison.OrdinalIgnoreCase))) != null)
                        break;
            }
            else if (code.IndexOf('.') > 0)
            {
                var arr     = code.Split('.');
                var dbCode  = arr[0];
                var dsCode  = arr[1];

                ds = Databases
                    .FirstOrDefault(x => x.Code == dbCode)
                    ?.Datasets.FirstOrDefault(x => string.Equals(x.Code, dsCode, StringComparison.OrdinalIgnoreCase));
            }
            else
            {
                foreach (var db in Databases)
                    if ((ds = db.Datasets.FirstOrDefault(x => x.Code.Equals(code, StringComparison.OrdinalIgnoreCase))) != null)
                        break;
            }

            return ds;
        }

        public string GetSdmxQueryString(ISdmxFilter filter, List<KeyValuePair<string, string>> @params = null)
        {
            return DataAccess.GetSdmxQueryString(filter, @params);
        }

        public T BuildFilterFromSdmxQueryString<T>(string query) where T:ISdmxFilter, new()
        {
            var arr         = query.Split(new[] { '?' }, StringSplitOptions.RemoveEmptyEntries);
            var criteria    = string.Join("/", arr[0].Split('/').Take(2));

            SWFilter swFilter;

            try { swFilter = this.BuildSwFilter(criteria); }
            catch { return default(T); }

            var filter = new T() {SWFilter = swFilter};

            if (arr.Length == 1)
                return filter;

            var values = arr[1]
                .Split('&')
                .Select(x => x.Split('='))
                .Where(a => a.Length == 2)
                .Select(a => new KeyValuePair<string, string>(a[0], a[1]))
                .ToArray();

            return DataAccess.InitFilter(filter, values);
        }

        #endregion

        protected static T Build(T repository)
        {
            return _Repositories.GetOrAdd(repository.Code, repository);
        }

        public virtual void ClearCache()
        {
	        if (_loadDatabases.IsValueCreated)
	        {
		        _loadDatabases  = new Lazy<IList<IDatabase>>(DataAccess.LoadDatabases);
	        }

			DataAccess.ClearObjectCache();
        }
    }
}
