﻿using System.Collections.Generic;
using System.Threading;
using DLMXL.Models.Interfaces;

namespace DLMXL.Models
{
    public abstract class SWBase : IDlmXLAddinObject
    {
        private static int _NextId = 1000000;

        public static int GetNextId()
        {
            Interlocked.Increment(ref _NextId);
            return _NextId;
        }

        public string Uid { get; protected set; }
        public int Id { get; }
        public int Index { get; }
        public string Code { get;}
        public Dictionary<string, string> Names { get; protected set;}
        public string DefaultName { get; }
        public virtual string FriendlyName => this.Code;
        protected SWBase(int id, int index, string code, string defaultName, Dictionary<string, string> names)
        {
            Uid         = id > 0 ? id.ToString() : code;
            Id          = id;
            Index       = index;
            Code        = code;
            DefaultName = defaultName;
            Names		= names ?? new Dictionary<string, string>();
        }

		public override string ToString()
		{
			return $"{DefaultName} ({Code})";
		}

        public string GetLabel(string pattern, string language)
        {
            return this.Names.ContainsKey(language)
                ? string.Format(pattern, FriendlyName, this.Names[language])
                : this.FriendlyName;
        }
    }
}
