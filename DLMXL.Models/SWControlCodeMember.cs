﻿using System.Collections.Generic;
using DLMXL.Models.Interfaces;

namespace DLMXL.Models
{
    public class SWControlCodeMember : SWBase, IControlCodeMember
    {
        protected SWControlCodeMember(int id, string code, string defaultName, Dictionary<string, string> names) : base(id, id, code, defaultName, names)
        {}

        public static SWControlCodeMember Build(
            int id,
            string code,
            string defaultName,
            Dictionary<string, string> names = null
        )
        {
            return new SWControlCodeMember(
                id,
                code,
                defaultName,
                names
            );
        }
    }
}
