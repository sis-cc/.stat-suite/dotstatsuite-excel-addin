﻿using System;
using System.Collections.Generic;
using DLMXL.Models.Interfaces;

namespace DLMXL.Models
{
    public class SWDatabase : SWBase, IDatabase
    {
        private readonly Lazy<IList<IDataset>> _loadDatasets;

        public IList<IDataset> Datasets => _loadDatasets.Value;

        public IDatabase Parent { get; protected set; }

        public IList<IDatabase> Children { get; protected set; }

        protected SWDatabase(
            IDataAccess access,
            int id,
            string code,
            string defaultName,
            Dictionary<string, string> names = null
        ) 
	        : base(id, 0, code, defaultName, names)
        {
            _loadDatasets   = new Lazy<IList<IDataset>>(()=>access.LoadDatasets(this));
            Children        = new List<IDatabase>(10);
        }

        public static SWDatabase Build(
	        IDataAccess access, 
	        int id, 
	        string code,
	        string defaultName,
	        Dictionary<string, string> names = null
	    )
        {
            return new SWDatabase(access, id, code, defaultName, names);
        }

        public void AddChildMember(IDatabase database)
        {
            Children.Add(database);
        }

        public void SetParent(IDatabase database)
        {
            Parent = database;
        }
    }
}