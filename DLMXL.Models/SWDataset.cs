﻿using System;
using System.Collections.Generic;
using System.Linq;
using DLMXL.Models.Interfaces;

namespace DLMXL.Models
{
    public class SWDataset : SWBase, IDataset
    {
        private readonly Lazy<IList<IQuery>> _loadQueries;
        private readonly Lazy<IList<IDimension>> _loadDimensions;
        private readonly Lazy<IList<IAttribute>> _loadAttributes;
        private readonly Lazy<IMsd> _loadMsd;

        public IDataAccess DataAccess { get; }
        public SdmxId DsdId { get; }
        public SdmxId DfId { get; }
        public IDatabase Database { get; set; }
        public IList<IDimension> Dimensions => _loadDimensions.Value;
        public IList<IAttribute> Attributes => _loadAttributes.Value;
        public IMsd Msd => _loadMsd.Value;
        public IList<IQuery> Queries => _loadQueries.Value;
        public bool HasATimeDimension => Dimensions.Any(x => x.IsTimeSeries);
        public string FullCode => (this.Database.Code + "." + this.Code).Trim('.');
        public override string FriendlyName => DfId?.ToString() ?? FullCode;

        #region Constructor
        public SWDataset(
            IDataAccess dataAccess,
            IDatabase database,
            string code,
            string defaultName,
            Dictionary<string, string> names = null,
            SdmxId dsdId = null,
            SdmxId dfId = null,
            int? id = null,
            int? index = null
        ) 
	        : base(id ?? -1, index ?? GetNextId(), code, defaultName, names)
        {
            _loadDimensions     = new Lazy<IList<IDimension>>(() => dataAccess.FetchDimensions(this));
			_loadAttributes     = new Lazy<IList<IAttribute>>(() => dataAccess.FetchAttributes(this));
            _loadQueries        = new Lazy<IList<IQuery>>(() => dataAccess.LoadQueries(this));
            _loadMsd  = new Lazy<IMsd>(() => dataAccess.LoadMsd(this));
            Database            = database;
            DsdId				= dsdId;
            DfId                = dfId;
            DataAccess          = dataAccess;
        }

        #endregion

    }
}
