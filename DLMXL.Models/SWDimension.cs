﻿using System;
using System.Collections.Generic;
using System.Linq;
using DLMXL.Models.Interfaces;

namespace DLMXL.Models
{
    public class SWDimension : SWBase, IDimension
    {
        private static readonly HashSet<string> FreqDimensions = new HashSet<string>(StringComparer.OrdinalIgnoreCase) { "FREQ", "Frequency" };

        public IDataset Dataset { get; protected set; }
        public bool IsTimeSeries { get; protected set; }
        public bool IsLocation { get; protected set; }
        public bool IsFrequency { get; protected set; }
        public IList<ICodelistMember> Members { get; protected set; }
        public Constraint Constraint { get; set; }

        protected SWDimension(
            IDataset ds,
            int id,
            int index,
            string code,
            string defaultName,
            Dictionary<string, string> names = null,
            bool isTimeSeries = false,
            bool isLocation = false,
            bool isFrequence = false,
            Constraint constraint = null
        )
	        : base( id, index, code, defaultName, names)
        {
            Dataset         = ds;
            IsTimeSeries    = isTimeSeries;
            IsLocation      = isLocation;
            IsFrequency     = isFrequence || FreqDimensions.Contains(code);
            Members         = new ICodelistMember[0];
            Constraint      = constraint;
        }

        public static SWDimension Build(
            IDataset ds,
            string code,
            string defaultName,
            Dictionary<string, string> names = null,
            bool isTimeSeries = false,
            bool isLocation = false,
            bool isFrequence = false,
            int? id = null,
            int? index = null,
            Constraint constraint = null
        )
        {
            return new SWDimension(
                ds,
                id ?? GetNextId(),
                index ?? GetNextId(),
                code,
				defaultName,
                names,
                isTimeSeries,
                isLocation,
                isFrequence,
                constraint
            );
        }


        #region Methods

        public void ResetMembers(IEnumerable<ICodelistMember> ml)
        {
            Members = Constraint?.Codes!=null 
                ? ml.Where(x=>Constraint.Codes.Contains(x.Code)).ToArray() 
                : ml.ToArray();
        }

        public ICodelistMember FindMember(int id)
        {
            return Members.FirstOrDefault(x => x.Id == id);
        }
        #endregion
    }
}
