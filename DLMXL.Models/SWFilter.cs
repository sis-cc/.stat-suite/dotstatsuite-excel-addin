﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DLMXL.Models.Interfaces;

namespace DLMXL.Models
{
    /// <summary>
    /// Deals only with common dimensions (no time dimension)
    /// </summary>
    public class SWFilter : ICloneable
    {
        public IDataset Dataset { get; }

        public IList<SWFilterDimension> FilterDimensions { get; }

        protected SWFilter(IDataset dataset)
        {
            Dataset             = dataset;
            FilterDimensions    = dataset.Dimensions
                                    .Select(dim => new SWFilterDimension(dim))
                                    .ToArray();
        }

        public object Clone()
        {
            var clone = BuildFilter(Dataset);

            for(var i=0;i<FilterDimensions.Count;i++)
                clone.FilterDimensions[i] = (SWFilterDimension) FilterDimensions[i].Clone();

            return clone;
        }

        public static SWFilter BuildFilter(IDataset dataset)
        {
            return new SWFilter(dataset);
        }

        /// <summary>
        /// Returns sdmx query without time Dimension
        /// </summary>
        /// <returns></returns>
        public virtual string GetSdmxQueryString()
        {
            var dfId = Dataset.DfId;
            var filter = new StringBuilder();
            var nonTimeDims = FilterDimensions.Where(x => !x.Dimension.IsTimeSeries).ToArray();

            foreach (var dim in nonTimeDims)
            {
                if (dim.SelectedMembers.Any() && dim.SelectedMembers.Count != dim.SelectableMembers.Count)
                {
                    foreach (var mem in dim.SelectedMembers)
                        filter.Append(Uri.EscapeDataString(mem.Code)).Append('+');

                    filter.Length -= 1;
                }

                filter.Append('.');
            }

            if (filter.Length == nonTimeDims.Length)
            {
                filter.Clear();
            }

            // --------------------------------
            
            if (filter.Length > 0)
            {
                filter.Length -= 1; 
            }
            else if(Dataset.DataAccess.IsQueryWithAllAsEmpty)
            {
                filter.Append("all");
            }

            // ----------------------------------

            var code = Dataset.DataAccess.IsQueryWithFullId && dfId != null
                ? $"{dfId.Agency},{dfId.Id},{dfId.Version}"
                : Dataset.FullCode;

            return code + "/" + filter;
        }

        public virtual string GetSdmxMetadataQueryString()
        {
            var filter = new StringBuilder();

            foreach (var fdim in FilterDimensions.Where(x => !x.Dimension.IsTimeSeries))
            {
                if (fdim.SelectedMembers.Any() && fdim.SelectedMembers.Count != fdim.SelectableMembers.Count)
                {
                    foreach (var mem in fdim.SelectedMembers)
                        filter.Append(Uri.EscapeDataString(mem.Code)).Append('+');

                    filter.Length -= 1;
                }
                else
                {
                    filter.Append("*");
                }

                filter.Append('.');
            }

            return filter.ToString(0, filter.Length - 1);
        }

        #region Equality
        public bool Equals(SWFilter filter)
        {
            if (FilterDimensions.Count != filter?.FilterDimensions.Count)
                return false;

            return !FilterDimensions
                        .Where((t, i) => !Equals(t, filter.FilterDimensions[i]))
                        .Any();
        }

        public override bool Equals(object obj)
        {
            var oth = obj as SWFilter;
            return oth != null && Equals(oth);
        }

        public override int GetHashCode()
        {
            unchecked
            {
                return FilterDimensions.Aggregate(7, (current, fdim) => (current * 31 + fdim.GetHashCode()));
            }
        }
        #endregion
    }
}
