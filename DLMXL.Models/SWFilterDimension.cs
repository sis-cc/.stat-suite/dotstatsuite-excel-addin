﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using DLMXL.Models.Interfaces;

namespace DLMXL.Models
{
    public class SWFilterDimension: INotifyPropertyChanged, ICloneable
    {
        public IDimension Dimension { get; }

        public IList<ICodelistMember> SelectableMembers => Dimension.Members;
        public List<ICodelistMember> SelectedMembers { get; set; }

        public SWFilterDimension(IDimension dim)
        {
            Dimension           = dim;
            SelectedMembers     = new List<ICodelistMember>();
        }

        public bool IsMemberSelected(ICodelistMember mem)
        {
            return SelectedMembers.Contains(mem);
        }

        public void AddSelectedMember(ICodelistMember mem)
        {
            if (IsMemberSelected(mem))
                return;

            SelectedMembers.Add(mem);

            RaiseProperyHasChanged();
        }

        public void RemoveSelectedMember(ICodelistMember mem)
        {
            var res = SelectedMembers.Remove(mem);

            if (!res)
                return;

            RaiseProperyHasChanged();
        }

        public void AddSelectedMembers(IEnumerable<ICodelistMember> list)
        {
            var cnt = SelectedMembers.Count;

            try
            {
                SuspendEvents();

                foreach (var mem in list)
                    AddSelectedMember(mem);
            }
            finally
            {
                ResumeEvents();

                if (cnt != SelectedMembers.Count)
                    RaiseProperyHasChanged();
            }
        }

        public void RemoveSelectedMembers(IEnumerable<ICodelistMember> list)
        {
            var res = false;

            foreach (var mem in list)
                res |= SelectedMembers.Remove(mem);

            if (!res)
                return;

            RaiseProperyHasChanged();
        }

        public virtual object Clone()
        {
            var clone = new SWFilterDimension(Dimension);
            clone.SelectedMembers.AddRange(SelectedMembers);
            return clone;
        }

        public void ResetSelectedMembers(IEnumerable<ICodelistMember> selectedMembers = null)
        {
            SelectedMembers = selectedMembers?.ToList() ?? new List<ICodelistMember>();
            RaiseProperyHasChanged();
        }

        #region Equality

        public bool Equals(SWFilterDimension fdim)
        {
            if (fdim == null)
                return false;

            if (fdim.Dimension != Dimension || fdim.SelectedMembers.Count != SelectedMembers.Count)
                return false;

            var ms = new HashSet<ICodelistMember>(SelectedMembers);
            ms.ExceptWith(fdim.SelectedMembers);
            return ms.Count == 0;
        }

        public override bool Equals(object obj)
        {
            var oth = obj as SWFilterDimension;
            return oth != null && Equals(oth);
        }

        public override int GetHashCode()
        {
            return Dimension.GetHashCode();
        }

        #endregion

        #region INotifyPropertyChanged
        private int _ProgrammaticChange;
        public event PropertyChangedEventHandler PropertyChanged;

        protected void RaiseProperyHasChanged(string propertyName = "SelectedMembers")
        {
            if (_ProgrammaticChange > 0)
                return;

            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }

        public void SuspendEvents()
        {
            _ProgrammaticChange++;
        }

        public void ResumeEvents()
        {
            _ProgrammaticChange--;
        }

        #endregion

        #region Has data

        public bool IsMemberSelectable(ICodelistMember mem)
        {
            return true;
            //            return (!_ExcludeMembersWithoutData || mem.DataStatus.Value.HasProdData || mem.ChildrenHaveProdData()
            //                     || mem.DataStatus.Value.HasImportData || mem.ChildrenHaveImportedData()) && MatchesMemberFilter(mem);
        }

        //        public bool HaveMembersWithData(int depth = -1)
        //        {
        //            SWDimension.Dataset.UpdateDimensionMembersWithData();
        //            return
        //                    SWDimension.Members.Any(
        //                            mem =>
        //                            (depth < 0 || mem.Depth == depth)
        //                            && (mem.DataStatus.Value.HasProdData || mem.DataStatus.Value.HasImportData));
        //        }

        #endregion
    }
}
