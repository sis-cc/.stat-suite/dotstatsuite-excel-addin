﻿namespace DLMXL.Models
{
    public class SdmxId
    {
	    public string FullId => string.Join(",", Agency, Id, Version);

        public string Id { get; set; }
        public string Agency { get; set; }
        public string Version { get; set; }

        public override string ToString()
        {
            return $"{Agency}:{Id}({Version})";
		}

        public override bool Equals(object obj)
        {
            var sdmxObj = obj as SdmxId;

            if (sdmxObj == null)
                return false;

            return this.ToString() == sdmxObj.ToString();
        }

        public override int GetHashCode()
        {
            return this.ToString().GetHashCode();
        }
    }
}
