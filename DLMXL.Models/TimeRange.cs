﻿using System;

namespace DLMXL.Models
{
    public class TimeRange
    {
        public SdmxDate StartDate { get; set; }
        public SdmxDate EndDate { get; set; }
    }

    public class SdmxDate
    {
        public DateTime Date { get; set; }
        public bool IsInclusive { get; set; }
    }
}
