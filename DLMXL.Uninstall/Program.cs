﻿using System;
using System.IO;
using System.Linq;
using Microsoft.Win32;

namespace DLMXL.Uninstall
{
    class Program
    {
        static void Main(string[] args)
        {
            const string addinKey = "Stat-DLM";
            const string addinFriendlyName = ".Stat DLM";

            Uninstall(addinKey, addinFriendlyName);

            var clickOnceCache = new DirectoryInfo(Path.Combine(
                Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData),
                @"Apps\2.0"
            ));

            if (clickOnceCache.Exists)
            {
                foreach (var dir in clickOnceCache.GetDirectories())
                {
                    if (dir.GetFiles($"{addinKey}.dll", SearchOption.AllDirectories).Any())
                    {
                        Console.WriteLine($"Clear clickonce cache {dir.FullName}");
                        dir.Delete(true);
                    }
                }
            }

            Console.WriteLine("Done!");
            Console.ReadKey();
        }

        static void Uninstall(string addinKey, string addinFriendlyName)
        {
            RemoveKeys(
                addinKey,
                @"SOFTWARE\Microsoft\Office\Excel\Addins",
                @"SOFTWARE\Microsoft\Office\Excel\AddinsData"
            );

            SearchFriendlyName(
                new RegPath(@"SOFTWARE\Microsoft\Windows\CurrentVersion\Uninstall", "DisplayName", addinFriendlyName),
                new RegPath(@"SOFTWARE\Microsoft\Windows\CurrentVersion\Uninstall", "UrlUpdateInfo", addinKey, false, false),
                new RegPath(@"SOFTWARE\Microsoft\VSTO\SolutionMetadata", "friendlyName", addinFriendlyName, true),
                new RegPath(@"SOFTWARE\Microsoft\VSTO\Security\Inclusion", "Url", addinKey, false, false),
                new RegPath(@"SOFTWARE\Microsoft\VSTA\Solutions", "ProductName", addinFriendlyName)
            );
        }

        static void RemoveKeys(string addinKey, params string[] rootKeys)
        {
            foreach (var rootKey in rootKeys)
            {
                var regKey = Registry.CurrentUser.OpenSubKey(rootKey, true);

                if (regKey != null)
                {
                    Console.WriteLine($@"Deleting a key {regKey.Name}\{addinKey}");
                    regKey.DeleteSubKey(addinKey, false);
                }
            }
        }

        static void SearchFriendlyName(params RegPath[] rootKeys)
        {
            foreach (var rootKey in rootKeys)
            {
                var regKey = Registry.CurrentUser.OpenSubKey(rootKey.Path, true);

                if (regKey != null)
                {
                    foreach (var subKeyName in regKey.GetSubKeyNames())
                    {
                        var subKey      = regKey.OpenSubKey(subKeyName);
                        var property    = subKey?.GetValue(rootKey.Property);

                        if (subKey!= null && rootKey.IsMatch(property?.ToString()))
                        {
                            Console.WriteLine($@"Deleting a key {subKey.Name}");
                            regKey.DeleteSubKey(subKeyName, false);

                            if (rootKey.InData)
                            {
                                foreach (var regKeyProp in regKey.GetValueNames())
                                {
                                    property = regKey.GetValue(regKeyProp);

                                    if (property?.ToString() == subKeyName)
                                    {
                                        regKey.DeleteValue(regKeyProp);
                                        break;
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }

        class RegPath
        {
            public string Path { get; }
            public string Property { get; }
            private string _pattern;
            private bool _exactMatch;
            public bool InData { get; }

            public RegPath(string path, string property, string pattern, bool inData = false, bool exactMatch = true)
            {
                Path = path;
                Property = property;
                _pattern = pattern;
                InData = inData;
                _exactMatch = exactMatch;
            }

            public bool IsMatch(string value)
            {
                return _exactMatch
                    ? _pattern == value
                    : value?.Contains(_pattern) ?? false;
            }
        }
    }
}
