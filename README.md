## DLM Excel-Addin application

DLM Excel-Addin is distributed as a ClickOnce application. 

---

## DLM Excel-Addin configuration

DLM Excel-Addin can be configured using a standard .NET application config file (App.config in the root of DLMXL.Excel folder).

**Note:** When the configuration file is changed in a deploy location, application should be resigned (see section Signing DLM Excel-Addin ClickOnce application).

The following parameters can be configured:

### Title

Title for the tab in the Excel ribbon where DLM Excel-Addin is located can be defined under the root node of configuration section. 

```
<dlmxlConfig title=".Stat DLM">
```

| *Key* | *Required* | *Description* |
|----|----|----|
| title | yes | Title for the tab in the Excel ribbon where DLM Excel-Addin is located|

---

### Languages

The languages used for displaying the content in the selection wizard are defined under the root node of configuration section.  
The first language provided is used as initial default until the user selects another language from the list presented in the selection wizard.

```
<dlmxlConfig languages="en,fr,es">
```

| *Key* | *Required* | *Description* |
|----|----|----|
| languages | yes | The languages used for displaying the content in the selection wizard. The first language in the list is used as inital default. |



### AuthRedirectUri

The redirectUri that is used when requesting an openId-Connect token from Authentication provider (Keycloak, ADFS etc) if connection is configured to use authentication (GenericRestConnection->authUri)
This redirectUri can be not-existant URI, it is used internally by addin to catch a response from Authentication provider to obtain a token. 
NB! The same redirectUri should be configured on the Authentication provider as allowed callback/redirect resource. 

```
<dlmxlConfig authRedirectUri="https://dotstat-suite-addin.oecd.org">
```

| *Key* | *Required* | *Description* | Default
|----|----|----|
| authRedirectUri | yes | redirectUri used with requests to Authentication provider (keycloak, adfs etc) | https://dotstat-suite-addin.oecd.org

---

### Available connections and connection parameters

“connections” node in the config file defines available data connections for DLM Excel-Addin and their parameters. 

Every connection should use unique Id attribute, which defines particular connection.  Id value is stored in the metadata of Excel file and enables to restore connection when the Excel file is reopened. This means that Id of connection should never change, otherwise it will not be possible to refresh the extracted data.
Addin supports following type of connections:

- [DotstatV7Connection](#dotstatv7connection)
- [DotstatV7SdmxConnection](#dotstatv7sdmxconnection)
- [StatworksConnection](#statworksconnection)
- [EurostatConnection](#eurostatconnection)
- [IMFConnection](#imfconnection)
- [PrognozConnection](#prognozconnection)
- [GenericRestConnection](#genericrestconnection)

In case DLM Excel-Addin needs to access, for example, test and production environments, it is allowed to define more than one connection of the same type.

#### DotstatV7Connection

Direct sql connection to .stat v7 Database

| *Key* | *Required* | *Description* |
|----|----|----|
| id | yes | Connection identifier |
| name | yes | Connection name. This parameter is shown in UI |
| dsn | yes | Connection dsn. This parameter is shown in UI when “show query syntax” button is clicked  |
| server | yes | SQL server name |
| db | yes | SQL database name |
| queryUri | yes | URI of rest service that returns Dotstat user queries |

#### DotstatV7SdmxConnection

Dotstat v7 flavor rest sdmx connection at RestSDMX/sdmx.ashx endpoint

| *Key* | *Required* | *Description* |
|----|----|----|
| id | yes | Connection identifier |
| name | yes | Connection name. This parameter is shown in UI |
| dsn | yes | Connection dsn. This parameter is shown in UI when “show query syntax” button is clicked  |
| server | yes | SQL server name |
| db | yes | SQL database name |
| queryUri | yes | URI of rest service that returns Dotstat user queries |

#### StatworksConnection

Direct sql connection to Statworks Database

| *Key* | *Required* | *Description* |
|----|----|----|
| id | yes | Connection identifier |
| name | yes | Connection name. This parameter is shown in UI |
| dsn | yes | Connection dsn. This parameter is shown in UI when “show query syntax” button is clicked  |
| server | yes | SQL server name |
| db | yes | SQL database name |

#### EurostatConnection

Eurostat sdmx connections, that supports asyncronous download

| *Key* | *Required* | *Description* |
|----|----|----|
| id | yes | Connection identifier |
| name | yes | Connection name. This parameter is shown in UI |
| baseUri | yes | Base URI of Eurostat rest service (http://ec.europa.eu/eurostat/SDMX/diss-web/rest/) |

#### IMFConnection

Rest service with IMD Sdmx flavor, not directly consumed by SdmxSourceLib. When output of service is standard can be replaced with GenericRestConnection

| *Key* | *Required* | *Description* |
|----|----|----|
| id | yes | Connection identifier |
| name | yes | Connection name. This parameter is shown in UI |
| baseUri | yes | URI of IMF rest service (http://dataservices.imf.org/REST/) |

#### PrognozConnection

Prognoz WCF service, specific for OECD

| *Key* | *Required* | *Description* |
|----|----|----|
| id | yes | Connection identifier |
| name | yes | Connection name. This parameter is shown in UI |
| baseUri | yes | URI of progrnoz WCF service endpoint |

#### GenericRestConnection

Standard sdmx rest connection, that is provided for example by Eurostat's NSI web service

| *Key* | *Required* | *Description* | *Default* |
|----|----|----|----|
| id | yes | Connection identifier ||
| name | yes | Connection name. This parameter is shown in UI ||
| baseUri | yes | Base URI of rest servoce, ex http://nsi-service/rest/ ||
| flowQuery | yes | URI, that is appended to baseUri to fetch dataflow list ||
| structureQuery | yes | URI, that is appended to baseUri to fetch structure ||
| dataQuery | yes | URI, that is appended to baseUri to get data ||
| isExternal | yes | Base URI of Eurostat rest service ||
| transferUri| no| [Transfer service base uri + api version](example http://transfer-base-uri/2), if set allows saving data and retrieval of logs| null |
| saveDataspace| yes, if transferUri present| Transfer service dataspace ID where data will be stored | null |
| authUri| no| OpenId Connect authentication base path, if retrieval and/or saving requires authentication <br/> The identity provider should whitelist the redirect URL "https://dotstat-suite-addin.oecd.org". This URL doesn't exist, the addin just catches the request to obtain the needed information. | null |
| authClientId| yes, if authUri present | ClientId in Identity service (ex keycloak) | null |
| authScope| no | OpenId requested scope | openid profile email |
| isMetadata| no| Configures if datasource supports referential metadata, default false | false |

```
    <connections>
      <DotstatConnection 
        id="DotStat" 
        name=".STAT"
        dsn="DOTSTAT"
        server="DOTSTATDB"
        db="DotStat"
        queryUri="http://dotstat.oecd.org/OECDStatWCF_QueryManager/my/queries/"/>
      
      <StatworksConnection 
        id="Statworks"
        name="Statworks"
        dsn="STATWORKS"
        server="vs-statworks-1"
        db="StatworksMain"/>
      
      <EurostatConnection 
        id="Eurostat" 
        name="Eurostat" 
        baseUri="http://ec.europa.eu/eurostat/SDMX/diss-web/rest/"/>

      <GenericRestConnection
        id="siscc-stable"
        name="SIS-CC stable"
        baseUri="http://nsi-stable-siscc.redpelicans.com/rest/"
        flowQuery="dataflow/all/all/latest"
        structureQuery="datastructure/all/{0}/{2}/?references=children"
        dataQuery="data/{0}"
        isExternal="false"/>

      <GenericRestConnection
        id="qa"
        name="QA"
        baseUri="https://nsi-qa-stable.siscc.org/rest/"
        flowQuery="dataflow/all/all/latest"
        structureQuery="dataflow/{1}/{0}/{2}?references=all&amp;detail=referencepartial"
        dataQuery="data/{0}"
        isExternal="false"
        transferUri="https://transfer-qa.siscc.org/1.2"
        saveDataspace="qa:stable"
        authUri="https://keycloak.siscc.org/auth/realms/OECD"
        authClientId="app"
        authScope="openid profile email"
        isMetadata="true"/>

    </connections>
```

---

### Programming a new connection type for DLM Excel-Addin

A new type of connection can be introduced without recompiling Excel plugin itself (no need for DevExpress license). To create new connection type 2 classes should be added into DW.DAL project:
1. Class that inherits from IDataAccess interface and implements Data access logic of new type of connection
2. Class that inherits from DLMXL.DAL.Config.Connection and implements connection configuration.

---

## Signing DLM Excel-Addin ClickOnce application

Before deploying application to the installation location it should be signed with a trusted certificate [with Code signing purpose](https://docs.microsoft.com/en-us/visualstudio/deployment/clickonce-and-authenticode), which root certificate is preinstalled in the organization.

**Note:** Before signing a new version of DLM Excel-Addin ensure that it is properly configured for the environments where it is planned to be used.

Visual Studio signs application automatically on deploy/publish, if configured properly.

Alternatively, already published addin can be re-signed with the [Mage utility](https://msdn.microsoft.com/en-us/library/acz3y3te(v=vs.110).aspx). Mage is automatically installed with Visual Studio. Copy of this utility is also located in the install folder.

In Install directory there is included sample self-signed certificate Stat-DLM.pfx. It should be used only for test purposes. The steps to resign a plugin are following:

1. Unzip Stat-DLM.zip in the install location. As a result following files & folders are extracted:
![folders](./docs/folders.png?raw=true "folders")  
At this stage plugin can be already installed by running setup.exe, but it will be installed with a config of OECD and will be using test OECD code signing certificate.

2. Before resigning one might change a config file that is used in a plugin to match organization needs. Config can be found in `Application Files\Stat-DLM_*_*_*_*\Stat-DLM.dll.config.deploy` text file.

3. It's recommended to replace default self-signed certificate Stat-DLM.pfx with certificate issued by your organizations's trusted cert authority, or alternatively one can buy certificate from a well-known provider like [Comodo](https://comodosslstore.com/code-signing) or similar.

4. Necessary commands for re-signing the DLM Excel-Addin are included in the **resign.bat** batch file. 
Predefined batch file can be used for DLM Excel-Addin re-signing, if the certificate for signing is located in the same directory as resign.bat 
and has a name "Stat-DLM.pfx". If the certificate for signing is located in the different directory than resign.bat or has a different name,
the CERT_PATH should be updated in the resign.bat file. 

5. As a result of the execution of the resign.bat batch file the DLM Excel-Addin application is signed and is ready for deployment.

6. Run setup.exe to install an excel plugin.

---

## Deploying DLM Excel-Addin

DLM Excel-Addin is deployed using the common procedure for [deployment of ClickOnce applications](https://msdn.microsoft.com/en-us/library/t71a733d.aspx).
Like other ClickOnce applications, DLM Excel-Addin can be deployed to network file share or web server.

The following folders and files should be copied/replaced in the deployment location once new version of DLM Excel-Addin is signed.

![folders](./docs/folders.png?raw=true "folders")

---

## Managing styles

Configuration of the look and feel including fonts; background; etc.., can be done by using DevExpress skin. The Skin editor can be launched from the DevExpress menu after installation of DevExpress components.

This requires installation of the Dev Express Trial Version V14  (can be downloaded on this page). DLM Excel-Addin should be recompiled and resigned  after it is configured.

More information on usage of DevExpress skin designer can be found [here](https://documentation.devexpress.com/#SkinEditor/CustomDocument1630)

The current skin (datawizard) is in DataWizard.Skins.dll that is generated by DevExpress skin designer.